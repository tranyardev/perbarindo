 $(document).ready(function(){

    // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');
        
    $("#pp").change(function(){
        readURL(this, "#pp");
    });
    $("#frm_change_pass").validationEngine();
    $("#frm_edit_profile").validationEngine();

    $("#form").on("keypress", ":input:not(textarea)", function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

    if (typeof target != 'undefined'){

        $("#form").submit(function(){

            showLoading();

            if($(this).validationEngine("validate")){

                toastr.info("Saving data...", "Loading");
                $("#btn-submit").button("loading");
                saveFormData();

                return false;

            }

        });

        $("#form").validationEngine({promptPosition : "topLeft"});

    }

    
});

if (typeof target != 'undefined'){

    function saveFormData(){

        var formData = new FormData($("#form")[0]);

        $.ajax({
            url: target,
            type: "POST",
            data: formData,
            dataType: "json",
            async: true,
            xhr: function()
              {
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress
                    $("#upload-progress").show();
                    $("#upload-progress").html("Uploading Image ... " + Math.round(percentComplete) + " %");
                    if(percentComplete==100){
                        $("#upload-progress").html("File Uploaded !! Processing image and saving data... ");
                    }
                    console.log(percentComplete);
                  }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    console.log(percentComplete);
                  }
                }, false);
                return xhr;
            },
            success: function (res) {

                hideLoading();
                if(res.status=="1"){
                    toastr.success(res.msg, "Response Server");
                }else{
                    toastr.error(res.msg, "Response Server");
                }

            },
            error: function(xhr, textStatus, error){
                 
                  alert(textStatus + " : " + xhr.statusText);
                 
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

}

function hideLoading(){

    $("body,html").animate({ scrollTop: 0 }, 600);
    $("#form_wrapper").removeClass("js");
    $("#preloader").hide();
    $("#btn-submit").button("reset");

}
function showLoading(){

    $("#form_wrapper").addClass("js");
    $("#preloader").show();

}
function cancelForm(){

    window.history.back();

}
function resetForm(){

    $('#form')[0].reset();
    $("#cover_preview").html('<h1>Cover Module</h1>');
    for (var i in CKEDITOR.instances) {

        CKEDITOR.instances[i].setData('');

    }

}

function convertToSlug(Text)
{
    return Text
        .toString()
        .trim()
        .toLowerCase()
        .replace(/\s+/g, "-")
        .replace(/[^\w\-]+/g, "")
        .replace(/\-\-+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
}

function filterSlug(Text){

    return Text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")

}

function setPermalink(source, destination){

    var txt = $("#" + source).val();
    var permalink = convertToSlug(txt);

    $("#"+destination).val(permalink);


}

function filterPermalink(selector){

    var txt = $("#" + selector).val();
    var permalink = filterSlug(txt);

    $("#"+selector).val(permalink);


}

function readURL(input, selector) {
    for(var i =0; i< input.files.length; i++){
        if (input.files[i]) {            

            var reader = new FileReader();
            var sFileName = input.files[i].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            

            if(sFileExtension === 'jpg' || sFileExtension === 'jpeg' || sFileExtension === 'png'){

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive img-pop-prev"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                    console.log(e);
                }

            }else if(sFileExtension === 'pdf'){
                
                reader.onload = function (e) {

                    var fpdf = $('<div id="pdfviewer" style="height: 700px;margin-top:20px;"><object data="'+e.target.result+'" type="application/pdf" width="100%" height="100%">This browser does not support PDFs. Please change or update your browser.</object></div>');
                    $(selector + '_preview').html(fpdf);
                    
                }

            }else if(sFileExtension === 'csv' || sFileExtension === 'xls' || sFileExtension === 'xlsx'){

                //yang ini belum dicoba
                reader.onload = function (e) {
                     console.log(e.target.result);
                    var fexcel = $('<div id="excelviewer" style="height: 700px;margin-top:20px;"></div>');
                    // img.attr('src', e.target.result);
                    $('#excelviewer').jexcel({
                        csv:e.target.result,
                        csvHeaders:true
                    });

                    $('#download').on('click', function () {
                        $('#excelviewer').jexcel('download');
                    });

                    $(selector + '_preview').html(fexcel);
                }


            }
            
            reader.readAsDataURL(input.files[i]);
        }
    }
}

function resetFileUpload(selector){

    $(selector + '_preview').html("<h1>Preview " + upperCaseFirst(selector.replace('#','')) + "</h1>");
    $(selector).val('');

}

function upperCaseFirst(value) {
    var regex = /(\b[a-z](?!\s))/g;
    return value ? value.replace(regex, function (v) {
      return v.toUpperCase();
    }) : '';
}

function remove(id){

    $("#modal_confirm").modal('show');
    $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

}



if (typeof edit_url != 'undefined'){

    function edit(id){

        window.location.href = edit_url+"/"+id;

    }

}

if (typeof target != 'undefined'){

    function proceedRemove(id){

        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

}