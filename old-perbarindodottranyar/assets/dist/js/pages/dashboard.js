/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs, .icon",
    forcePlaceholderSize: true,
    zIndex: 999999,
    start: function(event, ui) {
      ui.item.startPos = ui.item.index();
      ui.item.startSection = $(this).attr("id");
    },
    update: function(event, ui) {

      var start_section = ui.item.startSection;
      var end_section = $(this).attr('id');
      var target = base_url+'dashboard/dashboard/update_widget_position';

      if(start_section!=end_section){

        var data = { section: end_section, position: ui.item.index(), id: ui.item.attr('id') }

      }else{

        var data = { section: start_section, position: ui.item.index(), id: ui.item.attr('id') }

      }

      $.post(target,data);

    }
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom, .connectedSortable .small-box .icon").css("cursor", "move");

  //The Calender

  $.get(base_url + 'dashboard/dashboard/getEventDateOnCurrentMonth', function(res){


    $("#calendar").datepicker({
      format: "dd/mm/yyyy",
      autoclose: true,
      todayHighlight: true,
      beforeShowDay: function(date){
        var d = date;
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        var formattedDate = curr_date + "/" + curr_month + "/" + curr_year

        if ($.inArray(formattedDate, res) != -1){
          return {
            classes: 'active'
          };
        }
        return;
      }
    });

  },'json');


  $.get(base_url + 'dashboard/dashboard/getCurrenYearVisitorStat', function(res){

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var line = new Morris.Line({
      element: 'line-chart',
      data: res,
      xkey: 'm',
      ykeys: ['a'],
      labels: [current_year],
      lineColors: ['#efefef'],
      lineWidth: 2,
      hideHover: 'auto',
      gridTextColor: "#fff",
      gridStrokeWidth: 0.4,
      pointSize: 4,
      pointStrokeColors: ["#efefef"],
      gridLineColor: "#efefef",
      gridTextFamily: "Open Sans",
      gridTextSize: 10,
      xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
        var month = months[x.getMonth()];
        return month;
      },
      dateFormat: function(x) {
        var month = months[new Date(x).getMonth()];
        return month;
      },
    });

  },'json');

  $.get(base_url + 'dashboard/dashboard/getVisitorStatByCity', function(res){

    $('#world-map-markers').vectorMap({
      map: 'world_mill_en',
      normalizeFunction: 'polynomial',
      hoverOpacity: 0.7,
      hoverColor: false,
      backgroundColor: 'transparent',
      regionStyle: {
        initial: {
          fill: 'rgba(210, 214, 222, 1)',
          "fill-opacity": 1,
          stroke: 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.7,
          cursor: 'pointer'
        },
        selected: {
          fill: 'yellow'
        },
        selectedHover: {}
      },
      markerStyle: {
        initial: {
          fill: '#00a65a',
          stroke: '#111'
        }
      },
      markers: res
    });

  },'json');

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    line.redraw();
  });

  $('.img-fill').imagefill({target:'.background-image'});

});
