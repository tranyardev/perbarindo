<?php


class Cron extends CI_Controller{
	
	function __construct(){

		parent::__construct();

	}


	function check_payment_status(){

		$date_time_now = date("Y-m-d h:i:s");

		$event_registrations = $this->db->query("SELECT * FROM event_registrations WHERE status = 'WAITING_TRANSACTION'")->result_array();

		if(count($event_registrations)>0){

			foreach ($event_registrations as $er) {
				
				if($date_time_now > $er['expired_date']){

					$this->db->where('id',$er['id']);
					$this->db->update('event_registrations', array('status' => 'TRANSACTION_EXPIRED'));

				}

			}

		}

	}


}