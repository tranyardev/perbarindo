<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="old_cover" id="old_cover" value="<?php echo $dataedit['picture']; ?>">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Slider</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control validate[required]" id="name" value="<?php echo $dataedit['name']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="cover">Picture</label>
                        <input type="file" class="form-control" name="cover" id="cover">
                        <div class="img-prev"  id="cover_preview">
                            <?php
                            if($dataedit['picture'] != ""){

                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/slider/';
                                $file = $upload_path.'/'.$dataedit['picture'];

                                if(file_exists($file)){

                                    $path = base_url().'upload/slider/'.$dataedit['picture'];

                                    echo "<img src='".$path."' class='img-responsive' />
                                        <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#cover_preview\");'><i class='fa fa-remove'></i>
                                        </a>";

                                }else{

                                    echo "<h1>Slider Image</h1>";

                                }
                            }else{ ?>

                                <h1>Slider Image</h1>

                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0" <?php ($dataedit['status']=="0")? $attr="selected": $attr="";echo $attr;?>>Draft</option>
                            <option value="1" <?php ($dataedit['status']=="1")? $attr="selected": $attr="";echo $attr;?>>Publish</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

        </div>
        <div class="col-md-6">


        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#cover").change(function(){
            readURL(this, "#cover");
        });

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();


    });

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Cover Module</h1>");
        $(selector).val('');

    }

    function saveFormData(){

        var target = base_url+"configuration/slider/edit/<?php echo $dataedit['slider_id']; ?>";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }

</script>