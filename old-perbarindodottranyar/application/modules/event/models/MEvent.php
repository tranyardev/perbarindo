<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class MEvent extends CI_Model
{
    function getEventById($id){

        $result = $this->db->query("SELECT * FROM events WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getEvent(){

        $result = $this->db->query("SELECT * FROM events")->result_array();
        return $result;

    }
    function getUpComingEvent(){

        $result = $this->db->query("SELECT * FROM events WHERE start_date > CURRENT_DATE()")->result_array();
        return $result;

    }

    function getConfigurationItem($scope, $key){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope = '$scope' AND key = '$key'")->result_array();
        return @$result[0]['value'];

    }

    function getEventRegistration($regcode){

        $result = $this->db->query("SELECT a.*,b.*,c.*, 
            c.name as member_name, 
            c.email as member_email, 
            f.name as member_job_position,
            e.name as dpd_name, 
            b.name as event_name, 
            d.name as bpr_name,
            a.status as trx_status, 
            a.id as reg_id 
            FROM event_registrations a
            LEFT JOIN events b ON b.id = a.event_id
            LEFT JOIN members c ON c.id = a.member_id
            LEFT JOIN job_positions f ON f.id = c.job_position
            LEFT JOIN bpr d ON d.id = a.bpr
            LEFT JOIN dpd e ON e.id = d.dpd
            WHERE regcode='$regcode'")->result_array();

        return @$result[0];

    }

    function getEventMember($reg_id, $type){

        $result = $this->db->query("SELECT a.*,b.*,c.*,b.name as package_name, c.name as member_name  FROM event_members a
            LEFT JOIN packages b ON b.id = a.package_id
            LEFT JOIN members c ON c.id = a.member_id
            WHERE a.registration_id='$reg_id' AND a.is_twin_sharing='$type'")->result_array();

        return $result;

    }

    function getEventAllMemberForTicket($id_event){
        $result = $this->db->query("
                    select 
                     c.name dpd, b.name bpr, a.name, d.event_id, f.name event_name, e.regcode, g.status

                    from 
                     members a,
                     bpr b,
                     dpd c,
                     event_members d,
                     event_registrations e,
                     events f,
                     payment_confirmations g

                    where
                     a.bpr=b.id AND 
                     b.dpd=c.id AND 
                     d.member_id=a.id AND 
                     d.registration_id = e.id AND 
                     e.event_id = f.id AND 
                     g.event_registration_id=e.id AND 
                     g.status='1' AND 
                     f.id='".$id_event."'

                    group by 
                     c.id,
                     c.name,
                     b.name,
                     a.name,
                     d.event_id,
                     f.name,
                     e.regcode,
                     g.status

                    order by 
                     c.id ASC
                    limit 10
                     ")->result_array();

        return $result;
    }

    function getAllEventMember($id_event){

        $result = $this->db->query("select distinct on (registration_id) registration_id from event_members  where event_id='$id_event'")->result_array();

        return $result;


    }

    function getRegCode($id_registration){

        $result = $this->db->query("select * from event_registrations where id='$id_registration'")->result_array();

        return @$result[0];


    }

    function getEventMemberForTicketByDpd($id_event,$id_dpd){
        $result = $this->db->query("
                    select 
                     c.name dpd, b.name bpr, a.name, d.event_id, f.name event_name, e.regcode, g.status

                    from 
                     members a,
                     bpr b,
                     dpd c,
                     event_members d,
                     event_registrations e,
                     events f,
                     payment_confirmations g

                    where
                     a.bpr=b.id AND 
                     b.dpd=c.id AND 
                     d.member_id=a.id AND 
                     d.registration_id = e.id AND 
                     e.event_id = f.id AND 
                     g.event_registration_id=e.id AND 
                     g.status='1' AND 
                     f.id='".$id_event."' AND 
                     c.id='".$id_dpd."'

                    group by 
                     c.id,
                     c.name,
                     b.name,
                     a.name,
                     d.event_id,
                     f.name,
                     e.regcode,
                     g.status

                    order by 
                     c.id ASC
                     ")->result_array();

        return $result;
    }
}