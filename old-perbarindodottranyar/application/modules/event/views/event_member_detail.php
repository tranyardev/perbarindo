<div class="row" id="form_wrapper">
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Member Registration</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <fieldset>
                        <legend>Event</legend>
                        <table style="width: 100%" class="tbl_member_detail">
                            <tr>
                                <td class="caption">Event Name</td>
                                <td>:</td>
                                <td><?php echo $member['name']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Start Date</td>
                                <td>:</td>
                                <td><?php echo $member['start_date']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">End Date</td>
                                <td>:</td>
                                <td><?php echo $member['end_date']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Address</td>
                                <td>:</td>
                                <td><?php echo $member['address']; ?></td>
                            </tr>
                        </table>
                    </fieldset>
                    <br>
                    <fieldset>
                        <legend>Member Detail</legend>
                        <table style="width: 100%" class="tbl_member_detail">
                            <tr>
                                <td class="caption">Registration Date</td>
                                <td>:</td>
                                <td><?php echo $member['registration_date']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Name</td>
                                <td>:</td>
                                <td><?php echo $member['fullname']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Email</td>
                                <td>:</td>
                                <td><?php echo $member['email']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Nick Name</td>
                                <td>:</td>
                                <td><?php echo $member['nick_name']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Address</td>
                                <td>:</td>
                                <td><?php echo $member['member_address']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Gender</td>
                                <td>:</td>
                                <td><?php ($member['gender']=="L")? $gender="Laki - laki":$gender="Perempuan";echo $gender; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Birth Place</td>
                                <td>:</td>
                                <td><?php echo $member['birth_place']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Job</td>
                                <td>:</td>
                                <td><?php echo $member['job']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Phone Number</td>
                                <td>:</td>
                                <td><?php echo $member['phone_number']; ?></td>
                            </tr>
                            <tr>
                                <td class="caption">Mobile Number</td>
                                <td>:</td>
                                <td><?php echo $member['mobile_phone']; ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="button" onclick="window.history.back()" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

        </div>

    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){


    });
</script>