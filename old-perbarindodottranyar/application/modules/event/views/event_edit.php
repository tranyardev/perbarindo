<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="description" id="post_description" value="<?php echo $dataedit['description']; ?>">
        <input type="hidden" name="old_cover" id="old_cover" value="<?php echo $dataedit['cover']; ?>">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Event</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" reqired name="name" class="form-control validate[required]" id="name" value="<?php echo $dataedit['name']; ?>">
                    </div>
                   
                    <!-- Date range -->
                    <div class="form-group">
                        <label>Date</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="date" class="form-control pull-right" id="event_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                     <!-- Date range -->
                    <div class="form-group">
                        <label>Registration Date</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="reg_date" class="form-control pull-right" id="event_registration_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <!-- /.form group -->
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description"><?php echo $dataedit['description']; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0" <?php ($dataedit['status']=="0")? $attr="selected": $attr="";echo $attr;?>>Draft</option>
                            <option value="1" <?php ($dataedit['status']=="1")? $attr="selected": $attr="";echo $attr;?>>Publish</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="quota">Quota</label>
                        <input type="text" name="quota" maxlength="100" class="form-control" id="quota" placeholder="Quota" value="<?php echo $dataedit['quota']; ?>">
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cover</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="cover">Cover</label>
                        <input type="file" class="form-control" name="cover" id="cover">
                        <div class="img-prev"  id="cover_preview">
                            <?php
                            if($dataedit['cover'] != ""){

                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                                $file = $upload_path.'/'.$dataedit['cover'];

                                if(file_exists($file)){

                                    $path = base_url().'upload/event/'.$dataedit['cover'];

                                    echo "<img src='".$path."' class='img-responsive' />
                                        <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#cover_preview\");'><i class='fa fa-remove'></i>
                                        </a>";

                                }else{

                                    echo "<h1>Cover Event</h1>";

                                }
                            }else{ ?>

                                <h1>Cover Event</h1>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>

        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Address Detail</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <input id="searchInput" class="controls" type="text" value="<?php echo $dataedit['address']; ?>">
                    <div id="map"></div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text"  name="address" class="form-control validate[required]" id="location" value="<?php echo $dataedit['address']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text"  name="country" class="form-control validate[required]" id="country" value="<?php echo $dataedit['country']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text"  name="city" class="form-control validate[required]" id="city" value="<?php echo $dataedit['city']; ?>">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <input type="text" readonly name="latitude" class="form-control" id="lat" placeholder="Latitude" value="<?php echo $dataedit['latitude']; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="longitude">Longitude</label>
                                <input type="text" readonly name="longitude" class="form-control" id="lon" placeholder="Longitude" value="<?php echo $dataedit['longitude']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>

            </div>

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){
        CKEDITOR.replace('description',ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

            });

        }

        $("#searchInput").trigger("click");
        $("#cover").change(function(){
            readURL(this, "#cover");
        });

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

        //Date range picker
        $('#event_date').daterangepicker({
            locale: {
                format: 'YYYY/MM/DD'
            },
            startDate: '<?php echo $dataedit['start_date']; ?>',
            endDate: '<?php echo $dataedit['end_date']; ?>'
        });

         $('#event_registration_date').daterangepicker({
            locale: {
                format: 'YYYY/MM/DD'
            },
            startDate: '<?php echo $dataedit['registration_start_date']; ?>',
            endDate: '<?php echo $dataedit['registration_end_date']; ?>'
        });

    });

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Cover Module</h1>");
        $(selector).val('');

    }

    function saveFormData(){

        var target = base_url+"event/event/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){


        $('#form')[0].reset();
        $("#cover_preview").html('<h1>Cover Module</h1>');
        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }
</script>