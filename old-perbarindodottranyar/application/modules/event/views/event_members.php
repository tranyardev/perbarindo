<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
       
       <div class="row">
           <div class="col-md-3">
               <div class="img-prev"  id="cover_preview">
                    <?php
                    if($event['cover'] != ""){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                        $file = $upload_path.'/'.$event['cover'];

                        if(file_exists($file)){

                            $path = base_url().'upload/event/'.$event['cover'];

                            echo "<img src='".$path."' class='img-responsive' />";

                        }else{

                            echo "<h1>Cover Event</h1>";

                        }
                    }else{ ?>

                        <h1>Cover Event</h1>

                    <?php } ?>
                </div>
           </div>
           <div class="col-md-9">
               <h1><?php echo $event['name']; ?></h1>
               <p><i class="fa fa-calendar"></i> Start Date : <?php echo $event['start_date']?></p>
               <p><i class="fa fa-calendar"></i> End Date   : <?php echo $event['end_date']?></p>
               <p><i class="fa fa-map-marker"></i> <?php echo $event['address']; ?></p>
              
           </div>
       </div>
       
    </div>
    <!-- /.box-body -->
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event Member</h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <input type="file" class="form-control" name="excel" id="excel" style="visibility: hidden;">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Event" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Reg ID</th>
                <th>Bpr</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    </form>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print',
            exportOptions: {
                    columns: ':visible'
            },
            
            title: function(){
                 return '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Event</span>';
            }
        },
        'colvis'
    ];

    var columns =  [

            { "data" : "$.member" },
            { "data" : "b.email" },
            { "data" : "c.id", "searchable" : false },
            { "data" : "$.bpr", "sortable" : false, "searchable" : false }

        ];

    buttons.push( {
        text: '<i class="fa fa-file-excel-o"></i> Import Data',
        action: function ( e, dt, node, config ) {
           
            $("#excel").trigger("click");

        }
    });

    buttons.push( {
        text: '<i class="fa fa-file-excel-o"></i> Download Template',
        action: function ( e, dt, node, config ) {
            window.location.href = base_url + "/templates/excel/event-member-import-template.xlsx"
        }
    });

    $(document).ready(function(){

        InitDatatable();

        $("#excel").on("change",function(){
            
            var formData = new FormData($('#form')[0]);

            $.ajax({
                type: "POST",
                url: base_url + "event/event/import/<?php echo $event['id']; ?>",
                data: formData,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {

                    toastr.info("Importing file...", 'Loading');
                    
                },
                success: function(res) {

                    if(res.status!="1"){

                        toastr.error(res.msg, 'Response Server');

                    }else{

                        toastr.success(res.msg, 'Response Server');

                    }

                    $('#dttable').dataTable().fnDestroy();

                    InitDatatable();
                  
                   
                },
                error: function() {

                    toastr.error('Something went wrong!', 'Response Server');
                }

            });

        });

    });

    function InitDatatable(){

        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "event/event/dataTableMember",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function edit(id){
        window.location.href="<?php echo base_url(); ?>event/event/edit_activity/"+id;
    }

    function show_activities(id){
        window.location.href="<?php echo base_url(); ?>event/event/activities/"+id;
    }

    function show_members(id){
        window.location.href="<?php echo base_url(); ?>event/event/members/"+id;
    }

    function remove(id){

        $("#modal_confirm").modal('show');
        $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

    }

    function proceedRemove(id){

        var target = '<?php echo base_url(); ?>event/event/removeActivity';
        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
</script>
