<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Event" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>City</th>
                <th>Status</th>

                <?php
                    if($edit_perm!='1' && $delete_perm!='1' && $activity_read_perm!='1' && $member_read_perm!='1'){

                    }else{
                        echo '<th>Action</th>';
                    }
                ?>



            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    var add_perm = '<?php echo $add_perm; ?>';
    var edit_perm = '<?php echo $edit_perm; ?>';
    var delete_perm = '<?php echo $delete_perm; ?>';
    var activity_read_perm = '<?php echo $activity_read_perm; ?>';
    var member_read_perm = '<?php echo $member_read_perm; ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar Event';
            }
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print',
            exportOptions: {
                    columns: ':visible'
            },
            
            title: function(){
                 return '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Event</span>';
            }
        },
        'colvis'
    ];

    var columns =  [

            { "data" : "a.name" },
            { "data" : "a.start_date", "searchable": false},
            { "data" : "a.end_date", "searchable": false},
            { "data" : "a.city"},
            { "data" : "$.status", "searchable": false},
            { "data" : "$.op", "orderable": false, "searchable": false},

        ];

    if(edit_perm != '1' && delete_perm != '1' && activity_read_perm != '1' && member_read_perm != '1'){

        columns = [

            { "data" : "a.name" },
            { "data" : "a.start_date", "searchable": false},
            { "data" : "a.end_date", "searchable": false},
            { "data" : "a.city"},
            { "data" : "$.status", "searchable": false}

        ];

    }

    if(add_perm=='1'){

        buttons.push( {
            text: '<i class="fa fa-plus"></i> New Event',
            action: function ( e, dt, node, config ) {
                window.location.href = base_url + "event/event/addnew"
            }
        });

    }

    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){

        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "event/event/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function edit(id){
        window.location.href="<?php echo base_url(); ?>event/event/edit/"+id;
    }

    function show_activities(id){
        window.location.href="<?php echo base_url(); ?>event/event/activities/"+id;
    }

    function show_members(id){
        window.location.href="<?php echo base_url(); ?>event/event/members/"+id;
    }

    function remove(id){

        $("#modal_confirm").modal('show');
        $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

    }

    function proceedRemove(id){

        var target = '<?php echo base_url(); ?>event/event/remove';
        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
</script>
