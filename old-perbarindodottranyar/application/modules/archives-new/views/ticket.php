<!DOCTYPE html>
<html>
    <head>  

    <style> 

      body {
        background: rgb(204,204,204); 
      }
      page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
      }
      page[size="A4"] {  
        width: 21cm;
        min-height: 29.7cm; 
      }
      page[size="A4"][layout="portrait"] {
        width: 29.7cm;
        min-height: 21cm;  
      }
      page[size="A3"] {
        width: 29.7cm;
        height: 42cm;
      }
      page[size="A3"][layout="portrait"] {
        width: 42cm;
        height: 29.7cm;  
      }
      page[size="A5"] {
        width: 14.8cm;
        height: 21cm;
      }
      page[size="A5"][layout="portrait"] {
        width: 21cm;
        height: 14.8cm;  
      }
      @media print {
        body, page {
          margin: 0;
          box-shadow: 0;
        }
      }
      table {
          font-size: 12pt;
          font-family: "Times New Roman", Times, serif;
      }
      
      .table2 tr>td,
      table tr>td {
          border: 1px solid #DDD; 
      }
      
      table tr>td>table {
          border: 1px solid #9e9e9e;
          padding: 0px;
      }
      
      table tr>td>table tr>th {  
          border: 0px solid #fde0bc; 
      }
      
      table tr>td>table tr>td {
          border: 0px solid #DDD;
          background-color: #FFF; 
      }
      
      table tr>td>table tr>th>b {
          font-weight: 600;
          color: #757575; 
      }
      
      table tr>td>table tr>td>span {
          color: #8a8585; 
      }
      
      .bordered-dashed {
          border-style: dashed;
          border-color: #c5c5c5;
          border-width: 1.5px;
          margin-top: 10px;
          margin-bottom: 10px; 
      }
      
      th.rotate {
          white-space: nowrap;
          padding-top: 165px;
          width: 12px; 
      }
      
      th.rotate>div {
          transform: translate(1px, 5px) rotate(630deg);
          width: 12px; 
      }
      
      th.rotate>div>span {
          border-bottom: 1px solid #ccc;
          width: 12px; 
      }

      @media print {
          .page-break {page-break-after: always;}
      }

      /** End Print **/ 
    </style>
</head>

<body>

  <page size="A4" >
  <br>
  <?php echo $tickets; ?>
  </page>
  <br>
</body>
</html>