<?php


class Marchives extends CI_Model
{
    function getCertificateData($event_member_id){

        $result = $this->db->query("
            SELECT b.name, c.name bpr, d.name corporate, e.name event_name, e.event_theme, e.start_date, e.end_date, e.city 
            FROM 
             event_members a, 
             members b, 
             bpr c,
             corporates d,
             events e
             
            WHERE 
             a.id=".$event_member_id." AND 
             a.member_id=b.id AND 
             b.bpr=c.id AND 
             c.corporate=d.id AND 
             a.event_id=e.id
             ")->result_array();

        return $result;

    }

    function getReceiptData($event_member_id){

        $result = $this->db->query("
            SELECT b.name, c.name bpr, d.name corporate, e.event_theme, e.start_date, e.end_date, e.city, e.address, e.name event_name, f.total_cost, f.regcode, e.id event_id, b.id member_id
            FROM 
             event_members a, 
             members b, 
             bpr c,
             corporates d,
             events e,
             event_registrations f,
             payment_confirmations g
             
            WHERE 
             a.id='".$event_member_id."' AND 
             a.member_id=b.id AND 
             b.bpr=c.id AND 
             c.corporate=d.id AND 
             a.event_id=e.id AND 
             a.registration_id=f.id AND 
             f.id=g.event_registration_id AND 
             f.status='TRANSACTION_COMPLETED'
             ")->result_array();

        return $result;

    }

    // function getDocumentEventByMemberId($event_member_id){

    //     $result = $this->db->query("
    //                 SELECT a.* 
    //                 FROM 
    //                  event_documents a, (SELECT * FROM event_members WHERE id='".$event_member_id."') as b 
    //                 WHERE 
    //                  a.doc_type_id=2 AND 
    //                  a.member_id = b.member_id AND 
    //                  a.event_id = b.event_id
    //             ")->result_array(); //doc_type_id 2 = kwitansi
    //     return @$result[0]['total_receipt_out'];

    // }

    function checkEventDoc($data){

        $sql = "SELECT * FROM event_documents WHERE event_id = '".$data['event_id']."'
                AND doc_type_id='".$data['doc_type_id']."'
                AND member_id='".$data['member_id']."'
                AND regcode='".$data['regcode']."'";

        $result = $this->db->query($sql)->result_array();

        return @$result[0];

    }

    function getNumberReceipt($event_member_id){
        $result = $this->db->query("
                    SELECT a.*                  
                    FROM 
                     event_documents a, (SELECT * FROM event_members WHERE id='".$event_member_id."') as b 
                    WHERE 
                     a.doc_type_id=2 AND 
                     a.member_id = b.member_id AND 
                     a.event_id = b.event_id
                ")->result_array(); //doc_type_id 2 = kwitansi
        return @$result[0];
    }

    function getNumberReceipt_data($event_member_id){
        $result = $this->db->query("
                    SELECT a.*
                    FROM 
                     event_documents a, (SELECT * FROM event_members WHERE id='".$event_member_id."') as b
                    WHERE 
                     a.doc_type_id=2 AND 
                     a.member_id = b.member_id AND 
                     a.event_id = b.event_id
                    ORDER BY 
                     a.id DESC
                ")->result_array(); //doc_type_id 2 = kwitansi
        return @$result[0];
    }

    function getArchivesConfig($event_id){
        $result = $this->db->query("
                    SELECT * FROM events WHERE id='".$event_id."'
                ")->result_array();
        return @$result[0];
    }

    function getEventRegistration($regcode){

        $result = $this->db->query("SELECT a.*,b.*,c.*, 
            c.name as member_name, 
            c.email as member_email, 
            f.name as member_job_position,
            e.name as dpd_name, 
            b.name as event_name, 
            d.name as bpr_name,
            a.status as trx_status, 
            a.id as reg_id 
            FROM event_registrations a
            LEFT JOIN events b ON b.id = a.event_id
            LEFT JOIN members c ON c.id = a.member_id
            LEFT JOIN job_positions f ON f.id = c.job_position
            LEFT JOIN bpr d ON d.id = a.bpr
            LEFT JOIN dpd e ON e.id = d.dpd
            WHERE regcode='$regcode'")->result_array();

        return @$result[0];

    }

    function getEventMember($reg_id, $type){

        $result = $this->db->query("SELECT a.*,b.*,c.*,b.name as package_name, c.name as member_name  FROM event_members a
            LEFT JOIN packages b ON b.id = a.package_id
            LEFT JOIN members c ON c.id = a.member_id
            WHERE a.registration_id='$reg_id' AND a.is_twin_sharing='$type'")->result_array();

        return $result;

    }

    function getApprovedPaymentMember($event_id=12, $id_document=1){
        /*SELECT a.id, b.name, c.name bpr, d.name corporate, e.event_theme, e.start_date, e.end_date, e.city, e.address, e.name event_name, f.total_cost, (SELECT COUNT(*) FROM event_documents WHERE event_id=e.id AND member_id=a.member_id AND doc_type_id='".$id_document."') document_out*/
        $result = $this->db->query("            
            SELECT a.member_id, a.id, b.name, c.name bpr, d.name corporate, e.event_theme, e.start_date, e.end_date, e.city, e.address, e.name event_name, f.total_cost
            FROM 
             event_members a, 
             members b, 
             bpr c,
             corporates d,
             events e,
             event_registrations f,
             payment_confirmations g
             
            WHERE              
             a.member_id=b.id AND 
             b.bpr=c.id AND 
             c.corporate=d.id AND 
             a.event_id=e.id AND 
             a.registration_id=f.id AND 
             f.id=g.event_registration_id AND 
             f.status='TRANSACTION_COMPLETED' AND 
             a.event_id = '".$event_id."' 
             ORDER BY 
              a.id ASC
             ")->result_array();

        return $result;
    }

}