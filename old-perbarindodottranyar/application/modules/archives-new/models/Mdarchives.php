<?php

class Mdarchives extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_receipt_print = $this->mcore->checkPermission($this->user_group, 'archives_receipt_print');
        $this->allow_certificate_print = $this->mcore->checkPermission($this->user_group, 'archives_certificate_print');

    }

    public function appendToSelectStr() {

        $receipt = '';
        $certificate = '';

        if($this->allow_receipt_print){
            $receipt = '<a href="'.base_url().'archives/generate_receipt/\',a.id,\'" class="btn btn-info btn-simple btn-sm" target="_blank">Kwitansi</a>&nbsp;';
        }

        if($this->allow_certificate_print){
            $certificate = '<a href="'.base_url().'archives/generate_certificate/\',a.id,\'" class="btn btn-primary btn-simple btn-sm" target="_blank">Sertifikat</a>&nbsp;';
            $certificate .= '<a href="'.base_url().'archives/generate_ticket_event/\',h.regcode,\'" class="btn btn-primary btn-simple btn-sm" target="_blank">Ticket</a>&nbsp;';
        }

        $op = "concat('".$receipt.$certificate."')";

        $str = array(
            
            "member_name" => "b.name",
            "bpr" => "e.name",
            "dpd" => "f.name",
            "kehadiran" => "
                                CASE
                                    WHEN  (g.event_member_id > 0) THEN 'Hadir' 
                                    ELSE 'Tidak Hadir' 
                                END
                            ",
            "op" => $op


        );

        return $str;

    }

    public function fromTableStr() {
        return "event_members a";
    }

    public function joinArray(){
        return array(

            "members b" => "b.id=a.member_id",
            "bpr e" => "e.id=b.bpr",            
            "dpd f" => "f.id=e.dpd",
            "event_member_presences g|left" => "g.event_member_id=b.id",
            "event_registrations h" => "h.id=a.registration_id"

        );
    }

    public function whereClauseArray(){
        return array(
            "a.event_id" => $_SESSION['archives_event_id']
        );
    }


}