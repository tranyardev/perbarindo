<?php

class Mdpaymentconfirmation extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'payment_confirmation_update');

    }
    public function appendToSelectStr() {
        $st = null;

        if($this->allow_edit){
            $st = "CASE WHEN a.status = '0' THEN concat('<select class=\"form-control\" id=\"status',a.id,'\" onchange=\"changeStatus(',a.status,',this,',a.id,');\"><option value=\"0\" selected>Waiting for Approve</option><option value=\"1\">Approved</option></select>') ELSE concat('<select class=\"form-control\" id=\"status',a.id,'\" onchange=\"changeStatus(',a.status,',this,',a.id,');\"><option value=\"0\">Waiting for Approve</option><option value=\"1\" selected>Approved</option></select>') END";
        }

        /*if($this->allow_edit){
            $st = "CASE WHEN a.status = '0' THEN concat('<select class=\"form-control\" id=\"status',a.id,'\" onchange=\"changeStatus(',a.status,',this,',a.id,');\"><option value=\"0\" selected>Waiting for Approve</option><option value=\"1\">Approved</option></select>') ELSE concat('<select disabled class=\"form-control\" id=\"status',a.id,'\" onchange=\"changeStatus(',a.status,',this,',a.id,');\"><option value=\"0\">Waiting for Approve</option><option value=\"1\" selected>Approved</option></select>') END";
        }*/

        $money = '<span class="price">\',b.total_cost,\'</span>';
        $mo = "concat('".$money."')";

        $preview = '<a class="btn btn-sm btn-info" href="javascript:preview(\',a.id,\');"><i class="fa fa-eye"></i></a>&nbsp;';

        if($st != null){

            $op = "concat('".$preview."')";

            $str = array(

                "bpr" => "f.name",
                "banksender" => "a.bank",
                "bankreceiver" => "c.bank",
                "amount" => $mo,
                "status" => $st,
                "op" => $op

            );

        }else{

            $mo = "concat('".$money."')";

            $str = array(

                "bpr" => "f.name",
                "banksender" => "a.bank",
                "bankreceiver" => "c.bank",
                "amount" => $mo,
                "op" => $op

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "payment_confirmations a";
    }

    public function joinArray(){
        
        return array(
            "event_registrations b" => "b.id = a.event_registration_id",
            "bank_transfer_accounts c" => "c.id = a.to_account",

            "aauth_users d" => "d.id = a.member_id",
            
            // "members e" => "e.aauth_user_id = d.id",
            "bpr f" => "f.id = b.bpr"
        );
        
    }
    public function whereClauseArray(){
        return null;
    }


}