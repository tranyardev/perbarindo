<?php

class Bankaccount extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbankaccount");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'bank_account_view';
            $this->add_perm = 'bank_account_add';
            $this->edit_perm = 'bank_account_update';
            $this->delete_perm = 'bank_account_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "bank_transfer_accounts";
        $this->dttModel = "Mdbankaccount";
        $this->pk = "id";

    }

    function index(){

        $breadcrumb = array(
            "<i class='fa fa-history'></i> Bank Account" => base_url()."bankaccount"
        );

        $data['page'] = 'bankaccount';
        $data['page_title'] = 'Bank Account';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'finance';
        $data['data']['submenu'] = 'bank_account';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    
    function addnew(){

        if(count($_POST)>0){

            $data = array(
                "bank" => $this->input->post("bank"),
                "account_name" => $this->input->post("account_name"),
                "no_rek" => $this->input->post("account_number"),
                "status" => $this->input->post("status"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")
            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-history'></i> Bank Account" => base_url()."bankaccount",
                "Add Bank Account" => base_url()."bankaccount/bankaccount/addnew",
            );

            $data['page'] = 'bankaccount_add';
            $data['page_title'] = 'Bank Account';
            $data['page_subtitle'] = 'Add Bank Account';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['data']['parent_menu'] = 'finance';
            $data['data']['submenu'] = 'bank_account';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(
                "bank" => $this->input->post("bank"),
                "account_name" => $this->input->post("account_name"),
                "no_rek" => $this->input->post("account_number"),
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{
            
            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Bank Account" => base_url()."bankaccount",
                "Edit Bank Account" => base_url()."bankaccount/bankaccount/edit/".$id,
            );

            $data['page'] = 'bankaccount_edit';
            $data['page_title'] = 'Bank Account';
            $data['page_subtitle'] = 'Edit bank Account';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['data']['dataedit'] = $this->Mbankaccount->getBankAccountById($id);
            $data['data']['parent_menu'] = 'finance';
            $data['data']['submenu'] = 'bank_account';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

}