<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="description" id="post_description" value='<?php echo $dataedit['description']; ?>'>
        <input type="hidden" name="price" id="post_price" value='<?php echo $dataedit['price']; ?>'>

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit FAQ</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="event">Event</label>
                        <select class="form-control" name="event" id="event">
                            <?php
                            foreach ($event as $e){
                                if($e['id']==$dataedit['event_id']){
                                    $selected = "selected";
                                }else{
                                    $selected = "";
                                }
                                echo "<option ".$selected." value='".$e['id']."'>".$e['name']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" maxlength="100" name="name" class="form-control validate[required]" id="name" placeholder="Package Name (Required)" value="<?php echo $dataedit['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" id="price" class="form-control validate[required]" id="price" placeholder="Price (Required)" value="<?php echo $dataedit['price']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description"><?php echo $dataedit['description']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="twin_sharing">Twin Sharing</label>
                        <select class="form-control" name="twin_sharing" id="twin_sharing">
                            <option value="0" <?php ($dataedit['twin_sharing']=="0")? $attr="selected": $attr="";echo $attr;?>>No</option>
                            <option value="1" <?php ($dataedit['twin_sharing']=="1")? $attr="selected": $attr="";echo $attr;?>>Yes</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->
        </div>
        
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){
        CKEDITOR.replace('description',ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

            });

        }

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

        $('#price').priceFormat({
            prefix: 'Rp ',
            thousandsSeparator: '.',
            limit: 13,
            centsLimit: 0
        });

        $('#price').on('change', function() {

            $("#post_price").val($("#price").unmask());

        });

    });

    function saveFormData(){

        var target = base_url+"packages/packages/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            resetForm();

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
</script>
