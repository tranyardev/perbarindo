<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

 class Addons extends MX_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('email');
        $this->load->model('maddons');
    }

    function index(){
         
    }
    function recurseRmdir($dir) {
      $files = array_diff(scandir($dir), array('.','..'));
      foreach ($files as $file) {
        (is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
      }
      return rmdir($dir);
    }
    function mymodules(){

        $data['page'] = 'layout/my_addons';
        $data['data']['test'] = "test";
        $this->load->view('layout/body',$data);

    }
    function store(){

        $data['page'] = 'layout/store_addons';
        $data['data']['addons_product'] = $this->maddons->getModules();
        $this->load->view('layout/body',$data);

    }
    function create(){

        if(count($_POST)>0){

            /* uploading several files */

            if(count($_FILES)>0){

                if($_POST['slug']!=""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.$_POST['slug'];
                    $package_file = basename($_FILES["file"]["name"]);
                    $this->makedirs($upload_path);

                    /* upload package module */
                    if($_FILES["file"]["type"]=="application/zip"){

                        if($package_file==$_POST['slug'].".zip"){

                            if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path."/".$package_file)) {


                            }else{

                                $this->recurseRmdir($upload_path);
                                $res['msg'] = "Oops! Something went wrong!";
                                $res['status'] = "0";

                                echo json_encode($res);
                                exit;

                            }

                        }else{


                            $this->recurseRmdir($upload_path);
                            $res['msg'] = "Invalid package file name ! Should be same with slug name.";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $this->recurseRmdir($upload_path);
                        $res['msg'] = "Invalid package file! Should be zip archive.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    /* upload cover */

                    if($_FILES["cover"]["name"]!=""){

                        if($_FILES["cover"]["type"]=="image/png" or 
                           $_FILES["cover"]["type"]=="image/jpg" or
                           $_FILES["cover"]["type"]=="image/jpeg"){
                            
                            $cover = basename($_FILES["cover"]["name"]);

                            if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                            }else{

                                $this->recurseRmdir($upload_path);
                                $res['msg'] = "Oops! Something went wrong!";
                                $res['status'] = "0";

                                echo json_encode($res);
                                exit;

                            }

                        }else{

        
                            $this->recurseRmdir($upload_path);
                            $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }

                    /* upload screenshots */

                    for($i=1;$i < 4; $i++){

                        if($_FILES["ss".$i]["name"]!=""){

                            if($_FILES["ss".$i]["type"]=="image/png" or 
                               $_FILES["ss".$i]["type"]=="image/jpg" or
                               $_FILES["ss".$i]["type"]=="image/jpeg"){
                                
                                $ss = basename($_FILES["ss".$i]["name"]);

                                if (move_uploaded_file($_FILES["ss".$i]["tmp_name"], $upload_path."/".$ss)) {
                                    $_POST['ss'.$i] = $ss;
                                }else{

                                    $this->recurseRmdir($upload_path);
                                    $res['msg'] = "Oops! Something went wrong!";
                                    $res['status'] = "0";

                                    echo json_encode($res);
                                    exit;

                                }

                            }else{


                                $this->recurseRmdir($upload_path);
                                $res['msg'] = "Invalid Screenshot ".$i." file! Should be PNG/JPG/JPEG.";
                                $res['status'] = "0";

                                echo json_encode($res);
                                exit;

                            }

                        }
                        

                    }

                    /* insert data to database */

                    $insert = array(

                        "name" => $_POST['name'],
                        "description" => $_POST['description'],
                        "slug" => $_POST['slug'],
                        "version" => $_POST['version'],
                        "group" => $_POST['group'],
                        "status" => $_POST['status'],
                        "developer" => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
                        "cover" => @$cover,
                        "ss1" => @$_POST['ss1'],
                        "ss2" => @$_POST['ss2'],
                        "ss3" => @$_POST['ss3'],
                        "file" => @$package_file,
                        "video_url" => @$_POST['video_url'],
                        "created_by" => $this->session->userdata("id")

                    );

                    $result = $this->db->insert("addons_store",$insert);

                    if($result){
                        $res['msg'] = "Module created successfully!";
                        $res['status'] = "1";
                    }else{
                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";
                    }
                   

                    echo json_encode($res);
 
                }

            }
           

        }else{

          $data['page'] = 'layout/create_addons';
          $data['data']['test'] = "test";
          $this->load->view('layout/body',$data);

        }
        
    }



    function update(){

        if(count($_POST)>0){

            /* uploading several files */

            if(count($_FILES)>0){

                if($_POST['slug']!=""){
                	
                	$upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.$_POST['slug'];

                	if($_FILES["file"]["name"]!=""){

                		
	                    $package_file = basename($_FILES["file"]["name"]);

	                    /* upload package module */
	                    if($_FILES["file"]["type"]=="application/zip"){

	                        if($package_file==$_POST['slug'].".zip"){


	                        	$old_file = $upload_path."/".$_POST['old_file'];

	                        	if(file_exists($old_file)){

	                        		@unlink($old_file);

	                        	}

	                            if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path."/".$package_file)) {


	                            }else{

	                               
	                                $res['msg'] = "Oops! Something went wrong!";
	                                $res['status'] = "0";

	                                echo json_encode($res);
	                                exit;

	                            }

	                        }else{


	                            
	                            $res['msg'] = "Invalid package file name ! Should be same with slug name.";
	                            $res['status'] = "0";

	                            echo json_encode($res);
	                            exit;

	                        }

	                    }else{

	                        $res['msg'] = "Invalid package file! Should be zip archive.";
	                        $res['status'] = "0";

	                        echo json_encode($res);
	                        exit;

	                    }

                	}
                    

                    /* upload cover */

                    if($_FILES["cover"]["name"]!=""){

                        if($_FILES["cover"]["type"]=="image/png" or 
                           $_FILES["cover"]["type"]=="image/jpg" or
                           $_FILES["cover"]["type"]=="image/jpeg"){
                            
                            $cover = basename($_FILES["cover"]["name"]);

                        	$old_cover = $upload_path.'/'.$_POST['old_cover'];

                        	if(file_exists($old_cover)){

                        		@unlink($old_cover);

                        	}

                            if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                            }else{

                     
                                $res['msg'] = "Oops! Something went wrong!";
                                $res['status'] = "0";

                                echo json_encode($res);
                                exit;

                            }

                        }else{

        
                            $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }

                    /* upload screenshots */

                    for($i=1;$i < 4; $i++){

                        if($_FILES["ss".$i]["name"]!=""){

                            if($_FILES["ss".$i]["type"]=="image/png" or 
                               $_FILES["ss".$i]["type"]=="image/jpg" or
                               $_FILES["ss".$i]["type"]=="image/jpeg"){
                                
                                $ss = basename($_FILES["ss".$i]["name"]);

                            	$old_ss = $upload_path."/".$_POST['old_ss'.$i];

                            	if(file_exists($old_ss)){

                            		@unlink($old_ss);

                            	}

                                if (move_uploaded_file($_FILES["ss".$i]["tmp_name"], $upload_path."/".$ss)) {
                                    $_POST['ss'.$i] = $ss;
                                }else{

                                    $res['msg'] = "Oops! Something went wrong!";
                                    $res['status'] = "0";

                                    echo json_encode($res);
                                    exit;

                                }

                            }else{


                                $res['msg'] = "Invalid Screenshot ".$i." file! Should be PNG/JPG/JPEG.";
                                $res['status'] = "0";

                                echo json_encode($res);
                                exit;

                            }

                        }
                        

                    }

                    /* insert data to database */

                    $insert = array(

                        "name" => $_POST['name'],
                        "description" => $_POST['description'],
                        "slug" => $_POST['slug'],
                        "version" => $_POST['version'],
                        "group" => $_POST['group'],
                        "status" => $_POST['status'],
                        "video_url" => @$_POST['video_url'],
                        "updated_by" => $this->session->userdata("id"),
                        "last_update" => date("Y-m-d h:i:s"),

                    );

                    if(isset($package_file)){

                    	if($package_file!=""){

                    		$insert["file"] = $package_file;

                    	}

                    }


                    if(isset($cover)){

                    	if($cover!=""){

                    		$insert["cover"] = $cover;

                    	}

                    }

                    if(isset($_POST['ss1'])){

                    	if($_POST['ss1']!=""){

                    		$insert["ss1"] = $_POST['ss1'];

                    	}

                    }

                    if(isset($_POST['ss2'])){

                    	if($_POST['ss2']!=""){

                    		$insert["ss2"] = $_POST['ss2'];

                    	}

                    }

                    if(isset($_POST['ss3'])){

                    	if($_POST['ss3']!=""){

                    		$insert["ss3"] = $_POST['ss3'];


                    	}

                    }

                    $this->db->where("module_id", $_POST['module_id']);
                    $result = $this->db->update("addons_store",$insert);

                    if($result){
                        $res['msg'] = "Module Updated successfully!";
                        $res['status'] = "1";
                    }else{
                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";
                    }
                   

                    echo json_encode($res);
 
                }

            }
           

        }
        
    }

    function edit($id=null){

        if($id!=null){

            $module = $this->maddons->getModule($id);

            $data['page'] = 'layout/edit_addons';
            $data['data']['module'] = $module;
            $this->load->view('layout/body',$data);


        }

    }

    function view($id=null){

        if($id!=null){

            $module = $this->maddons->getModule($id);

            $data['page'] = 'layout/detail_addons';
            $data['data']['addons_detail'] = $module;
            $this->load->view('layout/body',$data);


        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $_POST['id'];

            $query = $this->db->query("SELECT * FROM addons_store WHERE module_id='".$id."'")->result_array();
            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.@$query[0]['slug'];

            if(file_exists($upload_path)){
                $this->recurseRmdir($upload_path);
            }
            
            $this->db->where('module_id', $id);
            $delete = $this->db->delete('addons_store');

            if($delete){

                $result = array("status" => "1", "msg" => "Successfully delete module!");

            }else{

                $result = array("status" => "0", "msg" => "Oops! Something went wrong.");

            }

            echo json_encode($result);

        }

    }

    function approval(){

    	$data['page'] = 'layout/approval_addons';
        $this->load->view('layout/body',$data);

    }

    function update_addons_status(){

    	if(count($_POST)>0){

    		$result = $this->db->query("UPDATE addons_store SET status = '".$_POST['status']."' WHERE module_id='".$_POST['id']."'");

    		if($result){

    			$res = array("status" => 1, "msg" => "Status Successfully Updated!");

    		}else{

    			$res = array("status" => 0, "msg" => "Oops! Someting went wrong.");

    		}

    		echo json_encode($res);
    	}

    }

    function makedirs($dirpath, $mode=0777) {
        return is_dir($dirpath) || mkdir($dirpath, $mode, true);
    }

    public function dataTable() {
    	
        //Important to NOT load the model and let the library load it instead.  
        $this->load->library('Datatable', array('model' => 'MDAddons', 'rowIdCol' => 'a.module_id'));

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    public function dataTableApproval() {
    	
        //Important to NOT load the model and let the library load it instead.  
        $this->load->library('Datatable', array('model' => 'MDAddonsApproval', 'rowIdCol' => 'a.module_id'));

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

 }