<div class="box">
    <div class="box-header">
        <h3 class="box-title">Archives</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Archives" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>City</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>

<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    var columns =  [

        { "data" : "a.name"},
        { "data" : "a.start_date", "searchable": false},
        { "data" : "a.end_date", "searchable": false},
        { "data" : "a.city"},
        { "data" : "$.status", "searchable": false},
        { "data" : "$.op", "orderable": false, "searchable": false},

    ];


    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){


        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "archives/archives/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": [],
            "drawCallback": function( settings ) {
                $("img.scale").imageScale();
            }
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function detail(id){
        window.location.href="<?php echo base_url(); ?>archives/archives/detail/"+id;
    }

    function config(id){
        window.location.href="<?php echo base_url(); ?>archives/archives/config/"+id;
    }
</script>
