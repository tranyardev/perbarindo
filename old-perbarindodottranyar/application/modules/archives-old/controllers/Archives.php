<?php

class Archives extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("ciqrcode");
        $this->load->model("mcore");
        $this->load->model("event/Mevent2");
        $this->load->model("Marchives");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'archives_view'; // nanti ganti ini !
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "events";
        $this->dttModel = "Mdevent";
        $this->pk = "id";

        $this->tableDetail = "event_members";
        $this->dttModelDetail = "Mdarchives";
        $this->pkDetail = "id";

        $this->receipt_perm = 'archives_receipt_print';
        $this->certificate_perm = 'archives_certificate_print';

        $this->receipt_config_perm = 'archives_receipt_config';
        $this->certificate_config_perm = 'archives_certificate_config';

    }

    function index(){

        $breadcrumb = array(
            "<i class='fa fa-book'></i> Archives" => base_url()."archives"
        );

        $data['page'] = 'archives';
        $data['page_title'] = 'Archives';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'archives';
        $data['data']['submenu'] = '';
        $this->load->view('layout/body',$data);

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    function detail($id){
        
        $breadcrumb = array(
            "<i class='fa fa-book'></i> Archives " => base_url()."archives",
            "Archives Detail List" => base_url()."archives/archives/detail/".$id,
        );

        $_SESSION['archives_event_id'] = $id;

        $data['page'] = 'archives_detail';
        $data['page_title'] = 'Archives';
        $data['page_subtitle'] = 'Archives Detail List';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'archives';
        $data['data']['submenu'] = '';
        $data['data']['event'] = $this->Mevent2->getEventById($id);
        $data['data']['receipt_perm'] = $this->mcore->checkPermission($this->user_group, $this->receipt_perm);
        $data['data']['certificate_perm'] = $this->mcore->checkPermission($this->user_group, $this->certificate_perm);
        $this->load->view('layout/body',$data);

    }

    public function dataTableDetail() {

        $this->load->library('Datatable', array('model' => $this->dttModelDetail, 'rowIdCol' => 'a.'.$this->pkDetail));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    function config($id){
        
        if(count($_POST)>0){

            if(isset($_POST['old_logo'])){ //handle update receipt

                /* upload logo */
                if($_FILES["logo"]["name"]!=""){

                    if($_FILES["logo"]["type"]=="image/png" or
                        $_FILES["logo"]["type"]=="image/jpg" or
                        $_FILES["logo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/archives/';
                        $array = explode('.', $_FILES['logo']['name']);
                        $extension = end($array);
                        $logo = md5(uniqid(rand(), true)).".".$extension;

                        $old_logo = $upload_path.'/'.@$_POST['old_logo'];

                        if(file_exists($old_logo)){

                            @unlink($old_logo);

                        }

                        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $upload_path."/".$logo)) {

                        }else{

                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $logo = $this->input->post("old_logo");

                }

                $data = array(

                    "event_receipt_format" => $this->input->post("receipt_format"),
                    "event_receipt_logo" => $logo

                );

                $this->db->where($this->pk, $id);
                $update = $this->db->update($this->table, $data);

                if($update){
                    $res = array("status" => "1", "msg" => "Successfully update data!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }

                echo json_encode($res);

            }else{ //handle update certificate

                /* upload background */
                if($_FILES["background"]["name"]!=""){

                    if($_FILES["background"]["type"]=="image/png" or
                        $_FILES["background"]["type"]=="image/jpg" or
                        $_FILES["background"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/archives/';
                        $array = explode('.', $_FILES['background']['name']);
                        $extension = end($array);
                        $background = md5(uniqid(rand(), true)).".".$extension;

                        $old_background = $upload_path.'/'.@$_POST['old_background'];

                        if(file_exists($old_background)){

                            @unlink($old_background);

                        }

                        if (move_uploaded_file($_FILES["background"]["tmp_name"], $upload_path."/".$background)) {

                        }else{

                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $background = $this->input->post("old_background");

                }

                $data = array(

                    "event_certificate_background" => $background

                );

                $this->db->where($this->pk, $id);
                $update = $this->db->update($this->table, $data);

                if($update){
                    $res = array("status" => "1", "msg" => "Successfully update data!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }

                echo json_encode($res);

            }
            

        }else{

            $breadcrumb = array(
                "<i class='fa fa-book'></i> Archives " => base_url()."archives",
                "Archives Config" => base_url()."archives/archives/config/".$id,
            );

            $_SESSION['archives_config_event_id'] = $id;

            $data['page'] = 'archives_config';
            $data['page_title'] = 'Archives';
            $data['page_subtitle'] = 'Archives Config';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'archives';
            $data['data']['submenu'] = '';
            $data['data']['dataedit'] = $this->Marchives->getArchivesConfig($id);
            $data['data']['receipt_config_perm'] = $this->mcore->checkPermission($this->user_group, $this->receipt_config_perm);
            $data['data']['certificate_config_perm'] = $this->mcore->checkPermission($this->user_group, $this->certificate_config_perm);
            
            $this->load->view('layout/body',$data);

        }

    }

    function generate_certificate($id_event_member){
        
        $this->load->model("Marchives");

        $certificate_data = $this->Marchives->getCertificateData($id_event_member);
        $c_data = @$certificate_data[0];

        $sdate = date_parse($c_data['start_date']);
        $edate = date_parse($c_data['end_date']);

        if(($sdate['month']==$edate['month'])&&($sdate['year']==$edate['year'])){
            $month = $this->convertMonth($edate['month']);
            $certDate = $sdate['day'].' - '.$edate['day'].' '.$month.' '.$edate['year'];
        }elseif(($sdate['month']!=$edate['month'])&&($sdate['year']==$edate['year'])){
            $smonth = $this->convertMonth($sdate['month']);
            $emonth = $this->convertMonth($edate['month']);
            $certDate = $sdate['day'].' '.$smonth.' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
        }else{
            $smonth = $this->convertMonth($sdate['month']);
            $emonth = $this->convertMonth($edate['month']);
            $certDate = $sdate['day'].' '.$smonth.' '.$sdate['year'].' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
        }

        $html = '<table border="0" cellpadding="7" cellspacing="0" width="100%" style="">
                  <tr>
                      <td><br/><br/><br/><br/><br/><br/><br/><br/> <br/><br/>   </td>
                  </tr>
                    <tr>
                          <td align="center">Diberikan kepada :</td>
                    </tr>
                    <tr> 
                        <td align="center">
                        
                           <b id="undline"><h2>'.$c_data['name'].'</h2></b> 
                           '.$c_data['corporate'].' '.$c_data['bpr'].'
                        </td> 
                    </tr> 
                    <tr>
                        <td><br/>  </td>
                    </tr>
                      <tr>
                      <td align="center">Atas Partisipasi dalam :</td>
                    </tr>
                     
                </table>

                <table border="0" cellpadding="7" cellspacing="0" width="100%"> 
                  
                    <tr> 
                        <td align="left" style="background:url('.base_url().'upload/archives/garis.png) no-repeat;background-size: 84%;" > 
                              <b><h2 class="text-white">&nbsp;&nbsp;&nbsp;'.$c_data['event_name'].'</h2></b> 
                        </td> 
                    </tr>
                    <tr>
                       <td align="left"><b>&nbsp;&nbsp;&nbsp;"'.$c_data['event_theme'].'"</b></td> 
                    </tr>  
                </table>

                <table border="0" cellpadding="7" cellspacing="0" width="100%"  >
                    <tr align="center">
                          <td align="center" colspan="4">'.$c_data['city'].', '.$certDate.'</td>
                    </tr>
                      <tr>
                      <td><br/></td>
                    </tr>
                    <tr align="center">
                        <td align="center" width="20%" colspan="2"> DPP PERBARINDO</td> 
                        <td align="center" width="20%" colspan="2"> PANITIA SEMINAR NASIONAL DAN <br> RAKERNAS PERBARINO</td>
                    </tr>

                    <tr align="center">
                         <td align="center" width="20%">
                             <br/>  <br/>  
                         </td>
                         <td align="center" width="20%">
                             <br/>  <br/>  
                         </td>
                          <td align="center" width="20%">
                             <br/>  <br/> 
                         </td>
                         <td align="center" width="20%">
                             <br/>  <br/>   
                         </td>
                    </tr>
                   
                    <tr align="center"> 
                         <td align="right" width="20%">
                            <b id="undline" >Joko Suyanto</b><br>
                            Ketua Umum<br/> <br/> 
                           </td>
                         <td align="center" width="20%">
                            <b id="undline">Riwandari Juniasti</b><br>
                            Sekertaris Jendral<br/> <br/> 
                         </td>
                         <td align="center" width="20%">
                            <b id="undline">Yenny Rustan</b><br>
                            Ketua SC<br/> <br/>
                         </td>
                        <td align="left" width="20%">
                            <b id="undline">Meidi Widiyanto</b><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Ketua OC<br/> <br/> 
                        </td>
                    </tr>
                    <tr>
                      <td></td>
                    </tr>
                </table>';                

        $data['certificate'] = $html;
        $this->load->view('certificate',$data);

    }

    function generate_receipt($id_event_member=12){

        $this->load->model("Marchives");

        $receipt_data = $this->Marchives->getReceiptData($id_event_member); //print_r($receipt_data);exit;
        $r_data = @$receipt_data[0];

        $sdate = date_parse($r_data['start_date']);
        $edate = date_parse($r_data['end_date']);

        if(($sdate['month']==$edate['month'])&&($sdate['year']==$edate['year'])){
            $month = $this->convertMonth($edate['month']);
            $recpDate = $sdate['day'].' - '.$edate['day'].' '.$month.' '.$edate['year'];
        }elseif(($sdate['month']!=$edate['month'])&&($sdate['year']==$edate['year'])){
            $smonth = $this->convertMonth($sdate['month']);
            $emonth = $this->convertMonth($edate['month']);
            $recpDate = $sdate['day'].' '.$smonth.' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
        }else{
            $smonth = $this->convertMonth($sdate['month']);
            $emonth = $this->convertMonth($edate['month']);
            $recpDate = $sdate['day'].' '.$smonth.' '.$sdate['year'].' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
        }

        $costNumber = number_format($r_data['total_cost'] , 0, ',', '.');
        $costLetter = $this->terbilang($r_data['total_cost']);

        $numberReceipt = $this->Marchives->getNumberReceipt($id_event_member) + 1;

        $html = '<table  border="0" cellpadding="7" cellspacing="0" width="100%" id="nopadnomar">
                    <tr id="nopadnomar">
                        <td align="left">
                            <img src="'.base_url().'/upload/archives/perbarindo-logo.png" class="img-responsive" style="width:auto;height: 75%;"><br/> 
                        </td>
                        <td><button id="btn-witansi">KWINTASI</button></td>
                        <td align="right">
                            <img src="'.base_url().'/upload/archives/event-rakernas-2016-logo.png" class="img-responsive" style="width:auto;height: 10%;"><br/> 
                        </td>
                    </tr>
                </table>

                <table border="0" cellpadding="7" cellspacing="0" width="100%">
                    <tr>
                        <th align="left" width="20%">Nomor</th>
                        <th align="left">:</th>
                        <td align="left"><b>326/SN/Perbarindo/X/2017</b> <hr/></td>
                    </tr>
                    <tr>
                        <th align="left">Sudah terima dari</th>
                        <th align="left">:</th>
                        <td align="left">'.$r_data['corporate'].' '.$r_data['bpr'].' / '.$r_data['name'].' <hr/></td>
                    </tr>
                    <tr>
                        <th align="left">Uang sejumlah</th>
                        <th align="left">:</th>
                        <td align="left" id="box-border" class="bordered-dashed"> <b>'.$costLetter.' Rupiah</b> </td>
                    </tr>
                    <tr>
                        <th align="left" valign="top">Untuk pembayaran</th>
                        <th align="left" valign="top">:</th>
                        <td align="left">Partisipasi peserta '.$r_data['event_name'].' <br/> pada tanggal '.$recpDate.' di '.$r_data['address'].' <hr/></td>
                    </tr>
                </table>

                <table border="0" cellpadding="7" cellspacing="0" width="100%">
                    <tr align="center">
                        <td align="left" width="20%">Rp.&nbsp;<span id="box-border">&nbsp; '.$costNumber.'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        <td align="center"></td>
                        <td align="left" width="20%">
                            Jakarta,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$this->convertMonth(date('m')).' '.date('Y').'<br>
                            <b>PERBARINDO</b><br/> <br/> 
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"><br/><br/>  </td>
                        <td width="30%"><br/> <br/> </td>
                        <td align="left" width="20%" align="center"> <br/><br/>  </td>
                    </tr>
                    <tr align="center">
                        <td align="left" width="20%">www.perbarindo.or.id<br> <small>Cetakan ke-'.($r_data['document_out']+1).'</small></td>
                        <td align="left" width="30%"></td>
                        <td align="left" width="20%">
                            <b id="undline">Tulus Rahardjo</b><br>
                            Bendahara Umum<br/> <br/> 
                        </td>
                    </tr>

                </table>';

        $data['receipt'] = $html;
        $this->load->view('receipt',$data);

    }

    function convertMonth($month_number){

        switch ($month_number) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            default:
                return "Desember";
                break;
        }
    }  

    function terbilang($bilangan) {

      $angka = array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
      $kata = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
      $tingkat = array('','Ribu','Juta','Milyar','Triliun');

      $panjang_bilangan = strlen($bilangan);

      /* pengujian panjang bilangan */
      if ($panjang_bilangan > 15) {
        // $kalimat = "Diluar Batas";
        $kalimat = $bilangan;
        return $kalimat;
      }

      /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
      for ($i = 1; $i <= $panjang_bilangan; $i++) {
        $angka[$i] = substr($bilangan,-($i),1);
      }

      $i = 1;
      $j = 0;
      $kalimat = "";

      /* mulai proses iterasi terhadap array angka */
      while ($i <= $panjang_bilangan) {

        $subkalimat = "";
        $kata1 = "";
        $kata2 = "";
        $kata3 = "";

        /* untuk ratusan */
        if ($angka[$i+2] != "0") {
          if ($angka[$i+2] == "1") {
            $kata1 = "seratus";
          } else {
            $kata1 = $kata[$angka[$i+2]] . " Ratus";
          }
        }

        /* untuk puluhan atau belasan */
        if ($angka[$i+1] != "0") {
          if ($angka[$i+1] == "1") {
            if ($angka[$i] == "0") {
              $kata2 = "Sepuluh";
            } elseif ($angka[$i] == "1") {
              $kata2 = "Sebelas";
            } else {
              $kata2 = $kata[$angka[$i]] . " Belas";
            }
          } else {
            $kata2 = $kata[$angka[$i+1]] . " Puluh";
          }
        }

        /* untuk satuan */
        if ($angka[$i] != "0") {
          if ($angka[$i+1] != "1") {
            $kata3 = $kata[$angka[$i]];
          }
        }

        /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
        if (($angka[$i] != "0") OR ($angka[$i+1] != "0") OR
            ($angka[$i+2] != "0")) {
          $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
        }

        /* gabungkan variabe sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
        $kalimat = $subkalimat . $kalimat;
        $i = $i + 3;
        $j = $j + 1;

      }

      /* mengganti satu ribu jadi seribu jika diperlukan */
      if (($angka[5] == "0") AND ($angka[6] == "0")) {
        $kalimat = str_replace("Satu Ribu","Seribu",$kalimat);
      }

      return trim($kalimat);

    }

    function generate_ticket_event($regcode){

        $event_registration = $this->Marchives->getEventRegistration($regcode);
        $event_member = $this->Marchives->getEventMember($event_registration['reg_id'],'0');
        $event_member_twin_sharing = $this->Marchives->getEventMember($event_registration['reg_id'],'1');

        $html = "";

        $i=1;
        foreach($event_member as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Event</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $html.="<div class='page-break'></div>";

        $i=1;
        foreach($event_member_twin_sharing as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];
            $reservation_code2 = $event_registration['regcode'].'-'.$em['twin_sharing_with'];

            $sharing_with = $this->mevent->getEventMemberById($event_registration['reg_id'], $em['twin_sharing_with'], '2');


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);
            $qrcode2 = $this->generate_qr_code($reservation_code2, $reservation_code2);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$sharing_with['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$em['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$sharing_with['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code2.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode2.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }
        
        $data['tickets'] = $html;
        $this->load->view('ticket',$data);
        // return $top.$html.$below;
        // return $top.$html.$below;
        // return $html;


    }

    function ssh2($command){

        $connection = ssh2_connect('10.50.11.50', 22);
        ssh2_auth_password($connection, 'root', '4k535TranyaR');

        $stream1= ssh2_exec($connection, $command);

        stream_set_blocking($stream1, true);

        $output = stream_get_contents($stream1);

    }

    function generate_qr_code($text,$filename){

        $params['data'] = $text;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $_SERVER['DOCUMENT_ROOT'].'/upload/qrcode/'.$filename;
        $this->ciqrcode->generate($params);

        $command = "chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/upload/qrcode';

        $this->ssh2($command);

        return '<img src="'.base_url().'upload/qrcode/'.$filename.'" class="img-responsive" style="width: auto;height: 70px;">';

    }

    function generate_receipt_all_dpd($id_event=12){

        $this->load->model("Marchives");

        $approvedMemberList = $this->Marchives->getApprovedPaymentMember($id_event);

        $html = '';
        $i = 0;

        foreach ($approvedMemberList as $aM) {
            $i++;
            $receipt_data = $this->Marchives->getReceiptData($aM['id']); //print_r($receipt_data);exit;
            $r_data = @$receipt_data[0];

            $sdate = date_parse($r_data['start_date']);
            $edate = date_parse($r_data['end_date']);

            if(($sdate['month']==$edate['month'])&&($sdate['year']==$edate['year'])){
                $month = $this->convertMonth($edate['month']);
                $recpDate = $sdate['day'].' - '.$edate['day'].' '.$month.' '.$edate['year'];
            }elseif(($sdate['month']!=$edate['month'])&&($sdate['year']==$edate['year'])){
                $smonth = $this->convertMonth($sdate['month']);
                $emonth = $this->convertMonth($edate['month']);
                $recpDate = $sdate['day'].' '.$smonth.' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
            }else{
                $smonth = $this->convertMonth($sdate['month']);
                $emonth = $this->convertMonth($edate['month']);
                $recpDate = $sdate['day'].' '.$smonth.' '.$sdate['year'].' - '.$edate['day'].' '.$emonth.' '.$edate['year'];
            }

            $costNumber = number_format($r_data['total_cost'] , 0, ',', '.');
            $costLetter = $this->terbilang($r_data['total_cost']);

            $numberReceipt = $this->Marchives->getNumberReceipt($aM['id']) + 1;

            $html .= '
                    <br/>
                    <br/>
                    <br/>
                <fieldset id="wrap-card">
                    <fieldset id="border-wrap-rable">
                    <table  border="0" cellpadding="7" cellspacing="0" width="100%" id="nopadnomar">
                        <tr id="nopadnomar">
                            <td align="left">
                                <img src="'.base_url().'/upload/archives/perbarindo-logo.png" class="img-responsive" style="width:auto;height: 75%;"><br/> 
                            </td>
                            <td><button id="btn-witansi">KWINTASI</button></td>
                            <td align="right">
                                <img src="'.base_url().'/upload/archives/f4b0b2abbb3792f3d83a572a16f56d32.png" class="img-responsive" style="width:auto;height: 10%;"><br/> 
                            </td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="7" cellspacing="0" width="100%">
                        <tr>
                            <th align="left" width="20%">Nomor</th>
                            <th align="left">:</th>
                            <td align="left"><b>'.$i.'/SN/Perbarindo/X/2017</b> <hr/></td>
                        </tr>
                        <tr>
                            <th align="left">Sudah terima dari</th>
                            <th align="left">:</th>
                            <td align="left">'.$r_data['corporate'].' '.$r_data['bpr'].' / '.$r_data['name'].' <hr/></td>
                        </tr>
                        <tr>
                            <th align="left">Uang sejumlah</th>
                            <th align="left">:</th>
                            <td align="left" id="box-border" class="bordered-dashed"> <b>'.$costLetter.' Rupiah</b> </td>
                        </tr>
                        <tr>
                            <th align="left" valign="top">Untuk pembayaran</th>
                            <th align="left" valign="top">:</th>
                            <td align="left">Partisipasi peserta '.$r_data['event_name'].' <br/> pada tanggal '.$recpDate.' di '.$r_data['address'].' <hr/></td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="7" cellspacing="0" width="100%">
                        <tr align="center">
                            <td align="left" width="20%">Rp.&nbsp;<span id="box-border">&nbsp; '.$costNumber.'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                            <td align="center"></td>
                            <td align="left" width="20%">
                                Jakarta,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$this->convertMonth(date('m')).' '.date('Y').'<br>
                                <b>PERBARINDO</b><br/> <br/> 
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"><br/><br/>  </td>
                            <td width="30%"><br/> <br/> </td>
                            <td align="left" width="20%" align="center"> <br/><br/>  </td>
                        </tr>
                        <tr align="center">
                            <td align="left" width="20%">www.perbarindo.or.id<br> <small>Cetakan ke-'.($r_data['document_out']+1).'</small></td>
                            <td align="left" width="30%"></td>
                            <td align="left" width="20%">
                                <b id="undline">Tulus Rahardjo</b><br>
                                Bendahara Umum<br/> <br/> 
                            </td>
                        </tr>

                    </table>

                    </fieldset>
                </fieldset>
                    

                    ';

        }        


        $top = '
            <!DOCTYPE html>
            <html>
                <head>  
                <style>

                  body {
                    background: white; 
                  }
                  page {
                    background: white;
                    display: block;
                    margin: 0 auto;
                    margin-bottom: 0.5cm;
                    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                  }
                  page[size="A4"] {  
                    width: 21cm;
                    height: 29.7cm; 
                  }
                  page[size="A4"][layout="portrait"] {
                    width: 29.7cm;
                    height: 21cm;  
                  }
                  page[size="A3"] {
                    width: 29.7cm;
                    height: 42cm;
                  }
                  page[size="A3"][layout="portrait"] {
                    width: 42cm;
                    height: 29.7cm;  
                  }
                  page[size="A5"] {
                    width: 14.8cm;
                    height: 21cm;
                  }
                  page[size="A5"][layout="portrait"] {
                    width: 21cm;
                    height: 28.8cm;
                  }
                  @media print {
                    body, page {
                      margin: 0;
                      box-shadow: 0;
                    }
                  }
                  
                  /*** STYLE COSTUME SETIFIKAT PRINT ***/

                   body {
                      -webkit-print-color-adjust: exact; 
                    }

                  table {
                      font-size: 12pt;
                      font-family: "Times New Roman", Times, serif;
                      color:#9c9b9b;
                  }
                  
                  .table2 tr>td, table tr>td {
                      border: 0px solid #DDD;
                  }
                  
                  table tr>td>table {
                      border: 0px solid #9e9e9e;
                      padding: 0px;
                  }
                  
                  table tr>td>table tr>th {  
                      border: 0px solid #fde0bc; 
                  }
                  
                  table tr>td>table tr>td {
                      border: 0px solid #DDD;
                  }
                  
                  table tr>td>table tr>th>b {
                      font-weight: 600;
                      color: #757575; 
                  }
                  
                  table tr>td>table tr>td>span {
                      color: #8a8585; 
                  }
                  
                  .bordered-dashed {
                      border-style: dashed;
                      border-color: #c5c5c5;
                      border-width: 1.5px;
                      margin-top: 10px;
                      margin-bottom: 10px; 
                  }
                  
                  th.rotate {
                      white-space: nowrap;
                      padding-top: 165px;
                      width: 12px; 
                  }
                  
                  th.rotate>div {
                      transform: translate(1px, 5px) rotate(630deg);
                      width: 12px; 
                  }
                  
                  th.rotate>div>span {
                      border-bottom: 1px solid #ccc;
                      width: 12px; 
                  } 

                  /** End Print **/ 

                  #border-wrap-rable {
                    border-left: 3px solid #ffa500;
                    border-bottom: 3px solid #f26127;

                    border-right: 0px solid #FFF;
                    border-top: 0px solid #FFF;
                    margin:0px;
                  }
                  #wrap-card {
                     padding-top:0px;
                     padding-bottom:4px;
                     padding-left:4px;
                     padding-right:0px; 
                  }
                  #btn-witansi {
                    font-weight:600;
                    padding:12px;
                    background-color: #ffd17e;
                    border:0px; 
                  }
                  #undline {
                       text-decoration: underline;
                  }

                  #box-border {
                    border:1px solid orange;
                    border-radius:0px 8px 2px;
                    background-color:#EEE;
                    -webkit-border-radius: 0px 8px 2px;
                    padding:4px; 
                  }
                  #nopadnomar {
                    margin:0px;padding:0px;
                  }

                  .bordered-dashed {
                    border-style: dashed !important;
                    border-color: orange !important;
                    border-width: 1.5px;
                    margin-top: 10px;
                    margin-bottom: 10px;
                }

                </style>
            </head>

            <body>
             <page size="A5" layout="portrait">

            
                
        ';

        $below = '
            

            </page>

            </body>
            </html>
        ';

        // $data['receipt'] = $html;
        // $this->load->view('receipt',$data);
        // return $top.$html.$below;
        echo $top.$html.$below;
        // return $html;

    }

}