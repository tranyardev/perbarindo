<?php

class Mbprproduct extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRProduct($bpr, $category, $offset, $limit){

        $where = "";

        if($category!==null && $category!="" && $category!="-"){

            $where .= " AND category='".$category."'";

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }

        $result = $this->db->query("SELECT a.*, b.name as product_category FROM bpr_products a 
                                    LEFT JOIN bpr_product_categories b ON b.id=a.category
                                    WHERE a.bpr='".$bpr."'".$where.$page)->result_array();
        return $result;

    }

    function searchProduct($bpr, $keyword, $category, $offset, $limit){

        $where = "";

        if($category!==null && $category!="" && $category!="-"){

            $where .= " AND a.category='".$category."'";

        }

        if($keyword!==null && $keyword!=""){

            $where .= " AND a.name ilike '%".$keyword."%'";

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }

        $result = $this->db->query("SELECT a.*, b.name as product_category FROM bpr_products a 
                                    LEFT JOIN bpr_product_categories b ON b.id=a.category
                                    WHERE a.bpr='".$bpr."'".$where.$page)->result_array();
        return $result;

    }

    function getProductBySlug($bpr,$slug){

        $result = $this->db->query("SELECT a.*, f.name as corp, e.name as product_category, b.name as bpr_name, c.name as dpd_name, d.name as author FROM bpr_products a 
                                    LEFT JOIN bpr b ON b.id=a.bpr
                                    LEFT JOIN dpd c ON c.id=b.dpd
                                    LEFT JOIN members d ON d.aauth_user_id=a.created_by 
                                    LEFT JOIN bpr_product_categories e ON e.id=a.category
                                    LEFT JOIN corporates f ON f.id=b.corporate
                                    WHERE a.permalink='".$slug."' AND a.status='1' AND a.bpr='".$bpr."'
                                    ORDER BY a.created_at DESC")->result_array();
        return @$result[0];

    }
    
 
}