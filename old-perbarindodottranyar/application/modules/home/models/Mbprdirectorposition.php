<?php

class Mbprdirectorposition extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRDirectorPosition(){

        $result = $this->db->query("SELECT * FROM director_positions")->result_array();
        return $result;

    }
    function getBPRDirectorPositionById($id){

        $result = $this->db->query("SELECT * FROM director_positions WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRDirectorPositionByBPR($bpr,$parent){

        if($parent!=""){

            $query = "SELECT * FROM director_positions WHERE bpr='".$bpr."' and parent is null";

        }else{

            $query = "SELECT * FROM director_positions WHERE bpr='".$bpr."'";

        }

        $result = $this->db->query($query)->result_array();
        return $result;

    }
    function getBPRChildDirectorPosition($bpr, $parent){

        $result = $this->db->query("SELECT * FROM director_positions WHERE bpr='".$bpr."' and parent='".$parent."'")->result_array();
        return $result;

    }
    
 
}