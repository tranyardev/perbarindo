<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class Mevent extends CI_Model
{

    function getUpComingEvent(){

        $result = $this->db->query("SELECT * FROM events WHERE start_date > current_date AND status = '1' ORDER BY start_date ASC LIMIT 4")->result_array();
        
        if(count($result) < 1){
             
             $result = $this->db->query("SELECT * FROM events WHERE status = '1' ORDER BY start_date DESC LIMIT 4")->result_array();

        }

        return $result;

    }

    function getLatestEvent($limit){

        $result = $this->db->query("SELECT * FROM events WHERE start_date > current_date AND status = '1' ORDER BY start_date ASC limit $limit")->result_array();

        if(count($result) < 1){
             
             $result = $this->db->query("SELECT * FROM events WHERE status = '1' ORDER BY start_date DESC limit $limit")->result_array();

        }

        return $result;

    }

    function findEvent(){
        
        
        
    }

    function getEventByRandom($id, $limit){

        $result = $this->db->query("SELECT * FROM events WHERE start_date > current_date AND id <> $id AND status = '1' ORDER BY random() LIMIT $limit")->result_array();

        if(count($result) < 1){
             
             $result = $this->db->query("SELECT * FROM events WHERE id <> $id AND status = '1' ORDER BY random() LIMIT $limit")->result_array();

        }

        return $result;

    }


    function getDPD(){

        $result = $this->db->query("SELECT * FROM dpd")->result_array();
        return $result;

    }

    function getBPR(){

        $result = $this->db->query("SELECT * FROM bpr")->result_array();
        return $result;

    }

    function getBPRbyDPD($id){

        if($id!="" && $id!=null){
            $result = $this->db->query("SELECT a.*,b.name as bpr_corporate FROM bpr a 
                LEFT JOIN corporates b ON b.id=a.corporate
                WHERE dpd = $id")->result_array();
        }else{
            $result = array();
        }
        
        return $result;

    }

    function getBPRbyId($id){

        $result = $this->db->query("SELECT * FROM bpr WHERE id = $id")->result_array();
        return @$result[0];

    }


    function getMemberByBPR($id){

        $result = $this->db->query("SELECT * FROM members WHERE bpr = $id")->result_array();
        return $result;

    }

    function getMemberById($id){

        $result = $this->db->query("SELECT * FROM members WHERE id = $id")->result_array();
        return $result;

    }

    function getJobPosition(){

        $result = $this->db->query("SELECT * FROM job_positions")->result_array();
        return $result;

    }

    function getEventById($id){

        $result = $this->db->query("SELECT a.*,b.* FROM events a 
            LEFT JOIN aauth_users b ON b.id = a.created_by
            WHERE a.id='$id'")->result_array();
        return @$result[0];

    }

    function getEventActivities($id, $date){

        $result = $this->db->query("SELECT * FROM event_activities 
            WHERE event_id='$id' AND date='$date'")->result_array();
        return $result;

    }
    function getEventPackage($id){

        $result = $this->db->query("SELECT * FROM packages 
            WHERE event_id='$id'")->result_array();
        return $result;

    }
    function getEventActivityDates($id){

        $result = $this->db->query("SELECT date FROM event_activities 
            WHERE event_id='$id' GROUP BY date")->result_array();
        return $result;

    }

    function getEventRegistration($regcode){

        $result = $this->db->query("SELECT a.*,b.*,c.*, 
            c.name as member_name, 
            c.email as member_email, 
            f.name as member_job_position,
            e.name as dpd_name, 
            b.name as event_name, 
            d.name as bpr_name,
            a.status as trx_status, 
            a.id as reg_id 
            FROM event_registrations a
            LEFT JOIN events b ON b.id = a.event_id
            LEFT JOIN members c ON c.id = a.member_id
            LEFT JOIN job_positions f ON f.id = c.job_position
            LEFT JOIN bpr d ON d.id = a.bpr
            LEFT JOIN dpd e ON e.id = d.dpd
            WHERE regcode='$regcode'")->result_array();

        return @$result[0];

    }

    function getAllEventRegistration($status){

        $where = "";

        if($status!="ALL"){

            $where = " AND a.status = '$status'";

        }

        $user = $this->session->userdata('id');

        $result = $this->db->query("SELECT a.*,b.*,c.*, 
            c.name as member_name, 
            c.email as member_email, 
            f.name as member_job_position,
            e.name as dpd_name, 
            b.name as event_name, 
            b.id as event_id,
            d.name as bpr_name, 
            a.status as trx_status,
            a.id as reg_id 
            FROM event_registrations a
            LEFT JOIN events b ON b.id = a.event_id
            LEFT JOIN members c ON c.id = a.member_id
            LEFT JOIN job_positions f ON f.id = c.job_position
            LEFT JOIN bpr d ON d.id = a.bpr
            LEFT JOIN dpd e ON e.id = d.dpd
            WHERE a.aauth_user_id = '$user' $where
            ")->result_array();

        return $result;

    }

    function getEventRegistrationByRegCode($regcode){

        $user = $this->session->userdata('id');

        $result = $this->db->query("SELECT a.*,b.*,c.*, 
            c.name as member_name, 
            c.email as member_email, 
            f.name as member_job_position,
            e.name as dpd_name, 
            b.name as event_name, 
            b.id as event_id,
            d.name as bpr_name, 
            a.status as trx_status,
            a.id as reg_id 
            FROM event_registrations a
            LEFT JOIN events b ON b.id = a.event_id
            LEFT JOIN members c ON c.id = a.member_id
            LEFT JOIN job_positions f ON f.id = c.job_position
            LEFT JOIN bpr d ON d.id = a.bpr
            LEFT JOIN dpd e ON e.id = d.dpd
            WHERE a.aauth_user_id = '$user' AND a.regcode = '$regcode'
            ")->result_array();

        return $result;

    }

    function getEventMember($reg_id, $type){

        $result = $this->db->query("SELECT a.*,b.*,c.*,b.name as package_name, c.name as member_name  FROM event_members a
            LEFT JOIN packages b ON b.id = a.package_id
            LEFT JOIN members c ON c.id = a.member_id
            WHERE a.registration_id='$reg_id' AND a.is_twin_sharing='$type'")->result_array();

        return $result;

    }

    function getEventMemberById($reg_id, $id, $type){

        if($type == "1"){

            $column = "a.member_id";

        }else{

            $column = "a.twin_sharing_with";

        }

        $result = $this->db->query("SELECT a.*,b.*,c.*,b.name as package_name, c.name as member_name  FROM event_members a
            LEFT JOIN packages b ON b.id = a.package_id
            LEFT JOIN members c ON c.id = ".$column."
            WHERE a.registration_id='$reg_id' AND ".$column." ='$id'")->result_array();

        return @$result[0];

    }

    function getTransferBankAccount(){

        $result = $this->db->query("SELECT * FROM bank_transfer_accounts WHERE status = '1'")->result_array();
        return $result;

    }

    function getConfigurationItem($scope, $key){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope = '$scope' AND key = '$key'")->result_array();
        return @$result[0]['value'];

    }

    function checkPaymentConfirmation($regcode){

        $result = $this->db->query("SELECT * FROM payment_confirmations WHERE regcode='$regcode'")->num_rows();
        return $result;

    }

    function getCurrentUser($id){

        $result = $this->db->query("SELECT * FROM aauth_users WHERE id='$id'")->result_array();
        return @$result[0];

    }
    function getMemberByUserId($id){

        $result = $this->db->query("SELECT a.*,b.dpd, a.name as member_name,a.email as member_email, b.name as bpr_name, c.name as dpd_name, a.id as member_id, d.name as position, e.name as bpr_corporate FROM members a 
            LEFT JOIN bpr b ON b.id=a.bpr
            LEFT JOIN dpd c ON c.id=b.dpd
            LEFT JOIN job_positions d ON d.id=a.job_position
            LEFT JOIN corporates e ON e.id=b.corporate
            WHERE aauth_user_id='$id'")->result_array();
        return @$result[0];

    }

    function getCustomPage($permalink){

        $result = $this->db->query("SELECT * FROM pages WHERE slug='$permalink'")->result_array();

        return @$result[0];

    }

    function getFAQ(){

        $result = $this->db->query("SELECT * FROM faq ORDER BY sequence")->result_array();

        return $result;

    }

    function getConfigurationByKey($scope, $key){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope='".$scope."' and key='".$key."'")->result_array();
        return @$result[0];

    }

}