<?php

class Mbprarticlecategory extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRArticleCategory($id){

        $result = $this->db->query("SELECT * FROM bpr_article_categories WHERE bpr = '".$id."'")->result_array();
        return $result;

    }
    function getBPRArticleCategoryById($id){

        $result = $this->db->query("SELECT * FROM bpr_article_categories WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRArticleCategoryByParent($bpr, $parent){

        $result = $this->db->query("SELECT * FROM bpr_article_categories WHERE bpr='".$bpr."' AND parent = '".$parent."'")->result_array();
        return $result;

    }
    
 
}