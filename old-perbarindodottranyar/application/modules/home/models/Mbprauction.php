<?php

class Mbprauction extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRAuction($bpr, $category, $offset, $limit){

        $where = "";

        if($category!==null && $category!="" && $category!="-"){

            $where .= " AND auction_category='".$category."'";

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }

        $result = $this->db->query("SELECT a.*, b.name as auction_category_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    WHERE a.bpr='".$bpr."'".$where.$page)->result_array();
        return $result;

    }

    function getAuctionByAssetCode($asset_code){

        $result = $this->db->query("SELECT a.*, b.name as auction_category_name, c.name as bpr_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    LEFT JOIN bpr c ON c.id=a.bpr
                                    WHERE a.asset_code='".$asset_code."'")->result_array();
        return @$result[0];

    }

    function getCountBPRAuction($bpr){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE bpr='".$bpr."'")->num_rows();
        return $result;

    }
    function getBPRAuctionById($id){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}