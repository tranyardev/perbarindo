<?php

class Mbprmutation extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRMutation(){

        $result = $this->db->query("SELECT * FROM bpr_products")->result_array();
        return $result;

    }
    function getBPRMutationById($id){

        $result = $this->db->query("SELECT * FROM bpr_products WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}