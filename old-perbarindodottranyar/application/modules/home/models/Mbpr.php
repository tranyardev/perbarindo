<?php

class Mbpr extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getBPRById($id){

        $result = $this->db->query("SELECT a.*, b.name as corp, c.name as dpd_name FROM bpr a 
                                    LEFT JOIN corporates b ON b.id = a.corporate
                                    LEFT JOIN dpd c ON c.id = a.dpd 
                                    WHERE a.id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPR(){

        $result = $this->db->query("SELECT a.*, b.name as corp, c.name as dpd_name FROM bpr a 
                                    LEFT JOIN corporates b ON b.id = a.corporate
                                    LEFT JOIN dpd c ON c.id = a.dpd")->result_array();
        return $result;

    }

    function searchBPR($keyword, $dpd, $offset, $limit){

        if($dpd != "" && $dpd != "-"){

            if($keyword != "-"){

                $where = " WHERE a.dpd='".$dpd."' AND a.name ilike '%".$keyword."%'";

            }else{

                $where = " WHERE a.dpd='".$dpd."'";

            }
            

        }else{

            if($keyword != "-"){

                $where = " WHERE a.name ilike '%".$keyword."%'";

            }else{

                $where = "";

            }

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }

        $sql = "SELECT a.*, b.name as corp, c.name as dpd_name FROM bpr a 
                LEFT JOIN corporates b ON b.id = a.corporate
                LEFT JOIN dpd c ON c.id = a.dpd".$where.$page;

        $result = $this->db->query($sql)->result_array();
        return $result;

    }

    function getListBPR($offset, $limit){

        $result = $this->db->query("SELECT a.*, b.name as corp, c.name as dpd_name FROM bpr a 
                                    LEFT JOIN corporates b ON b.id = a.corporate
                                    LEFT JOIN dpd c ON c.id = a.dpd
                                    LIMIT ".$limit." OFFSET ".$offset)->result_array();
        return $result;

    }

    function getBPRByDPDId($id){

        $result = $this->db->query("SELECT * FROM bpr WHERE dpd='".$id."'")->result_array();
        return $result;

    }
    function getBPRByName($bprName){

        $result = $this->db->query("SELECT * FROM bpr WHERE name='".$bprName."'")->result_array();
        return $result;

    }
 
}