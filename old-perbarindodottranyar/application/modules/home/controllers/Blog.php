<?php

class Blog extends MX_Controller{


	var $preloader = "";

	function __construct(){

		parent::__construct();
        $this->load->library("Aauth");
        $this->load->helper('download');
        $this->load->model("Mbpr");
        $this->load->model("Mdpd");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprfinanceasset");
        $this->load->model("Mbprdirectorposition");
        $this->load->model("Mbprdirector");
        $this->load->model("MCorporate");
        $this->load->model("Mbprproductcategory");
        $this->load->model("Mbprproduct");
        $this->load->model("Mbprarticlecategory");
        $this->load->model("Mbprarticle");
        $this->load->model("Mbprfinancepublication");

        $this->preloader = base_url()."assets/dist/img/preloader.gif";
 

	}


	public function index($ajax = false, $page = 0, $category = null){


		$data['page'] = 'blog';
        $data['og_title'] = 'PERBARINDO | BLOG';
        $data['og_site_name'] = 'PERBARINDO BPR LIST';
        $data['og_site_url'] = base_url();
        $data['og_site_description'] = 'Daftar BPR PERBARINDO';
        $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';
   
       	$data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';

	
		$alldata = $this->Mbprarticle->getArticle(null, null);

		$container = "blogpost";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 5;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/blog/index/true";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/blog/index/true/0/')";
		$page_config['suffix'] = "')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		$post = $this->Mbprarticle->getArticle($start, $page_config['per_page']);
		$paging = $this->pagination->create_links();

       if($ajax){


	         $html = "<section>";
	         $html.= "<div class='row'>";

	          foreach($post as $p){ 

	            if($p['cover'] != ""){

	              $path = "upload/media";
	                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                    $file = $upload_path.'/'.$p['cover'];

	                    if(file_exists($file)){

	                        $cover = base_url().$path.'/'.$p['cover'];

	                    }else{

	                        $cover = base_url().'public/assets/img/default_img.png';

	                    }

	            }else{

	              $cover = base_url().'public/assets/img/default_img.png';

	            }
	            
	            $content = strip_tags($p['content']);

	            $content = truncate($content,200);


	            $html.='<article class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
	                         <div class="card blefetHoverxs padding-0 updated">
	                          <div class="row margin-0">
	                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
	                             <div class="item__image center-block">
	                               <figurex> <img src="'. base_url().'"assets/dist/img/preloader.gif" class="lazy img-responsive" data-src="'.$cover.'" alt="'.$p['title'].'"> </figurex>
	                            </div>
	                          </div>
	                          <div class="col-lg-12 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
	                            <div class="text-headbpr">
	                              <h3><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'">'.$p['title'].'</a></h3> 
	                              <small><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>'.$p['published_date'].'</span></span></small>
	                            </div>
	                            <div class="text-dexsbpr">'.$content.'</div>
	                           <div class="action clearfix pull-right">
	                            <div class="text__content text--medium text-left">
	                              <span class="green form-btn-read">
	                               <a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'" class="btn btn-warning btn-round"> Baca </a>
	                             </span>
	                           </div> 
	                         </div> 
	                       </div>
	                     </div>
	                   </div>
	                 </article>'; 

	                                    

	            } 

	      

	        $html.='</div>';
	        $html.='</section>';
	        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
	                             
	        echo $html;

       }else{

       		$data['data']['post']=$post;
       		$data['data']['paging']=$paging;

       		$this->load->view('layout/main',$data);

       }

      

	}

	public function post_search($page = 0,$keyword = null){

		if(count($_POST)>0){

			$keyword = strip_tags($_POST['keyword']);
			$alldata = $this->Mbprarticle->searchBlogArticle($keyword, null, null);

		}else{

			$alldata = $this->Mbprarticle->searchBlogArticle(urldecode($keyword), null, null);

		}

		if($keyword == null || $keyword == ""){

			$param_keyword = "-"; 

		}else{

			$param_keyword = urlencode($keyword);

		}

		$container = "blogpost";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 3;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/search/blog/";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/search/blog/0/".$param_keyword."')";
		$page_config['suffix'] = "/".$param_keyword."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		if(count($_POST)>0){

			$post = $this->Mbprarticle->searchBlogArticle($keyword, $start, $page_config['per_page']);
			
		}else{

			$post = $this->Mbprarticle->searchBlogArticle(urldecode($keyword), $start, $page_config['per_page']);

		}

		$paging = $this->pagination->create_links();

       	if($keyword!=""){
       		$html = "<h4 class='page-title-src' style='text-align:center'>Hasil pencarian untuk: <span style='font-weight:bold;'>".$keyword."</span> (".count($alldata).")</h4> ";
       	}else{
       		$html = "";
       	}

       	$html = "<section>";
	    $html.= "<div class='row'>";

        if(count($post)>0){

        	 foreach($post as $p){ 

	            if($p['cover'] != ""){

	              $path = "upload/media";
	                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                    $file = $upload_path.'/'.$p['cover'];

	                    if(file_exists($file)){

	                        $cover = base_url().$path.'/'.$p['cover'];

	                    }else{

	                        $cover = base_url().'public/assets/img/default_img.png';

	                    }

	            }else{

	              $cover = base_url().'public/assets/img/default_img.png';

	            }
	            
	            $content = strip_tags($p['content']);

	            $content = truncate($content,200);


	            $html.='<article class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
	                         <div class="card blefetHoverxs padding-0 updated" data-expired-date="1507173746" data-expired-time="86400" data-transaction-status="12">
	                          <div class="row margin-0">
	                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
	                             <div class="item__image center-block">
	                               <figurex> <img src="'. base_url().'"assets/dist/img/preloader.gif" class="lazy img-responsive" data-src="'.$cover.'" alt="'.$p['title'].'"> </figurex>
	                            </div>
	                          </div>
	                          <div class="col-lg-12 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
	                            <div class="text-headbpr">
	                              <h3><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'">'.$p['title'].'</a></h3> 
	                              <small><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>'.$p['published_date'].'</span></span></small>
	                            </div>
	                            <div class="text-dexsbpr">'.$content.'</div>
	                           <div class="action clearfix pull-right">
	                            <div class="text__content text--medium text-left">
	                              <span class="green form-btn-read">
	                               <a href="#" class="btn btn-warning btn-round"  data-toggle="modal" data-target="#eventIDDetail"> Read </a>
	                             </span>
	                           </div> 
	                         </div> 
	                       </div>
	                     </div>
	                   </div>
	                 </article>'; 

	                                    

	            } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='</div>';
        $html.='</section>';
        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
                             
        echo $html;
	

	}

	function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }


}