<?php

class Bpr extends MX_Controller{


	var $preloader = "";

	function __construct(){

		parent::__construct();
        $this->load->library("Aauth");
        $this->load->helper('download');
        $this->load->model("Mbpr");
        $this->load->model("Mdpd");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprfinanceasset");
        $this->load->model("Mbprfinanceassettype");
        $this->load->model("Mbprdirectorposition");
        $this->load->model("Mbprdirector");
        $this->load->model("MCorporate");
        $this->load->model("Mbprproductcategory");
        $this->load->model("Mbprproduct");
        $this->load->model("Mbprarticlecategory");
        $this->load->model("Mbprarticle");
        $this->load->model("Mbprfinancepublication");
        $this->load->model("Mbprcontactperson");
        $this->load->model("Mbprbranch");
        $this->load->model("Mbprauction");
        $this->load->model("Mbprauctioncategory");


        $this->preloader = base_url()."assets/dist/img/preloader.gif";
 

	}

	function index($page=0,$ajax=false){

		$data['page'] = 'bpr_list';
        $data['og_title'] = 'PERBARINDO | BPR';
        $data['og_site_name'] = 'PERBARINDO BPR LIST';
        $data['og_site_url'] = base_url();
        $data['og_site_description'] = 'Daftar BPR PERBARINDO';
        $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';
   
       	$data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['dpd'] = $this->Mdpd->getDPD();

        $alldata = $this->Mbpr->getBPR();
		$container = "bprlist";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 10;
		$page_config['uri_segment'] = 2;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/bpr/";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/bpr/0/ajax')";
		$page_config['suffix'] = "/ajax')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);
		$data['data']['bpr'] = $this->Mbpr->getListBPR($start,$page_config['per_page']);
		$data['data']['paging'] = $this->pagination->create_links();


        if($ajax){

        	$html = "";

        	  	foreach($data['data']['bpr'] as $b){ 

        	  		if($b['logo'] != ""){
        	  			$path = "upload/bpr";
	                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                    $file = $upload_path.'/'.$b['logo'];

	                    if(file_exists($file)){
	                        $logo = base_url().$path.'/'.$b['logo'];
	                    }else{
	                        $logo = base_url().'public/assets/img/bpr_default.png';
	                    }
        	  		}else{
        	  			$logo = base_url().'public/assets/img/bpr_default.png';
        	  		}
        	  		

                   $html.='<article class="col-lg-12 col-md-6 col-sm-12 col-xs-12 padding-0">
	                       <div class="card block__box blefetHover padding-0 updated margin-b-0">
	                            <div class="row margin-0">
	                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 padding-12">
	                               <div class="item__image">
	                                <figurex><img src="'.$logo.'" alt="'.$b['corp'].' '.$b['name'].'" style="width: 150px;"> </figurex>
	                              </div>
	                            </div>
	                            <div class="col-lg-10 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
	                              <div class="text-headbpr">
	                                <h3><a href="'.base_url().'profile/bpr/'.$b['id'].'">'.$b['corp'].' '.$b['name'].'</a></h3> 
	                                <small><span class="date meta-item"><span class="fa fa-globe" aria-hidden="true"></span> <span>'.$b['dpd_name'].'</span></span></small>
	                              </div>
	                              <div class="text-dexsbpr">';

	                                if($b['about']==""){

	                                  $html.= '-';

	                                }else{

	                                  $html.= $b['about'];
	                                
	                                }

	                                

	        		$html.=	     '</div>
	                             <div class="action clearfix pull-right">
	                              <div class="text__content text--medium text-left">
	                                <span class="green form-btn-read">
	                                 <a href="'.base_url().'profile/bpr/'.$b['id'].'" class="btn btn-warning btn-round"> Lihat Profile </a>
	                               </span>
	                             </div> 
	                           </div> 
	                         </div>
	                       </div>
	                     </div>
	                   </article>';  

                } 

                $html.='<div class="form-group text-center text-center center-block"><br/>'.$data['data']['paging'].'</div>';
                                     
                echo $html;

        }else{

        	$this->load->view('layout/main',$data);

        }
        

	}

	public function search($page = 0, $dpd = null,$keyword = null){

		if(count($_POST)>0){

			$keyword = strip_tags($_POST['query']);
			$dpd = $_POST['dpd'];
			$alldata = $this->Mbpr->searchBPR($keyword, $dpd, null, null);

		}else{

			$alldata = $this->Mbpr->searchBPR(urldecode($keyword), $dpd, null, null);

		}

		if($dpd == null || $dpd == ""){

			$param_dpd = "-"; 

		}else{

			$param_dpd = $dpd; 

		}

		if($keyword == null || $keyword == ""){

			$param_keyword = "-"; 

		}else{

			$param_keyword = urlencode($keyword);

		}

		$container = "bprlist";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 10;
		$page_config['uri_segment'] = 3;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/search/bpr/";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/search/bpr/0/".$param_dpd."/".$param_keyword."')";
		$page_config['suffix'] = "/".$param_dpd."/".$param_keyword."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		if(count($_POST)>0){

			$data['data']['bpr'] = $this->Mbpr->searchBPR($keyword, $dpd, $start,$page_config['per_page']);

		}else{

			$data['data']['bpr'] = $this->Mbpr->searchBPR(urldecode($keyword), $dpd, $start,$page_config['per_page']);

		}
		$data['data']['paging'] = $this->pagination->create_links();


       
        $html = "<h4 class='page-title-src' style='text-align:center'>Hasil pencarian untuk: <span style='font-weight:bold;'>".$keyword."</span> (".count($alldata).")</h4> ";


        if(count($data['data']['bpr'])>0){

        	foreach($data['data']['bpr'] as $b){ 

		  		if($b['logo'] != ""){

		  			$path = "upload/bpr";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$b['logo'];

	                if(file_exists($file)){

	                    $logo = base_url().$path.'/'.$b['logo'];

	                }else{

	                    $logo = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$logo = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		

	           	$html.='<article class="col-lg-12 col-md-6 col-sm-12 col-xs-12 padding-0">
	                       <div class="card block__box blefetHover padding-0 updated margin-b-0">
	                            <div class="row margin-0">
	                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 padding-12">
	                               <div class="item__image">
	                                <figurex><img src="'.$logo.'" alt="'.$b['corp'].' '.$b['name'].'" style="width: 150px;"> </figurex>
	                              </div>
	                            </div>
	                            <div class="col-lg-10 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
	                              <div class="text-headbpr">
	                                <h3><a href="'.base_url().'profile/bpr/'.$b['id'].'">'.$b['corp'].' '.$b['name'].'</a></h3> 
	                                <small><span class="date meta-item"><span class="fa fa-globe" aria-hidden="true"></span> <span>'.$b['dpd_name'].'</span></span></small>
	                              </div>
	                              <div class="text-dexsbpr">';

	                                if($b['about']==""){

	                                  $html.= '-';

	                                }else{

	                                  $html.= $b['about'];
	                                
	                                }

	                                

	        	$html.=	         '</div>
	                             <div class="action clearfix pull-right">
	                              <div class="text__content text--medium text-left">
	                                <span class="green form-btn-read">
	                                 <a href="'.base_url().'profile/bpr/'.$b['id'].'" class="btn btn-warning btn-round"> Lihat Profile </a>
	                               </span>
	                             </div> 
	                           </div> 
	                         </div>
	                       </div>
	                     </div>
	                   </article>';  

	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$data['data']['paging'].'</div>';
                             
        echo $html;
	

	}

	public function profile($id=null){

		if($id != null){

			$data['page'] = 'bpr_profile';
	        $data['og_title'] = 'PERBARINDO | BPR PROFILE';
	        $data['og_site_name'] = 'PERBARINDO BPR PROFILE';
	        $data['og_site_url'] = base_url();
	        $data['og_site_description'] = 'Daftar BPR PERBARINDO';
	        $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';


	        $data['bpr'] = $this->Mbpr->getBPRById($id);
	        $data['stockholder'] = $this->Mbprstackholder->getBPRStockholderByBPR($id);
	        $data['director_position'] = $this->Mbprdirectorposition->getBPRDirectorPositionByBPR($id,"parent");
	        $data['assettype'] = $this->Mbprfinanceassettype->getBPRFinanceAssetType();
        	$data['asset'] = $this->Mbprfinanceasset->getBPRFinanceAssetByBPR($id);
	        $data['chart'] = $this->_get_director($data);
	        $data['product_category'] = $this->Mbprproductcategory->getBPRProductCategory($id, true);
	        $data['post_category'] = $this->Mbprarticlecategory->getBPRArticleCategory($id, true);
	        $data['auction_category'] = $this->Mbprauctioncategory->getBPRAuctionCategory();
	        $data['report'] = $this->Mbprfinancepublication->getBPRFinancePublication($id);
	        $data['branch'] = $this->Mbprbranch->getBPRBranch($id);
	        $data['contact_person'] = $this->Mbprcontactperson->getBPRContactPerson($id);

	   
	       	$data['body_class'] = 'profile-page';
	        $data['navbar']['navbar_class'] = 'navbar-transparent';

	        $this->load->view('layout/main',$data);

		}

	}

	public function getOrgChartData($id){

        $org = $this->Mbprdirector->getBPRChartOrg($id);
        $orgTopLevel = $this->Mbprdirector->getBPRChartOrgTopLevel($id);

        $chart = array();


        $primaryFields[] = "Name";
        $primaryFields[] = "Position";

        $i=1;
        foreach ($orgTopLevel as $otl) {

            $primaryFields[] = "Komisaris_".$i;
            $extraField["Komisaris_".$i] = $i.". ".$otl['name'];   
            $i++;

        }


        foreach ($org as $o) {

            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            if($o['photo'] != ""){

                if(file_exists($path.$o['photo'])){

                    $img = base_url().'upload/photo/'.$o['photo'];

                }else{

                    $img = base_url().'public/assets/img/default-avatar.png';

                }

            }else{

                $img = base_url().'public/assets/img/default-avatar.png';

            }
            
            array_push($chart, array("id" => $o['id'], "parentId" => $o['parent'], "Name" => $o['name'],"Position" => $o['position'], "pic" => $img));

        }

        if(count($orgTopLevel)>0){


            array_push($chart, array_merge($extraField,array("id" => @$orgTopLevel[0]['id'], "parentId" => null, "Name" => null, "Position" => @$orgTopLevel[0]['position'], "pic" => base_url().'public/assets/img/ceo-512.png')));

        }

        $res = array(
            "primaryFields" => $primaryFields,
            "sources" => $chart,
        );

        echo json_encode($res);

    }

	public function post($page = 0, $bpr= null, $category = null){
	
		$alldata = $this->Mbprarticle->getBPRArticle($bpr, $category, null, null);

		$container = "bprpost";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 4;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/post";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/post/0/".$bpr."/".$category."')";
		$page_config['suffix'] = "/".$bpr."/".$category."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		$post = $this->Mbprarticle->getBPRArticle($bpr, $category, $start, $page_config['per_page']);
		$paging = $this->pagination->create_links();

       
        $html = "";


        if(count($post)>0){

        	foreach($post as $p){ 

		  		if($p['cover'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['cover'];

	                if(file_exists($file)){

	                    $cover = base_url().$path.'/'.$p['cover'];

	                }else{

	                    $cover = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$cover = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		
		  		$content = strip_tags($p['content']);

		  		$content = $this->truncate($content,200);


	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$cover.'" class="lazy img-responsive" alt="'.$p['title'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'" rel="noopener noreferrer">'.$p['title'].'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                 	<p>'.$content.'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['post_category'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';

	                                

	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;

	}

	public function auction($page = 0, $bpr= null, $category = null){
	
		$alldata = $this->Mbprauction->getBPRAuction($bpr, $category, null, null);

		$container = "bprauction";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 4;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/auction";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/auction/0/".$bpr."/".$category."')";
		$page_config['suffix'] = "/".$bpr."/".$category."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		$post = $this->Mbprauction->getBPRAuction($bpr, $category, $start, $page_config['per_page']);
		$paging = $this->pagination->create_links();

       
        $html = "";


        if(count($post)>0){

        	foreach($post as $p){ 

		  		if($p['photo'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['photo'];

	                if(file_exists($file)){

	                    $photo = base_url().$path.'/'.$p['photo'];

	                }else{

	                    $photo = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$photo = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		
		  		$content = strip_tags($p['description']);

		  		$content = $this->truncate($content,200);

		  		$date = date_create($p['auction_date']);

		  		$label_status = "";

		  		if($p['status']=="1"){
		  			$label_status = "<span class='sold'>(TERJUAL)</span>";
		  		}

	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$photo.'" class="lazy img-responsive" alt="'.$p['asset_code'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/asset_code/'.$p['asset_code'].'" rel="noopener noreferrer">'.$p['auction_category_name'].'-'.$p['asset_code'].' '.$label_status.'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                  	<div class="price"> Rp. '.number_format($p['start_price'],0,",",".").'</div>
                                                 	
                                                 	<p>'.$p['city'].'</p>
                                                 	<p><b>Lelang</b> : '.date_format($date,"d-m-Y").', '.$p['start_time'].'-'.$p['end_time'].'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/asset_code/'.$p['asset_code'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['auction_category_name'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';

	                                

	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;

	}

	public function product($page = 0, $bpr= null, $category = null){
	
		$alldata = $this->Mbprproduct->getBPRProduct($bpr, $category, null, null);

		$container = "bprproduct";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 4;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/product";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/bpr/product/0/".$bpr."/".$category."')";
		$page_config['suffix'] = "/".$bpr."/".$category."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		$product = $this->Mbprproduct->getBPRProduct($bpr, $category, $start, $page_config['per_page']);
		$paging = $this->pagination->create_links();

       
        $html = "";


        if(count($product)>0){

        	foreach($product as $p){ 

		  		if($p['cover'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['cover'];

	                if(file_exists($file)){

	                    $cover = base_url().$path.'/'.$p['cover'];

	                }else{

	                    $cover = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$cover = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		
		  		$content = strip_tags($p['description']);

		  		$content = $this->truncate($content,200);


	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$cover.'" class="lazy img-responsive" alt="'.$p['name'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'" rel="noopener noreferrer">'.$p['name'].'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                    <div class="text--medium text-left">Deskripsi:</div>
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                 	<p>'.$content.'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['product_category'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';

	                                

	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;

	}

	public function product_search($page = 0, $bpr = null, $keyword = null, $category = null){

		if(count($_POST)>0){

			$keyword = strip_tags($_POST['keyword']);
			$category = $_POST['category'];
			$bpr = $_POST['bpr'];
			$alldata = $this->Mbprproduct->searchProduct($bpr, $keyword, $category, null, null);

		}else{

			$alldata = $this->Mbprproduct->searchProduct($bpr, urldecode($keyword), $category, null, null);

		}

		if($category == null || $category == ""){

			$param_category = "-"; 

		}else{

			$param_category = $category; 

		}

		if($keyword == null || $keyword == ""){

			$param_keyword = "-"; 

		}else{

			$param_keyword = urlencode($keyword);

		}

		$container = "bprproduct";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 3;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/search/product/";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/search/product/0/".$bpr."/".$param_keyword."/".$param_category."')";
		$page_config['suffix'] = "/".$bpr."/".$param_keyword."/".$param_category."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		if(count($_POST)>0){

			$product = $this->Mbprproduct->searchProduct($bpr, $keyword, $category, $start, $page_config['per_page']);
			
		}else{

			$product = $this->Mbprproduct->searchProduct($bpr, urldecode($keyword), $category, $start, $page_config['per_page']);

		}

		$paging = $this->pagination->create_links();

       	if($keyword!=""){
       		$html = "<h4 class='page-title-src' style='text-align:center'>Hasil pencarian untuk: <span style='font-weight:bold;'>".$keyword."</span> (".count($alldata).")</h4> ";
       	}else{
       		$html = "";
       	}

        if(count($product)>0){

        	foreach($product as $p){ 

		  		
		  		if($p['cover'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['cover'];

	                if(file_exists($file)){

	                    $cover = base_url().$path.'/'.$p['cover'];

	                }else{

	                    $cover = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$cover = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		
		  		$content = strip_tags($p['description']);

		  		$content = $this->truncate($content,200);


	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$cover.'" class="lazy img-responsive" alt="'.$p['name'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'" rel="noopener noreferrer">'.$p['name'].'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                    <div class="text--medium text-left">Deskripsi:</div>
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                 	<p>'.$content.'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['product_category'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';


	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;
	

	}

	public function post_search($page = 0, $bpr = null, $keyword = null, $category = null){

		if(count($_POST)>0){

			$keyword = strip_tags($_POST['keyword']);
			$category = $_POST['category'];
			$bpr = $_POST['bpr'];
			$alldata = $this->Mbprarticle->searchArticle($bpr, $keyword, $category, null, null);

		}else{

			$alldata = $this->Mbprarticle->searchArticle($bpr, urldecode($keyword), $category, null, null);

		}

		if($category == null || $category == ""){

			$param_category = "-"; 

		}else{

			$param_category = $category; 

		}

		if($keyword == null || $keyword == ""){

			$param_keyword = "-"; 

		}else{

			$param_keyword = urlencode($keyword);

		}

		$container = "bprpost";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 2;
		$page_config['uri_segment'] = 3;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/search/post/";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/search/post/0/".$bpr."/".$param_keyword."/".$param_category."')";
		$page_config['suffix'] = "/".$bpr."/".$param_keyword."/".$param_category."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		if(count($_POST)>0){

			$post = $this->Mbprarticle->searchArticle($bpr, $keyword, $category, $start, $page_config['per_page']);
			
		}else{

			$post = $this->Mbprarticle->searchArticle($bpr, urldecode($keyword), $category, $start, $page_config['per_page']);

		}

		$paging = $this->pagination->create_links();

       	if($keyword!=""){
       		$html = "<h4 class='page-title-src' style='text-align:center'>Hasil pencarian untuk: <span style='font-weight:bold;'>".$keyword."</span> (".count($alldata).")</h4> ";
       	}else{
       		$html = "";
       	}

        if(count($post)>0){

        	foreach($post as $p){ 

		  		
		  		if($p['cover'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['cover'];

	                if(file_exists($file)){

	                    $cover = base_url().$path.'/'.$p['cover'];

	                }else{

	                    $cover = base_url().'public/assets/img/bpr_default.png';

	                }

		  		}else{

		  			$cover = base_url().'public/assets/img/bpr_default.png';

		  		}
		  		
		  		$content = strip_tags($p['title']);

		  		$content = $this->truncate($content,200);


	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$cover.'" class="lazy img-responsive" alt="'.$p['title'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                   <h3><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'" rel="noopener noreferrer">'.$p['title'].'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                    <div class="text--medium text-left">Deskripsi:</div>
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                 	<p>'.$content.'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['post_category'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';


	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;
	

	}

	public function post_detail($bpr =null, $slug=null){

		if($slug!==null && $bpr!==null){

			$post = $this->Mbprarticle->getArticleBySlug($bpr,$slug);

			if(count($post)>0){

				$content = strip_tags($post['content']);

		  		$content = $this->truncate($content,200);

		  		if($post['cover']!=""){
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

	                if(file_exists($upload_path.$post['cover'])){
	                    $cover = base_url().'upload/media/'.$post['cover'];
	                }else{
	                    $cover = base_url().'assets/dist/img/img-1.png';
	                }

	            }else{
	                $cover = base_url().'assets/dist/img/img-1.png';
	            }

				$data['page'] = "bpr_post_detail";
				$data['page_title'] = "POST";
				$data['page_keyword'] = $post['meta_keyword'];
				$data['page_description'] = $post['meta_description'];
				$data['page_author'] = "PERBARINDO";
				$data['og_title'] = $post['title'];
		        $data['og_site_name'] = 'PERBARINDO';
		        $data['og_site_url'] = base_url().'bpr/post/'.$bpr.'/'.$post['permalink'];
		        $data['og_site_description'] = trim($content);
		        $data['og_site_image'] = $cover;
		   
		       	$data['body_class'] = 'profile-page';
		        $data['navbar']['navbar_class'] = 'navbar-transparent';

		        $data['data']['post'] = $post;
		        $data['data']['cover'] = $cover;
		        $data['data']['share_url'] = $data['og_site_url'];

		        $this->load->view('layout/main',$data);

			}else{

				$this->load->view('errors/html/error_404');

			}
			

		}

	}

	public function asset_code($code = null){

		if($code!==null){

			$auction = $this->Mbprauction->getAuctionByAssetCode($code);

			if(count($auction)>0){

				$content = strip_tags($auction['description']);

		  		$content = $this->truncate($content,200);

		  		if($auction['photo']!=""){
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

	                if(file_exists($upload_path.$auction['photo'])){
	                    $cover = base_url().'upload/media/'.$auction['photo'];
	                }else{
	                    $cover = base_url().'assets/dist/img/img-1.png';
	                }

	            }else{
	                $cover = base_url().'assets/dist/img/img-1.png';
	            }

				$data['page'] = "bpr_auction_detail";
				$data['page_title'] = "LELANG";
				$data['page_keyword'] = "lelang ".$auction['auction_category_name'].', lelang, '.$auction['auction_category_name'].'-'.$auction['asset_code'];
				$data['page_description'] = trim($content);
				$data['page_author'] = "PERBARINDO";
				$data['og_title'] = 'lelang '. $auction['auction_category_name'].'-'.$auction['asset_code'];
		        $data['og_site_name'] = 'PERBARINDO';
		        $data['og_site_url'] = base_url().'bpr/asset_code/'.$auction['asset_code'];
		        $data['og_site_description'] = trim($content);
		        $data['og_site_image'] = $cover;
		   
		       	$data['body_class'] = 'profile-page';
		        $data['navbar']['navbar_class'] = 'navbar-transparent';

		        $data['data']['auction'] = $auction;
		        $data['data']['cover'] = $cover;
		        $data['data']['share_url'] = $data['og_site_url'];

		        $this->load->view('layout/main',$data);

			}else{

				$this->load->view('errors/html/error_404');

			}
			

		}

	}

	public function product_detail($bpr =null, $slug=null){

		if($slug!==null && $bpr!==null){

			$post = $this->Mbprproduct->getProductBySlug($bpr,$slug);

			if(count($post)>0){

				$content = strip_tags($post['description']);

		  		$content = $this->truncate($content,200);

		  		if($post['cover']!=""){
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

	                if(file_exists($upload_path.$post['cover'])){
	                    $cover = base_url().'upload/media/'.$post['cover'];
	                }else{
	                    $cover = base_url().'assets/dist/img/img-1.png';
	                }

	            }else{
	                $cover = base_url().'assets/dist/img/img-1.png';
	            }

				$data['page'] = "bpr_product_detail";
				$data['page_title'] = "POST";
				$data['page_keyword'] = $post['meta_keyword'];
				$data['page_description'] = $post['meta_description'];
				$data['page_author'] = "PERBARINDO";
				$data['og_title'] = $post['name'];
		        $data['og_site_name'] = 'PERBARINDO';
		        $data['og_site_url'] = base_url().'bpr/post/'.$bpr.'/'.$post['permalink'];
		        $data['og_site_description'] = trim($content);
		        $data['og_site_image'] = $cover;
		   
		       	$data['body_class'] = 'profile-page';
		        $data['navbar']['navbar_class'] = 'navbar-transparent';

		        $data['data']['post'] = $post;
		        $data['data']['cover'] = $cover;
		        $data['data']['share_url'] = $data['og_site_url'];

		        $this->load->view('layout/main',$data);

			}else{

				$this->load->view('errors/html/error_404');

			}
			

		}

	}

	function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }

	public function get_stockholder($bpr){

        $stockholder = $this->Mbprstackholder->getBPRStockholderByBPR($bpr);

        $res = array();

        foreach ($stockholder as $s){
                
            $a = array( $s['name'], (float)$s['percentage']);
            
            array_push($res, $a);

        }

        echo json_encode($res);

    }

	private function _get_director($data){

		$html = '';

        foreach ($data['director_position'] as $parent) {
            
            $m1 = $this->Mbprdirector->getBPRDirectorByPosition($data['bpr']['id'],$parent['id']);
            $ch1 = $this->Mbprdirectorposition->getBPRChildDirectorPosition($data['bpr']['id'], $parent['id']);


            foreach ($m1 as $m) {

                $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

                if(file_exists($path.$m['photo'])){

                    $img = base_url().'upload/photo/'.$m['photo'];

                }else{

                    $img = base_url().'public/assets/img/default-avatar.png';

                }

                $html.= '<li>';
                $html.= '<img style="width: 100%;" title="'.$m['name'].'" src="'.$img.'">';
                $html.= '<em>'.$m['name'].'</em><br>';
                $html.= '<b>'.$parent['name'].'</b>';
                
                if(count($ch1)>0){
                    
                    $html .= '<ul>';

                    foreach ($ch1 as $c1) {

                        $m2 = $this->Mbprdirector->getBPRDirectorByPosition($data['bpr']['id'],$c1['id']);
                        $ch2 = $this->Mbprdirectorposition->getBPRChildDirectorPosition($data['bpr']['id'], $c1['id']);

                        foreach ($m2 as $m) {

                            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

                            if($m['photo'] != ""){

                                if(file_exists($path.$m['photo'])){

                                    $img = base_url().'upload/photo/'.$m['photo'];

                                }else{

                                    $img = base_url().'public/assets/img/default-avatar.png';

                                }

                            }else{

                                $img = base_url().'public/assets/img/default-avatar.png';

                            }
                            
                            $html.= '<li>';
                            $html.= '<img style="width: 100%;" title="'.$m['name'].'" src="'.$img.'">';
                            $html.= '<em>'.$m['name'].'</em><br>';
                            $html.= '<b>'.$c1['name'].'</b>';

                            if(count($ch2)>0){
                    
                                $html .= '<ul>';

                                foreach ($ch2 as $c2) {

                                    $m3 = $this->Mbprdirector->getBPRDirectorByPosition($data['bpr']['id'],$c2['id']);
                                    $ch3 = $this->Mbprdirectorposition->getBPRChildDirectorPosition($data['bpr']['id'], $c2['id']);

                                    foreach ($m3 as $m) {

                                        $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

                                        if($m['photo'] != ""){

                                            if(file_exists($path.$m['photo'])){

                                                $img = base_url().'upload/photo/'.$m['photo'];

                                            }else{

                                                $img = base_url().'public/assets/img/default-avatar.png';

                                            }

                                        }else{

                                            $img = base_url().'public/assets/img/default-avatar.png';

                                        }
                                        
                                        $html.= '<li>';
                                        $html.= '<img style="width: 100%;" title="'.$m['name'].'" src="'.$img.'">';
                                        $html.= '<em>'.$m['name'].'</em><br>';
                                        $html.= '<b>'.$c2['name'].'</b>';
                                        $html.= '</li>';

                                    }

                                }

                                $html .= '</ul>';


                            }

                            $html.= '</li>';

                        }

                    }

                    $html .= '</ul>';

                }   

                $html.= '</li>';
             
            }

        }

        return $html;

	}

	public function download_report($file=null){

		if($file!=null){

			force_download('upload/media/'.$file, NULL);

		}else{

			$this->load->view("errors/html/error_404");

		}

	}


}