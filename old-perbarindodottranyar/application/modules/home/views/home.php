<section>
        
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">

        <?php
          $i = 0;
          foreach ($upcoming_events as $ue) {
        ?>

          <div class="carousel-item <?php ($i==0)? $active = 'active': $active = '';echo $active; ?>">
          <div class="overlay-item-layer-orange"></div> 
          <div class="first-slide img-responsive lazy" style="height:600px;background: url('<?php echo setEventImage($ue['cover']); ?>') no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"  alt="<?php echo $ue['name']; ?>" alt="<?php echo $ue['name']; ?>" width="100%" >   


              <!-- <img class="first-slide img-responsive" src="<?php //echo setEventImage($ue['cover']); ?>" alt="<?php //echo $ue['name']; ?>" alt="<?php //echo $ue['name']; ?>" width="100%"> -->
              <div class="container">
                <div class="carousel-caption d-nonex d-md-blockx text-left">
                   <h1><?php echo $ue['name']; ?></h1>
                    <span class="visible-lg visible-md visible-sm hidden-xs"><?php echo $ue['description']; ?></span>
                    <p class="visible-lg visible-md visible-sm hidden-xs"><a class="btn btn-lg btn-primary" href="<?php echo base_url().'show/event/'.$ue['id']; ?>" role="button">Detail</a></p>
                    <a class="btn btn-xs btn-primary hidden-lg hidden-md hidden-sm visible-xs" href="<?php echo base_url().'show/event/'.$ue['id']; ?>" role="button">Detail</a>
                </div>
              </div> 
           </div> 
        </div>

        <?php
            $i++;    
          }
        ?>
        
        
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </section>
 

    <!-- Carosel end -->
     <section>
        <section class="jumbotron text-center" style="margin-bottom: -3rem;">
          <div class="container">
            <h2 class="jumbotron-heading">PERBARINDO EVENT</h2>
            <p class="lead text-muted">Situs Reservasi Online untuk acara yang diadakan oleh PERBARINDO</p>
            <p>
              <a href="<?php echo base_url() ?>about" class="btn btn-primary">Selengkapnya</a>
            </p>
            <br>
          </div>
        </section>
    </section>
 
    <section> 

        <div class="container marketing">
            <div class="card"> 
               <div class="search-travel">
                     <div class="seach-head text-center">
                         <h1 class="text-heading-epic homepage-header text-primary">
                            Find Event
                        </h1>
                     </div>
                       <form action="<?php echo base_url(); ?>find/event" method="post">
                            <input type="hidden" name="street_number" id="street_number">
                            <input type="hidden" name="route" id="route">
                            <input type="hidden" name="locality" id="locality">
                            <input type="hidden" name="administrative_area_level_1" id="administrative_area_level_1">
                            <input type="hidden" name="postal_code" id="postal_code">
                            <input type="hidden" name="country" id="country">
                        <div class="row">
                           
                            <div class="col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="Search events" title="Search events">
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="form-group has-success">
                                    <input type="text" name="location" id="autocomplete" class="form-control form-control-success" placeholder="City or Location" title="City or Location">
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="form-group has-ingo">
                                    <select name="date" class="form-control form-control-info">
                                         <option class="option-org" value="all" selected="selected">All Dates</option>
                                        <option class="option-org" value="today">Today</option>
                                        <option class="option-org" value="tomorrow">Tomorrow</option>
                                        <option class="option-org" value="this_week">This Week</option>
                                        <option class="option-org" value="next_week">Next Week</option>
                                        <option class="option-org" value="next_month">Next Month</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3" style="margin-top: -7px;">
                                <div class="input-group"> 
                                    <button class="btn btn-primary btn-block btn-xs" type="submit">
                                        <i class="fa fa-seach"></i> SEARCH
                                    </button>
                                </div>
                            </div>
          
                           
                        </div>    
                     </form>           
               </div>
            </div>
        
        </div>

    </section>
   
    <section>
        <div class="album text-muted">
             <div class="container">
    

        <row>
            <div class="col-lg-12 text-center">
                <h3 class="section-header l-align-left" aria-label="Popular Events in  Bandung, Indonesia">
                    <span data-automation="home-popular-events-header" aria-hidden="true">Latest Event</span> 
                </h3>
            </div>
        </row> 

        <div class="row">

           <?php foreach($latest_events as $oe){ ?>

                 <article class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="wrap-card poster-card">
                            <div class="head-card frame">  
                                <img class="lazy img-responsive" src="<?php echo base_url().'assets/dist/img/preloader.gif'; ?>" data-src="<?php  echo setEventImage($oe['cover']) ?>" alt="<?php  echo $oe['name'] ?>" data-holder-rendered="true">  
                            </div>
                            <div class="body-card">
                                <div class="poster-card__body">
                                    <?php
                                            $sdate=date_create($oe['start_date']);
                                            $sdaynum = date_format($sdate,"d");
                                            $sday = date_format($sdate,"l");
                                            $smonth = date_format($sdate,"F");
                                            $syear = date_format($sdate,"Y");

                                            $edate=date_create($oe['end_date']);
                                            $edaynum = date_format($edate,"d");
                                            $eday = date_format($edate,"l");
                                            $emonth = date_format($edate,"F");
                                            $eyear = date_format($edate,"Y");
                                    ?>                                    
                                    <time class="poster-card__date">

                                        <?php echo $sday.' , '.$smonth.' '.$sdaynum.' '.$syear ?>

                                    </time>
                                    <div class="poster-card__title">
                                      <a href="<?php echo base_url().'show/event/'.$oe['id']; ?>"> <?php echo $oe['name']; ?></a>
                                    </div>
                                    <div class="poster-card__venue">

                                       <?php echo $oe['city']; ?>

                                    </div>
          
                                </div>   
                            </div>     
                            <div class="poster-card__footer"> 
                                <div class="poster-card__tags"> 
                                       
                                </div>
                                <div class="poster-card__actions">
                                      <a class="js-share-event-card share-action" href="<?php echo base_url().'show/event/'.$oe['id']; ?>" data-eid="37867656179" title="Detail" aria-haspopup="true">
                                          <i class="fa fa-check"></i>
                                      </a>
                                      <a class="js-d-bookmark" data-eid="37867656179" data-bookmarked="false" title="show map" data-source-page="home:popular" href="http://maps.google.com/maps?q=<?php echo $oe['latitude']; ?>,<?php echo $oe['longitude']; ?>&z=17" aria-label="Save Event" dorsal-guid="1991d28d-4cdd-81ca-3e37-5ce9278b2747" data-xd-wired="bookmark" target="_blank">
                                          <i class="fa fa-map-marker"></i>
                                      </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  </article> 
            <?php } ?> 
        </div> 
      </div>
    </div>
    </section>

    <div class="wrapper">
        <div class="page-header page-header-small" style="background-image: url('<?php echo base_url(); ?>public/assets/img/AAAA.png');">
           
            <div class="container">
                <div class="content-center">
                    <h3 class="title">Perhimpunan Bank Perkreditan Indonesia</h3>
                    <div class="text-center">
                        <a href="<?php echo @$fb['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="<?php echo @$tw['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="<?php echo @$gg['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
         
       
        <div class="section section-contact-us text-center">
            <div class="container">
                <h3 class="title"><i class="fa fa-phone-square fa-lg"></i> Want to contact us?</h3>
                <p class="description">Contact us if you have any question.</p>
                <form id="contact_us">
                <div class="row">
                    <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
                        <div class="input-group input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons users_circle-08"></i>
                            </span>
                            <input type="text" name="name" class="form-control validate[required]" placeholder="Name...">
                        </div>
                        <div class="input-group input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons ui-1_email-85"></i>
                            </span>
                            <input type="text" name="email" class="form-control validate[required,custom[email]]" placeholder="Email...">
                        </div>
                        <div class="textarea-container">
                            <textarea class="form-control text-center validate[required]" name="message" rows="4" cols="80" placeholder="Type a message..."></textarea>
                        </div>
                        <div class="send-button">
                            <button type="submit" class="btn btn-primary btn-round btn-block btn-lg">Send Message</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        
    </div>



<button onclick="topFunction()" class="btn btn-primary btn-round" id="myBtn" title="Go to top"><i class="fa fa-angle-double-up fa-lg" aria-hidden="true"></i> Top</button>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();

            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });

            $("#contact_us").validationEngine();
            $("#contact_us").submit(function(){

              if($(this).validationEngine('validate')){

                var data = $(this).serialize();
                var target = base_url + 'submit/message';

                $("body").waitMe({

                      effect: 'pulse',
                      text: 'Sending Message...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'

                });

                $.post(target, data, function(res){

                   $("body").waitMe("hide");

                    if(res.status=="1"){
                        toastr.success(res.msg, 'Response Server');
                    }else{
                        toastr.error(res.msg, 'Response Server');
                    }

                     $('#contact_us')[0].reset();

                },'json');

                return false;

              }
              

            });


            $('#myBtn').each(function(){
              $(this).click(function(){ 
                  $('html,body').animate({ scrollTop: 0 }, 'slow');
                  return false; 
              });
          });

        });
    </script>
     <script>

          var placeSearch, autocomplete;
          var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
          };

          function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
          }

          function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
              }
            }
          }

          // Bias the autocomplete object to the user's geographical location,
          // as supplied by the browser's 'navigator.geolocation' object.
          function geolocate() {
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
              });
            }
          }
    </script>
    <script type="text/javascript">
              // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initAutocomplete"
        async defer></script>