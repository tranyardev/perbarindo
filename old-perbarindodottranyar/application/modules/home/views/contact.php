<div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/AAAA.png');">
            </div>
            <div class="container">
                <div class="content-center">
                    <h2 class="title">Contact Us.</h2>
                    <div class="text-center">
                        <a href="<?php echo @$fb['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="<?php echo @$tw['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="<?php echo @$gg['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="section section-contact-us text-center">
            <div class="container">
                <h3 class="title"><i class="fa fa-phone-square fa-lg"></i> Want to contact us?</h3>
                <p class="description">Contact us if you have any question.</p>
                <form id="contact_us">
                <div class="row">
                    <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
                        <div class="input-group input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons users_circle-08"></i>
                            </span>
                            <input type="text" name="name" class="form-control validate[required]" placeholder="Name...">
                        </div>
                        <div class="input-group input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons ui-1_email-85"></i>
                            </span>
                            <input type="text" name="email" class="form-control validate[required,custom[email]]" placeholder="Email...">
                        </div>
                        <div class="textarea-container">
                            <textarea class="form-control text-center validate[required]" name="message" rows="4" cols="80" placeholder="Type a message..."></textarea>
                        </div>
                        <div class="send-button">
                            <button type="submit" class="btn btn-primary btn-round btn-block btn-lg">Send Message</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();

            $("#contact_us").validationEngine();
            $("#contact_us").submit(function(){

              if($(this).validationEngine('validate')){

                var data = $(this).serialize();
                var target = base_url + 'submit/message';

                $("body").waitMe({

                      effect: 'pulse',
                      text: 'Sending Message...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'

                });

                $.post(target, data, function(res){

                   $("body").waitMe("hide");

                    if(res.status=="1"){
                        toastr.success(res.msg, 'Response Server');
                    }else{
                        toastr.error(res.msg, 'Response Server');
                    }

                     $('#contact_us')[0].reset();

                },'json');

                return false;

              }
              

            });
        });
    </script>