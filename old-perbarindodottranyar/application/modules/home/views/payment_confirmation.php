 <div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">


        <section>
          <div class="row" style=" margin-top: -444px;" >
            <div class="col-md-8"> 
                <div class="card">        
                        <div class="text-center padding-12" >
                            <h3 class="title margin-0"  >KONFIRMASI PEMBAYARAN</h3>
                             <h6 class="description margin-0"><?php echo $event_registration['event_name']; ?> - #<?php echo $event_registration['regcode']; ?></h6>
                        </div>

                        <hr>

                        <div class="panel panel-default">
                            <div class="text-center">
                                <div class="panel-heading">Silahkan isi form di bawah ini</div>
                            </div>
                              <div class="panel-body">




                              <!--==== BIGEN FORM ====-->

                                <div class="formpayment-confirm">
                                    <form id="frm-payment-confirm">
                                      <input type="hidden" name="regcode" value="<?php echo $event_registration['regcode']; ?>">  
                                      <input type="hidden" name="event_registration_id" value="<?php echo $event_registration['reg_id']; ?>">  
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Tanggal Transfer</label></div>
                                        <div class="col-md-9"><input name="transfer_date" type="date" class="form-control" placeholder="Pilih Tanggal Transfer"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Nama Pengirim</label></div>
                                        <div class="col-md-9"><input name="from_account" type="text" class="form-control"  placeholder="Masukan Nama Pengirim"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Bank Pengirim</label></div>
                                        <div class="col-md-9"> 
                                            <select class="form-control" name="bank">
                                                    <option value="" disabled="">Pilih Bank</option>
                                                    <option value="Bank Central Asia ( BCA )">Bank Central Asia ( BCA )</option>
                                                    <option value="Bank Rakyat Indonesia ( BRI )">Bank Rakyat Indonesia ( BRI )</option>
                                                    <option value="Bank Mandiri">Bank Mandiri</option>
                                                    <option value="Bank Negara Indonesia 1946 ( BNI )">Bank Negara Indonesia 1946 ( BNI )</option>
                                                    <option value="Bank Danamon Indonesia">Bank Danamon Indonesia</option>
                                                    <option value="Bank Permata tbk ( Permata )">Bank Permata tbk ( Permata )</option>
                                                    <option value="Bank Internasional Indonesia ( BII )">Bank Internasional Indonesia ( BII )</option>
                                                    <option value="Bank Pan Indonesia">Bank Pan Indonesia</option>
                                                    <option value="Bank CIMB Niaga ( CIMB )">Bank CIMB Niaga ( CIMB )</option>
                                                    <option value="Bank UOB Buana">Bank UOB Buana</option>
                                                    <option value="Bank OCBC NISP">Bank OCBC NISP</option>
                                                    <option value="Citibank">Citibank</option>
                                                    <option value="JP Morgan Chase Bank">JP Morgan Chase Bank</option>
                                                    <option value="Bank of America NA &amp; SA">Bank of America NA &amp; SA</option>
                                                    <option value="Bank Windu Kentjana International">Bank Windu Kentjana International</option>
                                                    <option value="Bank Artha Graha Internasional">Bank Artha Graha Internasional</option>
                                                    <option value="Bangkok Bank Ltd.">Bangkok Bank Ltd.</option>
                                                    <option value="The Hongkong &amp; Shanghai Banking Corp ( HSBC )">The Hongkong &amp; Shanghai Banking Corp ( HSBC )</option>
                                                    <option value="The Bank of Tokyo Mitsubishi-UFJ Ltd.">The Bank of Tokyo Mitsubishi-UFJ Ltd.</option>
                                                    <option value="Bank Sumitomo Mitsui Indonesia">Bank Sumitomo Mitsui Indonesia</option>
                                                    <option value="Bank DBS Indonesia">Bank DBS Indonesia</option>
                                                    <option value="Bank Resona Perdania">Bank Resona Perdania</option>
                                                    <option value="Bank Mizuho Indonesia">Bank Mizuho Indonesia</option>
                                                    <option value="Standard Chartered Bank">Standard Chartered Bank</option>
                                                    <option value="ABN Amro Bank NV">ABN Amro Bank NV</option>
                                                    <option value="Bank Capital Indonesia">Bank Capital Indonesia</option>
                                                    <option value="Bank BNP Paribas Indonesia">Bank BNP Paribas Indonesia</option>
                                                    <option value="Korea Exchange Bank Indonesia">Korea Exchange Bank Indonesia</option>
                                                    <option value="ANZ Panin Bank">ANZ Panin Bank</option>
                                                    <option value="Deutsche Bank AG.">Deutsche Bank AG.</option>
                                                    <option value="Bank Woori Indonesia">Bank Woori Indonesia</option>
                                                    <option value="The Bank of China">The Bank of China</option>
                                                    <option value="Bank Bumi Arta">Bank Bumi Arta</option>
                                                    <option value="Bank Ekonomi Raharja">Bank Ekonomi Raharja</option>
                                                    <option value="Bank Antardaerah">Bank Antardaerah</option>
                                                    <option value="Rabobank International Indonesia">Rabobank International Indonesia</option>
                                                    <option value="Bank Mutiara">Bank Mutiara</option>
                                                    <option value="Bank Mayapada International">Bank Mayapada International</option>
                                                    <option value="BPD Jawa Barat">BPD Jawa Barat</option>
                                                    <option value="BPD DKI Jakarta">BPD DKI Jakarta</option>
                                                    <option value="BPD Yogyakarta">BPD Yogyakarta</option>
                                                    <option value="BPD Jawa Tengah">BPD Jawa Tengah</option>
                                                    <option value="BPD Jawa Timur">BPD Jawa Timur</option>
                                                    <option value="BPD Jambi">BPD Jambi</option>
                                                    <option value="BPD Aceh">BPD Aceh</option>
                                                    <option value="BPD Sumatera Utara">BPD Sumatera Utara</option>
                                                    <option value="BPD Sumatera Barat">BPD Sumatera Barat</option>
                                                    <option value="BPD Riau">BPD Riau</option>
                                                    <option value="BPD Sumatera Selatan dan Bangka Belitung">BPD Sumatera Selatan dan Bangka Belitung</option>
                                                    <option value="BPD Lampung">BPD Lampung</option>
                                                    <option value="BPD Kalimantan Selatan">BPD Kalimantan Selatan</option>
                                                    <option value="BPD Kalimantan Barat">BPD Kalimantan Barat</option>
                                                    <option value="BPD Kalimantan Timur">BPD Kalimantan Timur</option>
                                                    <option value="BPD Kalimantan Tengah">BPD Kalimantan Tengah</option>
                                                    <option value="BPD Sulawesi Selatan">BPD Sulawesi Selatan</option>
                                                    <option value="BPD Sulawesi Utara">BPD Sulawesi Utara</option>
                                                    <option value="BPD Nusa Tenggara Barat">BPD Nusa Tenggara Barat</option>
                                                    <option value="BPD Bali">BPD Bali</option>
                                                    <option value="BPD Nusa Tenggara Timur">BPD Nusa Tenggara Timur</option>
                                                    <option value="BPD Maluku">BPD Maluku</option>
                                                    <option value="BPD Papua">BPD Papua</option>
                                                    <option value="BPD Bengkulu">BPD Bengkulu</option>
                                                    <option value="BPD Sulawesi Tengah">BPD Sulawesi Tengah</option>
                                                    <option value="BPD Sulawesi Tenggara">BPD Sulawesi Tenggara</option>
                                                    <option value="Bank Nusantara Parahyangan">Bank Nusantara Parahyangan</option>
                                                    <option value="Bank Swadesi">Bank Swadesi</option>
                                                    <option value="Bank Muamalat Indonesia">Bank Muamalat Indonesia</option>
                                                    <option value="Bank Mestika Dharma">Bank Mestika Dharma</option>
                                                    <option value="Bank Metro Ekspres">Bank Metro Ekspres</option>
                                                    <option value="Bank Sinarmas">Bank Sinarmas</option>
                                                    <option value="Bank Maspion Indonesia">Bank Maspion Indonesia</option>
                                                    <option value="Bank Ganesha">Bank Ganesha</option>
                                                    <option value="Bank ICBC Indonesia">Bank ICBC Indonesia</option>
                                                    <option value="Bank Kesawan">Bank Kesawan</option>
                                                    <option value="Bank Tabungan Negara ( BTN )">Bank Tabungan Negara ( BTN )</option>
                                                    <option value="Bank HS 1906">Bank HS 1906</option>
                                                    <option value="Bank Tabungan Pensiunan Nasional">Bank Tabungan Pensiunan Nasional</option>
                                                    <option value="Bank Victoria Syariah">Bank Victoria Syariah</option>
                                                    <option value="Bank BRI Syariah">Bank BRI Syariah</option>
                                                    <option value="Bank Jabar Banten Syariah">Bank Jabar Banten Syariah</option>
                                                    <option value="Bank Mega">Bank Mega</option>
                                                    <option value="Bank BNI Syariah">Bank BNI Syariah</option>
                                                    <option value="Bank Bukopin">Bank Bukopin</option>
                                                    <option value="Bank Syariah Mandiri">Bank Syariah Mandiri</option>
                                                    <option value="Bank Bisnis Internasional">Bank Bisnis Internasional</option>
                                                    <option value="Bank Andara">Bank Andara</option>
                                                    <option value="Bank Jasa Jakarta">Bank Jasa Jakarta</option>
                                                    <option value="Bank Hana">Bank Hana</option>
                                                    <option value="Bank ICB Bumiputera">Bank ICB Bumiputera</option>
                                                    <option value="Bank Yudha Bhakti">Bank Yudha Bhakti</option>
                                                    <option value="Bank Mitraniaga">Bank Mitraniaga</option>
                                                    <option value="Bank Rakyat Indonesia Agroniaga ( BRI Agro )">Bank Rakyat Indonesia Agroniaga ( BRI Agro )</option>
                                                    <option value="Bank SBI Indonesia">Bank SBI Indonesia</option>
                                                    <option value="Bank Royal Indonesia">Bank Royal Indonesia</option>
                                                    <option value="Bank Nationalnobu">Bank Nationalnobu</option>
                                                    <option value="Bank Syariah Mega Indonesia">Bank Syariah Mega Indonesia</option>
                                                    <option value="Bank Ina Perdana">Bank Ina Perdana</option>
                                                    <option value="Bank Panin Syariah">Bank Panin Syariah</option>
                                                    <option value="Bank Prima Master">Bank Prima Master</option>
                                                    <option value="Bank Syariah Bukopin">Bank Syariah Bukopin</option>
                                                    <option value="Bank Dipo International">Bank Dipo International</option>
                                                    <option value="Bank Barclays Indonesia">Bank Barclays Indonesia</option>
                                                    <option value="Bank Liman International">Bank Liman International</option>
                                                    <option value="Bank Anglomas Internasional">Bank Anglomas Internasional</option>
                                                    <option value="Bank Kesejahteraan Ekonomi">Bank Kesejahteraan Ekonomi</option>
                                                    <option value="Bank BCA Syariah">Bank BCA Syariah</option>
                                                    <option value="Bank Artos Indonesia">Bank Artos Indonesia</option>
                                                    <option value="Bank Sahabat Purba Danarta">Bank Sahabat Purba Danarta</option>
                                                    <option value="Bank Multi Arta Sentosa ( MAS )">Bank Multi Arta Sentosa ( MAS )</option>
                                                    <option value="Bank Mayora">Bank Mayora</option>
                                                    <option value="Bank Index Selindo">Bank Index Selindo</option>
                                                    <option value="Bank Eksekutif Internasional">Bank Eksekutif Internasional</option>
                                                    <option value="Bank Centratama Nasional">Bank Centratama Nasional</option>
                                                    <option value="Bank Fama Internasional">Bank Fama Internasional</option>
                                                    <option value="Bank Sinar Harapan Bali">Bank Sinar Harapan Bali</option>
                                                    <option value="Bank Victoria International">Bank Victoria International</option>
                                                    <option value="Bank Harda Internasional">Bank Harda Internasional</option>
                                                    <option value="Bank Agris">Bank Agris</option>
                                                    <option value="Maybank Indocorp">Maybank Indocorp</option>
                                                    <option value="Bank OCBC Indonesia">Bank OCBC Indonesia</option>
                                                    <option value="Bank Chinatrust Indonesia">Bank Chinatrust Indonesia</option>
                                                    <option value="Bank Commonwealth">Bank Commonwealth</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                        </div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Nomor Rekening Pengirim</label></div>
                                        <div class="col-md-9"><input type="text" name="rek_number" class="form-control"  placeholder="Contoh: 791234567890" autocomplete="off"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Bank Tujuan</label></div>
                                        <div class="col-md-9"> 
                                               <select class="form-control" name="to_account">
                                                  <option value="" disabled="">Pilih Bank</option>
                                                    <?php foreach($bank_transfer_account as $bank){ ?>
                                                        <option value="<?php echo $bank['id']; ?>"><?php echo $bank['no_rek']; ?> - <?php echo $bank['account_name']; ?> - <?php echo $bank['bank']; ?></option>
                                                    <?php } ?> 
                                              </select>
                                        </div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">Bukti Pembayaran (Max 10 Mb)</label></div>
                                        <div class="col-md-9"><input type="file" class="form-control" name="file" autocomplete="off"></div>
                                      </div>
                                      
                                      
                                        <div class="form-group text-center">
                                              <button type="submit" class="btn btn-primary btn-lg">Konfirmasi Pembayaran</button>
                                        </div>
                                    </form>
                                </div>
                                   
                              <!--==== END FORM ====--> 
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-4">
                 <div class="card">         
                        <div class="kc-summary-details"> 
                          <div class="kc-summary-item-container">
                          <img class="kc-summary-item-image" src="<?php echo setEventImage($event_registration['cover']) ?>" role="presentation">
                              <div class="kc-summary-item">   
                                  <div class="title-ivent">
                                      
                                      <div class="listing-hero-body ">
                                          <h5 class="listing-hero-title-x" data-automation="listing-title"><?php echo $event_registration['event_name']; ?></h5>
                                          <meta content="<?php echo $event_registration['event_name']; ?>">
                                          <div class="l-mar-top-3">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <span><i class="fa fa-map-marker"></i> &nbsp;<strong>Lokasi</strong> <br><?php echo $event_registration['address']; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="l-mar-top-3" style="padding-top: 10px;">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <?php
                                                        $sdate=date_create($event_registration['start_date']);
                                                        $start_date = date_format($sdate,"d/m/Y");

                                                        $edate=date_create($event_registration['end_date']);
                                                        $end_date = date_format($edate,"d/m/Y");
                                                      ?>
                                                      <span><i class="fa fa-calendar"></i> <strong>Tanggal</strong> <br><?php echo $start_date; ?> - <?php echo $end_date; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                     
                                  </div>
                               
                              </div>
                          </div>
                          <div class="kc-summary-info-container"> 
                          <table class="table">
                              
                              <tr>
                                  <td>
                                      <span ><b>Total</b></span>
                                  </td>
                                  
                                  <td class="text-left">
                                    
                                    <span class="text-orange" id="total"></span>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <span ><b>Kode Unik</b></span>
                                  </td>
                                  
                                  <td class="text-left">
                                    
                                    <span class="text-orange"><?php echo $event_registration['uniq_number']; ?></span>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <span ><b>Total Pembayaran</b></span>
                                  </td>
                                 
                                  <td class="text-left">
                                    <strong><span class="text-orange" id="total_trx"></span></strong>
                                  </td>
                              </tr>  

                          </table> 
                          </div>
                      </div>

                </div>
            </div>
          </div>
        </section>



            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    var total = '<?php echo $event_registration['total_cost']; ?>';
    var total_trx = parseInt(total) + parseInt('<?php echo $event_registration['uniq_number']; ?>'); 

     // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');

    $('#total').html(numeral(total).format("$0,0"));
    $('#total_trx').html(numeral(total_trx).format("$0,0"));

    $("#frm-payment-confirm").submit(function(){
        
        var target = base_url + 'confirm/transaction';
        
        var $loading = $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                
                if(data.status=="1"){
                    
                    window.location.reload();

                }else{
                    
                    alert(data.msg);

                }

                $loading.hide();

            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });

  });
</script>