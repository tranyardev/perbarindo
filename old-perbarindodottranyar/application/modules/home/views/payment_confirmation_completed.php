<div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">

            <section>
              <div class="card rowz" style=" margin-top: -444px;">
                  <div class="col-md-12">
                            

                          <div class="text-center padding-12">
                              <h3 class="title margin-0" >PEMBAYARAN TELAH DIKONFIRMASI</h3>
                              <h6 class="description margin-0"><?php echo $event_registration['regcode']; ?></h6>
                          </div>

                          <hr>

                          <div class="panel panel-default">
                              
                              <div class="panel-body">

                                <!--==== BIGEN FORM ====-->

                                  <div class="padding-12">
                                    <div class="alert alert-info"><p style="text-align: center">Anda telah sudah melakukan konfirmasi untuk transaksi reservasi <a href="<?php echo base_url(); ?>payment_detail/<?php echo $event_registration['regcode']; ?>">#<?php echo $event_registration['regcode']; ?></a>. Transaksi anda sedang kami proses. Terimakasih.</p></div>
                                  </div>
                                     
                                <!--==== END FORM ====--> 
                              </div>
                          </div>

                    </div>
                </div>
              </section>

            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

   
  
  });
</script>