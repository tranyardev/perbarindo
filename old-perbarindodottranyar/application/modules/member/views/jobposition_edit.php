<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Division</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" maxlength="50" name="name" class="form-control validate[required]" id="name" value="<?php echo $dataedit['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description"><?php echo $dataedit['description']; ?></textarea>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

        </div>
        <div class="col-md-6">

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"member/jobposition/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }

                resetForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }

</script>