<?php

class MDMember extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'member_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'member_delete');

    }
    public function appendToSelectStr() {
        $edit = '';
        $delete = '';
        $str = array(
            "vgender" => "CASE WHEN a.gender = 'L' THEN 'Laki-laki' ELSE 'Perempuan' END",
            "photo" => "CASE WHEN a.photo != '' 
                THEN 
                    concat('<div class=\"image-container\"><img src=\"".base_url()."upload/photo/',a.photo,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                ELSE
                    concat('<div class=\"image-container\"><img src=\"".base_url()."public/assets/img/default-avatar.png\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                END",
            "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END"
        );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(
                "job_position" => "b.name",
                "bpr" => "c.name",
                "dpd" => "d.name",
                "vgender" => "CASE WHEN a.gender = 'L' THEN 'Laki-laki' ELSE 'Perempuan' END",
                "photo" => "CASE WHEN a.photo != '' 
                THEN 
                    concat('<div class=\"image-container\"><img src=\"".base_url()."upload/photo/',a.photo,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                ELSE
                    concat('<div class=\"image-container\"><img src=\"".base_url()."public/assets/img/default-avatar.png\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                END",
                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "members a";
    }

    public function joinArray(){
        return array(
            "job_positions b" => "b.id=a.job_position",
            "bpr c" => "c.id=a.bpr",
            "dpd d" => "d.id=c.dpd"
        );
    }

    public function whereClauseArray(){
        return null;
    }


}