<?php

class Member extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MMember");
        $this->load->model("MJobposition");
        $this->load->model("User/Muser2");
        $this->load->model("Master/Mbpr");
        $this->load->model("Master/Mdpd");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'member_view';
            $this->add_perm = 'member_add';
            $this->edit_perm = 'member_update';
            $this->delete_perm = 'member_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "members";
        $this->dttModel = "MDMember";
        $this->pk = "id";

        $this->aauthTable = "aauth_users";
        $this->aauthPk = "id";

    }

    function index(){


        $breadcrumb = array(
            "<i class='fa fa-user-circle-o'></i> Member" => base_url()."member"
        );

        $data['page'] = 'member';
        $data['page_title'] = 'Member';
        $data['page_subtitle'] = 'Member';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'member_management';
        $data['data']['submenu'] = 'member';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    function addnew(){

        if(count($_POST)>0){

            /* upload cover */

            $photo = null;

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{

                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            $data = array(

                "photo" => $photo,
                "name" => $this->input->post("name"),
                "gender" => $this->input->post("gender"),
                "bpr" => $this->input->post("bpr"),
                "job_position" => $this->input->post("job_position"),
                "email" => $this->input->post("email"),
                "no_hp" => $this->input->post("no_hp"),
                "status" => $this->input->post("status"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")
            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){

                $last_id = $this->db->insert_id();

                $create_user = $this->aauth->create_user($this->input->post('email'),$this->input->post("email"),$this->input->post("email"));

                if($create_user){

                    $last_idaauth = $this->db->insert_id();
                    $this->aauth->add_member($last_idaauth, "10"); // 10 is hardcode id for group member

                    $data = array(
                        "first_name" => $this->input->post("name"),
                        "status" => '1',
                    );

                    $this->db->where($this->aauthPk, $last_idaauth);
                    $updateaauthuser = $this->db->update($this->aauthTable, $data);

                    if($updateaauthuser){

                        $data = array(
                            "aauth_user_id" => $last_idaauth,
                        );

                        $this->db->where($this->pk, $last_id);
                        $updatemember = $this->db->update($this->table, $data);

                        if($updatemember){
                            $res = array("status" => "1", "msg" => "Successfully add data!");
                        }else{
                            $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");    
                        }
                        
                    }else{
                        $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                    }

                    echo json_encode($res);

                }else{
                    // $res = array("status" => "0", "msg" => "Oop! Something went wrong. Could not create user.");
                    $res = array("status" => "0", "msg" => print_r($this->aauth->get_errors_array()));
                    echo json_encode($res);
                }

            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                echo json_encode($res);
            }

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-user-circle-o'></i> Member" => base_url()."member",
                "Add New" => base_url()."member/member/addnew",
            );

            $data['page'] = 'member_add';
            $data['page_title'] = 'Member';
            $data['page_subtitle'] = 'Add New Member';
            $data['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['job_position'] = $this->MJobposition->getJobposition();
            $data['data']['dpd'] = $this->Mdpd->getDpd();
            $data['data']['bpr'] = $this->Mbpr->getBPR();
            $data['data']['parent_menu'] = 'member_management';
            $data['data']['submenu'] = 'member';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $member = $this->MMember->getMemberById($id);

            /* upload cover */

            $photo = null;

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    $old_photo = $upload_path.'/'.@$_POST['old_photo'];

                    if(file_exists($old_photo)){

                        @unlink($old_photo);

                    }


                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{

                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $photo = $this->input->post("old_photo");

            }

            $data = array(

                "photo" => $photo,
                "name" => $this->input->post("name"),
                "gender" => $this->input->post("gender"),
                "bpr" => $this->input->post("bpr"),
                "job_position" => $this->input->post("job_position"),
                "email" => $this->input->post("email"),
                "no_hp" => $this->input->post("no_hp"),
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){               

                $user = $this->Muser2->getUserByEmail($member['email']);

                $updateUser = $this->aauth->update_user($user['id'], $this->input->post("email"), $this->input->post("email"), $this->input->post("email"));

                if($updateUser){

                    $res = array("status" => "1", "msg" => "Successfully update password!");

                }else{

                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

                }

            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-user-circle-o'></i> Member" => base_url()."member",
                "Edit Member" => base_url()."member/member/edit/".$id,
            );

            $data['page'] = 'member_edit';
            $data['page_title'] = 'Member';
            $data['page_subtitle'] = 'Edit Member';
            $data['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MMember->getMemberById($id);
            $data['data']['bpr'] = $this->Mbpr->getBPRById($data['data']['dataedit']['bpr']);
            $data['data']['dpd'] = $this->Mdpd->getDpd();
            $data['data']['job_position'] = $this->MJobposition->getJobposition();
            $data['data']['parent_menu'] = 'member_management';
            $data['data']['submenu'] = 'member';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id_member = $this->input->post("id");

            $member = $this->MMember->getMemberById($id_member);

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            $photo = $upload_path.'/'.$member['photo'];

            if(file_exists($photo)){

                @unlink($photo);

            }

            $this->db->where($this->pk, $id_member);
            $delete = $this->db->delete($this->table);

            if($delete){


                $email = $member['email'];

                $user = $this->Muser2->getUserByEmail($email);

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

                $photo = $upload_path.'/'.$user['picture'];

                if(file_exists($photo)){

                    @unlink($photo);

                }

                $delete = $this->aauth->delete_user($user['id']);

                if($delete){
                    $res = array("status" => "1", "msg" => "Successfully delete data!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }

                echo json_encode($res);
                // $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                echo json_encode($res);
            }
            
        }

    }

    function getBpr(){

        if(count($_POST)>0){

            $id_dpd = $this->input->post("dpd");

            $id_bpr = 0;
            if(isset($_POST['bpr'])){
                $id_bpr = $this->input->post("bpr");
            }

            $bpr = $this->Mbpr->getBPRByDPDId($id_dpd);

            $dataBpr = "";

            foreach ($bpr as $b){
                if($b['id']==$id_bpr){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $dataBpr .= "<option ".$selected." value='".$b['id']."'>".$b['name']."</option>";
            }

            echo json_encode($dataBpr);

        }

    }

}