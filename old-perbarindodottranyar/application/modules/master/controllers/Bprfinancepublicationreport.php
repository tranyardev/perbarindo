<?php

class Bprfinancepublicationreport extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbprfinancepublication");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr_finance_publication_reports";
        $this->dttModel = "Mdbprfinancepublication";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Laporan Publikasi Keuangan";
        $data['page_subtitle'] = "Modul Laporan Publikasi Keuangan BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_report', 
            'submenu' => 'bpr_finance_publication_report' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Laporan Publikasi'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Laporan Publikasi</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Upload',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprfinancepublicationreport/add';
                }"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "report_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "report_year" => array(

                "data" => "a.year",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "report_attachement" => array(

                "data" => "$.attachement",
                "searchable" => false,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "name" => array(

                "label" => "Judul Laporan",
                "type" => "text",
                "placeholder" => "Nama/Judul Laporan",
                "class" => "form-control validate[required]",

            ),
            "year" => array(

                "label" => "Tahun",
                "type" => "text",
                "placeholder" => "Tahun",
                "class" => "form-control validate[required]",

            ),
            "attachement" => array(

                "label" => "File Lampiran",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/media/',
                "id" => "attachement",

            )


        );


    }

    public function add(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                    $_FILES["attachement"]["type"]=="application/vnd.ms-excel" ||
                    $_FILES["attachement"]["type"]=="text/csv" ||
                    $_FILES["attachement"]["type"]=="application/pdf" ||
                    $_FILES["attachement"]["type"]=="application/x-pdf" ||
                    $_FILES["attachement"]["type"]=="image/png" ||
                    $_FILES["attachement"]["type"]=="image/jpg" ||
                    $_FILES["attachement"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                    $array = explode('.', $_FILES['attachement']['name']);
                    $extension = end($array);
                    $attachement = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["attachement"]["tmp_name"], $upload_path."/".$attachement)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    $_POST['attachement'] = $attachement;


                }else{


                    $res['msg'] = "Invalid attachement file! Should be XLS,XLSX,PDF,PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Upload Laporan Publikasi";
            $data['page_subtitle'] = "Modul Laporan Publikasi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["attachement"]["name"]!=""){

                    if($_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                        $_FILES["attachement"]["type"]=="application/vnd.ms-excel" ||
                        $_FILES["attachement"]["type"]=="text/csv" ||
                        $_FILES["attachement"]["type"]=="application/pdf" ||
                        $_FILES["attachement"]["type"]=="application/x-pdf" ||
                        $_FILES["attachement"]["type"]=="image/png" ||
                        $_FILES["attachement"]["type"]=="image/jpg" ||
                        $_FILES["attachement"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                        $array = explode('.', $_FILES['attachement']['name']);
                        $extension = end($array);
                        $attachement = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_attachement'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["attachement"]["tmp_name"], $upload_path."/".$attachement)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid attachement file! Should be XLS,XLSX,PDF,PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $attachement = $this->input->post("old_attachement");

                }

                $_POST['attachement'] = $attachement;

            }

            if(isset($_POST['old_attachement'])){
                unset($_POST['old_attachement']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];


            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Edit Laporan Publikasi";
            $data['page_subtitle'] = "Modul Laporan Publikasi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "cover";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}