<?php

class Corporate extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('excel');
        $this->load->model("mcore");
        $this->load->model("Mcorporate");


        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'corporate_view';
            $this->add_perm = 'corporate_add';
            $this->edit_perm = 'corporate_update';
            $this->delete_perm = 'corporate_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "corporates";
        $this->dttModel = "Mdcorporate";
        $this->pk = "id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> Corporate " => base_url()."corporate"
        );

        $data['page'] = 'corporate';
        $data['page_title'] = $this->lang->line('master_data');
        $data['page_subtitle'] = $this->lang->line('corporate_list');
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'corporate';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description")

            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('corporate_list') => base_url()."master/corporate",
                "Add New" => base_url()."master/corporate/addnew",
            );

            $data['page'] = 'corporate_add';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('corporate_add');
        
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'corporate';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){


            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
            
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('corporate_list') => base_url()."master/corporate",
                "Edit" => base_url()."master/corporate/edit/".$id,
            );

            $data['page'] = 'corporate_edit';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('corporate_edit');
           
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->Mcorporate->getcorporateById($id);
            $data['data']['parent_menu'] = 'corporates';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

}