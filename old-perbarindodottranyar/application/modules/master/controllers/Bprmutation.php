<?php

class Bprmutation extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbprmutation");
        $this->load->model("Memployee");
        $this->load->model("Mbprdirectorposition");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr_job_mutations";
        $this->dttModel = "Mdbprmutation";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Mutasi Jabatan";
        $data['page_subtitle'] = "Modul Mutasi Jabatan BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_directors', 
            'submenu' => 'bpr_mutation' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_mutation_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_mutation_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_mutation_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_mutation_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Mutasi Jabatan'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Mutasi Jabatan</span>'
            
            ),
            'colvis'

        );

    }

    private function _get_datatable_columns(){


        return array(

            "mutation_member" => array(

                "data" => "$.member_name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "mutation_from_position" => array(

                "data" => "$.from_position",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "mutation_to_position" => array(

                "data" => "$.to_position",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "mutation_date" => array(

                "data" => "a.date",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "member_id" => array(

                "label" => "Pegawai",
                "type" => "sourcequery",
                "class" => "form-control validate[required]",
                "source" => $this->Memployee->getEmployee(),

            ),
            "position" => array(

                "label" => "Posisi Direksi",
                "type" => "sourcequery",
                "class" => "form-control validate[required]",
                "source" => $this->Mbprdirectorposition->getBPRDirectorPosition(),

            )

        );


    }

    public function add(){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];


            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Tambah Jabatan Direksi";
            $data['page_subtitle'] = "Modul Jabatan Direksi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));
            $director = $this->Mbprdirector->getBPRDirectorById($id);

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;

            $this->load->library("Cinta",$data);
            $res = $this->cinta->process();

            if($res['status']=="1"){

                if($director['position'] != $_POST['position'] && $director['member_id'] == $_POST['member_id']){

                    $data = array(

                        "from_job_position" => $director['position'],
                        "to_job_position" => $_POST['position'],
                        "member_id" => $_POST['member_id'],
                        "date" => date("Y-m-d h:i:s"),
                        "created_by" => $this->session->userdata('id'),
                        "updated_by" => $this->session->userdata('id'),
                        "created_at" => date("Y-m-d h:i:s"),
                        "updated_at" => date("Y-m-d h:i:s"),

                    );


                    $this->db->insert("bpr_job_mutations", $data);

                }

            }

            echo json_encode($res);  

        }else{

            $data['page_title'] = "Edit Jabatan Direksi";
            $data['page_subtitle'] = "Modul Jabatan Direksi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}