<?php

class Bpremployee extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Memployee");
        $this->load->model("Mjobposition");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "members";
        $this->dttModel = "Mdemployee";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Pegawai";
        $data['page_subtitle'] = "Modul Pegawai BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_directors', 
            'submenu' => 'bpr_employee' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "order" => [[1, "asc" ]],
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip",
            "drawCallback" => "function( settings ) {
                $('img.scale').imageScale();
            }"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Pegawai'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Pegawai</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bpremployee/add';
                }"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "employee_photo" => array(

                "data" => "$.photo",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "employee_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "employee_gender" => array(

                "data" => "$.vgender",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "employee_job_position" => array(

                "data" => "$.job_position",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "employee_email" => array(

                "data" => "a.email",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "photo" => array(

                "label" => "Foto",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/photo',
                "id" => "photo",

            ),
            "name" => array(

                "label" => "Nama",
                "type" => "text",
                "placeholder" => "Nama Lengkap",
                "class" => "form-control validate[required]"

            ),
            "gender" => array(

                "label" => "Jenis Kelamin",
                "type" => "sourcearray",
                "source" => array("L" => "Laki - laki", "P" => "Perempuan"),
                "class" => "form-control validate[required]"
            ),
            "no_hp" => array(

                "label" => "No HP",
                "type" => "text",
                "placeholder" => "Nomor Ponsel",
                "class" => "form-control validate[required]"

            ),
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]"

            ),
            "job_position" => array(

                "label" => "Jabatan",
                "type" => "sourcequery",
                "class" => "form-control validate[required]",
                "source" => $this->Mjobposition->getJobposition(),

            ),
            "status" => array(

                "label" => "Status",
                "type" => "sourcearray",
                "source" => array("1" => "Active", "0" => "Inactive"),
                "class" => "form-control validate[required]"
            )


        );


    }

    public function add(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    $_POST['photo'] = $photo;


                }else{


                    $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Tambah Pegawai";
            $data['page_subtitle'] = "Modul Pegawai BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["photo"]["name"]!=""){

                    if($_FILES["photo"]["type"]=="image/png" or
                        $_FILES["photo"]["type"]=="image/jpg" or
                        $_FILES["photo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_photo'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $photo = $this->input->post("old_photo");

                }

                $_POST['photo'] = $photo;

            }

            if(isset($_POST['old_photo'])){
                unset($_POST['old_photo']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Edit Pegawai";
            $data['page_subtitle'] = "Modul Pegawai BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "photo";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}