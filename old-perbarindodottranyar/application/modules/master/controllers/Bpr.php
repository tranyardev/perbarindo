<?php

class Bpr extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('excel');
        $this->load->model("mcore");
        $this->load->model("Mbpr");
        $this->load->model("Mdpd");
        $this->load->model("Mcorporate");


        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'bpr_view';
            $this->add_perm = 'bpr_add';
            $this->edit_perm = 'bpr_update';
            $this->delete_perm = 'bpr_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "bpr";
        $this->dttModel = "Mdbpr";
        $this->pk = "id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> bpr " => base_url()."bpr"
        );

        $data['page'] = 'bpr';
        $data['page_title'] = $this->lang->line('master_data');
        $data['page_subtitle'] = $this->lang->line('bpr_list');
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'bpr';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(

                "name" => $this->input->post("name"),
                "address" => $this->input->post("address"),
                "dpd" => $this->input->post("dpd"),
                "email" => $this->input->post("email"),
                "telp" => $this->input->post("telp"),
                "fax" => $this->input->post("fax"),
                "website" => $this->input->post("website"),
                "corporate" => $this->input->post("corporate")

            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('bpr_list') => base_url()."master/bpr",
                "Add New" => base_url()."master/bpr/addnew",
            );

            $data['page'] = 'bpr_add';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('bpr_add');
            $data['dpd'] =  $this->Mdpd->getDPD();
            $data['corporates'] =  $this->Mcorporate->getCorporate();
        
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'bpr';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){


            $data = array(

                "name" => $this->input->post("name"),
                "address" => $this->input->post("address"),
                "dpd" => $this->input->post("dpd"),
                "email" => $this->input->post("email"),
                "telp" => $this->input->post("telp"),
                "fax" => $this->input->post("fax"),
                "website" => $this->input->post("website"),
                "corporate" => $this->input->post("corporate")
            
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('bpr_list') => base_url()."master/bpr",
                "Edit" => base_url()."master/bpr/edit/".$id,
            );

            $data['page'] = 'bpr_edit';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('bpr_edit');
            
            $data['dpd'] =  $this->Mdpd->getDPD();
            $data['corporates'] =  $this->Mcorporate->getCorporate();

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->Mbpr->getBPRById($id);
            $data['data']['parent_menu'] = 'bprs';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }
    function import(){


        if($_FILES["excel"]["name"]!=""){

            if($_FILES["excel"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
                $_FILES["excel"]["type"]=="application/vnd.ms-excel" or
                $_FILES["excel"]["type"]=="application/x-msexcel" or
                $_FILES["excel"]["type"]=="application/x-ms-excel" or
                $_FILES["excel"]["type"]=="application/x-excel" or
                $_FILES["excel"]["type"]=="application/x-dos_ms_excel" or
                $_FILES["excel"]["type"]=="application/xls" or
                $_FILES["excel"]["type"]=="application/wps-office.xlsx" or
                $_FILES["excel"]["type"]=="application/x-xls" or
                $_FILES["excel"]["type"]=="application/msexcel"){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/excel';
                $array = explode('.', $_FILES['excel']['name']);
                $extension = end($array);
                $file = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["excel"]["tmp_name"], $upload_path."/".$file)) {

                    $import = $this->import_from_excel($upload_path."/".$file); 

                    if($import){

                        @unlink($upload_path."/".$file);

                    }

                    $res = array("status" => "1", "msg" => "Data imported Successfully");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Something went wrong while uploading file");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid file extention. Must be .xls or .xlsx");

            }

            echo json_encode($res);

        }

    }

    function import_from_excel($file){

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          
        for($i=2;$i<=$totalrows;$i++)
        {

                $corporate= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
                $name= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                $address= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                $telp= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                $email= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                $fax= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 
                $dpd= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                $website= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();

                $dpd = $this->Mdpd->getDPDByName(trim($dpd));
                $corporate = $this->Mcorporate->getCorporateByName(trim($corporate));

                $data = array(
                    "name" => $name, 
                    "corporate" => @$corporate[0]['id'],
                    "dpd" => @$dpd[0]['id'],
                    "address" => $address,
                    "telp" => $telp,
                    "email" => $email,
                    "fax" => $fax,
                    "website" => $website,
                );

                $exist = $this->db->get_where($this->table, array('name' => $name));

                if($exist->num_rows()<1){

                    $this->db->insert($this->table,$data);

                }

        }   

        return true;

    }

}