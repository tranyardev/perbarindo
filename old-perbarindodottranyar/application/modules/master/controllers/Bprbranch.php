<?php

class Bprbranch extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbpr");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprfinanceasset");
        $this->load->model("Mbprdirectorposition");
        $this->load->model("Mbprdirector");
        $this->load->model("MCorporate");
        $this->load->model("Mdpd");
        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr";
        $this->dttModel = "Mdbprbranch";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Cabang BPR";
        $data['page_subtitle'] = "Modul Cabang BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'master', 
            'submenu' => 'bpr_branch' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_branch_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_branch_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_branch_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_branch_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Cabang BPR'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Cabang BPR</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprbranch/add';
                }"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "branch_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            
            "branch_address" => array(

                "data" => "a.address",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "branch_telp" => array(

                "data" => "a.telp",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

           
            "name" => array(

                "label" => "Nama Cabang",
                "type" => "text",
                "placeholder" => "Nama Cabang",
                "class" => "form-control validate[required]"

            ),
            "address" => array(

                "label" => "Alamat",
                "type" => "textarea",
                "placeholder" => "Alamat cabang",
                "class" => "form-control validate[required]"
            ),
            "telp" => array(

                "label" => "Telp.",
                "type" => "text",
                "input_type" => "number",
                "placeholder" => "No Telepon",
                "class" => "form-control",


            ),
            "fax" => array(

                "label" => "Fax.",
                "type" => "text",
                "input_type" => "number",
                "placeholder" => "Fax",
                "class" => "form-control",
                
            ),
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required, custom[email]]",
                
            ),
            "location" => array(

                "label" => "Lokasi",
                "type" => "geolocation",
                "class" => "form-control",
                "libs_js" => array(
                    "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                    "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                ),
                "libs_css" => array(
                    "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmap.css",
                ),
            ),

        );


    }

    public function add(){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['is_branch'] = "1";
            $_POST['central_branch'] = $member['bpr'];
            $_POST['dpd'] = $member['dpd'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Tambah Cabang BPR";
            $data['page_subtitle'] = "Modul Cabang BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";
           

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['is_branch'] = "1";
            $_POST['central_branch'] = $member['bpr'];
            $_POST['dpd'] = $member['dpd'];

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Edit Cabang BPR";
            $data['page_subtitle'] = "Modul Profile BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "cover";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}