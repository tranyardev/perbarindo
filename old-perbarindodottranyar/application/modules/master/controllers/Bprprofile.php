<?php

class Bprprofile extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbpr");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprfinanceassettype");
        $this->load->model("Mbprfinanceasset");
        $this->load->model("Mbprdirectorposition");
        $this->load->model("Mbprdirector");
        $this->load->model("MCorporate");
        $this->load->model("Mdpd");
        $this->load->model("Memployee");
        $this->load->model("Mbprcontactperson");
        $this->load->model("Mbprfinancepublication");



        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr";
        $this->pk = "id";

    }

    function index(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $data['bpr'] = $this->Mbpr->getBPRById($member['bpr']);

        $data['director_position'] = $this->Mbprdirectorposition->getBPRDirectorPositionByBPR($member['bpr'],"parent");
        $data['assettype'] = $this->Mbprfinanceassettype->getBPRFinanceAssetType($member['bpr']);
        $data['asset'] = $this->Mbprfinanceasset->getBPRFinanceAssetByBPR($member['bpr']);
        $data['contact_person'] = $this->Mbprcontactperson->getBPRContactPerson($member['bpr']);
        $data['branches'] = $this->Mbpr->getBPRBranches($member['bpr']);
        $data['report'] = $this->Mbprfinancepublication->getBPRFinancePublication($member['bpr']);

       
        $data['page'] = "bpr_profile";
        $data['page_title'] = "Profile BPR ".$member['bpr_name'];
        $data['page_subtitle'] = "Profile BPR";
  
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
    
        $this->load->view('layout/body',$data);

    }

    public function get_finance_asset(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));
        $assettype = $this->Mbprfinanceassettype->getBPRFinanceAssetType($member['bpr']);
        $data = array();
        foreach ($assettype as $at) {
            
            $asset = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($member['bpr'],$at['id']);

            $data[] =  array("type" => $at['slug'], "value" => @$asset['value']); 

        }

        $res = array(
            "status" => "1",
            "data" => $data
        );

        echo json_encode($res);

    }

    public function save_asset(){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $assettype = $this->Mbprfinanceassettype->getBPRFinanceAssetType($member['bpr']);

            $data = array();

            foreach ($assettype as $at) {
            
                $asset = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($member['bpr'],$at['id']);

                if($asset!=null){
                    /* do update */

                    $update = array(
                        "value" => $_POST[$at['slug']],
                        "updated_at" => date("Y-m-d h:i:s"),
                        "updated_by" => $this->session->userdata('id')
                    );

                    $this->db->where("finance_asset_type", $at['id']);
                    $this->db->update("bpr_finance_assets", $update);


                }else{
                    /* do insert */

                    $insert = array(
                        "bpr" => $member['bpr'],
                        "finance_asset_type" => $at['id'],
                        "value" => $_POST[$at['slug']],
                        "created_at" => date("Y-m-d h:i:s"),
                        "created_by" => $this->session->userdata('id')
                    );

                    $this->db->insert("bpr_finance_assets", $insert);

                }

                 $data[] =  array("type" => $at['slug'], "value" => $_POST[$at['slug']]); 

            }

            $res = array(
                "status" => "1",
                "msg" => "Data berhasil tersimpan!",
                "data" => $data
            );

            echo json_encode($res);
        }

    }

    public function get_stockholder_list($level){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $stockholder = $this->Mbprstackholder->getBPRStockholderByLevel($member['bpr'],$level);

        $html = "";
        foreach ($stockholder as $list) {
            
            $html.= "<li><a href='javascript:edit_stockholder(".$list['id'].");'>".$list['name']."</a> (".$list['percentage']."%)</li>";

        }

        if($level == "PSP"){

            if(count($stockholder) < 1){
                $html.= '<li class="add_more"><a href="javascript:add_stackholder(\''.$level.'\')">+ Tambah Pemegang Saham</a></li>';
            }

        }else{

            $html.= '<li class="add_more"><a href="javascript:add_stackholder(\''.$level.'\')">+ Tambah Pemegang Saham</a></li>';
        
        }
        
        

        echo $html;

    }

    public function get_director_list($slug){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $director = $this->Mbprdirector->getBPRDirectorBySlug($member['bpr'],$slug);

        $extend_label = ""; 

        $html = "";
        foreach ($director as $list) {
            
            if($slug=="-"){
                $extend_label = " - ".$list['position_name']." - ".$list['position_title'];
            }

            $html.= "<li><a href='javascript:edit_director(".$list['id'].");'>".$list['name'].$extend_label."</a></li>";

        }


        if($slug=="komisaris"){

            $html.= '<li class="add_more"><a href="javascript:add_director(\''.$slug.'\')">+ Tambah Komisaris</a></li>';
        
        }else if($slug=="dirut"){
        

            if(count($director)<1){

                $html.= '<li class="add_more"><a href="javascript:add_director(\''.$slug.'\')">+ Tambah Direktur Utama</a></li>';

            }
        
        }else{

            $html.= '<li class="add_more"><a href="javascript:add_director(\''.$slug.'\')">+ Tambah Direktur</a></li>';
        
        }

        echo $html;

    }

    public function getOrgChartData(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $org = $this->Mbprdirector->getBPRChartOrg($member['bpr']);
        $orgTopLevel = $this->Mbprdirector->getBPRChartOrgTopLevel($member['bpr']);

        $chart = array();


        $primaryFields[] = "Name";
        $primaryFields[] = "Position";

        $i=1;
        foreach ($orgTopLevel as $otl) {

            $primaryFields[] = "Komisaris_".$i;
            $extraField["Komisaris_".$i] = $i.". ".$otl['name'];   
            $i++;

        }


        foreach ($org as $o) {

            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            if($o['photo'] != ""){

                if(file_exists($path.$o['photo'])){

                    $img = base_url().'upload/photo/'.$o['photo'];

                }else{

                    $img = base_url().'public/assets/img/default-avatar.png';

                }

            }else{

                $img = base_url().'public/assets/img/default-avatar.png';

            }
            
            array_push($chart, array("id" => $o['id'], "parentId" => $o['parent'], "Name" => $o['name'],"Position" => $o['position'], "pic" => $img));

        }

        if(count($orgTopLevel)>0){


            array_push($chart, array_merge($extraField,array("id" => @$orgTopLevel[0]['id'], "parentId" => null, "Name" => null, "Position" => @$orgTopLevel[0]['position'], "pic" => base_url().'public/assets/img/ceo-512.png')));

        }

        $res = array(
            "primaryFields" => $primaryFields,
            "sources" => $chart,
        );

        echo json_encode($res);

    }

    function testemail(){

$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>BPR Verifikasi</title>
    <!-- Designed by https://github.com/kaytcat -->
    <!-- Header image designed by Freepik.com -->

    <style type="text/css">
    /* Take care of image borders and formatting */

    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
    a img { border: none; }
    table { border-collapse: collapse !important; }
    #outlook a { padding:0; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass {width:100%;}
    .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
    table td {border-collapse: collapse;}
    .ExternalClass * {line-height: 115%;}


    /* General styling */

    td {
      font-family: Arial, sans-serif;
      color: #5e5e5e;
      font-size: 16px;
      text-align: left;
    }

    body {
      -webkit-font-smoothing:antialiased;
      -webkit-text-size-adjust:none;
      width: 100%;
      height: 100%;
      color: #5e5e5e;
      font-weight: 400;
      font-size: 16px;
    }


    h1 {
      margin: 10px 0;
    }

    a {
      color: #2b934f;
      text-decoration: none;
    }


    .body-padding {
      padding: 0 75px;
    }


    .force-full-width {
      width: 100% !important;
    }

    .icons {
      text-align: right;
      padding-right: 30px;
    }

    .logo {
      text-align: left;
      padding-left: 30px;
    }

    .computer-image {
      padding-left: 30px;
    }

    .header-text {
      text-align: left;
      padding-right: 30px;
      padding-left: 20px;
    }

    .header {
      color: #232925;
      font-size: 24px;
    }

  ul{

      list-style: none;

  }
  ul li ul{

      list-style: none;

  }
  
  ul li{
      padding: 10px;
      border-bottom: dashed 1px #aaa;
      border-left: dashed 1px #aaa;
  }


    </style>

    <style type="text/css" media="screen">
        @media screen {
          @import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
          /* Thanks Outlook 2013! */
          * {
            font-family: "PT Sans", "Helvetica Neue", "Arial", "sans-serif" !important;
          }
        }
    </style>

  </head>
  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tr>
      <td align="center" valign="top" style="background-color:#ffffff" width="100%">

      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320"  bgcolor="#232925">
          <tr>
            <td align="center" valign="top">

              <table class="force-full-width" cellspacing="0" cellpadding="0" bgcolor="#232925" style="width: 100%;">
                <tr>
                  <td style="background-color:#232925;padding-left: 30px;" class="logo">
                    <br>
                    <a href="#"><img src="http://perbarindo-event.tranyar.com/public/assets/img/logo.png" alt="Logo"></a>
                  </td>
                  <td class="icons">
                   
                  </td>
                </tr>
              </table>

              <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#232925" style="width: 100%;">
                <tr>
                  <td class="computer-image">
                    <br>
                    <br class="mobile-hide" />
                    <img style="display:block;" width="224" height="213" src="https://www.filepicker.io/api/file/CoMxXSlVRDuRQWNwnMzV" alt="hello">
                  </td>
                  <td style="color: #ffffff;" class="header-text">
                    <br>
                    <br>
                    <span style="font-size: 24px;">Hi Admin!</span><br>
                    Mohon verifikasi data struktur organisasi bpr dibawah ini.
                    <br>
                    <br>
                   
                  </td>
                </tr>
              </table>


              <table class="force-full-width" cellspacing="0" cellpadding="30" bgcolor="#ebebeb">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="0" class="force-full-width">
                      <tr>
                        <td>
                          <span style="color: #232925;font-size: 24px;">BPR MLATI PUNDI ARTHA</span> <br>
                          Mlati Pundi Artha, Jalan Kaliurang, Sardonoharjo, Sleman Regency, Special Region of Yogyakarta, Indonesia <br>
                          02312732739<br>
                          mlatipundiartha@gmail.com <br><br>
                        </td>
                        <td style="text-align:right; vertical-align:top;">
                          
                        </td>
                      </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                      <tr>
                        <td style="background-color:#dedede; padding: 5px; font-weight:bold; ">
                          Struktur Organisasi
                        </td>
                      </tr>
                      <tr>
                        <td style="background-color:#f3f3f3; padding: 10px 5px;">
                          <div class="col-md-12">
                        
                        <h4 style="border-bottom: dashed 1px #aaa;">Pemegang Saham</h4>
                        <ul class="list_people" style="list-style:none;">
                            <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">
                                <h5 style="border-bottom: dashed 1px #aaa;">Pemegang Saham Pengendali(PSP)</h5>
                                <ul  style="list-style:none;">
                                  <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">Indra Ganiarto (55%)</li>
                                </ul>
                            </li>
                             <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">
                                <h5 style="border-bottom: dashed 1px #aaa;">Pemegang Saham Biasa</h5>
                                <ul  style="list-style:none;">
                                    <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">Ayu Mustika</a> (45%)</li>
                                </ul>
                            </li>
                        </ul>

                        <h4 style="border-bottom: dashed 1px #aaa;">Komisaris</h4>

                        <ul class="list_people"  style="list-style:none;">
                          <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">Reina Trendl</li>
                          <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">IU</li>
                        </ul>

                        <h4 style="border-bottom: dashed 1px #aaa;">Direksi</h4>
                        <ul class="list_people"  style="list-style:none;">

                            <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">
                                <h5 style="border-bottom: dashed 1px #aaa;">Direktur Utama</h5>
                                <ul style="list-style:none;"><li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">Yoona</li></ul>
                            </li>
                            <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">
                              <h5 style="border-bottom: dashed 1px #aaa;">Direktur</h5>
                                <ul  style="list-style:none;">
                                  <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">Tsubasa Honda - Direktur I - Pemasaran</li>
                                  <li style="padding: 10px;border-bottom: dashed 1px #aaa;border-left: dashed 1px #aaa;">J Fla - Direktur II - Development</li>
                                </ul>
                            </li>
                           
                        </ul>

                    </div>
                        </td>
                      <tr>
                      <tr>
                        <td>
                          <br>
                          <div><!--[if mso]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:40px;v-text-anchor:middle;width:150px;" stroke="f" fillcolor="#2b934f">
                              <w:anchorlock/>
                              <center>
                            <![endif]-->
                                <a href="http://perbarindo-event.tranyar.com"
                            style="background-color:#2b934f;color:#ffffff;display:inline-block;font-family:Helvetcia, sans-serif;font-size:16px;font-weight:light;line-height:40px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;width:100%;">Aktifasi</a>
                                <br>
                              <a href="http://perbarindo-event.tranyar.com"
                            style="background-color:red;color:#ffffff;display:inline-block;font-family:Helvetcia, sans-serif;font-size:16px;font-weight:light;line-height:40px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;width:100%;margin-top: 10px;">Data Tidak Sesuai</a>
                              <!--[if mso]>
                                </center>
                              </v:rect>
                            <![endif]-->
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>


              <table class="force-full-width" cellspacing="0" cellpadding="20" bgcolor="#2b934f" style="width: 100%;">
                <tr>
                  <td style="background-color:#232925; color:#ffffff; font-size: 14px; text-align: center;">&copy; PERBARINDO 2017 All Rights Reserved
                  </td>
                </tr>
              </table>


            </td>
          </tr>
        </table>

      </center>
      </td>
    </tr>
  </table>
  </body>
</html>';
    
        $email = "indra.developer.web.id@gmail.com";
        $subject = "PERBARINDO - VERIFIKASI BPR MLATI PUNDI ARTHA";
        $from = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $company = "PERBARINDO";

        if($this->send_mail($email, $subject, $message, @$from["value"], $company)){
            echo "Mail Sent";
        }else{
            echo "Failed";
        }

    }



    function send_mail($email, $subject, $message, $from, $company){

        $smtp_host = $this->Mbpr->getConfigurationByKey("SMTP_HOST", "EMAIL");
        $smtp_port = $this->Mbpr->getConfigurationByKey("SMTP_PORT", "EMAIL");
        $smtp_user = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $smtp_pass = $this->Mbpr->getConfigurationByKey("SMTP_PASSWORD", "EMAIL");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => @$smtp_host['value'],
            'smtp_port' => @$smtp_port['value'],
            'smtp_user' => @$smtp_user['value'],
            'smtp_pass' => @$smtp_pass['value'],
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from($from, $company);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
          return true;
        }
         else
        {
          return false;
        }
    }    

    public function request_verification(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        /* check stackholder */

        // psp

        $psp = $this->Mbprstackholder->getBPRStockholderByLevel($member['bpr'],"PSP");

        // regular

        $regular = $this->Mbprstackholder->getBPRStockholderByLevel($member['bpr'],"REGULAR");

        /* check director */

        // komisaris

        $komisaris = $this->Mbprdirector->getBPRDirectorBySlug($member['bpr'],"komisaris");

        // dirut

        $dirut = $this->Mbprdirector->getBPRDirectorBySlug($member['bpr'],"dirut");

        // direktur

        $dir = $this->Mbprdirector->getBPRDirectorBySlug($member['bpr'],"-");

        $stackholder_psp_count = count($psp);
        $stackholder_regular_count = count($regular);
        $komisaris_count = count($komisaris);
        $dirut_count = count($dirut);
        $dir_count = count($dir);

        $errors = array();

        if($stackholder_psp_count < 1){

            $errors[] = "Mohon lengkapi data Pemegang Saham Pengendali.";

        }

        if($stackholder_regular_count < 1){

            $errors[] = "Mohon lengkapi data Pemegang Saham Lainnya.";

        }

        if($komisaris_count < 1){

            $errors[] = "Mohon lengkapi data Komisaris.";

        }

        if($dirut_count < 1){

            $errors[] = "Mohon lengkapi data Direktur Utama.";

        }

        if($dir_count < 1){

            $errors[] = "Mohon lengkapi data Direktur.";

        }

        if(count($errors)<1){
            
            /* send email request verification */


            $res = array(

                "status" => "1",
                "msg" => "Request Anda telah berhasil dikirim dan segera kami verifikasi."

            );

        }else{

             $res = array(

                "status" => "0",
                "msg" => "Maaf, request anda tidak bisa kami proses karena kelengkapan data belum terpenuhi.",
                "error" => $errors

            );

        }

        echo json_encode($res);


    }

    public function edit_stockholder($id=null){

        if($id!=null){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $stockholder = $this->Mbprstackholder->getBPRStackholderById($id);

            if(count($stockholder)>0){

                $res = array(

                    "status" => "1",
                    "data" => $stockholder

                );
                
            }else{


                $res = array(

                    "status" => "0",
                    "data" => array()

                );

            }

            echo json_encode($res);
        }
        
        
    }

    public function edit_director($id=null){

        if($id!=null){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $director = $this->Mbprdirector->getBPRDirectorDetail($id);

            if($director['gender'] == "L"){
                $gender = "<option value='L' selected >Laki - laki</option><option value='P'>Perempuan</option>";
            }else{
                $gender = "<option value='L'>Laki - laki</option><option selected value='P' selected>Perempuan</option>";
            }

            $photo = "";

            if($director['photo'] != ""){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                $file = $upload_path.'/'.$director['photo'];

                if(file_exists($file)){

                    $photo = base_url().'upload/photo/'.$director['photo'];

                }

            }

            if(count($director)>0){

                $res = array(

                    "status" => "1",
                    "data" => $director,
                    "gender" => $gender,
                    "photo" => $photo

                );
                
            }else{


                $res = array(

                    "status" => "0",
                    "data" => array()

                );

            }

            echo json_encode($res);
        }
        
        
    }

    public function get_stockholder(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $stockholder = $this->Mbprstackholder->getBPRStockholderByBPR($member['bpr']);

        $res = array();

        foreach ($stockholder as $s){
                
            $a = array( $s['name'], (float)$s['percentage']);
            
            array_push($res, $a);

        }

        echo json_encode($res);

    }

    public function add_director(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'upload/photo';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                

                }else{


                    $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $created_by = $this->session->userdata("id");
            $created_at = date("Y-m-d h:i:s");
            $bpr = $member['bpr'];

            if($_POST['position_slug']=="-"){

                $parent = $this->Mbprdirectorposition->getBPRDirectorPositionBySlug("dirut");
                
                $director_position = array(

                    "name" => $_POST['label'],
                    "parent" => $parent['id'],
                    "slug" => "-",
                    "bpr" => $bpr,
                    "position_title" => $_POST['position_title'],
                    "created_by" => $created_by,
                    "created_at" => $created_at

                );

                $insert_position = $this->db->insert("director_positions", $director_position);

                if($insert_position){

                    $position = $this->db->insert_id();

                }


            }

           

            $data = array(

                "name" => $_POST['name'],
                "gender" => $_POST['gender'],
                "no_hp" => $_POST['no_hp'],
                "email" => $_POST['email'],
                "status" => "1",
                "bpr" => $bpr,
                "created_at" => $created_at,
                "created_by" => $created_by,

            );

            if(isset($photo)){
                $data["photo"] = $photo;
            }


            $insert = $this->db->insert("members", $data);

            if($insert){

                $member_id = $this->db->insert_id();

                if($_POST['position_slug']!="-"){

                    $p = $this->Mbprdirectorposition->getBPRDirectorPositionBySlug($_POST['position_slug']);
                    $position = $p['id'];

                }
                
                $date = urldecode($_POST['valid_date']);
                $ex_date = explode("-",$date);
                $start_date = trim($ex_date[0]);
                $end_date = trim($ex_date[1]);

                $data = array(

                    "member_id" => $member_id,
                    "valid_start_date" => $start_date,
                    "valid_end_date" => $end_date,
                    "bpr" => $bpr,
                    "position" => $position,
                    "created_at" => $created_at,
                    "created_by" => $created_by,

                );

                

                $insert = $this->db->insert("directors", $data);

                if($insert){

                    $res = array(
                        "status" => "1",
                        "msg" => "Data berhasil ditambahkan.",
                    );

                }else{

                    $res = array(
                        "status" => "0",
                        "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba lagi.",
                    );

                }

            }else{

                $res = array(
                    "status" => "0",
                    "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba lagi.",
                );

            }

            echo json_encode($res);

        }

    }

     public function update_director(){

        if(count($_POST)>0){

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    $old_photo = $upload_path.'/'.@$_POST['old_director_photo'];

                    if(file_exists($old_photo)){

                        @unlink($old_photo);

                    }


                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {


                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                }else{


                    $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $photo = $this->input->post("old_director_photo");

            }


            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $updated_by = $this->session->userdata("id");
            $updated_at = date("Y-m-d h:i:s");
            $bpr = $member['bpr'];

            $data = array(

                "name" => $_POST['name'],
                "gender" => $_POST['gender'],
                "no_hp" => $_POST['no_hp'],
                "email" => $_POST['email'],
                "status" => "1",
                "bpr" => $bpr,
                "updated_at" => $updated_at,
                "updated_by" => $updated_by,

            );

            if(isset($photo)){
                $data["photo"] = $photo;
            }

            $this->db->where("id", $_POST['member_id']);
            $update = $this->db->update("members", $data);

            if($update){

                
                $date = urldecode($_POST['valid_date']);
                $ex_date = explode("-",$date);
                $start_date = trim($ex_date[0]);
                $end_date = trim($ex_date[1]);

                $data = array(

                   
                    "valid_start_date" => $start_date,
                    "valid_end_date" => $end_date,
                    "updated_at" => $updated_at,
                    "updated_by" => $updated_by,

                );

                $this->db->where("id", $_POST['director_id']);
                $update = $this->db->insert("directors", $data);

                if($update){

                    $res = array(
                        "status" => "1",
                        "msg" => "Data berhasil diupdate.",
                    );

                }else{

                    $res = array(
                        "status" => "0",
                        "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba lagi.",
                    );

                }

            }else{

                $res = array(
                    "status" => "0",
                    "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba lagi.",
                );

            }

           

        }else{

            $res = array(
                "status" => "0",
                "msg" => "Oops! Telah terjadi kesalahan. Request param kosong.",
            );

        }

        echo json_encode($res);

    }

    public function remove_director(){

        if(count($_POST)>0){

            $director_id = $_POST['id'];
            $member_id = $_POST['member_id'];

            $member = $this->Memployee->getEmployeeById($member_id);

            if($member['photo']!=""){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                $old_photo = $upload_path.'/'.$member['photo'];

                if(file_exists($old_photo)){

                    @unlink($old_photo);

                }

            }

            $this->db->where("id", $director_id);
            $delete = $this->db->delete("directors");

            if($delete){

                $this->db->where("id", $member_id);
                $delete = $this->db->delete("members");

                if($delete){
                    $res = array(
                        "status" => "1",
                        "msg" => "Data berhasil dihapus.",
                    );
                }else{
                    $res = array(
                        "status" => "0",
                        "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba lagi.",
                    );
                }

            }
            
            echo json_encode($res);

        }

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_profile', 
            'submenu' => 'bpr_employee' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_delete"),

        );

    }
    
    private function _get_fields_edit(){


        return array(

           
            "corporate" => array(

                "label" => "Tipe Perusahaan",
                "type" => "sourcequery",
                "source" => $this->MCorporate->getCorporate(),
                "class" => "form-control validate[required]",
                
            ),
            "dpd" => array(

                "label" => "DPD",
                "type" => "sourcequery",
                "source" => $this->Mdpd->getDPD(),
                "class" => "form-control validate[required]",
                
            ),
            "name" => array(

                "label" => "Nama",
                "type" => "text",
                "placeholder" => "Nama BPR",
                "class" => "form-control validate[required]"

            ),
            "about" => array(

                "label" => "Keterangan",
                "type" => "editor",
                "class" => "form-control validate[required]",
                "id" => "about"
            ),
            "address" => array(

                "label" => "Alamat",
                "type" => "textarea",
                "class" => "form-control validate[required]"
            ),
            "telp" => array(

                "label" => "Telp.",
                "type" => "text",
                "input_type" => "number",
                "placeholder" => "No Telepon",
                "class" => "form-control",


            ),
            "fax" => array(

                "label" => "Fax.",
                "type" => "text",
                "input_type" => "number",
                "placeholder" => "Fax",
                "class" => "form-control",
                
            ),
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required, custom[email]]",
                
            ),
            "website" => array(

                "label" => "Website",
                "type" => "text",
                "placeholder" => "http://bprdomain.com",
                "class" => "form-control",
                
            ),
            "fb" => array(

                "label" => "Facebook",
                "type" => "text",
                "placeholder" => "http://facebook.com/namaprofile",
                "class" => "form-control",
                
            ),
            "twt" => array(

                "label" => "Twitter",
                "type" => "text",
                "placeholder" => "http://twitter.com/namaprofile",
                "class" => "form-control",
                
            ),
            "gplus" => array(

                "label" => "Google Plus",
                "type" => "text",
                "placeholder" => "http://plus.google.com/namaprofile",
                "class" => "form-control",
                
            ),
            "insta" => array(

                "label" => "Instagram",
                "type" => "text",
                "placeholder" => "http://instagram.com/namaprofile",
                "class" => "form-control",
                
            ),
            "cover" => array(

                "label" => "Cover",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/bpr',
                "id" => "cover",

            ),
            "logo" => array(

                "label" => "Logo",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/bpr',
                "id" => "logo",

            ),
            "location" => array(

                "label" => "Location",
                "type" => "geolocation",
                "class" => "form-control",
                "libs_js" => array(
                    "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                    "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                ),
                "libs_css" => array(
                    "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmap.css",
                ),
            ),

        );


    }

    public function edit(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $id = $member['bpr'];

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["cover"]["name"]!=""){

                    if($_FILES["cover"]["type"]=="image/png" or
                        $_FILES["cover"]["type"]=="image/jpg" or
                        $_FILES["cover"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/bpr/';
                        $array = explode('.', $_FILES['cover']['name']);
                        $extension = end($array);
                        $cover = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_cover'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $cover = $this->input->post("old_cover");

                }

                if($_FILES["logo"]["name"]!=""){

                    if($_FILES["logo"]["type"]=="image/png" or
                        $_FILES["logo"]["type"]=="image/jpg" or
                        $_FILES["logo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/bpr/';
                        $array = explode('.', $_FILES['logo']['name']);
                        $extension = end($array);
                        $logo = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_logo'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $upload_path."/".$logo)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid logo file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $logo = $this->input->post("old_logo");

                }

                $_POST['cover'] = $cover;
                $_POST['logo'] = $logo;

            }

            if(isset($_POST['old_cover'])){
                unset($_POST['old_cover']);
            }

            if(isset($_POST['old_logo'])){
                unset($_POST['old_logo']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['page_title'] = "Edit Bpr Profile";
            $data['page_subtitle'] = "Modul Profile BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "photo";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}