<?php

class Mdbprfinancepublication extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_finance_publication_report_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_finance_publication_report_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(  

            "attachement" => "concat('<a href=\"".base_url()."upload/media/',a.attachement,'\" target=\"_blank\">Download</a>')"

        );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(
                "attachement" => "concat('<a href=\"".base_url()."upload/media/',a.attachement,'\" target=\"_blank\">Download</a>')",
                "op" => $op

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr_finance_publication_reports a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(
            "a.bpr" => $member['bpr']
        );
    }


}