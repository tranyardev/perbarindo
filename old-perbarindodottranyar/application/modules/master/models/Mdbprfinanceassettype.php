<?php

class Mdbprfinanceassettype extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_finance_asset_type_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_finance_asset_type_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array();

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr_finance_asset_types a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(
            "a.bpr" => $member['bpr']
        );
    }


}