<?php

class Mdbprproduct extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_product_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_product_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

                 "status" => "CASE WHEN a.status='1' THEN 'Published' ELSE 'Draft' END",
                 "category" => "b.name"

            );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op,
                "status" => "CASE WHEN a.status='1' THEN 'Published' ELSE 'Draft' END",
                "category" => "b.name"

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr_products a";
    }

    public function joinArray(){
        return array(
            "bpr_product_categories b|left" => "b.id=a.category"
        );
    }

    public function whereClauseArray(){
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(
            "a.bpr" => $member['bpr']
        );
    }


}