<?php

class Mbprauction extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRAuction(){

        $result = $this->db->query("SELECT * FROM bpr_auctions")->result_array();
        return $result;

    }
    function getCountBPRAuction($bpr){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE bpr='".$bpr."'")->num_rows();
        return $result;

    }
    function getBPRAuctionById($id){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}