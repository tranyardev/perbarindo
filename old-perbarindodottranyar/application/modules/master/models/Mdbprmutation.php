<?php

class Mdbprmutation extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        // $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_mutation_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_mutation_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

                 "from_position" => "c.name",
                 "to_position" => "d.name",
                 "member_name" => "b.name"

            );

        // if($this->allow_edit){
        //     $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        // }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($delete!=''){

            $op = "concat('".$delete."')";
            $str = array(

                "op" => $op,
                "from_position" => "c.name",
                "to_position" => "d.name",
                "member_name" => "b.name"

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr_job_mutations a";
    }

    public function joinArray(){
        return array(
            "members b|left" => "b.id=a.member_id",
            "director_positions c|left" => "c.id=a.from_job_position",
            "director_positions d|left" => "d.id=a.to_job_position",
        );
    }

    public function whereClauseArray(){
        
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(
            "a.bpr" => $member['bpr']
        );
        
    }


}