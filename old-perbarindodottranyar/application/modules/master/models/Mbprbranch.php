<?php

class Mbprbranch extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRBranch($bpr){

        $result = $this->db->query("SELECT * FROM bpr WHERE central_branch='".$bpr."'")->result_array();
        return $result;

    }
    function getCountBPRBranch($bpr){

        $result = $this->db->query("SELECT * FROM bpr WHERE central_branch='".$bpr."'")->num_rows();
        return $result;

    }
    function getBPRBranchById($id){

        $result = $this->db->query("SELECT * FROM bpr_contact_persons WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}