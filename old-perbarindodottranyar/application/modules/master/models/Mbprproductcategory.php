<?php

class Mbprproductcategory extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRProductCategory(){

        $result = $this->db->query("SELECT * FROM bpr_product_categories")->result_array();
        return $result;

    }
    function getBPRProductCategoryById($id){

        $result = $this->db->query("SELECT * FROM bpr_product_categories WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}