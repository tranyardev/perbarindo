<div class="row" id="form_wrapper">
    <form role="form" id="form">

        <div class="col-md-6">
   
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $this->lang->line('dpd_edit'); ?></h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label for="name"><?php echo $this->lang->line('name'); ?></label>
                        <input type="text" reqired name="name" class="form-control validate[required]" id="name" value="<?php echo $dataedit['name']; ?>">
                    </div>
                  
                    <div class="form-group">
                        <label for="description"><?php echo $this->lang->line('description'); ?></label>
                        <textarea class="form-control" name="description" id="description"><?php echo $dataedit['description']; ?></textarea>
                    </div>
                  
                </div>

                 <div class="box-footer">
                    &nbsp;<button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>

        </div>
        
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                toastr.info("Updating data...", 'Loading');
                $("#btn-submit").button("loading");
                saveFormData();

                return false;

            }

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"master/dpd/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();

        $.post(target, data, function(res){

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            $("#btn-submit").button("reset");
            resetForm();

        },'json');

    }

    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>