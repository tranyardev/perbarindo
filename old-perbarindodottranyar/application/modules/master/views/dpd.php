<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?php echo $this->lang->line('dpd_list'); ?></h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <input type="file" class="form-control" name="excel" id="excel" style="visibility: hidden;">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="<?php echo $this->lang->line('dpd_search'); ?>" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><?php echo $this->lang->line('name'); ?></th>
                <th><?php echo $this->lang->line('description'); ?></th>

                <?php
                    if($edit_perm!='1' && $delete_perm!='1'){

                    }else{
                        echo '<th style="width: 80px;">'.$this->lang->line('action').'</th>';
                    }
                ?>

            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    </form>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line('modal_delete_title'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo $this->lang->line('modal_delete_body'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                <button type="button" class="btn btn-primary" id="btn_action"><?php echo $this->lang->line('yes'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    var add_perm = '<?php echo $add_perm; ?>';
    var edit_perm = '<?php echo $edit_perm; ?>';
    var delete_perm = '<?php echo $delete_perm; ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy',
            exportOptions: {
                    columns: ':visible'
            }
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel',
            exportOptions: {
                    columns: ':visible'
            }
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV',
            exportOptions: {
                    columns: ':visible'
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Daftar DPD';
            }
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print',
            exportOptions: {
                    columns: ':visible'
            },
            
            title: function(){
                 return '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar DPD</span>';
            }

        },
        'colvis'
    ];

    var columns =  [

            { "data" : "a.name" },
            { "data" : "a.description"},
            { "data" : "$.op", "orderable": false, "searchable": false},

        ];

    if(edit_perm != '1' && delete_perm != '1'){

        columns = [

            { "data" : "a.name" },
            { "data" : "a.description"},

        ];

    }

    if(add_perm=='1'){

        buttons.push( {
            text: '<i class="fa fa-plus"></i> <?php echo $this->lang->line('add'); ?>',
            action: function ( e, dt, node, config ) {
                window.location.href = base_url + "master/dpd/addnew"
            }
        });

    }

    buttons.push( {
            text: '<i class="fa fa-file-excel-o"></i> Import Data',
            action: function ( e, dt, node, config ) {
               
                $("#excel").trigger("click");

            }
    });

    buttons.push( {
            text: '<i class="fa fa-file-excel-o"></i> Download Template',
            action: function ( e, dt, node, config ) {
                window.location.href = base_url + "/templates/excel/dpd-import-template.xlsx"
            }
    });

    $(document).ready(function(){

        InitDatatable();

        $("#excel").on("change",function(){

            var formData = new FormData($('#form')[0]);

            $.ajax({
                type: "POST",
                url: base_url + "master/dpd/import",
                data: formData,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {

                    toastr.info("Importing file...", 'Loading');
                    
                },
                success: function(res) {

                    if(res.status!="1"){

                        toastr.error(res.msg, 'Response Server');

                    }else{

                        toastr.success(res.msg, 'Response Server');

                    }

                    $('#dttable').dataTable().fnDestroy();

                    InitDatatable();
                  
                   
                },
                error: function() {

                    toastr.error('Something went wrong!', 'Response Server');
                }

            });

        });

    });

    function InitDatatable(){

        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "master/dpd/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons
        });

        $('#dt_search').keyup(function(){

            table.search($(this).val()).draw();

        });

    }

    function edit(id){
        window.location.href="<?php echo base_url(); ?>master/dpd/edit/"+id;
    }
    function remove(id){

        $("#modal_confirm").modal('show');
        $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

    }

    function proceedRemove(id){

        var target = '<?php echo base_url(); ?>master/dpd/remove';
        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

</script>
