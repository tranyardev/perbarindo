<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="content" id="post_content" value='<?php echo $dataedit['content']; ?>'>

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Page</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="title" class="form-control" id="title" value="<?php echo $dataedit['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="description">Content</label>
                        <textarea class="form-control" id="content"><?php echo $dataedit['content']; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0" <?php ($dataedit['status']=="0")? $attr="selected": $attr="" ;echo $attr;?>>Draft</option>
                            <option value="1" <?php ($dataedit['status']=="1")? $attr="selected": $attr="" ;echo $attr;?>>Publish</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">SEO</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label for="permalink">Permalink</label>
                        <input type="text" name="permalink" class="form-control" id="permalink" value="<?php echo $dataedit['permalink']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="meta_keyword">Meta keyword</label>
                        <input type="text" name="meta_keyword" class="form-control" id="meta_keyword" value="<?php echo $dataedit['meta_keywords']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta Description</label>
                        <textarea class="form-control" name="meta_description"  id="meta_description"><?php echo $dataedit['meta_description']; ?></textarea>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>

            </div>
            <!-- /.box -->
        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        CKEDITOR.replace('content', ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_content").val(CKEDITOR.instances[i].getData());

            });

        }

        $("#form").submit(function(){

            showLoading();

            setTimeout('saveFormData();',3000);

            return false;

        });

    });

    function saveFormData(){

        var target = base_url+"pages/custom_page/edit/<?php echo $dataedit['page_id']; ?>";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
</script>