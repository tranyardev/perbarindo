<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class MUser extends CI_Model
{
    function getUserById($id){

        $result = $this->db->query("SELECT a.*, c.id as user_group, c.name as user_group_name FROM aauth_users a
                                    LEFT JOIN aauth_user_to_group b ON b.user_id=a.id
                                    LEFT JOIN aauth_groups c ON c.id=b.group_id WHERE a.id='".$id."'")->result_array();
        return @$result[0];

    }
    function getUser(){

        $result = $this->db->query("SELECT * FROM aauth_users")->result_array();
        return $result;

    }
    function checkUsername($username){

        $result = $this->db->query("SELECT * FROM aauth_users WHERE username='".$username."'")->num_rows();
        return $result;

    }
    function getBPR(){

        $result = $this->db->query("SELECT a.*, b.name as corp FROM bpr a LEFT JOIN corporates b on b.id=a.corporate WHERE a.name <> ''")->result_array();
        return $result;

    }
    function getBPRMember($bpr){

        $result = $this->db->query("SELECT * FROM members WHERE bpr='$bpr' AND members.name <> ''")->result_array();
        return $result;

    }
}