<?php

class MDUser extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'user_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'user_delete');


    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

            "fullname" => "concat(a.first_name,' ',a.last_name)",
            "photo" => "CASE WHEN a.picture != '' 
                THEN 
                    concat('<div class=\"image-container\"><img src=\"".base_url()."upload/photo/',a.picture,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                ELSE
                    concat('<div class=\"image-container\"><img src=\"".base_url()."public/assets/img/default-avatar.png\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                END",
            "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",

        );;

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "fullname" => "concat(a.first_name,' ',a.last_name)",
                "photo" => "CASE WHEN a.picture != '' 
                THEN 
                    concat('<div class=\"image-container\"><img src=\"".base_url()."upload/photo/',a.picture,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                ELSE
                    concat('<div class=\"image-container\"><img src=\"".base_url()."public/assets/img/default-avatar.png\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')
                END",
                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "aauth_users a";
    }

    public function joinArray(){
        return array(
            "aauth_user_to_group b|left" => "b.user_id=a.id",
            "aauth_groups c|left" => "c.id=b.group_id"
        );
    }

    public function whereClauseArray(){
        return null;
    }


}