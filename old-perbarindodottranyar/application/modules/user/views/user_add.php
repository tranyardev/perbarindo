<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New User</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <div class="img-prev" id="photo_preview"><h1>Photo Preview</h1></div>
                                <br>
                                <input type="file" class="form-control" name="photo" id="photo">

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" required name="first_name" class="form-control" id="first_name" placeholder="First Name (Required)">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" required name="last_name" class="form-control" id="last_name" placeholder="Last Name (Required)">
                            </div>
                            <div class="form-group">
                                <label for="division">User Group</label>
                                <select class="form-control" name="user_group" id="user_group">
                                    <?php
                                    foreach ($user_groups as $ug){
                                        echo "<option value='".$ug['id']."'>".$ug['name']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="division">User BPR</label>
                                <select class="form-control" name="user_bpr" id="user_bpr">
                                    <option>-- pilih dari member bpr --</option>
                                    <?php
                                    foreach ($bpr as $r){

                                        $members = $this->MUser->getBPRMember($r['id']);

                                        echo "<optgroup label='".$r['corp']." ".$r['name']."'>";

                                        foreach ($members as $m) {
                                            echo "<option value='".$m['id']."'>".$m['name']."</option>";
                                        }

                                        echo "</optgroup>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" required name="username" class="form-control" id="username" placeholder="Username (Required)">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text"  name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password"  name="password" maxlength="15" class="form-control" id="password" placeholder="password">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
        <div class="col-md-6">

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            showLoading();

            setTimeout('saveFormData();',3000);

            return false;

        });

        $("#photo").change(function(){
            readURL(this, "#photo");
        });

    });

    function saveFormData(){

        var target = base_url+"user/user/addnew";

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }

                resetForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Photo Preview</h1>");
        $(selector).val('');

    }

    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>