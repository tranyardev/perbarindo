<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class User extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MUser");
        $this->load->model("MUserGroup");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'user_view';
            $this->add_perm = 'user_add';
            $this->edit_perm = 'user_update';
            $this->delete_perm = 'user_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "aauth_users";
        $this->dttModel = "MDUser";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> User App" => base_url()."user/user"
        );

        $data['page'] = 'user';
        $data['page_title'] = 'User Management';
        $data['page_subtitle'] = 'User App';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'user_management';
        $data['data']['submenu'] = 'user';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }

    function generate_user_bpr(){

        $bpr = $this->MUser->getBPR();

        $i=1;
        foreach ($bpr as $b) {
            
            $username = "adminbpr".$b['id'];

            $checkusername = $this->MUser->checkUsername($username);

            if($checkusername < 1){

                $pass = $username;
                $email = $username.'@perbarindo.com';
                $first_name = "Admin";
                $last_name = $b['corp']." ".$b['name'];

                $create_user = $this->aauth->create_user($email,$pass,$username);

                if($create_user){

                    $last_id = $this->db->insert_id();
                    $this->aauth->add_member($last_id, 12);

                    $data = array(
                        "first_name" => $first_name,
                        "last_name" => $last_name,
                        "status" => "1",
                    );

                    $this->db->where($this->pk, $last_id);
                    $update = $this->db->update($this->table, $data);

                    if($update){

                        $data = array(

                            "aauth_user_id" => $last_id,
                            "name" => $first_name." ".$last_name,
                            "gender" => "L",
                            "bpr" => $b['id'],
                            "email" => $email,
                            "status" => "1",
                            "created_by" => $this->session->userdata("id"),
                            "created_at" => date("Y-m-d h:i:s")

                        );

                        $insert = $this->db->insert("members", $data);

                        sleep(1);

                        echo $i." Admin BPR ".$b['name']." created. user: ".$username.", pass: ".$pass." <br>";

                    }

                }

            }
            $i++;
        }

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            /* upload cover */

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }


            $create_user = $this->aauth->create_user($this->input->post('email'),$this->input->post('password'),$this->input->post("username"));

            if($create_user){

                $last_id = $this->db->insert_id();
                $this->aauth->add_member($last_id, $this->input->post("user_group"));

                $data = array(
                    "first_name" => $this->input->post("first_name"),
                    "last_name" => $this->input->post("last_name"),
                    "picture" => $photo,
                    "status" => $this->input->post("status"),
                );

                $this->db->where($this->pk, $last_id);
                $update = $this->db->update($this->table, $data);

                if($update){

                    /* update member aauth_id */

                    if(isset($_POST['user_bpr']) && $_POST['user_bpr']!=""){
                    
                        $this->db->where("id", $_POST['user_bpr']);
                        $this->db->update("members", array("aauth_user_id" => $last_id));
                    
                    }
                    
                    $res = array("status" => "1", "msg" => "Successfully update data!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }

                echo json_encode($res);


            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Could not create user.");
                echo json_encode($res);
            }

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User App" => base_url()."user/user",
                "Add New" => base_url()."user/user/addnew",
            );

            $data['page'] = 'user_add';
            $data['page_title'] = 'User App';
            $data['page_subtitle'] = 'Add New User App';
            $data['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['user_groups'] = $this->MUserGroup->getUserGroup();
            $data['data']['bpr'] = $this->MUser->getBPR();
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'user';
            $this->load->view('layout/body',$data);

        }



    }

    function edit($id){

        if(count($_POST)>0){


            /* upload cover */

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    $old_photo = $upload_path.'/'.@$_POST['old_photo'];

                    if(file_exists($old_photo)){

                        @unlink($old_photo);

                    }


                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $photo = $this->input->post("old_photo");

            }

            $data = array(
                "first_name" => $this->input->post("first_name"),
                "last_name" => $this->input->post("last_name"),
                "email" => $this->input->post("email"),
                "username" => $this->input->post("username"),
                "status" => $this->input->post("status"),
                "picture" => $photo,
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){

                if($this->input->post("old_user_group") != $this->input->post("user_group")){

                    if($this->input->post("old_user_group")!=""){
                        $this->aauth->remove_member($id, $this->input->post("old_user_group"));
                    }

                    $this->aauth->add_member($id, $this->input->post("user_group"));

                }

                $res = array("status" => "1", "msg" => "Successfully update data!");

            }else{

                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User App" => base_url()."user/user",
                "Edit" => base_url()."user/user/edit/".$id,
            );

            $data['page'] = 'user_edit';
            $data['page_title'] = 'User App';
            $data['page_subtitle'] = 'Edit User';
            $data['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MUser->getUserById($id);
            $data['data']['user_groups'] = $this->MUserGroup->getUserGroup();
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'user';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $user = $this->MUser->getUserById($id);

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            $photo = $upload_path.'/'.$user['picture'];

            if(file_exists($photo)){

                @unlink($photo);

            }

            $delete = $this->aauth->delete_user($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    function change_pass(){

        if(count($_POST)>0){

            $update = $this->aauth->update_user($this->input->post("id"), $this->input->post("email"), $this->input->post("new_password"), $this->input->post("username"));

            if($update){

                $res = array("status" => "1", "msg" => "Successfully update password!");

            }else{

                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

            }

            echo json_encode($res);

        }

    }



}