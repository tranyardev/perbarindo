<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class User_group extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MPermission");
        $this->load->model("MUserGroup");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'user_group_view';
            $this->add_perm = 'user_group_add';
            $this->edit_perm = 'user_group_update';
            $this->delete_perm = 'user_group_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }


        $this->table = "aauth_groups";
        $this->dttModel = "MDUserGroup";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group"
        );

        $data['page'] = 'user_group';
        $data['page_title'] = 'User Management';
        $data['page_subtitle'] = 'User Group';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'user_management';
        $data['data']['submenu'] = 'user_group';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $insert = $this->aauth->create_group($name, $definition);

            if($insert){

                $keys = array_keys($_POST);
                $a = count($_POST);

                for($i=0;$i < $a;$i++){

                    $key = $keys[$i];
                    $ex = explode("_",$key);

                    if(count($ex)>1){

                        $perm_id = $ex[1];
                        $perm = $this->MPermission->getPermissionById($perm_id);

                        $this->aauth->allow_group($name,$perm['name']);


                    }


                }

                $res = array("status" => "1", "msg" => "Successfully add data!");

            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group",
                "Add New" => base_url()."user/user_group/addnew",
            );

            $data['page'] = 'user_group_add';
            $data['page_title'] = 'User Group';
            $data['page_subtitle'] = 'Add New User Group';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['perms'] = $this->MPermission->getPermissionModule();
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'user_group';
            $this->load->view('layout/body',$data);

        }



    }

    function edit($id){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $update = $this->aauth->update_group($id, $name, $definition);

            $perm_id = null;

            if($update){

                $keys = array_keys($_POST);
                $a = count($_POST);
                $perm_newdata = array();

                for($i=0;$i < $a;$i++){

                    $key = $keys[$i];
                    $ex = explode("_",$key);

                    if(count($ex)>1){

                        $perm_id = $ex[1];
                        $perm = $this->MPermission->getPermissionById($perm_id);

                        $this->aauth->allow_group($name,$perm['name']);

                        array_push($perm_newdata, $perm_id);


                    }


                }

                $permissions = $this->MPermission->getPermissionByGroupId($id);
                $perm_exist = array();
                foreach ($permissions as $prm){

                    array_push($perm_exist,$prm['id']);

                }


                $delete_perms = array();

                for($k=0;$k < count($perm_exist);$k++){

                    if(!in_array($perm_exist[$k], $perm_newdata)){
                        array_push($delete_perms, $perm_exist[$k]);
                    }

                }


                if(count($delete_perms)>0){

                    $c = count($delete_perms);

                    for($i=0;$i<$c;$i++){

                        $perm_id = $delete_perms[$i];
                        $p = $this->MPermission->getPermissionById($perm_id);
                        $this->aauth->deny_group($name,$p['name']);

                    }

                }

                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group",
                "Edit" => base_url()."user/user_group/edit/".$id,
            );

            $data['page'] = 'user_group_edit';
            $data['page_title'] = 'User Group';
            $data['page_subtitle'] = 'Edit User Group';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MUserGroup->getUserGroupById($id);
            $data['data']['perms'] = $this->MPermission->getPermissionModule();
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'user_group';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $delete = $this->aauth->delete_group($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}