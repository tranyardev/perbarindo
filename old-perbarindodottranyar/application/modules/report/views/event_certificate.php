<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event Certificate Report</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Event Certificate Report" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>City</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    var columns =  [

        { "data" : "a.name"},
        { "data" : "a.start_date", "searchable": false},
        { "data" : "a.end_date", "searchable": false},
        { "data" : "a.city"},
        { "data" : "$.status", "searchable": false},
        { "data" : "$.op", "orderable": false, "searchable": false},

    ];


    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){


        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "report/eventcertificate/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": [],
            "drawCallback": function( settings ) {
                $("img.scale").imageScale();
            }
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function detail(id){
        window.location.href="<?php echo base_url(); ?>report/eventcertificate/detail/"+id;
    }
</script>
