<?php

class Mdeventstarterkit extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

    }

    public function appendToSelectStr() {

       
            $str = array(

                "member_name" => "c.name",
                "bpr" => "f.name",
                "starterkit" => "d.name",
                "opr" => "concat(e.first_name,' ',e.last_name)",

            );

        return $str;

    }

    public function fromTableStr() {
        return "member_starterkit_reports a";
    }

    public function joinArray(){
        return array(

            "event_registrations b|left" => "b.regcode=a.regcode",
            "members c|left" => "c.id=b.member_id",
            "starterkit_items d|left" => "d.id=a.starterkit_id",
            "aauth_users e|left" => "e.id=a.operator_id",
            "bpr f|left" => "f.id=c.bpr",

        );
    }

    public function whereClauseArray(){
        return array("b.event_id" => $_SESSION['event_report_id']);
    }


}