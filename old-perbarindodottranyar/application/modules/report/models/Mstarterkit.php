<?php

class Mstarterkit extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getCorporateById($id){

        $result = $this->db->query("SELECT * FROM corporates WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getCorporate(){

        $result = $this->db->query("SELECT * FROM corporates")->result_array();
        return $result;

    }
    function getCorporateByName($name){

        $result = $this->db->query("SELECT * FROM corporates WHERE name = '$name'")->result_array();
        return $result;

    }
}