<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('truncate')){

    function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }


}

if ( ! function_exists('getMenuChilds')){

    function getMenuChilds($id)
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getMenuChilds($id);

    }

}

if ( ! function_exists('getMenus')){

    function getMenus()
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getMenus();

    }


}

if ( ! function_exists('getUserGroup')){

    function getUserGroup($user_id)
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getUserGroup($user_id);

    }


}

if ( ! function_exists('getActiveSkin')){

    function getActiveSkin()
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getActiveSkin();

    }


}

if ( ! function_exists('rest_api'))
{
    function rest_api($config){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Access-Code:411'));
        curl_setopt($ch, CURLOPT_URL, $config['url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['request']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        try{

            $server_output = curl_exec($ch);

            $response = json_decode($server_output);

        }catch (Exception $e){

            $response = array("error" => $e->getMessage());

        }

        return object_to_array($response);

    }
}
if ( ! function_exists('object_to_array'))
{
    function object_to_array($nested_object){
        return json_decode(json_encode($nested_object), true);
    }
}

if ( ! function_exists('getEventActivities'))
{
    function getEventActivities($id, $date){
       
        $ci =& get_instance();
        $ci->load->model('Mevent');
        $activities = $ci->Mevent->getEventActivities($id, $date);

        return $activities;

    }
}

if ( ! function_exists('setEventImage'))
{
    function setEventImage($file){
       
        $real_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
        $full_path = $real_path.$file;

        if($file != "" && file_exists($full_path)){
            return base_url().'upload/event/'.$file;
        }else{
            return base_url().'public/assets/img/default_event_cover.png';
        }

    }
}

if ( ! function_exists('setMediaImage'))
{
    function setMediaImage($file){
       
        $real_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
        $full_path = $real_path.$file;

        if($file != "" && file_exists($full_path)){
            return base_url().'upload/media/'.$file;
        }else{
            return base_url().'public/assets/img/default_event_cover.png';
        }

    }
}

if ( ! function_exists('truncate'))
{
    function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }
}