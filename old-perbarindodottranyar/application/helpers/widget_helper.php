<?php
/**
 * Created by PhpStorm.
 * User: indra
 * Date: 20/04/17
 * Time: 11:26
 */

if ( ! function_exists('setWidget')){

    function setWidget($section)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $widget = $ci->MDashboard->getWidget($section);

        foreach ($widget as $w){

            getWidget($w['name'],$w['widget_id']);

        }


    }

}

if ( ! function_exists('getWidget')){

    function getWidget($name,$id)
    {

       switch ($name){

           case "event_count":

               generateWidgetEventCount($id);

               break;
           case "upcoming_event_count":

               generateWidgetUpcomingEventCount($id);

               break;
           case "visitor_by_city":

               generateWidgetVisitorByCity($id);

               break;
           case "latest_registered_user":

               generateWidgetLatestRegisteredUser($id);

               break;
           case "event_registration_count":

               generateWidgetEventRegistrationCount($id);

               break;
           case "member_count":

               generateWidgetMemberCount($id);

               break;
           case "current_year_visitor":

               generateWidgetVisitor($id);

               break;
           // case "current_month_event":

           //     generateWidgetCurrentMonthEvent($id);

           //     break;
           case "upcoming_event":

               generateWidgetUpcomingEvent($id);

               break;


       }


    }

}
if ( ! function_exists('generateWidgetEventCount')){

    function generateWidgetEventCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $event_count = $ci->MDashboard->getEventCount();


        $html = '<div class="small-box bg-aqua" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$event_count.'</h3>
        
                        <p>Event</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-paper"></i>
                    </div>
                    <a href="'.base_url().'event/event" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetUpcomingEventCount')){

    function generateWidgetUpcomingEventCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $upcoming_event_count = $ci->MDashboard->getUpcomingEventStatCount();


        $html = ' <div class="small-box bg-green" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$upcoming_event_count.'</h3>
        
                        <p>Upcoming Events</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-calendar"></i>
                    </div>
                    <a href="'.base_url().'event/event" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetEventRegistrationCount')){

    function generateWidgetEventRegistrationCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $event_registration_count = $ci->MDashboard->getEventRegistrationCount();


        $html = '<div class="small-box bg-yellow" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$event_registration_count.'</h3>
        
                        <p>Event Registration</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-at"></i>
                    </div>
                    <a href="'.base_url().'event/event" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetMemberCount')){

    function generateWidgetMemberCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $member_count = $ci->MDashboard->getMemberCount();


        $html = ' <div class="small-box bg-red" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$member_count.'</h3>
        
                        <p>Member Count</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-earth"></i>
                    </div>
                    <a href="'.base_url().'member/member" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetVisitorByCity')){

    function generateWidgetVisitorByCity($id)
    {

        $html = '<div class="box box-success">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Visitors By City</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="pad">
                                    <!-- Map will be created here -->
                                    <div id="world-map-markers" style="height: 360px;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetLatestRegisteredUser')){

    function generateWidgetLatestRegisteredUser($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $lastest_registered_users = $ci->MDashboard->getLatestRegisteredUsers();

        $html = ' <div class="box box-danger" id="'.$id.'">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Registered User</h3>

                <div class="box-tools pull-right">
                    <span class="label label-danger">8 latest user</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">';

            foreach ($lastest_registered_users as $user){

                $date = date_create($user['date_created']);
                $join_date = date_format($date,"d-m-Y");

                if($user['picture'] != ""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $file = $upload_path.'/'.$user['picture'];

                    if(file_exists($file)){

                        $path = base_url().'upload/photo/'.$user['picture'];

                    }else{

                        $path = base_url().'public/assets/img/default-avatar.png';

                    }

                }else {

                    $path = base_url().'public/assets/img/default-avatar.png';

                }

                $name = $user['first_name'].' '.$user['last_name'];

                if(trim($name)==""){
                    $name = $user['username'];
                }
                $html .= '<li>
                            <div class="image-container">
                                <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                            </div>
                            <a class="users-list-name" href="'.base_url().'user/user/edit/'.$user['id'].'">'.$name.'</a>
                            <span class="users-list-date">'.$join_date.'</span>
                          </li>';

            }

        $html .='</ul>
                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="'.base_url().'user/user" class="uppercase">View All Users</a>
            </div>
            <!-- /.box-footer -->
        </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetVisitor')){

    function generateWidgetVisitor($id)
    {


        $html = '<div class="box box-solid bg-teal-gradient">
                    <div class="box-header" id="'.$id.'">
                        <i class="fa fa-th"></i>
        
                        <h3 class="box-title">Visitor In '.date('Y').'</h3>
        
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div>
                    <div class="box-footer no-border">
                        <div class="row">
                            <div class="col-md-12"><span class="label label-primary">yAxis : Visitor Count</span> <span class="label label-danger">xAxis : Month</span></div>
                        </div>
                    </div>
                </div>';

        echo $html;

    }

}


if ( ! function_exists('generateWidgetUpcomingEvent')){

    function generateWidgetUpcomingEvent($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $upcoming_events = $ci->MDashboard->getLastUpcomingEvent();

        $html = ' <div class="box box-warning">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Upcoming Event</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                            if(count($upcoming_events)>0){

                                foreach($upcoming_events as $event){

                                    $s_date=date_create($event['start_date']);
                                    $start_date = date_format($s_date,"d-m-Y");

                                    $e_date=date_create($event['end_date']);
                                    $end_date = date_format($e_date,"d-m-Y");

                                    if($event['cover'] != ""){

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                                        $file = $upload_path.'/'.$event['cover'];

                                        if(file_exists($file)){

                                            $path = base_url().'upload/event/'.$event['cover'];

                                        }else{

                                            $path = base_url().'assets/dist/img/default_img.jpg';

                                        }
                                    }else{

                                    }

                                    $html.='
                                                        <li class="item">
                                                            <div class="product-img">
                                                                <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                            </div>
                                                            <div class="product-info">
                                                                <a href="'.base_url().'show/event/'.$event['id'].'" target="_blank" class="product-title">'.$event['name'].'</a>
                                                                <span class="product-description">
                                                                    <span><i class="fa fa-user"></i> '.$event['author'].'</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> '.$start_date.' - '.$end_date.'</span>
                                                                </span>
                                                            </div>
                                                        </li>
                                                    ';

                                }

                            }else{

                                $html.="<p class='no-data'>No data available</p>";

                            }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetCurrentMonthEvent')){

    function generateWidgetCurrentMonthEvent($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $top_upcoming_events = $ci->MDashboard->getCurrentMonthEvent();

        $html = ' <div class="box box-danger">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Event On This Month</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                            if(count($top_upcoming_events)>0) {

                                foreach ($top_upcoming_events as $event) {


                                    $s_date = date_create($event['start_date']);
                                    $start_date = date_format($s_date, "d-m-Y");

                                    $e_date = date_create($event['end_date']);
                                    $end_date = date_format($e_date, "d-m-Y");

                                    if ($event['cover'] != "") {

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'] . '/upload/event/';
                                        $file = $upload_path . '/' . $event['cover'];

                                        if (file_exists($file)) {

                                            $path = base_url() . 'upload/event/' . $event['cover'];

                                        } else {

                                            $path = base_url() . 'assets/dist/img/default_img.jpg';

                                        }
                                    } else {

                                    }

                                    $html.='
                                            <li class="item">
                                                <div class="product-img">
                                                    <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                </div>
                                                <div class="product-info">
                                                    <a href="javascript:void(0)" class="product-title">' . $event['name'] . ' <span class="label label-danger pull-right"><i class="fa fa-user"></i> '.$event['member_count'].'</span></a>
                                                    <span class="product-description">
                                                        <span><i class="fa fa-user"></i> ' . $event['author'] . '</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> ' . $start_date . ' - ' . $end_date . '</span>
                                                    </span>
                                                </div>
                                            </li>
                                        ';

                                }

                            }else{

                                $html.="<p class='no-data'>No data available</p>";

                            }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}