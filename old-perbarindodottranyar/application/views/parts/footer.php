<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> Beta 0.9.2
    </div>
    <strong>Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a> 2017 <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a></strong>
</footer>