  <div class="box">
  <div class="box-header">
    <h3 class="box-title">My Modules</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="input-group input-group-lg">
      <span class="input-group-addon"><i class="fa fa-search"></i></span>
      <input type="text" id="dt_search" class="form-control" placeholder="Find Module" aria-describedby="sizing-addon1">
    </div>
    <br>
    <table id="dttable" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Module</th>
        <th>Version</th>
        <th>Group</th>
        <th>Status</th>
        <th style="width: 80px;">Action</th>
      </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>


  <div class="modal" id="modal_confirm">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <p>Do you want to delete this module?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


<script type="text/javascript">
  $(document).ready(function(){

      InitDatatable();

  });

  function InitDatatable(){

     var table = $('#dttable').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>addons/dataTable",
            "type": "POST"
        },
        "columns": [
            { "data" : "a.name" },
            { "data" : "a.version"},
            { "data" : "a.group"},
            { "data" : "$.status"},
            { "data" : "$.op", "orderable": false},
        ],
        "lengthChange":false,
        "dom": 'Bfrtip'
    });

    $('#dt_search').keyup(function(){
          table.search($(this).val()).draw() ;
    });

  }

  function edit(id){
    window.location.href="<?php echo base_url(); ?>addons/edit/"+id;
  }
  function remove(id){
    
    $("#modal_confirm").modal('show');
    $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

  }

  function proceedRemove(id){

    var target = '<?php echo base_url(); ?>addons/remove';
    var data = { id : id }

    $("#btn_action").button("loading");

    $.post(target, data, function(res){

      $("#btn_action").button("reset");
      $("#modal_confirm").modal('hide');
      if(res.status=="1"){
        toastr.success(res.msg, 'Response Server');

        $('#dttable').dataTable().fnDestroy();

        InitDatatable();

      }else{
        toastr.error(res.msg, 'Response Server');
      }

    },'json');

  }
</script>