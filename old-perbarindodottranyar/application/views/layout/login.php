<?php
$app['base_url'] = base_url();
$app['assetdir'] = "assets";
$app['title'] = "PERBARINDO | Login";
$app['meta'] = array(
    "keywords" => "PERBARINDO",
    "description" => "PERBARINDO CMS",
    "author" => "PERBARINDO"
);
$app['css'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
    "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
    "ionicons" => $app['base_url'].$app['assetdir'].'/bower_components/ionicons/css/ionicons.min.css',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/square/blue.css',
    "adminlte" => $app['base_url'].$app['assetdir'].'/dist/css/AdminLTE.css',
);
$app['footer_js'] = array(
    "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/icheck.min.js',

);


if(isset($data)){
    // print_r($data);
    // print($data['username']);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
    /* generate metatag */
    $meta_key = array_keys($app['meta']);
    for($i=0;$i < count($app['meta']);$i++){
        echo '<meta name="'.$meta_key[$i].'" content="'.$app['meta'][$meta_key[$i]].'">';
    }
    /* generate css */
    $css_key = array_keys($app['css']);
    for($i=0;$i < count($app['css']);$i++){
        echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
    }
    ?>
    <style>
        .login-page, .register-page {
            background: #000 !important;
        }
        .background-image {
            /*background-image: url('<?php //echo base_url().'assets/dist/img/bg-login.jpg'; ?>');*/
            background-color: #00A65A;
            background-size: cover;
            display: block;
            filter: blur(5px);
            -webkit-filter: blur(5px);
            height: 100%;
            left: 0;
            position: fixed;
            right: 0;
            z-index: 1;
        }

        .content {
            background: rgba(255, 255, 255, 0.35);
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            border-radius: 3px;
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
            padding: 1em;
            position: absolute;
            top: 300px;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="background-image"></div>
<div class="login-box content">
    <div class="login-logo" align="center">
        &nbsp;<a href="<?php echo base_url().'admin';?>"><img src="<?php echo base_url(); ?>assets/dist/img/app_logo.png" class="img-responsive" style="margin-left: auto; margin-right: auto"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Process</h4>
            Authenticating... Please wait!
        </div>
        <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
            Invalid email or password. Please check your email or password!
        </div>
        <div id="granted" class="alert alert-success alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Access Granted!</h4>
            Redirecting to Dashboard Page... Please Wait!
        </div>
        <form id="login">
            <div class="form-group has-feedback">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input id="remember" type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="submit_btn" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php
/* generate js */
$footer_js_key = array_keys($app['footer_js']);
for($i=0;$i < count($app['footer_js']);$i++){
    echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
}
?>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
        $("#login").submit(function(){

            loading();

            var data = $(this).serialize();
            var target = '<?php echo base_url(); ?>auth/session/login';

            disable_form();

            $.post(target,data,function(res){
                 if(res.granted){
                     granted();
                 }else{
                     denied();
                     enable_form();
                 }
            },'json');

            return false;
        });
    });

    function disable_form(){
        $("#email").attr('disabled','disabled');
        $("#password").attr('disabled','disabled');
        $("#remember").attr('disabled','disabled');
        $("#submit_btn").attr('disabled','disabled');
    }
    function enable_form(){
        $("#email").removeAttr('disabled');
        $("#password").removeAttr('disabled');
        $("#remember").removeAttr('disabled');
        $("#submit_btn").removeAttr('disabled');
    }
    function loading(){
        $("#loading").fadeIn();
        $("#denied").hide();
        $("#granted").hide();
    }
    function denied(){
        $("#loading").hide();
        $("#denied").fadeIn();
        $("#granted").hide();
    }
    function granted(){
        $("#loading").hide();
        $("#denied").hide();
        $("#granted").fadeIn();

        setTimeout('window.location.reload()',3000);
    }
</script>
</body>
</html>

