<style>
    .glyphicon { margin-right:5px; }
    .thumbnail
    {
        margin-bottom: 20px;
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .item.list-group-item
    {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 10px;
    }
    .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
    {
        background: #428bca;
    }

    .item.list-group-item .list-group-image
    {
        margin-right: 10px;
    }
    .item.list-group-item .thumbnail
    {
        margin-bottom: 0px;
    }
    .item.list-group-item .caption
    {
        padding: 9px 9px 0px 9px;
    }
    .item.list-group-item:nth-of-type(odd)
    {
        background: #eeeeee;
    }

    .item.list-group-item:before, .item.list-group-item:after
    {
        display: table;
        content: " ";
    }

    .item.list-group-item img
    {
        float: left;
    }
    .item.list-group-item:after
    {
        clear: both;
    }
    .list-group-item-text
    {
        margin: 0 0 11px;
    }
    .stars-default{
        margin-left: 15px;
        margin-bottom: 5px;
    }
    .user-install{
        margin-left: 15px;
    }
    .thumb-addon > img{
        height: 216px;
    }
    .thumb-addon > .caption{
        height: 158px;
    }
    .thumb-addon > .caption > .list-group-item-text{
        height: 36px;
    }
    .s-addon{
        width: 400px;
        margin-right: 20px;
    }
    .list-group-item > .thumbnail{

        height: 216px;

    }
    .list-group-item > .thumbnail > img{

        width: 384px;

    }
    .list-group-item > .thumbnail > .caption > .list-mode{

        display: block !important;

    }
    .list-group-item > .thumbnail > .caption > .grid-mode{

        display: none !important;

    }
    @keyframes placeHolderShimmer{
        0%{
            background-position: -468px 0
        }
        100%{
            background-position: 468px 0
        }
    }

    .animated-background {
        animation-duration: 1s;
        animation-fill-mode: forwards;
        animation-iteration-count: infinite;
        animation-name: placeHolderShimmer;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        background-size: 800px 104px;
        height: 96px;
        position: relative;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Addons Store</h3>
                <div class="pull-right">
                    <div class="input-group pull-left s-addon">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        <input type="text" class="form-control" placeholder="Search Module...">
                    </div><!-- /input-group -->
                    <div class="btn-group">
                        <a href="#" id="list" class="btn btn-default"><span class="glyphicon glyphicon-th-list">
                            </span>List</a> <a href="#" id="grid" class="btn btn-default"><span
                                class="glyphicon glyphicon-th"></span>Grid</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">


                <div id="products" class="list-group">

                    <?php
                        foreach ($addons_product as $ap) {
                            if($ap['cover']!=""){
                                $cover = base_url().'repo/vendor/'.$ap['slug'].'/'.$ap['cover'];
                            }else{
                                $cover = "http://placehold.it/400x250/000/fff";
                            }
                    ?>
                    <div class="item  col-xs-3 col-lg-3">
                        <div class="thumbnail thumb-addon">
                            <img class="group list-group-image" src="<?php echo $cover; ?>" alt="" />
                            <div class="caption">

                                    <h4 class="group inner list-group-item-heading">
                                        <a href="<?php echo base_url(); ?>addons/view/<?php echo $ap['module_id']; ?>"><?php echo $ap['name']; ?></a>
                                    </h4>

                                <p class="group inner list-group-item-text grid-mode">
                                    <?php echo truncate(strip_tags($ap['description']), 70); ?>
                                </p>

                                <p class="group inner list-group-item-text list-mode" style="display: none;">
                                    <?php echo truncate(strip_tags($ap['description']), 200); ?>
                                </p>

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                       <div class="stars-default" data-rating="<?php echo $ap['rate']; ?>">
                                            <input type=hidden name="rating"/>
                                       </div>
                                       <span class="user-install"><i class="fa fa-download"></i> <?php ($ap['install_count']!="")? $count = $ap['install_count']:$count = "0";echo $count; ?></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                     <!-- <div class="item  col-xs-3 col-lg-3">
                        <div class="thumbnail thumb-addon">
                            <div class="animated-background" style="height: 216px;">
                            </div>
                            <div class="caption">

                                    <h4 class="group inner list-group-item-heading animated-background" style="height: 16px;">

                                    </h4>


                                    <div class="animated-background" style="height: 12px;margin-bottom: 2px;margin-top: 15px;"></div>
                                    <div class="animated-background" style="height: 12px;margin-bottom: 2px;margin-top: 5px;width: 150px;"></div>


                                <div class="row-fluid" style="margin-top: 10px;">
                                    <div class="col-xs-12 col-md-6" style="padding-left: 0px;">
                                       <div class="animated-background" style="height: 12px;height: 60px"></div>

                                    </div>
                                    <div class="col-xs-12 col-md-6" style="padding-right: 0px;">
                                        <div class="animated-background" style="height: 12px;height: 60px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div> -->
                </div>

                </div>
            </div>
        </div>
    </div>

 <script type="text/javascript">
     (function ( $ ) {

         $.fn.rating = function( method, options ) {
             method = method || 'create';
             // This is the easiest way to have default options.
             var settings = $.extend({
                 // These are the defaults.
                 limit: 5,
                 value: 0,
                 glyph: "glyphicon-star",
                 coloroff: "gray",
                 coloron: "gold",
                 size: "2.0em",
                 cursor: "default",
                 onClick: function () {},
                 endofarray: "idontmatter"
             }, options );
             var style = "";
             style = style + "font-size:" + settings.size + "; ";
             style = style + "color:" + settings.coloroff + "; ";
             style = style + "cursor:" + settings.cursor + "; ";



             if (method == 'create')
             {
                 //this.html('');    //junk whatever was there

                 //initialize the data-rating property
                 this.each(function(){
                     attr = $(this).attr('data-rating');
                     if (attr === undefined || attr === false) { $(this).attr('data-rating',settings.value); }
                 })

                 //bolt in the glyphs
                 for (var i = 0; i < settings.limit; i++)
                 {
                     this.append('<span data-value="' + (i+1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
                 }

                 //paint
                 this.each(function() { paint($(this)); });

             }
             if (method == 'set')
             {
                 this.attr('data-rating',options);
                 this.each(function() { paint($(this)); });
             }
             if (method == 'get')
             {
                 return this.attr('data-rating');
             }
             //register the click events
             this.find("span.ratingicon").click(function() {
                 rating = $(this).attr('data-value')
                 $(this).parent().attr('data-rating',rating);
                 paint($(this).parent());
                 settings.onClick.call( $(this).parent() );
             })
             function paint(div)
             {
                 rating = parseInt(div.attr('data-rating'));
                 div.find("input").val(rating);  //if there is an input in the div lets set it's value
                 div.find("span.ratingicon").each(function(){    //now paint the stars

                     var rating = parseInt($(this).parent().attr('data-rating'));
                     var value = parseInt($(this).attr('data-value'));
                     if (value > rating) { $(this).css('color',settings.coloroff); }
                     else { $(this).css('color',settings.coloron); }
                 })
             }

         };

     }( jQuery ));

     $(document).ready(function(){
         $(".stars-default").rating();
         $('#list').click(function(event){
             event.preventDefault();
             $('#products .item').addClass('list-group-item');
         });
         $('#grid').click(function(event){
             event.preventDefault();
             $('#products .item').removeClass('list-group-item');
             $('#products .item').addClass('grid-group-item');
         });
     });

 </script>