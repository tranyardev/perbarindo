<?php
$app['base_url'] = base_url();
$app['assetdir'] = "assets";
$app['title'] = "Tranyar Dev | Register";
$app['meta'] = array(
    "keywords" => "Tranyar",
    "description" => "Tranyar CMS",
    "author" => "Tranyar Developer"
);
$app['css'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
    "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
    //"ionicons" => $app['base_url'].$app['assetdir'].'/bower_components/ionicons/css/ionicons.min.css',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/square/blue.css',
    "adminlte" => $app['base_url'].$app['assetdir'].'/dist/css/AdminLTE.css',
);
$app['footer_js'] = array(
    "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    //"icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/icheck.min.js',

);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
    /* generate metatag */
    $meta_key = array_keys($app['meta']);
    for($i=0;$i < count($app['meta']);$i++){
        echo '<meta name="'.$meta_key[$i].'" content="'.$app['meta'][$meta_key[$i]].'">';
    }
    /* generate css */
    $css_key = array_keys($app['css']);
    for($i=0;$i < count($app['css']);$i++){
        echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
    }
    ?>
    <style type="text/css">
        .btn-reg{

            margin-top: 10px;

        }
        .recaptcha-wrapper{
            margin-left: auto;
            margin-right: auto;
        }
        .g-recaptcha{
            width: 304px;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><img src="<?php echo base_url(); ?>assets/dist/img/tranyardev-logo.png" class="img-responsive"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Join us as Developer.</p>
        <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Process</h4>
            Registering... Please wait!
        </div>
        <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Oops! Something went wrong.</h4>
            Are you bot? Please turn into human dude!
        </div>
        <div id="passnotmatch" class="alert alert-danger alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Oops! Something went wrong.</h4>
            Confirmation password doesnt match!
        </div>
        <div id="granted" class="alert alert-success alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            One step to be developer! Please check your mail and click activation account.
        </div>
        <form id="register">
            <div class="form-group has-feedback">
                <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" required>
                
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required>
                
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="confirm-password" id="confirm-password" class="form-control" placeholder="Confirm Your Password" required>
             
            </div>
            <div class="row">
                <div class="col-md-12 recaptcha-wrapper"><?php echo $this->recaptcha->render(); ?></div>
                <div class="col-md-12">
                    <button type="submit" id="submit_reg" class="btn btn-primary btn-block btn-flat btn-reg">Register</button>
                </div>
            </div>
           
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php
/* generate js */
$footer_js_key = array_keys($app['footer_js']);
for($i=0;$i < count($app['footer_js']);$i++){
    echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
}
?>
<script>
    $(document).ready(function () {
       
        $("#register").submit(function(){
    
            if($("#confirm-password").val()==$("#password").val()){

                loading();

                var data = $(this).serialize();
                var target = '<?php echo base_url(); ?>dashboard/register';

                disable_form();

                $.post(target,data,function(res){
                     if(res.granted){
                         granted();
                     }else{
                         denied();
                         enable_form();
                     }
                },'json');

            }else{

                passnotmatch();

            }
            

            return false;
        });
    });

    function disable_form(){
        $("#email").attr('disabled','disabled');
        $("#password").attr('disabled','disabled');
        $("#confirm-password").attr('disabled','disabled');
        $("#first_name").attr('disabled','disabled');
        $("#last_name").attr('disabled','disabled');
        $("#username").attr('disabled','disabled');
        $("#submit_btn").attr('disabled','disabled');
    }
    function enable_form(){
        $("#email").removeAttr('disabled');
        $("#password").removeAttr('disabled');
        $("#confirm-password").removeAttr('disabled');
        $("#first_name").removeAttr('disabled');
        $("#last_name").removeAttr('disabled');
        $("#username").removeAttr('disabled');
        $("#submit_btn").removeAttr('disabled');
    }
    function loading(){
        $("#loading").fadeIn();
        $("#denied").hide();
        $("#granted").hide();
        $("#passnotmatch").hide();
    }
    function denied(){
        $("#loading").hide();
        $("#denied").fadeIn();
        $("#granted").hide();
        $("#passnotmatch").hide();
    }
    function granted(){
        $("#loading").hide();
        $("#denied").hide();
        $("#granted").fadeIn();
        $("#passnotmatch").hide();
    }

    function passnotmatch(){
        $("#loading").hide();
        $("#denied").hide();
        $("#granted").hide();
        $("#passnotmatch").fadeIn();
    }
</script>
</body>
</html>

