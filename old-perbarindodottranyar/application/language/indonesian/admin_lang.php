<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* E-mail Messages */
/* brp branch */

$lang['branch_name'] = 'Nama Cabang';
$lang['branch_address'] = 'Alamat';
$lang['branch_telp'] = 'Telp';


/* brp contact person */

$lang['contact_name'] = 'Nama Kontak';
$lang['contact_address'] = 'Alamat';
$lang['contact_telp'] = 'Telp';
$lang['contact_mobile_phone'] = 'Hp';
$lang['contact_email'] = 'Email';


/* brp director position */

$lang['director_position_name'] = 'Nama Posisi';
$lang['director_position_parent'] = 'Induk';

/* brp director */

$lang['director_name'] = 'Nama';
$lang['director_position'] = 'Posisi';

/* brp employee */

$lang['employee_photo'] = 'Foto';
$lang['employee_name'] = 'Nama Pegawai';
$lang['employee_gender'] = 'Jenis Kelamin';
$lang['employee_job_position'] = 'Jabatan';
$lang['employee_email'] = 'Email';

/* brp mutation */

$lang['mutation_member'] = 'Nama';
$lang['mutation_from_position'] = 'Dari Posisi';
$lang['mutation_to_position'] = 'Ke Posisi';
$lang['mutation_date'] = 'Tanggal Mutasi';

/* brp mutation */

$lang['stockholder_name'] = 'Nama';
$lang['stockholder_percentage'] = 'Nilai Saham';

/* brp article category */

$lang['category_name'] = 'Nama Kategori';
$lang['category_parent'] = 'Induk Kategori';
$lang['category_status'] = 'Status';
$lang['category_sequence'] = 'Urutan';

/* brp article */

$lang['post_title'] = 'Judul';
$lang['post_status'] = 'Status';
$lang['post_category'] = 'Kategori';
$lang['post_created_at'] = 'Tanggal';

/* brp article */

$lang['product_category_name'] = 'Nama Kategori';
$lang['product_category_parent'] = 'Induk Kategori';
$lang['product_category_status'] = 'Status';
$lang['product_category_sequence'] = 'Urutan Kategori';


/* brp article */

$lang['product_name'] = 'Judul';
$lang['product_status'] = 'Status';
$lang['product_category'] = 'Kategori';
$lang['product_created_at'] = 'Tanggal';

/* brp article */

$lang['asset_type_name'] = 'Nama Tipe Asset';


/* brp article */

$lang['asset_year'] = 'Tahun';
$lang['asset_value'] = 'Nilai Asset';
$lang['finance_asset_type'] = 'Tipe Asset';


/* brp article */

$lang['report_name'] = 'Judul Laporan';
$lang['report_year'] = 'Tahun';
$lang['report_attachement'] = 'File Lampiran';

/* brp article */

$lang['auction_code'] = 'Kode Asset';
$lang['auction_status'] = 'Status Asset';
$lang['auction_category'] = 'Kategori';
$lang['auction_date'] = 'Jadwal Lelang';

// DPD Module
$lang['master_data'] = 'Data Master';
$lang['dpd_list'] = 'Daftar DPD';
$lang['dpd_add'] = 'Tambah DPD';
$lang['dpd_edit'] = 'Edit data DPD';
$lang['dpd'] = 'DPD';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['dpd_search'] = 'Cari DPD';

// BPR Module
$lang['bpr_list'] = 'Daftar BPR';
$lang['bpr_add'] = 'Tambah BPR';
$lang['bpr_edit'] = 'Edit data BPR';
$lang['address'] = 'Alamat';
$lang['telp'] = 'No Telp';
$lang['fax'] = 'Fax';
$lang['email'] = 'Email';
$lang['website'] = 'Website';
$lang['corporate'] = 'PT/PD';

// Corporate
$lang['corporate_list'] = 'Daftar PT/PD';
$lang['corporate_add'] = 'Tambah PT/PD';
$lang['corporate_edit'] = 'Edit data PT/PD';
$lang['corporate_search'] = 'Cari PT/PD';

// Corporate
$lang['corporate_list'] = 'Daftar PT/PD';
$lang['corporate_add'] = 'Tambah PT/PD';
$lang['corporate_edit'] = 'Edit data PT/PD';
$lang['corporate_search'] = 'Cari PT/PD';

$lang['action'] = 'Aksi';
$lang['modal_delete_title'] = 'konfirmasi';
$lang['modal_delete_body'] = 'Data pada baris ini akan dihapus. Anda yakin?';
$lang['yes'] = 'Ya';
$lang['no'] = 'Tidak';
$lang['submit'] = 'Submit';
$lang['back'] = 'Kembali';
$lang['add'] = 'Tambah';
$lang['search_data'] = 'Cari Data';