<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* E-mail Messages */

// DPD Module
$lang['master_data'] = 'Data Master';
$lang['dpd_list'] = 'Daftar DPD';
$lang['dpd_add'] = 'Tambah DPD';
$lang['dpd_edit'] = 'Edit data DPD';
$lang['dpd'] = 'DPD';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['dpd_search'] = 'Cari DPD';

// BPR Module
$lang['bpr_list'] = 'Daftar BPR';
$lang['bpr_add'] = 'Tambah BPR';
$lang['bpr_edit'] = 'Edit data BPR';
$lang['address'] = 'Alamat';
$lang['telp'] = 'No Telp';
$lang['fax'] = 'Fax';
$lang['email'] = 'Email';
$lang['website'] = 'Website';
$lang['corporate'] = 'PT/PD';

// Corporate
$lang['corporate_list'] = 'Daftar PT/PD';
$lang['corporate_add'] = 'Tambah PT/PD';
$lang['corporate_edit'] = 'Edit data PT/PD';
$lang['corporate_search'] = 'Cari PT/PD';

$lang['action'] = 'Aksi';
$lang['modal_delete_title'] = 'konfirmasi';
$lang['modal_delete_body'] = 'Data pada baris ini akan dihapus. Anda yakin?';
$lang['yes'] = 'Ya';
$lang['no'] = 'Tidak';
$lang['submit'] = 'Submit';
$lang['back'] = 'Kembali';
$lang['add'] = 'Tambah';