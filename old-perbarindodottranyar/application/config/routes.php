<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['login'] = 'home/login';
$route['signup'] = 'home/signup';
$route['logout'] = 'home/logout';
$route['signup_member'] = 'home/create_user';
$route['login_member'] = 'home/login_member';
$route['register'] = 'dashboard/register';
$route['find/event'] = 'home/find_event';
$route['about'] = 'home/about';
$route['contact'] = 'home/contact';
$route['faq'] = 'home/faq';
$route['myprofile'] = 'home/myprofile';
$route['submit/registration'] = 'home/submit_registration';
$route['payment_confirmation/(:any)'] = 'home/payment_confirmation/$1';
$route['payment_info/(:any)'] = 'home/payment_info/$1';
$route['checkout/payment/(:any)'] = 'home/checkout/$1';
$route['checkout/payment_info/(:any)'] = 'home/checkout_payment_info/$1';
$route['payment_detail/(:any)'] = 'home/payment_detail/$1';
$route['confirm/transaction'] = 'home/confirm_transaction';
$route['checkout/cancel_reservation/(:any)'] = 'home/checkout_cancel_reservation/$1';
$route['event/registration/(:num)'] = 'home/event_registration/$1';
$route['autocomplete/bpr/(:num)'] = 'home/get_bpr_by_dpd/$1';
$route['autocomplete/member/(:num)'] = 'home/get_member_by_bpr/$1';
$route['member/get/(:num)'] = 'home/get_member_by_id/$1';
$route['get_event_registration/status/(:any)'] = 'home/get_event_registration_by_status/$1';
$route['get_event_registration/regcode/(:any)'] = 'home/get_event_registration_by_regcode/$1';
$route['term_of_condition'] = 'home/term_of_condition';
$route['submit/message'] = 'home/submit_message';
$route['cancel/transaction'] = 'home/cancel_transaction';
$route['show/event/(:num)'] = 'home/show_event/$1';
$route['change/user/picture'] = 'home/user_upload_photo';
$route['update/user/profile'] = 'home/user_update_profile';
$route['update/member/profile'] = 'home/member_update_profile';
$route['update/user/password'] = 'home/change_pass';
$route['admin'] = 'dashboard';
$route['verify/user/(:any)'] = 'dashboard/verify_user/$1';
$route['bpr/(:num)/(:any)'] = 'home/bpr/index/$1/$2';
$route['bpr'] = 'home/bpr';
$route['profile/bpr/(:num)'] = 'home/bpr/profile/$1';
$route['search/bpr'] = 'home/bpr/search';
$route['search/bpr/(:num)/(:any)/(:any)'] = 'home/bpr/search/$1/$2/$3';
$route['search/product/(:num)/(:any)/(:any)/(:any)'] = 'home/bpr/product_search/$1/$2/$3/$4';
$route['search/post/(:num)/(:any)/(:any)/(:any)'] = 'home/bpr/post_search/$1/$2/$3/$4';
$route['search/product'] = 'home/bpr/product_search/';
$route['search/post'] = 'home/bpr/post_search/';
$route['search/blog'] = 'home/blog/post_search/';
$route['search/blog/(:num)'] = 'home/blog/post_search/$1';
$route['search/blog/(:num)/(:any)'] = 'home/blog/post_search/$1/$2';
$route['bpr/post/(:num)/(:any)'] = 'home/bpr/post_detail/$1/$2';
$route['bpr/product/(:num)/(:any)'] = 'home/bpr/product_detail/$1/$2';
$route['bpr/asset_code/(:any)'] = 'home/bpr/asset_code/$1';
$route['blog'] = 'home/blog';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
