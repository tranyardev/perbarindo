<?php

class MDAddons extends MY_Model implements DatatableModel{

        function __construct(){

            parent::__construct();

        }
        public function appendToSelectStr() {
                return array(

                    "status" => "CASE WHEN a.status = '0' THEN 'Draft' 
                                 WHEN a.status = '1' THEN 'Pending'
                                 ELSE 'Published' END",

                    "op" => "concat('<a class=\"btn btn-sm btn-primary\" href=\"javascript:edit(',a.module_id,');\"><i class=\"fa fa-pencil\"></i></a>&nbsp;<a class=\"btn btn-sm btn-danger\" href=\"javascript:remove(',a.module_id,');\"><i class=\"fa fa-remove\"></i></a>')"


                );
        }

        public function fromTableStr() {
            return 'addons_store a';
        }

        public function joinArray(){
            return null;
        }

        public function whereClauseArray(){
            return null;
        }


}