<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Permission extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MPermission");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'permission_view';
            $this->add_perm = 'permission_add';
            $this->edit_perm = 'permission_update';
            $this->delete_perm = 'permission_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }


        $this->table = "aauth_perms";
        $this->dttModel = "MDPermission";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> Permission" => base_url()."user/permission"
        );

        $data['page'] = 'permission';
        $data['page_title'] = 'User Management';
        $data['page_subtitle'] = 'Permission';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'user_management';
        $data['data']['submenu'] = 'permission';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $insert = $this->aauth->create_perm($name, $definition);



            $last_id = $this->db->insert_id();
            $data = array(
                "module" => $this->input->post("module"),
            );

            $this->db->where($this->pk, $last_id);
            $update = $this->db->update($this->table, $data);



            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> Permission" => base_url()."user/permission",
                "Add New" => base_url()."user/permission/addnew",
            );

            $data['page'] = 'permission_add';
            $data['page_title'] = 'Permission';
            $data['page_subtitle'] = 'Add Permission';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'permission';
            $this->load->view('layout/body',$data);

        }



    }

    function edit($id){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $update = $this->aauth->update_perm($id, $name, $definition);




            $data = array(
                "module" => $this->input->post("module"),
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);




            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> Permission" => base_url()."user/permission",
                "Edit" => base_url()."user/permission/edit/".$id,
            );

            $data['page'] = 'permission_edit';
            $data['page_title'] = 'Permission';
            $data['page_subtitle'] = 'Edit Permission';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MPermission->getPermissionById($id);
            $data['data']['parent_menu'] = 'user_management';
            $data['data']['submenu'] = 'permission';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $delete = $this->aauth->delete_perm($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}