<?php
/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Subscriber extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MSubscriber");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'subscribers_view';
            $this->add_perm = 'subscribers_add';
            $this->edit_perm = 'subscribers_update';
            $this->delete_perm = 'subscribers_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "subscribers";
        $this->dttModel = "MDSubscriber";
        $this->pk = "subscriber_id";
    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    function index(){

        $breadcrumb = array(
            "<i class='fa fa-rss-square'></i> Subscriber " => base_url()."newsletter/subscriber"
        );

        $data['page'] = 'subscriber';
        $data['page_title'] = 'Newsletter';
        $data['page_subtitle'] = 'Subscriber';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'newsletter_management';
        $data['data']['submenu'] = 'subscribers';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

    }
    public function dataTable() {

        $model = array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk);

        $this->load->library('Datatable', $model);
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    function addnew(){

        if(count($_POST)>0){


            $data = array(
                "email" => $this->input->post("email"),
                "subscribe_date" => date("Y-m-d h:i:s")
            );
            $result = $this->db->insert($this->table, $data);

            if($result){
                $respon = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
            }

            echo json_encode($respon);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Subscriber" => base_url()."newsletter/subscriber",
                "Add New" => base_url()."newsletter/subscriber/addnew",
            );

            $data['page'] = 'subscriber_add';
            $data['page_title'] = 'Subscriber';
            $data['page_subtitle'] = 'Add New Subscriber';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
            );

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'subscribers';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(
                "email" => $this->input->post("email")
            );

            $this->db->where($this->pk, $id);
            $result = $this->db->update($this->table, $data);
            if($result){
                $respon = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
            }

            echo json_encode($respon);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Subscriber" => base_url()."newsletter/subscriber",
                "Edit" => base_url()."newsletter/subscriber/edit/".$id,
            );

            $data['page'] = 'subscriber_edit';
            $data['page_title'] = 'Subscriber';
            $data['page_subtitle'] = 'Edit Subscriber';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MSubscriber->getSubscriberById($id);
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'subscribers';
            $this->load->view('layout/body',$data);

        }

    }
    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

}