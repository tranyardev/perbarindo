<div class="box">
    <div class="box-header">
        <h3 class="box-title">Newsletter</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Newsletter" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Status</th>
                <th>Success</th>
                <th>Failed</th>
                <th>Time</th>
                <?php
                    if($edit_perm!='1' && $delete_perm!='1' && $blast_perm!='1'){

                    }else{
                        echo '<th style="width: 105px;">Action</th>';
                    }
                ?>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div id="response"></div>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="newsletter_dialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Blast Newsletter</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="content_newsletter">
                <input type="hidden" id="member_newsletter">
                <input type="hidden" id="id_newsletter">
                <h3>To : </h3>
                <span id="recipient"></span>
                <h3>Preview :</h3>
                <div id="newsletter-body-dialog">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_blast" onclick="sentNewsletter();">Blast Newsletter</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var base_url = '<?php echo base_url(); ?>';
    var add_perm = '<?php echo $add_perm; ?>';
    var edit_perm = '<?php echo $edit_perm; ?>';
    var delete_perm = '<?php echo $delete_perm; ?>';
    var blast_perm = '<?php echo $blast_perm; ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy'
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel'
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV'
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF'
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print'
        }
    ];

    var columns =  [
            {"data" : "a.title"},
            {"data" : "$.status", bSortable : false, bSearchable: false},
            {"data" : "a.success"},
            {"data" : "a.failed"},
            {"data" : "a.last_blaster_at"},
            {"data" : "$.op", "orderable": false, "searchable": false},
        ];

    if(edit_perm != '1' && delete_perm != '1' && blast_perm != '1'){

        columns = [
            {"data" : "a.title"},
            {"data" : "$.status", bSortable : false, bSearchable: false},
            {"data" : "a.success"},
            {"data" : "a.failed"},
            {"data" : "a.last_blaster_at"},
        ];

    }

    if(add_perm=='1'){

        buttons.push( {
            text: '<i class="fa fa-plus"></i> New Newsletter',
            action: function ( e, dt, node, config ) {
                window.location.href = base_url + "newsletter/newsletter/addnew"
            }
        });

    }

    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){
        
        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "newsletter/newsletter/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function edit(id){
        window.location.href="<?php echo base_url(); ?>newsletter/newsletter/edit/"+id;
    }
    function remove(id){

        $("#modal_confirm").modal('show');
        $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

    }

    function proceedRemove(id){

        var target = '<?php echo base_url(); ?>newsletter/newsletter/remove';
        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
    function blastNewsletter(id){

        var target = base_url + 'newsletter/newsletter/get_data/'+id;

        $.get(target,function(res){

            $("#newsletter-body-dialog").html(res.content);
            $("#recipient").html(res.subscriber);
            $("#content_newsletter").val(res.content);
            $("#member_newsletter").val(res.subscriber);
            $("#id_newsletter").val(res.id);

            $('#newsletter_dialog').modal({
                backdrop: false,
                show: true
            });
        },'json');
    }
    function sentNewsletter(){
        if($("#content_newsletter").val()!=""&&$("#member_newsletter").val()!=""){
            var data = { id : $("#id_newsletter").val() , content : $("#content_newsletter").val(), subscriber : $("#member_newsletter").val() }
            var target = base_url + 'newsletter/newsletter/blast';
            $("#btn_blast").button('loading');
            $.post(target,data,function(res){

                $("#btn_blast").button('reset');
                toastr.success(res.msg, 'Response Server');

                $("#response").html(res.msg);

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            },'json');
        }else{
            alert('Email dan Member tidak boleh kosong! Silahkan cek email template beserta email member.');
        }
    }
</script>