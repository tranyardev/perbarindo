<?php
class MDSubscriber extends MY_Model implements DatatableModel{
        function __construct(){

            parent::__construct();
            $this->load->library('mcore');
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'subscribers_update');
            $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'subscribers_delete');

        }

        public function appendToSelectStr() {

            $edit = '';
            $delete = '';
            $str = null;

            if($this->allow_edit){
                $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.subscriber_id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
            }

            if($this->allow_delete){
                $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.subscriber_id,\');"><i class="fa fa-remove"></i></a>';
            }

            if($edit!='' || $delete!=''){

                $op = "concat('".$edit.$delete."')";
                $str = array(
                    "op" => $op

                );

            }

            return $str;
        }

        public function fromTableStr() {
            return 'subscribers a';
        }

        public function joinArray(){
            return null;
        }

        public function whereClauseArray(){
            return null;
        }
   }
?>