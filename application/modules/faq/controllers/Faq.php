<?php

/**
 * User: riyan
 * Date: 04/04/17
 * Time: 15:01
 */

class Faq extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mfaq");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'faq_view';
            $this->add_perm = 'faq_add';
            $this->edit_perm = 'faq_update';
            $this->delete_perm = 'faq_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "faq";
        $this->dttModel = "Mdfaq";
        $this->pk = "faq_id";

    }


    function index(){

        $breadcrumb = array(
            "<i class='fa fa-history'></i> Faq" => base_url()."faq"
        );

        $data['page'] = 'faq';
        $data['page_title'] = 'FAQ';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['faq'] = $this->Mfaq->getFaq();
        $data['data']['parent_menu'] = 'faq';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(
                "title" => $this->input->post("title"),
                "content" => $this->input->post("description"),
                "status" => $this->input->post("status"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s"),
                "client" => $this->session->userdata("client")
            );
            

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-history'></i> FAQ" => base_url()."faq",
                "Tambah FAQ" => base_url()."faq/faq/addnew",
            );

            $data['page'] = 'faq_add';
            $data['page_title'] = 'FAQ';
            $data['page_subtitle'] = 'Tambah FAQ';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['data']['parent_menu'] = 'faq';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(
                "title" => $this->input->post("title"),
                "content" => $this->input->post("description"),
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s"),
                "client" => $this->session->userdata("client")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{
            
            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> FAQ" => base_url()."faq",
                "Edit" => base_url()."faq/faq/edit/".$id,
            );

            $data['page'] = 'faq_edit';
            $data['page_title'] = 'FAQ';
            $data['page_subtitle'] = 'Edit FAQ';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['data']['dataedit'] = $this->Mfaq->getFaqById($id);
            $data['data']['parent_menu'] = 'faq';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    /*
    function rearrange_article_category_position(){

        if(count($_POST)>0){

            $a = array_keys($_POST['menuItem']);
            $b = count($a);

            $seq = 1;

            for($i=0;$i < $b;$i++){

               $id = $a[$i];

               $this->MArticleCategory->setArticleCategorySequence($id, $seq);

               $seq++;

            }

            echo json_encode(array("status" => 1, "msg" => "Article Category position successfully updated!"));
        }

    }
    */

}