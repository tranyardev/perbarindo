<?php

/**
 * User: riyan
 * Date: 04/04/17
 * Time: 10:05
 */

class Mdfaq extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'faq_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'faq_delete');
        $this->cl = $this->session->userdata('client');

    }

    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

            "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",

        );;

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.faq_id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.faq_id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );

        }

        return $str;

    }

    public function fromTableStr() {
        return "faq a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return array(
            "a.client" => $this->cl
        );
    }


}