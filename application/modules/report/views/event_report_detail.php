<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event Report Detail</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Event Report Detail" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Nama</th>
                <th>BPR</th>
                <th>DPP/DPD</th>
                <th>HP</th>
                <th>Keterangan</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Event Report Detail';
            }
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Event Report Detail';
            }
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Event Report Detail';
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Event Report Detail';
            }
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print',
            exportOptions: {
                    columns: ':visible'
            },
            
            title: function(){
                 return '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Event Report Detail</span>';
            }
        },
        'colvis'
    ];

    var columns =  [

        { "data" : "$.member_name"},
        { "data" : "$.bpr"},
        { "data" : "$.dpd"},
        { "data" : "b.no_hp"},
        { "data" : "$.keterangan", "searchable": false},

    ];


    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){


        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "report/eventreport/dataTableDetail",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons

        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

</script>
