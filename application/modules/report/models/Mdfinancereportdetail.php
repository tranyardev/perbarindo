<?php

class Mdfinancereportdetail extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

    }

    public function appendToSelectStr() {

            $str = array(
                "member_name" => "b.name",
                "bpr" => "e.name",
                "dpd" => "f.name",
                "scan_receipt" => "
                                    CASE
                                        WHEN (m.scanned = '1') THEN 'Diambil'
                                        ELSE 'Belum'
                                    END
                                ",
                "keterangan" => "
                                    CASE 
                                        WHEN  (l.status = 'TRANSACTION_COMPLETED') THEN 'Sudah Bayar' 
                                        ELSE 'Belum Bayar' 
                                    END || 
                                    CASE
                                        WHEN  (g.event_member_id > 0) THEN ', Hadir' 
                                        ELSE ', Tidak Hadir' 
                                    END || 
                                    CASE
                                        WHEN  (a.is_twin_sharing = '1') THEN ', Sekamar dengan ' || k.name 
                                        ELSE '' 
                                    END

                                "

            );

        return $str;

    }

    public function fromTableStr() {
        return "event_members a";
    }

    public function joinArray(){
        return array(

            "members b" => "b.id=a.member_id",
            "bpr e" => "e.id=b.bpr",            
            "dpd f" => "f.id=e.dpd",
            "event_member_presences g|left" => "g.event_member_id=b.id",
            "packages h|left" => "h.id=a.package_id",
            "payment_confirmations i|left" => "i.event_registration_id=a.registration_id",
            // "event_registrations j|left" => "j.id=a.registration_id",
            "event_registrations j|left" => "j.id=a.registration_id and j.member_id=a.member_id",
            "members k|left" => "k.id=a.twin_sharing_with",
            "event_registrations l|left" => "l.id=a.registration_id",
            "event_documents m|left" => "m.regcode=l.regcode and m.doc_type_id=2"

        );
    }

    public function whereClauseArray(){
        return array(
            "a.event_id" => $_SESSION['finance_report_id'] //,
            // "j.status" => "TRANSACTION_COMPLETED"
        );
    }


}