<?php

class Mdeventreportdetail extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

    }

    public function appendToSelectStr() {

        // $hadir = '<a class="btn btn-sm btn-primary" href="javascript:detail(\',a.id,\');"><i class="fa fa-book"></i></a>&nbsp;';
        // $op = "concat('".$detail."')";
            $str = array(

                // "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                // "op" => $op
                "member_name" => "b.name",
                "bpr" => "e.name",
                "dpd" => "f.name",
                "keterangan" => "CASE WHEN g.event_member_id > 0 THEN 'Hadir' ELSE 'Tidak Hadir' END"

            );

        return $str;

    }

    public function fromTableStr() {
        return "event_members a";
    }

    public function joinArray(){
        return array(

            "members b" => "b.id=a.member_id",
            "bpr e" => "e.id=b.bpr",            
            "dpd f" => "f.id=e.dpd",
            "event_member_presences g|left" => "g.event_member_id=a.id",

        );
    }

    public function whereClauseArray(){
        return array("a.event_id" => $_SESSION['event_report_id']);
    }


}