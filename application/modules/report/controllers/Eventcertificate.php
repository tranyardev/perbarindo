<?php

class Eventcertificate extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("event/Mevent2");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');
        
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'event_certificate_view';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "events";
        $this->dttModel = "Mdevent";
        $this->pk = "id";

        $this->tableDetail = "event_documents";
        $this->dttModelDetail = "Mdeventcertificate";
        $this->pkDetail = "id";

    }

    function index(){

        $breadcrumb = array(

            "<i class='fa fa-book'></i> Event Report" => base_url()."report/eventcertificate"

        );

        $data['page'] = 'event_certificate';
        $data['page_title'] = 'Event Certificate Report';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'report';
        $data['data']['submenu'] = 'event_certificate';
        $this->load->view('layout/body',$data);

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    function detail($id){
        
        $breadcrumb = array(
            "<i class='fa fa-book'></i> Detail Report " => base_url()."report/eventcertificate",
            "Detail " => base_url()."report/eventcertificate/detail/".$id,
        );

        $_SESSION['event_report_id'] = $id;

        $data['page'] = 'event_certificate_detail';
        $data['page_title'] = 'Detail Report';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'report';
        $data['data']['submenu'] = 'event_certificate_detail';
        $data['data']['event'] = $this->Mevent2->getEventById($id);
        $this->load->view('layout/body',$data);

    }

    public function dataTableDetail() {

        $this->load->library('Datatable', array('model' => $this->dttModelDetail, 'rowIdCol' => 'a.'.$this->pkDetail));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

}