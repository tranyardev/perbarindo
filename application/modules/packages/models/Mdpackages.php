<?php

/**
 * User: riyan
 * Date: 10/06/17
 * Time: 10:05
 */

class Mdpackages extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'packages_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'packages_delete');

    }

    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

            "event_name" => "b.name",
            "package" => "a.name"

        );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "event_name" => "b.name",
                "package" => "a.name",
                "op" => $op

            );

        }

        return $str;

    }

    public function fromTableStr() {
        return "packages a";
    }

    public function joinArray(){
        return array(
            "events b" => "b.id=a.event_id"
        );
    }

    public function whereClauseArray(){
        return null;
    }


}