<?php

class Packages extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mpackages");
        $this->load->model("event/Mevent2");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'packages_view';
            $this->add_perm = 'packages_add';
            $this->edit_perm = 'packages_update';
            $this->delete_perm = 'packages_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "packages";
        $this->dttModel = "Mdpackages";
        $this->pk = "id";

    }

    function index(){

        $breadcrumb = array(
            "<i class='fa fa-history'></i> Packages" => base_url()."packages"
        );

        $data['page'] = 'packages';
        $data['page_title'] = 'Packages';
        $data['page_subtitle'] = '';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['packages'] = $this->Mpackages->getPackages();
        $data['data']['parent_menu'] = 'event_management';
        $data['data']['submenu'] = 'packages';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    
    function addnew(){

        if(count($_POST)>0){

            $data = array(
                "event_id" => $this->input->post("event"),
                "name" => $this->input->post("name"),
                "price" => $this->input->post("price"),                
                "description" => $this->input->post("description"),
                "twin_sharing" => $this->input->post("twin_sharing"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")
            );
            

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-history'></i> Packages" => base_url()."packages",
                "Add Packages" => base_url()."packages/packages/addnew",
            );

            $data['page'] = 'packages_add';
            $data['page_title'] = 'Packages';
            $data['page_subtitle'] = 'Add Packages';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['custom_js'] = array(
                "packages" => base_url()."assets/plugins/jquery-price-format/jquery.priceformat.min.js"
            );            
            $data['data']['event'] = $this->Mevent2->getEvent();
            $data['data']['parent_menu'] = 'event_management';
            $data['data']['submenu'] = 'packages';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(
                "event_id" => $this->input->post("event"),
                "name" => $this->input->post("name"),
                "price" => $this->input->post("price"),
                "description" => $this->input->post("description"),
                "twin_sharing" => $this->input->post("twin_sharing"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{
            
            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Packages" => base_url()."packages",
                "Edit Packages" => base_url()."packages/packages/edit/".$id,
            );

            $data['page'] = 'packages_edit';
            $data['page_title'] = 'Packages';
            $data['page_subtitle'] = 'Edit Packages';
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['custom_js'] = array(
                "packages" => base_url()."assets/plugins/jquery-price-format/jquery.priceformat.min.js"
            );
            $data['data']['event'] = $this->Mevent2->getEvent();
            $data['data']['dataedit'] = $this->Mpackages->getPackagesById($id);
            $data['data']['parent_menu'] = 'event_management';
            $data['data']['submenu'] = 'packages';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

}