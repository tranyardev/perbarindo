<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Bank Account</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                   <div class="form-group">
                        <label for="bank">Bank</label>
                        <input type="text" maxlength="50" name="bank" class="form-control validate[required]" id="bank" placeholder="Bank Name (Required)">
                    </div>
                    <div class="form-group">
                        <label for="account_name">Account Name</label>
                        <input type="text" maxlength="100" name="account_name" class="form-control validate[required]" id="account_name" placeholder="Account Name (Required)">
                    </div>
                    <div class="form-group">
                        <label for="account_number">Account Number</label>
                        <input type="text" maxlength="50" name="account_number" class="form-control validate[required]" id="account_number" placeholder="Account Number (Required)">
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0">Draft</option>
                            <option value="1">Publish</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->
        </div>
        
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"bankaccount/bankaccount/addnew";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            resetForm();

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }
</script>