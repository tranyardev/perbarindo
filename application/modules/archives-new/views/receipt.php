<!DOCTYPE html>
<html>
    <head>  
    <style>

      body {
        background: rgb(204,204,204); 
      }
      page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
      }
      page[size="A4"] {  
        width: 21cm;
        height: 29.7cm; 
      }
      page[size="A4"][layout="portrait"] {
        width: 29.7cm;
        height: 21cm;  
      }
      page[size="A3"] {
        width: 29.7cm;
        height: 42cm;
      }
      page[size="A3"][layout="portrait"] {
        width: 42cm;
        height: 29.7cm;  
      }
      page[size="A5"] {
        width: 14.8cm;
        height: 21cm;
      }
      page[size="A5"][layout="portrait"] {
        width: 21cm;
        height: 28.8cm;
      }
      @media print {
        body, page {
          margin: 0;
          box-shadow: 0;
        }
      }
      
      /*** STYLE COSTUME SETIFIKAT PRINT ***/

       body {
          -webkit-print-color-adjust: exact; 
        }

      table {
          font-size: 12pt;
          font-family: 'Times New Roman', Times, serif;
          color:#9c9b9b;
      }
      
      .table2 tr>td, table tr>td {
          border: 0px solid #DDD;
      }
      
      table tr>td>table {
          border: 0px solid #9e9e9e;
          padding: 0px;
      }
      
      table tr>td>table tr>th {  
          border: 0px solid #fde0bc; 
      }
      
      table tr>td>table tr>td {
          border: 0px solid #DDD;
      }
      
      table tr>td>table tr>th>b {
          font-weight: 600;
          color: #757575; 
      }
      
      table tr>td>table tr>td>span {
          color: #8a8585; 
      }
      
      .bordered-dashed {
          border-style: dashed;
          border-color: #c5c5c5;
          border-width: 1.5px;
          margin-top: 10px;
          margin-bottom: 10px; 
      }
      
      th.rotate {
          white-space: nowrap;
          padding-top: 165px;
          width: 12px; 
      }
      
      th.rotate>div {
          transform: translate(1px, 5px) rotate(630deg);
          width: 12px; 
      }
      
      th.rotate>div>span {
          border-bottom: 1px solid #ccc;
          width: 12px; 
      } 

      /** End Print **/ 

      #border-wrap-rable {
        border-left: 3px solid #ffa500;
        border-bottom: 3px solid #f26127;

        border-right: 0px solid #FFF;
        border-top: 0px solid #FFF;
        margin:0px;
      }
      #wrap-card {
         padding-top:0px;
         padding-bottom:4px;
         padding-left:4px;
         padding-right:0px; 
      }
      #btn-witansi {
        font-weight:600;
        padding:12px;
        background-color: #ffd17e;
        border:0px; 
      }
      #undline {
           text-decoration: underline;
      }

      #box-border {
        border:1px solid orange;
        border-radius:0px 8px 2px;
        background-color:#EEE;
        -webkit-border-radius: 0px 8px 2px;
        padding:4px; 
      }
      #nopadnomar {
        margin:0px;padding:0px;
      }

      .bordered-dashed {
        border-style: dashed !important;
        border-color: orange !important;
        border-width: 1.5px;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    </style>
</head>

<body>

<page size="A5" layout="portrait">

  <br/>
    <fieldset id="wrap-card">
        <fieldset id="border-wrap-rable">
            <?php echo $receipt; ?>
        </fieldset>
    </fieldset>

</page>

</body>
</html>