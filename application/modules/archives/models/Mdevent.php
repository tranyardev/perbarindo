<?php

class Mdevent extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_config = $this->mcore->checkPermission($this->user_group, 'archives_config');

    }

    public function appendToSelectStr() {

        
        $config = '';
        if($this->allow_config){
            $config = '<a class="btn btn-sm btn-warning" href="javascript:config(\',a.id,\');"><i class="fa fa-cog"></i></a>&nbsp;';
        }

        $detail = '<a class="btn btn-sm btn-primary" href="javascript:detail(\',a.id,\');"><i class="fa fa-search"></i></a>&nbsp;';
        
        $op = "concat('".$detail.$config."')";
            $str = array(

                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );

        return $str;

    }

    public function fromTableStr() {
        return "events a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}