<?php

class MDEventMember extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_show_detail = $this->mcore->checkPermission($this->user_group, 'event_member_show_detail');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'event_member_delete');

    }
    public function appendToSelectStr() {

        return array(

            "member" => "b.name",
            "bpr" => "concat(f.name,' ',e.name)",

        );
      
    }

    public function fromTableStr() {

        return "event_members a";
        
    }

    public function joinArray(){
        return array(

            "members b|left" => "b.id=a.member_id",
            "event_registrations c|left" => "c.id=a.registration_id",
            "packages d|left" => "d.id=a.package_id",
            "bpr e|left" => "e.id=c.bpr",
            "corporates f|left" => "f.id=e.corporate",

        );
    }

    public function whereClauseArray(){
        return null;
    }


}