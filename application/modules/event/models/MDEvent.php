<?php

class MDEvent extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'event_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'event_delete');
        $this->allow_show_activity = $this->mcore->checkPermission($this->user_group, 'event_activity');
        $this->allow_show_member = $this->mcore->checkPermission($this->user_group, 'event_member');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $activity = '';
        $member = '';
        $str = array(

            "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END"


        );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_show_activity){
            $activity = '<a class="btn btn-sm btn-warning" href="javascript:show_activities(\',a.id,\');"><i class="fa fa-cog"></i></a>&nbsp;';
        }

        if($this->allow_show_member){
            $member = '<a class="btn btn-sm btn-info" href="javascript:show_members(\',a.id,\');"><i class="fa fa-users"></i></a>';
        }

        if($edit!='' || $delete!='' || $activity!='' || $member!=''){

            $op = "concat('".$edit.$delete.$activity.$member."')";
            $str = array(

                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );


        }

        return $str;
    }

    public function fromTableStr() {
        return "events a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}