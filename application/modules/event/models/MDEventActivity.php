<?php

class MDEventActivity extends MY_Model implements DatatableModel{

    function __construct($params = null){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'event_activity_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'event_activity_delete');
        

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
       
        $str = null;

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

    
        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;
    }

    public function fromTableStr() {
        return "event_activities a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return array("a.event_id" => $_SESSION['event_id']);
    }


}