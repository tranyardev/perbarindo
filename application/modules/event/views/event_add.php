<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="description" id="post_description">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Event</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" maxlength="100" class="form-control validate[required]" id="name" placeholder="Event Name (Required)">
                    </div>
                   
                    <!-- Date range -->
                    <div class="form-group">
                        <label>Date</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="date" class="form-control pull-right" id="event_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <!-- Date range -->
                    <div class="form-group">
                        <label>Registration Date</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="reg_date" class="form-control pull-right" id="event_registration_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <!-- /.form group -->
                    <div class="form-group">
                        <label for="description">Description</label>
                        <div id="validate_content" class="descriptionformError parentFormform formError" style="opacity: 0.87; position: absolute; top: 319px; left: 745px; right: initial; margin-top: -50px; display: none;"><div class="formErrorContent">* This field is required</div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>
                        <textarea class="form-control" id="description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0">Draft</option>
                            <option value="1">Publish</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="quota">Quota</label>
                        <input type="text" name="quota" maxlength="100" class="form-control" id="quota" placeholder="Quota">
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cover</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="cover">Cover</label>
                        <input type="file" class="form-control validate[required]" name="cover" id="cover">
                        <div class="img-prev" reqired id="cover_preview"><h1>Cover Event</h1></div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>

        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Address Detail</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <input id="searchInput" class="controls" type="text" placeholder="Enter a location">
                    <div id="map"></div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control validate[required]" id="location" placeholder="Address">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" maxlength="50" name="country" class="form-control validate[required]" id="country" placeholder="Country">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" maxlength="50" name="city" class="form-control validate[required]" id="city" placeholder="City">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <input type="text" readonly name="latitude" class="form-control" id="lat" placeholder="Latitude" value="0">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="longitude">Longitude</label>
                                <input type="text" readonly name="longitude" class="form-control" id="lon" placeholder="Longitude" value="0">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>

            </div>

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){
        CKEDITOR.replace( 'description',ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

            });

        }

        $("#cover").change(function(){
            readURL(this, "#cover");
        });

        $("#form").submit(function(){

            var content =  $("#post_description").val();

            if(content!=""){

                if($(this).validationEngine('validate')){



                        $("#validate_content").hide();

                        showLoading();

                        setTimeout('saveFormData();',3000);


                }

            }else{

                $("#validate_content").show();

                $("html, body").animate({ scrollTop: 0 }, 600);

            }

            return false;

        });

        $("#form").validationEngine();

        //Date range picker
        $('#event_date').daterangepicker();
        $('#event_registration_date').daterangepicker();

    });

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Cover Module</h1>");
        $(selector).val('');

    }

    function saveFormData(){

        var target = base_url+"event/event/addnew";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }

                resetForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){


        $('#form')[0].reset();
        $("#cover_preview").html('<h1>Cover Module</h1>');
        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }
</script>