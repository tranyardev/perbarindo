<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="description" id="post_description" value="<?php echo $dataedit['activity']; ?>">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Event Activity</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    
                    <div class="form-group">
                        <label>Date</label>

                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="date" class="form-control pull-right" value="<?php echo $dataedit['date']; ?>" id="datepicker">
                        </div>

                    </div>

                    <!-- Date range -->
                    <div class="form-group">
                        <label>Start Time</label>

                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="start_time" name="start_time" type="text" class="form-control input-small" value="<?php echo $dataedit['start_time']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>End Time</label>

                        <div class="input-group bootstrap-timepicker timepicker">
                            <input id="end_time" name="end_time" type="text" class="form-control input-small" value="<?php echo $dataedit['end_time']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                        <!-- /.input group -->
                    </div>

                   

                    <div class="form-group">
                        <label for="updateable">Updateable</label>
                        <select class="form-control" name="updateable" id="updateable">
                            <option value="1" <?php ($dataedit['updateable']=="1")? $selected="selected": $selected="";echo $selected; ?> >Yes</option>
                            <option value="0" <?php ($dataedit['updateable']=="0")? $selected="selected": $selected="";echo $selected; ?>>No</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type" id="type">
                            <option value="1" <?php ($dataedit['type']=="1")? $selected="selected": $selected="";echo $selected; ?>>Primary</option>
                            <option value="0" <?php ($dataedit['type']=="0")? $selected="selected": $selected="";echo $selected; ?>>Optional</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary">
              
                <div class="box-body">
                    
           
                    <div class="form-group">
                            <label for="description">Activity</label>
                            <textarea class="form-control" id="description"><?php echo $dataedit['activity']; ?></textarea>
                        </div>

                    </div>
          

                <div class="box-footer">

                </div>

            </div>

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){
        CKEDITOR.replace( 'description',ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

            });

        }

        $("#start_time").timepicker();
        $("#end_time").timepicker();
        $("#datepicker").datepicker();


        $("#form").submit(function(){

            var content =  $("#post_description").val();

            if(content!=""){

                if($(this).validationEngine('validate')){

                    $("#validate_content").hide();

                    showLoading();

                    setTimeout('saveFormData();',3000);

                }

            }else{

                $("#validate_content").show();

                $("html, body").animate({ scrollTop: 0 }, 600);

            }

            return false;

        });

        $("#form").validationEngine();

    });

   
    function saveFormData(){

        var target = base_url+"event/event/edit_activity/<?php echo $dataedit['id'] ?>";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){


        $('#form')[0].reset();
        $("#cover_preview").html('<h1>Cover Module</h1>');
        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }
</script>