<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Event extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('googlemaps');
        $this->load->library('excel');
        $this->load->library("ciqrcode");
        $this->load->model("mcore");
        $this->load->model("MEventCategory");
        $this->load->model("MEvent");
        $this->load->model("MEventActivity");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'event_view';
            $this->add_perm = 'event_add';
            $this->edit_perm = 'event_update';
            $this->delete_perm = 'event_delete';

            $this->activity_read_perm = 'event_activity_view';
            $this->activity_add_perm = 'event_activity_add';
            $this->activity_edit_perm = 'event_activity_update';
            $this->activity_delete_perm = 'event_activity_delete';

            $this->member_read_perm = 'event_member_view';
            $this->member_add_perm = 'event_member_add';
            $this->member_edit_perm = 'event_member_update';
            $this->member_delete_perm = 'event_member_delete';

            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "events";
        $this->dttModel = "MDEvent";

        $this->tableActivity = "event_activities";
        $this->dttModelActivity = "MDEventActivity";
        $this->dttModelMember = "MDEventMember";

        $this->pk = "id";

        $this->aauthTable = "aauth_users";
        $this->aauthPk = "id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-calendar'></i> Program " => base_url()."event/event"
        );

        $data['page'] = 'event';
        $data['page_title'] = 'Event';
        $data['page_subtitle'] = 'Event Management';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'event';
        $data['data']['submenu'] = 'event_management';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $data['data']['activity_read_perm'] = $this->mcore->checkPermission($this->user_group, $this->activity_read_perm);
        $data['data']['member_read_perm'] = $this->mcore->checkPermission($this->user_group, $this->member_read_perm);
        $this->load->view('layout/body',$data);


    }
    function activities($id){

        $breadcrumb = array(
            "<i class='fa fa-calendar'></i> Event " => base_url()."event/event",
            "Activities " => base_url()."event/event/activities/".$id,
        );

        $_SESSION['event_id'] = $id;

        $data['page'] = 'event_activities';
        $data['page_title'] = 'Event Activities';
        $data['page_subtitle'] = 'Event Activities Management';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'event';
        $data['data']['submenu'] = 'event_management';
        $data['data']['event'] = $this->MEvent->getEventById($id);
        $data['data']['activity_add_perm'] = $this->mcore->checkPermission($this->user_group, $this->activity_add_perm);
        $data['data']['activity_edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->activity_edit_perm);
        $data['data']['activity_delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->activity_delete_perm);
        $this->load->view('layout/body',$data);


    }
    function members($id){

        $breadcrumb = array(
            "<i class='fa fa-calendar'></i> Event " => base_url()."event/event",
            "Members " => base_url()."event/event/members/".$id,
        );

        $_SESSION['event_id'] = $id;

        $data['page'] = 'event_members';
        $data['page_title'] = 'Event Member';
        $data['page_subtitle'] = 'Event Member Management';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'event';
        $data['data']['submenu'] = 'event_management';
        $data['data']['event'] = $this->MEvent->getEventById($id);
        $data['data']['member_add_perm'] = $this->mcore->checkPermission($this->user_group, $this->member_add_perm);
        $data['data']['member_edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->member_edit_perm);
        $data['data']['member_delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->member_delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    public function dataTableActivity() {

        $this->load->library('Datatable', array('model' => $this->dttModelActivity, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    public function dataTableMember() {

        $this->load->library('Datatable', array('model' => $this->dttModelMember, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){


            /* upload cover */

            if($_FILES["cover"]["name"]!=""){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $date = urldecode($this->input->post("date"));
            $ex_date = explode("-",$date);
            $start_date = trim($ex_date[0]);
            $end_date = trim($ex_date[1]);

            $date = urldecode($this->input->post("reg_date"));
            $ex_date = explode("-",$date);
            $reg_start_date = trim($ex_date[0]);
            $reg_end_date = trim($ex_date[1]);

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "start_date" => $start_date,
                "end_date" => $end_date,
                "registration_start_date" => $reg_start_date,
                "registration_end_date" => $reg_end_date,
                "address" => $this->input->post("address"),
                "city" => $this->input->post("city"),
                "country" => $this->input->post("country"),
                "latitude" => $this->input->post("latitude"),
                "longitude" => $this->input->post("longitude"),
                "cover" => $cover,
                "status" => $this->input->post("status"),
                "quota" => $this->input->post("quota"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")

            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){

                 /* media */

                $media = array(

                    "file" => $cover,
                    "path" => 'upload/event/',
                    "object_id" => $this->db->insert_id(),
                    "type" => "image",
                    "module" => "event",
                    "format" => $extension,
                    "created_by" => $this->session->userdata('id'),
                    "created_at" => date("Y-m-d h:i:s"),

                );

                $this->db->insert("media", $media);

                /* end media */

                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event" => base_url()."event",
                "Add New" => base_url()."event/event/addnew",
            );

            $data['page'] = 'event_add';
            $data['page_title'] = 'Event';
            $data['page_subtitle'] = 'Add New Event';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.css",
            );
            $data['custom_js'] = array(
                "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.js",
            );


            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
    
            $data['data']['parent_menu'] = 'event';
            $data['data']['submenu'] = 'event_management';
            $this->load->view('layout/body',$data);

        }



    }

    function addnew_activity($id){

        if(count($_POST)>0){


            $data = array(

                "event_id" => $id,
                "activity" => $this->input->post("description"),
                "date" => $this->input->post("date"),
                "start_time" => $this->input->post("start_time"),
                "end_time" => $this->input->post("end_time"),
                "updateable" => $this->input->post("updateable"),
                "type" => $this->input->post("type"),
               
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")

            );

            $insert = $this->db->insert($this->tableActivity, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event" => base_url()."event",
                "Event Activity" => base_url()."event/event/activities/".$id,
                "Add New" => base_url()."event/event/addnew_activity/".$id,
            );

            $data['page'] = 'event_activity_add';
            $data['page_title'] = 'Event Activity';
            $data['page_subtitle'] = 'Add New Event Activity';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.css",
            );
            $data['custom_js'] = array(
                "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.js",
            );


            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
    
            $data['data']['event_id'] = $id;
            $data['data']['parent_menu'] = 'event';
            $data['data']['submenu'] = 'event_management';
            $this->load->view('layout/body',$data);

        }



    }


    function edit($id){

        if(count($_POST)>0){

            /* upload cover */

            if($_FILES["cover"]["name"]!=""){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    $old_cover = $upload_path.'/'.@$_POST['old_cover'];

                    if(file_exists($old_cover)){

                        @unlink($old_cover);

                    }


                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $cover = $this->input->post("old_cover");

            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }


            $date = urldecode($this->input->post("date"));
            $ex_date = explode("-",$date);
            $start_date = trim($ex_date[0]);
            $end_date = trim($ex_date[1]);
           
            $date = urldecode($this->input->post("reg_date"));
            $ex_date = explode("-",$date);
            $reg_start_date = trim($ex_date[0]);
            $reg_end_date = trim($ex_date[1]);

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "start_date" => $start_date,
                "end_date" => $end_date,
                "registration_start_date" => $reg_start_date,
                "registration_end_date" => $reg_end_date,
                "address" => $this->input->post("address"),
                "city" => $this->input->post("city"),
                "country" => $this->input->post("country"),
                "latitude" => $this->input->post("latitude"),
                "longitude" => $this->input->post("longitude"),
                "cover" => $cover,
                "status" => $this->input->post("status"),
                "quota" => $this->input->post("quota"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")

            );


            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){

                 /* media */

                $media = array(

                    "file" => $cover,
                    "type" => "image",
                    "format" => $extension,
                    "updated_by" => $this->session->userdata('id'),
                    "updated_at" => date("Y-m-d h:i:s"),
                );

                $this->db->where("object_id", $id);
                $this->db->where("module", "event");
                $this->db->update("media", $media);

                /* end media */

                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event" => base_url()."event/event",
                "Edit" => base_url()."event/event/edit/".$id,
            );

            $data['page'] = 'event_edit';
            $data['page_title'] = 'Event';
            $data['page_subtitle'] = 'Edit Event';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.css",
            );
            $data['custom_js'] = array(
                "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.js",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MEvent->getEventById($id);
            $data['data']['parent_menu'] = 'event';
            $data['data']['submenu'] = 'event_management';
            $this->load->view('layout/body',$data);

        }

    }

    function edit_activity($id){

        if(count($_POST)>0){

            $data = array(

                "activity" => $this->input->post("description"),
                "date" => $this->input->post("date"),
                "start_time" => $this->input->post("start_time"),
                "end_time" => $this->input->post("end_time"),
                "updateable" => $this->input->post("updateable"),
                "type" => $this->input->post("type"),
               
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")

            );


            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->tableActivity, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event" => base_url()."event/event",
                "Edit" => base_url()."event/event/edit/".$id,
            );

            $data['page'] = 'event_activity_edit';
            $data['page_title'] = 'Event';
            $data['page_subtitle'] = 'Edit Event';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.css",
            );
            $data['custom_js'] = array(
                "googlemaps" => "https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initMap",
                "gmaps_autocomplete" => base_url()."assets/plugins/gmaps/gmaps.js",
                "daterange" => base_url()."assets/plugins/daterangepicker/daterangepicker.js",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MEventActivity->getEventActivityById($id);
            $data['data']['parent_menu'] = 'event';
            $data['data']['submenu'] = 'event_management';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){
                                                                                                  
        if(count($_POST)>0){

            $id = $this->input->post("id");

            $event = $this->MEvent->getEventById($id);

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';

            $cover = $upload_path.'/'.$event['cover'];

            if(file_exists($cover)){

                @unlink($cover);

            }

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

     function removeActivity(){
                                                                                                  
        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->tableActivity);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    function import($id){


        if($_FILES["excel"]["name"]!=""){

            if($_FILES["excel"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
                $_FILES["excel"]["type"]=="application/vnd.ms-excel" or
                $_FILES["excel"]["type"]=="application/x-msexcel" or
                $_FILES["excel"]["type"]=="application/x-ms-excel" or
                $_FILES["excel"]["type"]=="application/x-excel" or
                $_FILES["excel"]["type"]=="application/x-dos_ms_excel" or
                $_FILES["excel"]["type"]=="application/xls" or
                $_FILES["excel"]["type"]=="application/wps-office.xlsx" or
                $_FILES["excel"]["type"]=="application/x-xls" or
                $_FILES["excel"]["type"]=="application/msexcel"){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/excel';
                $array = explode('.', $_FILES['excel']['name']);
                $extension = end($array);
                $file = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["excel"]["tmp_name"], $upload_path."/".$file)) {

                    $import = $this->event_member_import($upload_path."/".$file,$id); 

                    if($import){

                        @unlink($upload_path."/".$file);

                    }

                    $res = array("status" => "1", "msg" => "Data imported Successfully");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Something went wrong while uploading file");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid file extention. Must be .xls or .xlsx");

            }

            echo json_encode($res);

        }

    }

    function event_member_import($file,$event_id){

        $this->load->model("master/Mbpr");
        $this->load->model("master/Mdpd");
        $this->load->model("member/Mjobposition2");
        $this->load->model("master/Mcorporate");

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

        for($i=2;$i<=$totalrows;$i++)
        {

            $dpd= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
            $pt= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
            $bpr= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
            $member_name= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
            $job_position= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
            $phone= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
            $email= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); 
            $gender= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
            $packet_cost= $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
            $total_paid= $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
            $p_date= $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
            $information= $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();

            $UNIX_DATE = ($p_date - 25569) * 86400;
            $paid_date = gmdate("Y-m-d", $UNIX_DATE);

            $member_name = trim($member_name);



            /*$member_name_temp=trim(str_replace('/[^A-Za-z]/', '', $member_name));

            if($email){
                $create_user = $this->aauth->create_user($email,$email,$email);
            }else{
                $member_name_temp = substr($member_name_temp,0,5);
                $member_name_temp .= $this->randomString();
                $create_user = $this->aauth->create_user($member_name_temp.'@example.com',$member_name_temp,$member_name_temp);
            }*/

            // $this->aauth->print_errors();

            

            $dpd_data = $this->Mdpd->getDPDByName(trim($dpd));
            if(count($dpd_data)<1){
                
                $dpd_table = "dpd";

                $data = array(
                    "name" => $dpd, 
                );

                $this->db->insert($dpd_table,$data);

                $dpd_data = $this->Mdpd->getDPDByName(trim($dpd));
            }

            $pt_data = $this->Mcorporate->getCorporateByName(trim($pt));
            if(count($pt_data)<1){
                
                $pt_table = "corporates";

                $data = array(
                    "name" => $pt, 
                );

                $this->db->insert($pt_table,$data);

                $pt_data = $this->Mcorporate->getCorporateByName(trim($pt));
            }

            $bpr_data = $this->Mbpr->getBPRByName(trim($bpr));
            if(count($bpr_data)<1){
                
                $bpr_table = "bpr";

                $data = array(
                    "name" => $bpr, 
                    "corporate" => @$pt_data[0]['id'], 
                    "dpd" => @$dpd_data[0]['id'], 
                );

                $this->db->insert($bpr_table,$data);

                $bpr_data = $this->Mbpr->getBPRByName(trim($bpr));
            }

            if(!$job_position){
                $job_position = "-";
            }
            $jp_data = $this->Mjobposition2->getJobpositionByName(trim($job_position));
            if(count($jp_data)<1){
                
                $jp_table = "job_positions";

                $data = array(
                    "name" => $job_position, 
                );

                $this->db->insert($jp_table,$data);

                $jp_data = $this->Mjobposition2->getJobpositionByName(trim($job_position));
            }


            /* MEMBER */

            $member_table = "members";
            $exist = $this->db->get_where($member_table, array('name' => $member_name, 'bpr' => @$bpr_data[0]['id']));

            if($exist->num_rows()<1){

                $data = array(
                    "name" => $member_name, 
                    "gender" => $gender,
                    "no_hp" => $phone,
                    "email" => $email,
                    "job_position" => @$jp_data[0]['id'],
                    "status" => '1',
                    "bpr" => @$bpr_data[0]['id'],
                    "created_by" => $this->session->userdata("id"),
                    "created_at" => date("Y-m-d h:i:s")
                );

                $insert = $this->db->insert($member_table,$data);

                if($insert){

                    $last_id = $this->db->insert_id();
                    $member_active_id = $last_id;
                    
                    $member_name_temp=trim(preg_replace('/[^A-Za-z]/', '', $member_name));

                    if($email){
                        $create_user = $this->aauth->create_user($email,$email,$email);
                    }else{
                        $member_name_temp = substr($member_name_temp,0,5);
                        $member_name_temp .= $this->randomString();
                        $create_user = $this->aauth->create_user($member_name_temp.'@example.com',$member_name_temp,$member_name_temp);
                    }                        

                    if($create_user){

                        $last_idaauth = $this->db->insert_id();
                        $this->aauth->add_member($last_idaauth, 10); // 10 is hardcode id for group member

                        $aauth_member_id = $last_idaauth;

                        $data = array(
                            "first_name" => $member_name_temp,
                            "status" => '1',
                        );

                        $this->db->where($this->aauthPk, $last_idaauth);
                        
                        $updateaauthuser = $this->db->update($this->aauthTable, $data);
                        // $this->db->update($this->aauthTable, $data);

                        if($updateaauthuser){

                            $data = array(
                                "aauth_user_id" => $last_idaauth,
                            );

                            $this->db->where("id", $last_id);
                            
                            $this->db->update("members", $data);
                            /*$updatemember = $this->db->update($this->table, $data);*/

                            /*if($updatemember){
                                $res = array("status" => "1", "msg" => "Successfully add data!");
                            }else{
                                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");    
                            }*/
                            
                        } /*else{
                            $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                        }*/

                        /*echo json_encode($res);*/

                    }/*else{
                        // $res = array("status" => "0", "msg" => "Oop! Something went wrong. Could not create user.");
                        $res = array("status" => "0", "msg" => print_r($this->aauth->get_errors_array()));
                        echo json_encode($res);
                    }*/

                }/*else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                    echo json_encode($res);
                }*/

            }else{
                $member_active = $exist->result_array();
                $member_active_id = @$member_active[0]['id'];
                $aauth_member_id = @$member_active[0]['aauth_user_id'];
            }

            /* EVENT REGISTRATION */

            $event_reg_table = "event_registrations";
            $event_reg = $this->db->get_where($event_reg_table, array('member_id' => $member_active_id, 'event_id' => $event_id));

            if($event_reg->num_rows()<1){

                $mtime = microtime();
                $microtime = substr($mtime,3,5);

                $regcode = "PRB-".date("Ymdhis").$microtime;
                $uniq_number = rand ( 100 , 999 );

                $expired_trx = $this->MEvent->getConfigurationItem('PAYMENT','EXPIRED_TRX');
                $expired_date = date("Y-m-d H:i:s", strtotime('+'.$expired_trx.' hours'));

                if($total_paid != $packet_cost){
                    $status_payment_reg = "WAITING_CONFIRMATION";
                }else{
                    $status_payment_reg = "TRANSACTION_COMPLETED";
                }                

                $data_reg = array(

                    "event_id" => $event_id,
                    "date" => date("Y-m-d h:i:s"),
                    "total_cost" => $packet_cost,
                    "regcode" => $regcode,
                    "bpr" => @$bpr_data[0]['id'],
                    "member_id" => $member_active_id,
                    "uniq_number" => $uniq_number,
                    "expired_date" => $expired_date,
                    "status" => $status_payment_reg,
                    "aauth_user_id" => $aauth_member_id, //wrong here

                );

                $reg = $this->db->insert($event_reg_table,$data_reg);
                $id_reg = $this->db->insert_id();

            }else{
                $event_reg_res = $event_reg->result_array();
                $id_reg = @$event_reg_res[0]['id'];
                $regcode = @$event_reg_res[0]['regcode'];
            }

            /* EVENT MEMBER */

            $package_data = $this->db->get_where("packages", array('event_id' => $event_id, 'price' => $packet_cost))->result_array();

            $event_member_table = "event_members";
            $event_member = $this->db->get_where($event_member_table, array('member_id' => $member_active_id, 'event_id' => $event_id, 'registration_id' => $id_reg));

            if($event_member->num_rows()<1){

                $data_event_member = array(

                    "event_id" => $event_id,
                    "registration_id" => $id_reg,
                    "member_id" => $member_active_id,
                    "package_id" => @$package_data[0]['id'],
                    "is_twin_sharing" => "0",
                    "information" => $information,

                );

                $this->db->insert($event_member_table,$data_event_member);

            }/*else{
                $event_member_res = $event_member->result_array();
                $id_event_member = $event_member_res[0]['id'];
            }*/

            /* PAYMENT CONFIRMATION */

            if($total_paid != $packet_cost){
                $status_payment_reg = "0";
            }else{
                $status_payment_reg = "1";
            }

            $data_payment_confirmation = array(

                "member_id" => $aauth_member_id,
                "event_registration_id" => $id_reg,
                "from_account" => $member_name,
                "rek_number" => "1",
                "to_account" => 1,
                "doc_transfer" => "existing-data-manual.png",
                "status" => $status_payment_reg,
                "regcode" => $regcode,
                "transfer_date" => $paid_date,
                "bank" => "existing-data-manual",

            );

            $this->db->insert("payment_confirmations",$data_payment_confirmation);

            /*
            $dpd = $this->Mdpd->getDPDByName(trim($dpd));
            $corporate = $this->Mcorporate->getCorporateByName(trim($corporate));

            $data = array(
                "name" => $name, 
                "corporate" => @$corporate[0]['id'],
                "dpd" => @$dpd[0]['id'],
                "address" => $address,
                "telp" => $telp,
                "email" => $email,
                "fax" => $fax,
                "website" => $website,
            );

            $exist = $this->db->get_where($this->table, array('name' => $name));

            if($exist->num_rows()<1){

                $this->db->insert($this->table,$data);

            }*/

        }   

        return true;

    }

    function randomString($length = 6) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    function generate_ticket_event($regcode){

        $event_registration = $this->MEvent->getEventRegistration($regcode);
        $event_member = $this->MEvent->getEventMember($event_registration['reg_id'],'0');
        $event_member_twin_sharing = $this->MEvent->getEventMember($event_registration['reg_id'],'1');


        // if($this->session->userdata('id') != $event_registration['aauth_user_id']){

        //     redirect('myprofile');

        // }

        $html = "";

        $i=1;
        foreach($event_member as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Event</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $html.="<div class='page-break'></div>";

        $i=1;
        foreach($event_member_twin_sharing as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];
            $reservation_code2 = $event_registration['regcode'].'-'.$em['twin_sharing_with'];

            $sharing_with = $this->mevent->getEventMemberById($event_registration['reg_id'], $em['twin_sharing_with'], '2');


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);
            $qrcode2 = $this->generate_qr_code($reservation_code2, $reservation_code2);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$sharing_with['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$em['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$sharing_with['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code2.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode2.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        

        
        // $data['tickets'] = $html;
        // $this->load->view('event_ticket',$data);
        // return $top.$html.$below;
        // return $top.$html.$below;
        return $html;


    }

    function ssh2($command){

        $connection = ssh2_connect('10.50.11.50', 22);
        ssh2_auth_password($connection, 'root', '4k535TranyaR');

        $stream1= ssh2_exec($connection, $command);

        stream_set_blocking($stream1, true);

        $output = stream_get_contents($stream1);

    }

    function generate_qr_code($text,$filename){

        $params['data'] = $text;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $_SERVER['DOCUMENT_ROOT'].'/upload/qrcode/'.$filename;
        $this->ciqrcode->generate($params);

        $command = "chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/upload/qrcode';

        $this->ssh2($command);

        return '<img src="'.base_url().'upload/qrcode/'.$filename.'" class="img-responsive" style="width: auto;height: 70px;">';

    }

    public function gen_ticket_all_dpd($event_id = 12){
        
        $memberEvent = $this->MEvent->getAllEventMember($event_id);        
        
        $data = "";
        foreach($memberEvent as $mE){ 

            $regcode = $this->MEvent->getRegCode($mE['registration_id']);

            // echo $regcode['regcode'].'<br>';
            $data .= $this->generate_ticket_event($regcode['regcode']);

        }
        

        $top = '
            <!DOCTYPE html>
            <html>
                <head>  

                <style> 

                  body {
                    background: rgb(204,204,204); 
                  }
                  page {
                    background: white;
                    display: block;
                    margin: 0 auto;
                    margin-bottom: 0.5cm;
                    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                  }
                  page[size="A4"] {  
                    width: 21cm;
                    min-height: 29.7cm; 
                  }
                  page[size="A4"][layout="portrait"] {
                    width: 29.7cm;
                    min-height: 21cm;  
                  }
                  page[size="A3"] {
                    width: 29.7cm;
                    height: 42cm;
                  }
                  page[size="A3"][layout="portrait"] {
                    width: 42cm;
                    height: 29.7cm;  
                  }
                  page[size="A5"] {
                    width: 14.8cm;
                    height: 21cm;
                  }
                  page[size="A5"][layout="portrait"] {
                    width: 21cm;
                    height: 14.8cm;  
                  }
                  @media print {
                    body, page {
                      margin: 0;
                      box-shadow: 0;
                    }
                  }
                  table {
                      font-size: 12pt;
                      font-family: "Times New Roman", Times, serif;
                  }
                  
                  .table2 tr>td,
                  table tr>td {
                      border: 1px solid #DDD; 
                  }
                  
                  table tr>td>table {
                      border: 1px solid #9e9e9e;
                      padding: 0px;
                  }
                  
                  table tr>td>table tr>th {  
                      border: 0px solid #fde0bc; 
                  }
                  
                  table tr>td>table tr>td {
                      border: 0px solid #DDD;
                      background-color: #FFF; 
                  }
                  
                  table tr>td>table tr>th>b {
                      font-weight: 600;
                      color: #757575; 
                  }
                  
                  table tr>td>table tr>td>span {
                      color: #8a8585; 
                  }
                  
                  .bordered-dashed {
                      border-style: dashed;
                      border-color: #c5c5c5;
                      border-width: 1.5px;
                      margin-top: 10px;
                      margin-bottom: 10px; 
                  }
                  
                  th.rotate {
                      white-space: nowrap;
                      padding-top: 165px;
                      width: 12px; 
                  }
                  
                  th.rotate>div {
                      transform: translate(1px, 5px) rotate(630deg);
                      width: 12px; 
                  }
                  
                  th.rotate>div>span {
                      border-bottom: 1px solid #ccc;
                      width: 12px; 
                  }

                  @media print {
                      .page-break {page-break-after: always;}
                  }

                  /** End Print **/ 
                </style>
            </head>

            <body>

              <page size="A4" >
              <br>
        ';

        $below = '
              </page>
              <br>
            </body>
            </html>
        ';

        echo $top.$data.$below;

    }

    public function gen_ticket_by_dpd_id($event_id = 12, $id_dpd = 24){
        
        // echo $event_id.' '.$id_dpd;
        // exit;

        $memberEvent = $this->MEvent->getEventMemberForTicketByDpd($event_id, $id_dpd);

        $data = "";
        foreach($memberEvent as $mE){ 

            $data .= $this->generate_ticket_event($mE['regcode']);

        }

        $top = '
            <!DOCTYPE html>
            <html>
                <head>  

                <style> 

                  body {
                    background: rgb(204,204,204); 
                  }
                  page {
                    background: white;
                    display: block;
                    margin: 0 auto;
                    margin-bottom: 0.5cm;
                    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                  }
                  page[size="A4"] {  
                    width: 21cm;
                    min-height: 29.7cm; 
                  }
                  page[size="A4"][layout="portrait"] {
                    width: 29.7cm;
                    min-height: 21cm;  
                  }
                  page[size="A3"] {
                    width: 29.7cm;
                    height: 42cm;
                  }
                  page[size="A3"][layout="portrait"] {
                    width: 42cm;
                    height: 29.7cm;  
                  }
                  page[size="A5"] {
                    width: 14.8cm;
                    height: 21cm;
                  }
                  page[size="A5"][layout="portrait"] {
                    width: 21cm;
                    height: 14.8cm;  
                  }
                  @media print {
                    body, page {
                      margin: 0;
                      box-shadow: 0;
                    }
                  }
                  table {
                      font-size: 12pt;
                      font-family: "Times New Roman", Times, serif;
                  }
                  
                  .table2 tr>td,
                  table tr>td {
                      border: 1px solid #DDD; 
                  }
                  
                  table tr>td>table {
                      border: 1px solid #9e9e9e;
                      padding: 0px;
                  }
                  
                  table tr>td>table tr>th {  
                      border: 0px solid #fde0bc; 
                  }
                  
                  table tr>td>table tr>td {
                      border: 0px solid #DDD;
                      background-color: #FFF; 
                  }
                  
                  table tr>td>table tr>th>b {
                      font-weight: 600;
                      color: #757575; 
                  }
                  
                  table tr>td>table tr>td>span {
                      color: #8a8585; 
                  }
                  
                  .bordered-dashed {
                      border-style: dashed;
                      border-color: #c5c5c5;
                      border-width: 1.5px;
                      margin-top: 10px;
                      margin-bottom: 10px; 
                  }
                  
                  th.rotate {
                      white-space: nowrap;
                      padding-top: 99px;
                      width: 12px; 
                  }
                  
                  th.rotate>div {
                      transform: translate(1px, 5px) rotate(630deg);
                      width: 12px; 
                  }
                  
                  th.rotate>div>span {
                      border-bottom: 1px solid #ccc;
                      width: 12px; 
                  }

                  @media print {
                      .page-break {page-break-after: always;}
                  }

                  /** End Print **/ 
                </style>
            </head>

            <body>

              <page size="A4" >
              <br>
        ';

        $below = '
              </page>
              <br>
            </body>
            </html>
        ';

        echo $top.$data.$below;
    }

}