<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Event_member extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MEventMember");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'event_member_view';
            $this->show_detail_perm = 'event_member_show_detail';
            $this->delete_perm = 'event_member_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "event_members";
        $this->dttModel = "MDEventMember";
        $this->pk = "event_member_id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-calendar'></i> Event Category" => base_url()."event/event_member"
        );

        $data['page'] = 'event_member';
        $data['page_title'] = 'Event';
        $data['page_subtitle'] = 'Event Member';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'event';
        $data['data']['submenu'] = 'event_member';
        $data['data']['show_detail_perm'] = $this->mcore->checkPermission($this->user_group, $this->show_detail_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function show_detail($id){

            $this->mcore->checkPermission($this->user_group, $this->show_detail_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event Member" => base_url()."event/event_member",
                "Detail" => base_url()."event/event_member/show_detail/".$id,
            );

            $data['page'] = 'event_member_detail';
            $data['page_title'] = 'Event Member';
            $data['page_subtitle'] = 'Registration Detail';
            $data['custom_css'] = array(
                "event" => base_url()."assets/modules/event/assets/css/event.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['member'] = $this->MEventMember->getEventMemberById($id);
            $data['data']['parent_menu'] = 'event';
            $data['data']['submenu'] = 'event_member';
            $this->load->view('layout/body',$data);



    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}