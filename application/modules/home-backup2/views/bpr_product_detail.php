 <div class="wrapper margin-topfix">
    <div class="page-header page-header-small"> 
         <div class="page-header-image lazy" data-parallax="true" style="background-image: url('http://perbarindo.tranyar.com/mocukup/Bghijau.png');">
                </div> 
        </div> 
    </div>  
    <div class="section">
        <div class="container">


        <section>
          <div class="row" style=" margin-top: -444px;" >
            <div class="col-md-12"> 
                <div class="card">  
                    <div  >      
                        <span class="label label-default">
                            <i class="fa fa-clock-o"></i> <?php echo $post['created_at']; ?>
                        </span>
                          &nbsp;
                        <span class="label label-author">
                            <i class="fa fa-user"></i> 
                            <?php echo $post['author']; ?> - <a href="<?php echo base_url(); ?>profile/bpr/<?php echo $post['bpr']; ?>"><?php echo $post['corp']; ?> <?php echo $post['bpr_name']; ?></a>
                        </span> 

                        <div class="text-center padding-12" >
                            <h3 class="title titel-total margin-0"  ><?php echo $post['name']; ?></h3>
                            <h6 class="description margin-0 text-orange" ><i class="fa fa-chain"></i> <?php echo $post['product_category']; ?></h6>
                        </div>
                    </div>
                        <!-- <hr> --> 
                        <div class="panel panel-default">
                            
                            <div class="panel-body">

                                  <div class="col-md-10 ml-md-auto mr-md-auto padding-12">
                                        <?php if($post['cover']!=""){ ?>
                                        <div class="cover" style="text-align: center">
                                            <img src="<?php echo base_url().'assets/dist/img/preloader-big.gif'; ?>" data-src="<?php echo $cover; ?>" class="lazy img-responsive">
                                        </div>
                                        <?php } ?>
                                        <div class="text-dexsbpr"><?php echo $post['description']; ?></div> 
                                       
                                      <br>
                                    <div class="box-share">
                                        <div class="listing-info__footer clrfix">
                                            <div class="share-wrapper l-sm-mar-bot-4">
                                                <h3 class="label-primary l-sm-align-left">Bagikan :</h3>
                                                <!-- Listing Share Options -->
                                                <div class="js-share-wrapper l-sm-align-left">
                                                    <section>
                                                        <section>
                                                            <ul class="grouped-ico grouped-ico--share-links js-social-share-list fx--fade-in" data-automation="social-share-inline">
                                                                <li>
                                                                    <a class="js-social-share btn btn-primary btn-round " href="https://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" target="_blank" title="Share this page on Facebook">
                                                                        <i class="ico-facebook-badge ico--color-understated-link ico--xlarge" data-automation="facebook-share-link">
                                                                        </i>
                                                                        <span class="is-hidden-accessible"><i class="fa fa-facebook-official"></i> Facebook</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a class="js-social-share btn btn-primary btn-round" href="https://www.linkedin.com/shareArticle?url=<?php echo $share_url; ?>" target="_blank" title="Share this page on LinkedIn">
                                                                        <i class="ico-linkedin-badge ico--color-understated-link ico--xlarge" data-automation="linkedin-share-link">
                                                                        </i>
                                                                        <span class="is-hidden-accessible"><i class="fa fa-linkedin-square"></i> LinkedIn</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a class="js-social-share btn btn-primary btn-round" href="https://twitter.com/intent/tweet?url=<?php echo $share_url; ?>&text=<?php echo $post['name']; ?>&via=<?php echo base_url();?>&hashtags=<?php echo $post['product_category']; ?>" target="_blank" title="Share this page on Twitter">
                                                                        <i class="ico-twitter-badge ico--color-understated-link ico--xlarge" data-automation="twitter-share-link">
                                                                        </i>
                                                                        <span class="is-hidden-accessible"><i class="fa fa-twitter-square"></i> Twitter</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a class="js-social-share btn btn-primary btn-round" href="https://plus.google.com/share?url=<?php echo $share_url; ?>" target="_blank" title="Share this page on google">
                                                                        <i class="ico-google-badge ico--color-understated-link ico--xlarge" data-automation="google-share-link">
                                                                         </i>
                                                                        <span class="is-hidden-accessible"><i class="fa fa-google-plus-official"></i> Google Plus</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </section>
                                                    </section>
                                                </div>
                                            </div>
                                            <!-- Fallback anchor for the link to the ticket area -->
                                            <span id="listing-ticket"></span>
                                        </div>
                                    </div>
                                  </div> 
                            </div>
                            <br><br/>
                  </div>
              </div>
            </div>
          </div>
        </section>


            </div> 
        </div>
    </div>
   
</div>
<script type="text/javascript">
    $(document).ready(function(){

         initLazyLoad();

         $('.text-dexsbpr img')
             .addClass('lazy')
             .attr("data-src", function( i, val ) {
                return $(this).attr("src");
             })
             .attr("src", base_url + "assets/dist/img/preloader.gif");

        setTimeout('initLazyLoad();',100);
       
    });

    function initLazyLoad(){

      $(".lazy").lazy({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 0
      });

    }
</script>