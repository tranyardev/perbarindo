 <?php $uniq_number =  $event_registration['uniq_number']; ?>
 <div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">


        <section>
          <div class="row" style=" margin-top: -444px;" >
            <div class="col-md-8"> 
                <div class="card">        
                        <div class="text-center padding-12" >
                            <h3 class="title margin-0"  >INFORMASI PEMBAYARAN</h3>
                            <h6 class="description margin-0"><?php echo $event_registration['event_name']; ?> - #<?php echo $event_registration['regcode']; ?></h6>
                        </div>

                        <hr>

                        <div class="panel panel-default">
                                    <div class="text-center padding-12">
                                        <div class="alert alert-warning">
                                            Biaya sudah termasuk kode transfer Rp <?php echo $uniq_number;?>
                                        Segera lakukan transfer dan konfirmasi pembayaran sebelum Jumat,<b> 6 Oktober 2017 pukul 10.22. </b> Jika tidak maka transaksi anda akan dianggap batal.
                                        </div>
                                    </div>
                                      <div class="panel-body">

                                            <div class="table-responsive padding-12">
                                                <?php foreach($bank_transfer_account as $bank){ ?>
                                                <table class="table table-striped border-b table-hovered">
                                                    <tr>
                                                        <td>
                                                            <?php echo $bank['bank']; ?>
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td>
                                                            <?php echo $bank['no_rek']; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Atas Nama
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td>
                                                             <?php echo $bank['account_name']; ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <?php } ?>
                                            </div>

                                            <div class="form-group text-center">
                                                 <a href="#" class="btn btn-primary btn-simple btn-lg"  data-toggle="modal" data-target="#cekBayar">Cara bayar</a>
                                                <a href="<?php echo base_url(); ?>payment_detail/<?php echo $event_registration['regcode']; ?>" class="btn btn-primary btn-lg">Detail Transaksi</a>
                                            </div>
                                      </div>
                            </div>
              </div>
            </div>
            <div class="col-md-4">
                 <div class="card">         
                        <div class="kc-summary-details"> 
                          <div class="kc-summary-item-container">
                          <img class="kc-summary-item-image" src="<?php echo setEventImage($event_registration['cover']) ?>" role="presentation">
                              <div class="kc-summary-item">   
                                  <div class="title-ivent">
                                      
                                      <div class="listing-hero-body ">
                                          <h5 class="listing-hero-title-x" data-automation="listing-title"><?php echo $event_registration['event_name']; ?></h5>
                                          <meta content="<?php echo $event_registration['event_name']; ?>">
                                          <div class="l-mar-top-3">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <span><i class="fa fa-map-marker"></i> &nbsp;<strong>Lokasi</strong> <br><?php echo $event_registration['address']; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="l-mar-top-3" style="padding-top: 10px;">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <?php
                                                        $sdate=date_create($event_registration['start_date']);
                                                        $start_date = date_format($sdate,"d/m/Y");

                                                        $edate=date_create($event_registration['end_date']);
                                                        $end_date = date_format($edate,"d/m/Y");
                                                      ?>
                                                      <span><i class="fa fa-calendar"></i> <strong>Tanggal</strong> <br><?php echo $start_date; ?> - <?php echo $end_date; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                     
                                  </div>
                               
                              </div>
                          </div>
                          <div class="kc-summary-info-container"> 
                          <table class="table">
                              
                              <tr>
                                  <td>
                                      <span ><b>Total</b></span>
                                  </td>
                                  
                                  <td class="text-left">
                                    
                                    <span class="text-orange" id="total"></span>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <span ><b>Kode Unik</b></span>
                                  </td>
                                  
                                  <td class="text-left">
                                    
                                    <span class="text-orange"><?php echo $uniq_number; ?></span>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <span ><b>Total Pembayaran</b></span>
                                  </td>
                                 
                                  <td class="text-left">
                                    <strong><span class="text-orange" id="total_trx"></span></strong>
                                  </td>
                              </tr>  

                          </table> 
                          </div>
                      </div>

                </div>
            </div>
          </div>
        </section>


        <section>
                      <!-- Sart Modal -->
                        <div class="modal fade" id="cekBayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header justify-content-center">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                        <h4 class="title title-up">Cara Bayar - Bank Transfer</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="kc-modal-body">
                                            <div class="kc-modal-warning">
                                                <div class="kc-warning-container">
                                                    <div class="kc-warning-text">
                                                        <div><span class="text-primary">Biaya sudah termasuk kode transfer Rp <?php echo $uniq_number;?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <ol class="kc-instruction-ol"><span><li>Lakukan pembayaran melalui transfer via Bank yang anda pilih.</li><li>Masukkan kode bank + nomor rekening yang dituju.</li><li>Pastikan nominal yang anda transfer tepat sesuai kode transfer demi kelancaran verifikasi pembayaran.</li><li>Konfirmasi pembayaran anda di bagian Konfirmasi Pembayaran.</li><li>Transfer dan konfirmasi pembayaran maksimal 24 jam, jika tidak maka transaksi anda akan dianggap batal.</li><li>Pembayaran akan diproses dalam 2 hari kerja setelah konfirmasi dilakukan.</li><li>Untuk transfer dari bank yang berbeda, anda mungkin akan dikenai biaya bank.</li></span></ol>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  End Modal -->
                </section>



            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    var total = '<?php echo $event_registration['total_cost']; ?>';
    var total_trx = parseInt(total) + parseInt('<?php echo $uniq_number; ?>'); 

     // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');

    $('#total').html(numeral(total).format("$0,0"));
    $('#total_trx').html(numeral(total_trx).format("$0,0"));


  });
</script>