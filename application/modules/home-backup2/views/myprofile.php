 <div class="wrapper margin-topfix">
    <div class="page-header page-header-small"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('http://perbarindo.tranyar.com/mocukup/Bghijau.png">
        </div> 
    </div>  
    <div class="section">
        <div class="container container-x">


        <section>
            <div class="row margin-topfxs">
                 <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <section>
                            
                            <div class="profile-sidebar">
                              <div class="bg-orange">
                                   <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic" align="center" style="background-image: url('<?php echo base_url()?>public/assets/img/A2.png');">
                                      <img src="<?php echo $this->session->userdata('picture'); ?>" class="img-responsive img-profile" alt="">
                                    </div>
                                    <!-- END SIDEBAR USERPIC -->
                              </div>
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                              <div class="profile-usertitle-name">
                                <?php
                                  if($this->session->userdata('first_name') != "" or $this->session->userdata('first_name') != "") {
                                    echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name').' ('.$this->session->userdata('username').')';
                                  }else{

                                    echo $this->session->userdata('username');

                                  }
                                ?>
                                <br>
                                <?php 

                                  if(@$member['name'] != ""){
                                    echo "#".@$member['name'];
                                  }
                                  
                                ?>
                              </div>
                              <div class="profile-usertitle-job">
                                <?php echo @$member['position']; ?>
                              </div>
                              <div class="profile-usertitle-job">
                                <?php echo @$member['bpr_corporate'].' '.@$member['bpr_name']; ?>
                              </div>
                              <div class="profile-usertitle-job">
                                <?php echo @$member['dpd_name']; ?>
                              </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                          
                          </div>
                          <form id="upload-photo" enctype="multipart/form-data" style="visibility: hidden;">
                            <input type="file" id="photo" name="photo">

                            <?php if($this->session->userdata("picture") != base_url().'public/assets/img/default-avatar.png'){ ?>
                            <input type="hidden" name="old_photo" value="<?php echo $this->session->userdata("picture"); ?>">
                            <?php } ?>

                          </form>
                        </section>

                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12"> 
                    <div class="card">
                      <ul class="nav nav-tabs justify-content-left" role="tablist">
                          <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#myticket" role="tab" aria-expanded="false">
                                  <i class="now-ui-icons shopping_tag-content"></i> My Tickets
                              </a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#myprofile" role="tab" aria-expanded="false">
                                  <i class="now-ui-icons users_circle-08"></i> My Profile
                              </a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-expanded="true">
                                  <i class="now-ui-icons ui-2_settings-90"></i> Settings
                              </a>
                          </li>
                      </ul>
                      <div class="card-body">
                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div class="tab-pane active" id="myticket" role="tabpanel" aria-expanded="false">
                                  <!--- BIGEN TAB HOME -->  
                                        <!--- DATA IVENT --> 
                                        <div class="container">
                                            <div class="block__box"> 
                                                <div class="row"> 
                                                        <div class="col-md-6 pull-left clearfix">
                                                            <div class="form-group">
                                                                <select id="reservation_status" name="search_status" class="form-control select hasCustomSelect" >
                                                                    <option value="ALL" selected="selected">Semua Status</option>
                                                                    <option value="WAITING_TRANSACTION">Menunggu Pembayaran</option>
                                                                    <option value="WAITING_CONFIRMATION">Menunggu Konfirmasi</option>
                                                                    <option value="TRANSACTION_COMPLETED">Transaksi Selesai</option>
                                                                    <option value="TRANSACTION_EXPIRED">Transaksi Kadaluarsa</option>
                                                                     <option value="TRANSACTION_CANCELED">Transaksi Dibatalkan</option>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                          <div class="col-md-6 pull-right clearfix">
                                                            <div class="form-group search__form"><i class="fa fa-search" title="search"></i>
                                                                <input type="text" id="search_invoice" class="form-control" placeholder="cari invoice">
                                                            </div>
                                                        </div>   
                                                </div>
                                                <hr>
                                                <section> 
                                                  <div class="row" id="event_registration_container"> 

                                                    <?php foreach($event_registration as $er){ ?>
                                                     <article class="col-md-12 padding-0">
                                                        <div class="card block__box padding-0 updated margin-b-0" data-expired-date="1507173746" data-expired-time="86400" data-transaction-status="12">
                                                            <div class="row margin-0">
                                                                <div class="col-md-6 padding-0">
                                                                    <div class="item__content">
                                                                         <div class="row margin-0">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                                                 <div class="item__image">
                                                                                    <figure> <img src="<?php echo setEventImage($er['cover']) ?>" alt="<?php echo $er['name'] ?>"> </figure>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
                                                                               <div class="text__content text--medium"><span class="green">Transaksi <a href="<?php echo base_url(); ?>payment_detail/<?php echo $er['regcode'] ?>" class="green">#<?php echo $er['regcode'] ?></a></span></div>
                                                                                <div class="text__content text--medium"><a href="<?php echo base_url(); ?>show/event/<?php echo $er['event_id'] ?>" rel="noopener noreferrer"><?php echo $er['event_name'] ?> - <?php echo $er['city'] ?></a></div>
                                                                                <div class="text__content text--medium orange mb10">Rp <?php echo number_format($er['total_cost'],2,",","."); ?></div>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 bordered-left ">

                                                                    <?php if($er['trx_status']=="WAITING_TRANSACTION"){ ?>

                                                                    <div class="status__content padding-12 bordered-dashed" user-type="buyer"> 
                                                                          <div class="clearfix padding-0">
                                                                              <div class="text--medium text-left">Menunggu Pembayaran</div>
                                                                              <div class="text--medium text-right"><a href="<?php echo base_url(); ?>payment_detail/<?php echo $er['regcode'] ?>" class="text-right" style="float: right">Lihat Detail</a></div><hr class="hr-xs">
                                                                          </div> 
                                                                          <div class="clearfik item__content">  
                                                                              <div class="text--medium text__content mb10">Segera lakukan pembayaran dalam : <strong class="red" data-date="1507173746" data-expired="86489"><span id="days_<?php echo $er['reg_id'];?>">0 hari</span> <span id="hours_<?php echo $er['reg_id'];?>">0 jam</span> <span id="minutes_<?php echo $er['reg_id'];?>">0 menit</span> <span id="seconds_<?php echo $er['reg_id'];?>">0 detik</span></strong></div>
                                                                              <div class="text--medium button__group"><a href="<?php echo base_url()?>payment_confirmation/<?php echo $er['regcode'] ?>" class="btn btn-primary btn-simple btn-sm" target="_blank">Konfirmasi Pembayaran</a> <a href="<?php echo base_url()?>checkout/payment_info/<?php echo $er['regcode'] ?>" class="btn btn-primary btn-simple  btn-sm" target="_blank">Info Bayar</a><a href="javascript:cancel_reservation('<?php echo $er['regcode'] ?>');" class="btn btn-primary btn-simple  btn-sm" target="_blank">Cancel</a></div> 
                                                                          </div>
                                                                          <script type="text/javascript">
                                                                            countDownTime('#days_<?php echo $er['reg_id'];?>','#hours_<?php echo $er['reg_id'];?>','#minutes_<?php echo $er['reg_id'];?>','#seconds_<?php echo $er['reg_id'];?>','<?php echo $er['expired_date'];?>');
                                                                          </script>
                                                                    </div>

                                                                    <?php }else if($er['trx_status']=="WAITING_CONFIRMATION"){ ?>

                                                                    <div class="status__content padding-12" user-type="buyer"> 
                                                                        <div class="clearfix padding-0">
                                                                            <div class="text--medium text-left">Menunggu Konfirmasi</div> 
                                                                            <div class="text--medium text-right"></div> <hr class="hr-xs">
                                                                         </div> 
                                                                        <div class="clearfix item__content"> 
                                                                            <div class="text--medium text__content mb10">Terimakasih telah melakukan konfirmasi. Kami akan segera memproses transaksi anda.</div>
                                                                        </div>
                                                                    </div>
                                                                    <?php }else if($er['trx_status']=="TRANSACTION_CANCELED"){ ?>

                                                                    <div class="status__content padding-12" user-type="buyer"> 
                                                                        <div class="clearfix padding-0">
                                                                            <div class="text--medium text-left">Transaksi Batal</div> 
                                                                            <div class="text--medium text-right"></div> <hr class="hr-xs">
                                                                         </div> 
                                                                        <div class="clearfix item__content"> 
                                                                            <div class="text--medium text__content mb10">Anda telah membatalkan reservasi ini.</div>
                                                                        </div>
                                                                    </div>

                                                                    <?php }else if($er['trx_status']=="TRANSACTION_COMPLETED"){ ?>

                                                                    <div class="status__content padding-12" user-type="buyer"> 
                                                                        <div class="clearfix padding-0">
                                                                            <div class="text--medium text-left">Transaksi Selesai</div> 
                                                                            <div class="text--medium text-right"></div> <hr class="hr-xs">
                                                                         </div> 
                                                                        <div class="clearfix item__content"> 
                                                                            <div class="text--medium text__content mb10">Terimakasih telah melakukan pembayaran. Ticket anda siap untuk dicetak.</div> 
                                                                            <div class="text--medium button__group"><a href="<?php echo base_url(); ?>home/generate_ticket/<?php echo $er['regcode'] ?>" class="btn btn-primary btn-simple btn-sm" target="_blank">Cetak Ticket</a> <!-- <a href="<?php echo base_url(); ?>home/download_pdf/<?php echo $er['regcode'] ?>" class="btn btn-primary btn-simple  btn-sm" target="_blank">Download Ticket</a> --></div>  
                                                                        </div>
                                                                    </div>

                                                                    <?php }else if($er['trx_status']=="TRANSACTION_EXPIRED"){ ?>

                                                                    <div class="status__content padding-12" user-type="buyer"> 
                                                                        <div class="clearfix padding-0">
                                                                            <div class="text--medium text-left">Transaksi Kadaluarsa</div> 
                                                                            <div class="text--medium text-right"><a href="#" class="text-right" style="float: right">Lihat Detail</a></div> <hr class="hr-xs">
                                                                         </div> 
                                                                        <div class="clearfix item__content">  
                                                                            <div class="text--medium text__content mb10">Maaf, transaksi dibatalkan karena Anda tidak melakukan pembayaran.</div>
                                                                            <div class="text--medium button__group"></div>  
                                                                        </div>
                                                                    </div>

                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <?php } ?>
                                                  
                                                   
                                                </div> 
                                                </section> 
                                            </div> 
                                        </div> 
                                        <!-- END DATA IVENT --> 
                                      


                                    <!-- Modal Detail ID Ivent -->
                                   
                                
                              </div>
                              <div class="tab-pane" id="myprofile" role="tabpanel" aria-expanded="false"> 
                                
                                    <h4><i class="now-ui-icons users_single-02"></i> Business Profile</h4>
                                    <form id="frm-member" class="form-horizontal main_form text-left">
                                    <input type="hidden" name="member_id" value="<?php echo @$member['member_id']; ?>">
                                    <fieldset> 
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">DPD</label>
                                            <div class="col-md-12">
                                               <select class="form-control select2" name="dpd" id="dpd" autocomplete="off" style="width: 100%">
                                                     <option value="">-- Pilih DPD --</option>

                                                  <?php 
                                                  foreach($dpd as $d){
                                                  
                                                    if(@$member['dpd']==$d['id']){
                                                        echo '<option selected value="'.$d['id'].'">'.$d['name'].'</option>';
                                                    }else{
                                                        echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';
                                                    }
                                                  
                                                  } 
                                                  ?>

                                                  </select>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">BPR</label>
                                            <div class="col-md-12 inputGroupContainer">
                                               <select class="form-control select2" name="bpr" id="bpr" autocomplete="off" style="width: 100%">
                                                    <option value="">-- Pilih BPR --</option>
                                                     <?php 
                                                    foreach($bpr as $d){
                                                    
                                                      if(@$member['bpr']==$d['id']){
                                                          echo '<option selected value="'.$d['id'].'">'.$d['bpr_corporate'].' '.$d['name'].'</option>';
                                                      }else{
                                                          echo '<option value="'.$d['id'].'">'.$d['bpr_corporate'].' '.$d['name'].'</option>';
                                                      }
                                                    
                                                    } 
                                                    ?>
                                              
                                                </select>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Name</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="name" placeholder="Name" class="form-control" type="text" value="<?php echo @$member['member_name'];?>">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Job Position</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                  <select name="job_position" class="form-control" id="text" autocomplete="off">
                                                   
                                                    <option value="">-- Jabatan --</option>

                                                    <?php 

                                                    foreach($job_positions as $job){

                                                    if(@$member['job_position']==$job['id']){
                                                      echo '<option selected value="'.$job['id'].'">'.$job['name'].'</option>';
                                                    }else{
                                                      echo '<option value="'.$job['id'].'">'.$job['name'].'</option>';
                                                    }
                                                    
                                                    
                                                    } 
                                                    ?>
                                              
                                                  </select>
                                               </div>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Mobile Phone</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="no_hp" placeholder="Mobile Phone" class="form-control" type="text" value="<?php echo @$member['no_hp'];?>">
                                                </div>
                                            </div>
                                        </div> 
                     
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">E-Mail</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="email" placeholder="E-Mail Address" class="form-control" type="text" value="<?php echo @$member['member_email'];?>">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Gender</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                  <select name="gender" class="form-control" id="text" autocomplete="off">
                                                   
                                                    <option value="L" <?php (@$member['gender']=="L")? $selected="selected":$selected="";echo $selected;?> >Laki - laki</option>
                                                    <option value="P" <?php (@$member['gender']=="P")? $selected="selected":$selected="";echo $selected;?>>Perempuan</option>
                                              
                                                  </select>
                                               </div>
                                            </div>
                                        </div> 
                                     
                                        <div class="form-group col-md-10">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-warning submit-button fa-lg"><i class="fa fa-floppy-o fa-lg" aria-hidden="true"></i> Save</button>
                                               
                                            </div>
                                        </div>
                                    </fieldset>
                                </form> 
                              </div>
                              
                              <div class="tab-pane" id="settings" role="tabpanel" aria-expanded="false">
                                  <!-- TAB SETTING --> 
                                    <h4><i class="now-ui-icons ui-2_settings-90"></i> Settings</h4>
                                    <form id="frm-profile" class="form-horizontal main_form text-left" >
                                    <fieldset> 
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">First Name</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="first_name" placeholder="First Name" class="form-control" type="text" value="<?php echo $this->session->userdata('first_name'); ?>">
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- Text input--> 
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Last Name</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="last_name" placeholder="Last Name" class="form-control" type="text" value="<?php echo $this->session->userdata('last_name'); ?>">
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- Text input-->
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">E-Mail</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="email" placeholder="E-Mail Address" class="form-control" type="text" value="<?php echo $this->session->userdata('email'); ?>">
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Username</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="username" disabled placeholder="Username" class="form-control" type="text" value="<?php echo $this->session->userdata('username'); ?>">
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- Button -->
                                        <div class="form-group col-md-10">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-warning submit-button fa-lg"><i class="fa fa-floppy-o fa-lg" aria-hidden="true"></i> Save</button>
                                               
                                            </div>
                                        </div>
                                    </fieldset>
                                    </form> 

                                <h4><i class="now-ui-icons ui-1_lock-circle-open"></i> Change Password</h4>
                                    <form id="frm-change-password" class="form-horizontal main_form text-left">
                                    <fieldset> 
                                       
                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Old Password</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="old_password" placeholder="Old Password" class="form-control validate[required]" type="password">
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">New Password</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="new_password"  id="new_password" placeholder="New Password" class="form-control validate[required]" type="password">
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-12">
                                            <label class="col-md-10 control-label">Confirm Password</label>
                                            <div class="col-md-12 inputGroupContainer">
                                                <div class="input-group">
                                                    <input name="confirm_password" placeholder="Confirm Password" class="form-control validate[required,equals[new_password]]" type="password">
                                                </div>
                                            </div>
                                        </div> 
                                       
                                        <!-- Button -->
                                        <div class="form-group col-md-10">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-warning submit-button fa-lg"><i class="fa fa-floppy-o fa-lg" aria-hidden="true"></i> Save</button>
                                                
                                            </div>
                                        </div>
                                    </fieldset>
                                </form> 
                                  <!-- END TAB SETTING --> 
                                
                              </div>
                          </div>
                      </div>
                  </div> 
                    </div>
                </div>
            </div>
        </section> 
       
            </div> 
        </div>
    </div>
</div>
 <section>
      <!-- Sart Modal -->
        <div class="modal fade" id="cancel_reservation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <h4 class="title title-up">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="kc-modal-body">
                            <p>Apakah anda yakin ingin membatalkan reservasi ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="do_cancel" class="btn btn-primary" data-regcode="" data-dismiss="modal">Ya</button> 
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button> 
                    </div>
                </div>
            </div>
        </div>
        <!--  End Modal -->
</section>
<script type="text/javascript">
  $(document).ready(function(){

    $('#dpd').select2();
    $('#dpd').on('select2:selecting', function(e) {
       var id = e.params.args.data.id;
       var target = base_url + 'autocomplete/bpr/'+id;

       $.get(target, function(res){

            $('#bpr').html(res);

            $('#bpr').removeAttr("disabled");
            $('#bpr').select2();

       });


    });
    $('#bpr').select2();

    $(".img-profile").click(function(){

      $("#photo").trigger("click");

    });

    $("#photo").on("change",function(){

        var target = base_url + 'change/user/picture';
      
        $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

        var formData = new FormData($("#upload-photo")[0]);

        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (res) {

                $("body").waitMe('hide');
                
                if(res.status=="1"){

                    toastr.success(res.msg, 'Response Server');

                    setTimeout('window.location.reload();',3000);

                }else{
                    toastr.error(res.msg, 'Response Server');
                }

            },
            cache: false,
            contentType: false,
            processData: false

        });



    });

    $("#frm-profile").submit(function(){

        var target = base_url + 'update/user/profile';
        var data = $(this).serialize();

        $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

        $.post(target,data,function(res){

          $("body").waitMe('hide');

          if(res.status=="1"){

              toastr.success(res.msg, 'Response Server');

              setTimeout('window.location.reload();',3000);

          }else{
              toastr.error(res.msg, 'Response Server');
          }

          

        },'json');

        return false;
        

    });

    $("#frm-member").submit(function(){

        var target = base_url + 'update/member/profile';
        var data = $(this).serialize();

        $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

        $.post(target,data,function(res){

          $("body").waitMe('hide');

          if(res.status=="1"){

              toastr.success(res.msg, 'Response Server');

              setTimeout('window.location.reload();',3000);

          }else{
              toastr.error(res.msg, 'Response Server');
          }

          

        },'json');

        return false;
        

    });

    $("#frm-change-password").validationEngine();

    $("#frm-change-password").submit(function(){

      if( $(this).validationEngine('validate')){

        var target = base_url + 'update/user/password';
        var data = $(this).serialize();

        $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

        $.post(target,data,function(res){

          if(res.status=="1"){
              toastr.success(res.msg, 'Response Server');
          }else{
              toastr.error(res.msg, 'Response Server');
          }

          $("body").waitMe('hide');

        },'json');

      }
      
      return false;

    });

    $("#reservation_status").on("change",function(){

      var status = $(this).val();
      var target = base_url + 'get_event_registration/status/'+status;

       $("#event_registration_container").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

      $.get(target, function(data){

        $("#event_registration_container").waitMe('hide');

        $("#event_registration_container").html(data);

      });


    });

    $("#search_invoice").keypress(function(e) {
        if(e.which == 13) {
            
          var keyword = $(this).val();

          var target = base_url + 'get_event_registration/regcode/'+keyword;

           $("#event_registration_container").waitMe({

                        effect: 'pulse',
                        text: 'Loading...',
                        bg: 'rgba(255,255,255,0.90)',
                        color: '#555'

                    });

          $.get(target, function(data){

            $("#event_registration_container").waitMe('hide');

            $("#event_registration_container").html(data);

          });

        }
    });
  });

 $("#do_cancel").click(function(){

      var data = { regcode : $(this).data("regcode") }
      var target = base_url + 'cancel/transaction';
      $("body").waitMe({

                  effect: 'pulse',
                  text: 'Loading...',
                  bg: 'rgba(255,255,255,0.90)',
                  color: '#555'

              });

      $.post(target,data,function(res){

        if(res.status=="1"){
            toastr.success(res.msg, 'Response Server');
        }else{
            toastr.error(res.msg, 'Response Server');
        }

        $("body").waitMe('hide');

        refresh_data();

      },'json');

 });

 function refresh_data(){

    var target = base_url + 'get_event_registration/status/ALL';

     $("#event_registration_container").waitMe({

                  effect: 'pulse',
                  text: 'Loading...',
                  bg: 'rgba(255,255,255,0.90)',
                  color: '#555'

              });
     
    $("#search_invoice").val("");

    $.get(target, function(data){

      $("#event_registration_container").waitMe('hide');

      $("#event_registration_container").html(data);

    });

 }

 function cancel_reservation(regcode){

    $("#do_cancel").attr("data-regcode", regcode);
    $("#cancel_reservation").modal("show");

 }
</script>