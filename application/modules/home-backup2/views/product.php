 
 <style type="text/css">
   .margin-100{
      margin-left: auto;
      margin-right: auto;
      margin-top: 100px;
      margin-bottom: 100px;
   }
 </style>
 <div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image lazy" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/product.png" >
        </div> 
    </div>  
    <div class="section">
        <div class="container">


       


       <section>  
            <div class="row marketing" style="margin-top: -477px;">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-left"> 
                  <div class="card"> 
                     <div class="search-travel">
                           <div class="seach-head text-center">
                               <h1 class="text-heading-epic homepage-header text-primary">
                                  BPR Produk
                              </h1>
                           </div>
                        
                           <div class="rowx clearfix"> 
                              <form id="search_product">
                              
                                <div class="col-md-12 pull-right clearfix">
                                    <div class="form-group search__form"><i class="fa fa-search" title="search"></i>
                                        <input type="text" name="keyword" class="form-control" id="keyword" placeholder="cari produk">
                                    </div>
                                </div>

                              </form>   
                        </div>
                                
                     </div>
                  </div> 
                </div>
            </div> 
        </section>
        <section>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-left"> 
                <div class="card" style="padding:12px;">   
                      
                  <div class="panel panel-default">


                    <div class="padding-12">

                        <header class="entry-header-outer container-wrapper">
                            <nav id="breadcrumb">
                                <a href="<?php echo base_url(); ?>"> <span class="fa fa-home fa-lg" aria-hidden="true"></span> Home</a>
                                <em class="delimiter">/</em>
                                <span class="current">Product</span>
                            </nav>
                            <!-- <h1 class="page-title-src">Search Results for: <span>BPR1</span></h1>  -->
                        </header>
                      
                    </div>  
                    <div class="panel-body"> 
                      <div id="bprproduct"></div>
                    </div>   

                  </div>
              </div>
            </div> 
          </div>
        </section>



            </div> 
        </div>
    </div>
   
</div>
 <script type="text/javascript"> 

    $(document).ready(function() { 
      
      $.get(base_url+'home/product/product_list/0', function(res){

        $("#bprproduct").html(res);
        $(".lazy").lazy({
          effect: "fadeIn",
          effectTime: 300,
          threshold: 0
        });

      });

      $("#keyword").keypress(function(e) {

          if(e.which == 13) {

            var keyword = $(this).val();
             
            $("#bprproduct").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

            $.get(base_url+'home/product/product_list/0/'+keyword, function(res){

              $("#bprproduct").html(res);
              $(".lazy").lazy({
                effect: "fadeIn",
                effectTime: 300,
                threshold: 0
              });

            });

            return false;


          }

      });

    });

    function pagination(container,target){

      $("html, body").animate({ scrollTop: 0 }, "slow");

      $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

      $("#"+container).load(target,function(){

        $(".lazy").lazy({
          effect: "fadeIn",
          effectTime: 300,
          threshold: 0
        });

      });


    }
</script>