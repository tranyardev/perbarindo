  
<!-- End Navbar -->
<div class="wrapper margin-topfix">
    <div class="page-header page-header-small"> 
        <div class="page-header-image lazy" data-parallax="true" data-src="http://perbarindo.tranyar.com/mocukup/Bghijau.png">
        </div> 
    </div>  
    <div class="section" style="background: #EEE!important;">
        <div class="container">


        <section>  
            <div class="container marketing margin-topfxs">
                <div class="card"> 
                   <div class="search-travel">
                         <div class="seach-head text-center">
                             <h1 class="text-heading-epic homepage-header text-primary">
                                Daftar BPR PERBARINDO
                            </h1>
                         </div>
                          <form id="search_bpr">
                        <div class="row">
                         
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="query" class="form-control" placeholder="Search BPR" title="Search events or categories">
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-3">
                                <div class="form-group has-ingo">
                                    <select class="form-control form-control-info" name="dpd">
                                         <option class="option-org"  value="" selected="selected">-- Pilih DPD --</option>
                                      <?php 

                                        foreach ($dpd as $d) {
                                          echo "<option value='".$d['id']."'>".$d['name']."</option>";
                                        }

                                      ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3" style="margin-top: -7px;">
                                <div class="input-group"> 
                                    <button type="submit" id="btn_search" class="btn btn-primary btn-block btn-xs">
                                        <i class="fa fa-seach"></i> SEARCH
                                    </button>
                                </div>
                            </div>
                         
                        </div>   
                         </form>            
                   </div>
                </div> 
            </div> 
        </section>
        <section>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-left"> 
                  <div class="card card-x" style="padding:12px;">   
                        
                          <div class="panel panel-default">


                              <div class="padding-12">
                                  <header class="entry-header-outer container-wrapper">
                                      <nav id="breadcrumb">
                                          <a href="<?php echo base_url(); ?>"> <span class="fa fa-home fa-lg" aria-hidden="true"></span> Home</a>
                                          <em class="delimiter">/</em>
                                          <span class="current">Daftar BPR</span>
                                      </nav>
                                      <!-- <h1 class="page-title-src">Search Results for: <span>BPR1</span></h1>  -->
                                  </header>
                                  <!-- <div class="alert alert-warning">
                                     <i class="fa fa-warning"></i> Data Tidak di temukan
                                  </div> -->
                              </div>  
                              <div class="panel-body"> 


                                <section> 
                                 <div class="rowx"> 
                                    <div class="row" id="bprlist"  >
                                      
                                      <?php 

                                      foreach($bpr as $b){ 

                                        if($b['logo'] != ""){

                                          $path = "upload/bpr";
                                          $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                          $file = $upload_path.'/'.$b['logo'];

                                          if(file_exists($file)){
                                              $logo = base_url().$path.'/'.$b['logo'];
                                          }else{
                                              $logo = base_url().'public/assets/img/bpr_default.png';
                                          }

                                        }else{
                                          $logo = base_url().'public/assets/img/bpr_default.png';
                                        }

                                        // FOR LAZY LOAD
                                        //  assets/dist/img/preloader.gif';  
                                      ?>

                                     <article class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-1" style="margin-bottom: 5px;" >
                                         <div class="card block__box blefetHoverx padding-0 updated margin-b-0">
                                              <div class="row margin-0 pad-roro">
                                                <div class="col col-lg-4 col-md-4 col-4 col-sm-4 col-xs-4 padding-0">
                                                 <div class="item__image frame" align="center">
                                                  <center> <img class="lazy img-responsive" data-src="<?php echo $logo; ?>" alt="<?php echo $b['corp'].' '.$b['name']; ?>" style="width: 129px;"> </center>
                                                </div>
                                              </div>
                                              <div class="col col-lg-8 col-md-8 col-8 col-sm-8 col-xs-8"> 
                                                <div class="text-headbpr text-ellipsis">
                                                  <h3 class="visible-lg visible-md hidden-sm hidden-xs"><a href="<?php echo base_url(); ?>profile/bpr/<?php echo $b['id'];?>"><?php echo $b['corp'].' '.$b['name']; ?></a></h3> 
                                                  <h1 style="font-size: 12px;" class="hidden-lg hidden-md visible-sm visible-xs"><a href="<?php echo base_url(); ?>profile/bpr/<?php echo $b['id'];?>"><?php echo $b['corp'].' '.$b['name']; ?></a></h1> 
                                                  <small>
                                                      <span class="date meta-item">
                                                          <span class="fa fa-globe" aria-hidden="true"></span> 
                                                          <span><?php echo $b['dpd_name']; ?></span>
                                                      </span>
                                                  </small>
                                                </div>
                                                <div class="text-dexsbpr">

                                                <?php

                                                  if($b['about']==""){
                                                    echo "-";
                                                  }else{
                                                    echo $b['about'];
                                                  }

                                                ?>  

                                               </div>
                                               <div class="action clearfix pull-right visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="text__content text--medium text-left">
                                                    <span class="green form-btn-read">
                                                     <a href="<?php echo base_url(); ?>profile/bpr/<?php echo $b['id'];?>" class="btn btn-warning btn-round btn-xs" > Lihat Profile </a>
                                                   </span>
                                                 </div> 
                                               </div> 
                                                <div class="action clearfix pull-right hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="text__content text--medium text-left">
                                                    <span class="green form-btn-read">
                                                     <a href="<?php echo base_url(); ?>profile/bpr/<?php echo $b['id'];?>" class="btn btn-warning btn-round btn-xs" style="font-size: 11px; padding: 7px;"> Lihat Profile </a>
                                                   </span>
                                                 </div> 
                                               </div> 
                                           </div>
                                         </div>
                                       </div>
                                     </article>  

                                  <?php } ?>

                                       <div class="form-group text-center text-center center-block"><br/>
                                  
                                          <?php echo $paging; ?>
                                       
                                      </div>

                                    </div>
                                    
                                 </div> 
                               </section>  

                             
                                
                        </div>   
                    </div>
                </div>
              </div> 
            </div>
          </div>
        </section>




            </div> 
        </div>
    </div>
    
</div>

<script type="text/javascript">
  $(document).ready(function(){
    
    $(".lazy").lazy({
      effect: "fadeIn",
      effectTime: 300,
      threshold: 0
    });

    $("#search_bpr").submit(function(){

      var data = $(this).serialize();
      var target = base_url + 'search/bpr';

      $("#btn_search").button("loading");

      $("bprlist").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

      $.post(target, data, function(res){

        $("#bprlist").html(res);

        $("#btn_search").button("reset");

      });

      return false;

    });

  });
  function pagination(container,target){

    $("html, body").animate({ scrollTop: 0 }, "slow");

    $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");
    $("#"+container).load(target);

  }
</script>