<div class="page-header" filter-color="orange">
    <div class="page-header-image" style="background-image:url('<?php echo base_url(); ?>public/assets/img/login.jpg')"></div>
    <div class="container">

        <div class="col-md-4 content-center">
            <div class="card card-login card-plain">
                <form class="form" id="reg">
                    <div class="header header-primary text-center">
                        <div class="logo-container visible-lg visible-md hidden-sm hidden-xs" style="width: 100px;">
                            <img src="<?php echo base_url(); ?>public/assets/img/square-logo.png" alt="">
                        </div> 
                        <br>
                        <br> 
                        <br> 
                    </div>
                    <div class="content">
                        
                        <div class="input-group form-group-no-border input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons users_circle-08"></i>
                            </span>
                            <input type="text" class="form-control validate[required]" name="username" placeholder="Username" data-prompt-position="topLeft">
                        </div>
                        <div class="input-group form-group-no-border input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons ui-1_email-85"></i>
                            </span>
                            <input type="text" class="form-control validate[required,custom[email]]" name="email" placeholder="Email" data-prompt-position="topLeft">
                        </div>
                        <div class="input-group form-group-no-border input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons objects_key-25"></i>
                            </span>
                            <input type="password" id="password" placeholder="Password" class="form-control validate[required]" name="password" data-prompt-position="topLeft" />
                        </div>
                        <div class="input-group form-group-no-border input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons objects_key-25"></i>
                            </span>
                            <input type="password" placeholder="Confirm Password" class="form-control validate[required, equals[password]]" name="confirm-password" data-prompt-position="topLeft"/>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <button type="submit" id="btn-submit" class="btn btn-primary btn-round btn-lg btn-block">Daftar</button>
                    </div>
                    <div class="pull-left">
                        <h6>
                            <a href="<?php echo base_url(); ?>login" class="link">Sudah punya akun?</a>
                        </h6>
                    </div>
                    <div class="pull-right">
                        <!-- <h6>
                            <a href="#pablo" class="link">Lupa Password?</a>
                        </h6> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        var callback_url = '<?php echo $this->session->userdata('callback_url'); ?>';

        $("#reg").validationEngine();

        $("#reg").submit(function(){

            if($(this).validationEngine("validate")){

                var target = base_url + 'signup_member';
                var data = $(this).serialize();

                $("body").waitMe({
                        effect: 'pulse',
                        text: 'Loading...',
                        bg: 'rgba(255,255,255,0.90)',
                        color: '#555'
                    });
                
                $.post(target, data, function(res){

                    if(res.status == "1"){
                        
                        toastr.success(res.msg, 'Response Server');

                        if(callback_url!=''){
                            setTimeout('window.location.href = callback_url;',3000);
                        }else{
                            setTimeout('window.location.href = base_url + "login";',3000);
                        }

                    }else{
                         toastr.error(res.msg, 'Response Server');
                    }

                    $("body").waitMe("hide");

                },'json');

                return false;

            }

        });

    });
</script>