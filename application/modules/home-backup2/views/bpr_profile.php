 <style type="text/css">
/** CSSS BPR PROFILE **/
.form-group {
  background-color: #FFF;
}
.tab-content .tab-pane {
  background: linear-gradient(#DDD,#fffffa); 

  border-top: solid 1px #ddd;
  border-left: solid 1px #ddd;
  border-right: solid 1px #ddd;
  border-bottom: solid 1px #fff;
  padding: 8px;
} 

.table-xs > tr > th , td {
    font-size: 12px;  
}
.table-xs td, .table-xs th { 
    font-size: 12px; 
    padding: 7px; 
    vertical-align: top;
    border-top: 1px solid #e9ecef;
}

.tab-pane h3  { 
    font-size: 22px;
    color: #484848;
    border-bottom: 1px solid #b5b5b5;
}

.nav-tabs>.nav-item>.nav-link.active {
    border-bottom: 2px solid #439d47!important;
    border-radius: 1px!important;
    border-top: 0px solid #ffffff!important;
    border-right: 0px solid #ffffff!important;   
    border-left: 1px solid #dcdcdc!important;
}




</style>
<div class="wrapper margin-topfix">
            <div class="page-header page-headerxs page-header-smallx" style="height: 36vh;"> 
                <?php 

                  // if($bpr['cover']!=""){

                  //   $real_path = $_SERVER['DOCUMENT_ROOT'].'/upload/bpr/';

                  //   if(file_exists($real_path.$bpr['cover'])){

                  //     $cover = base_url().'upload/bpr/'.$bpr['cover'];

                  //   }else{

                  //     $cover = base_url().'assets/dist/img/img-1.png';

                  //   }

                  // }else{

                  //   $cover = base_url().'assets/dist/img/img-1.png';

                  // }  
                ?>
                <div class="page-header-image lazy" data-parallax="true" style="background-image: url('http://perbarindo.tranyar.com/mocukup/Bghijau.png');">
                </div> 
            </div>   
            <div class="section">
                <div class="container container-x">


                <section>
                    <div class="row margin-topfxsxx" style="margin-top: -535px;">
                         <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                            <div class="card card-x">

                                <section>
                                    <div class="profile-sidebar">
                                      <div class="bg-orange">
                                           <!-- SIDEBAR USERPIC -->
                                            <div class="profile-userpic" align="center">

                                              <?php 

                                                if($bpr['logo'] != ""){

                                                  $path = "upload/bpr";
                                                  $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                                  $file = $upload_path.'/'.$bpr['logo'];

                                                  if(file_exists($file)){
                                                      $logo = base_url().$path.'/'.$bpr['logo'];
                                                  }else{
                                                      $logo = base_url().'public/assets/img/bpr_default.png';
                                                  }

                                                }else{
                                                  $logo = base_url().'public/assets/img/bpr_default.png';
                                                }

                                              ?>

                                              <img src="<?php echo $logo; ?>" class="img-responsive" alt="">
                                            </div>
                                            <!-- END SIDEBAR USERPIC -->
                                      </div>
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                      <div class="profile-usertitle-name">
                                        <?php echo $bpr['corp']; ?> <?php echo $bpr['name']; ?>
                                      </div>
                                      <div class="profile-usertitle-job">
                                        DPD : <?php ($bpr['dpd_name']!="")? $dpd=$bpr['dpd_name']:$dpd="-";echo $dpd; ?>
                                      </div>
                                      <div class="profile-summary-data">
                                          <ul>
                                            <li><i class="now-ui-icons business_globe"></i> Cabang <span class="badge badge-warning"><?php echo $stats['branch_count']; ?></span></li>
                                            <li><i class="now-ui-icons business_briefcase-24"></i> Produk <span class="badge badge-warning"><?php echo $stats['product_count']; ?></span></li>
                                            <li><i class="now-ui-icons shopping_cart-simple"></i> Lelang <span class="badge badge-warning"><?php echo $stats['auction_count']; ?></span></li>
                                            <li><i class="now-ui-icons education_paper"></i> Blog <span class="badge badge-warning"><?php echo $stats['article_count']; ?></span></li>
                                          </ul>
                                      </div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                 
                                  </div>
                                </section>
 
                            </div>
                        </div>  
                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12"> 
                            <div class="card  card-x">
                                <ul id="myTab" class="nav nav-tabs" role="tablist" data-tabs="tabs">
                                    <li role="tabx" class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" role="tab" id="profile" >
                                            <i class="now-ui-icons business_bank"></i> Profile
                                        </a>
                                    </li>
                                      <li role="tabx" class="nav-item">
                                        <a class="nav-link" data-toggle="tab" role="tab" id="branch" >
                                            <i class="now-ui-icons business_globe"></i> Cabang
                                        </a>
                                    </li>
                                    <li role="tabx" class="nav-item">
                                        <a class="nav-link" data-toggle="tab" role="tab" id="product" >
                                            <i class="now-ui-icons business_briefcase-24"></i> Produk
                                        </a>
                                    </li>
                                     <li role="tabx" class="nav-item">
                                        <a class="nav-link" data-toggle="tab" role="tab" id="lelang" >
                                            <i class="now-ui-icons shopping_cart-simple"></i> Lelang
                                        </a>
                                    </li>
                                    <li role="tabx" class="nav-item">
                                        <a class="nav-link" data-toggle="tab" role="tab" id="blog" >
                                            <i class="now-ui-icons education_paper"></i> Blog
                                        </a>
                                    </li>
                                    <li role="tabx" class="nav-item">
                                        <a class="nav-link" data-toggle="tab" role="tab" id="report" >
                                            <i class="now-ui-icons business_chart-bar-32"></i> Laporan
                                        </a>
                                    </li>
                                </ul>
                                <div class="card-body" style="padding: 3px;">
                                    <!-- Tab panes -->
                                      <div class="tab-content">
                                        <div class="tab-pane active" id="profile_content" >
                                            <!--- BIGEN TAB HOME -->   
                                              <div class="row panel-group panel-profile" id="accordion">
                                                <!-- Visible LG -->
                                                <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fa fa-info-circle fa-50px" aria-hidden="true"></i> <br/> Tentang BPR</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-institution fa-50px" aria-hidden="true"></i> <br/> Asset Keuangan</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><i class="fa fa-users fa-50px" aria-hidden="true"></i> <br/> Direksi</a>
                                                    </h4>
                                                  </div> 
                                                </div>

                                                <!-- / end -->

                                                <!-- hidden-lg hidden-md visible-sm visible-xs -->

                                                <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fa fa-info-circle fa-50px" aria-hidden="true"></i> <br/> Tentang BPR</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="col-md-12"> 
                                                    <div id="collapse1" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-info-circle fa-5px" aria-hidden="true"></i> Tentang BPR </h5> 
                                                      <?php
                                                        if($bpr['about']!=""){
                                                          echo $bpr['about'];
                                                        }else{
                                                          echo '<div class="alert alert-warning text-center">
                                                           <i class="fa fa-warning"></i> Belum ada data
                                                        </div>';
                                                        }
                                                      ?>
                                                      </div>
                                                    </div>
                                                </div> 

                                                <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-institution fa-50px" aria-hidden="true"></i> <br/> Asset Keuangan</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                <div class="col-md-12"> 
                                                    <div id="collapse2" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-institution fa-5px" aria-hidden="true"></i> Asset Keuangan </h5> 


                                                        <div class="rowx owl-carousel owl-loaded owl-drag" style="overflow: hidden!important;">


                                                                  <?php 
                                                                      
                                                                      $year = date("Y")-2;    

                                                                      for($i=$year;$i < date("Y") + 1;$i++){

                                                                      ?>

                                                                   <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                                                                          
                                                                          <table class="table table-bordered table-xs" width="100%">
                                                                              <thead>
                                                                                  <tr>
                                                                                    <?php echo "<th colspan='3'>Desember ".$i."</th>"; ?> 
                                                                                  </tr>
                                                                                  <tr>
                                                                                      <th>Produk</th>
                                                                                      <th>Nilai</th>
                                                                                      <th>Jumlah</th>
                                                                                  </tr>
                                                                              </thead>
                                                                              <tbody>

                                                                                  <?php 

                                                                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("1");

                                                                                      foreach ($products as $a) {

                                                                                          if($a['slug']=="kredit"){
                                                                                              $label = "Debitur";
                                                                                          }else if ($a['slug']=="tabungan") {
                                                                                              $label = "Penabung";
                                                                                          }else if ($a['slug']=="deposito") {
                                                                                              $label = "Pendeposito";
                                                                                          }
                                                                                  
                                                                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);
                                                                                          echo "<tr>";
                                                                                          echo "<td>".$a['name']."</td>";
                                                                                          echo "<td> Rp. ".number_format($ast['value'],0,",",".")."</td>";
                                                                                          echo "<td>".$ast['saving_account_count']." ".$label."</td>";       
                                                                                          echo "</tr>";

                                                                                      }

                                                                                  ?>
                                                                                  
                                                                                  <tr>
                                                                                      <td colspan="3">&nbsp;</td>
                                                                                  </tr>

                                                                                  <?php 

                                                                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("0");

                                                                                      foreach ($products as $a) {
                                                                                  
                                                                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);

                                                                                        
                                                                                          echo "<tr>";
                                                                                          echo "<td>".$a['name']."</td>";
                                                                                          echo "<td colspan='2'> Rp. ".number_format($ast['value'],0,",",".")."</td>"; 
                                                                                          echo "</tr>";

                                                                                      }

                                                                                  ?>
                                                                              </tbody>
                                                                          </table>

                                                                    </div>      

                                                                      <?php

                                                                      }

                                                                      ?> 
                                                                <br>
                                                        </div>

                                                        <?php /***

                                                        <table class="table table-striped table-bordered" style=" width: 1450px;">
                                                             <thead>
                                                                 <tr>
                                                                  <?php 
                                                                      
                                                                      $year = date("Y")-2;    

                                                                      for($i=$year;$i < date("Y") + 1;$i++){

                                                                          echo "<th>Desember ".$i."</th>";

                                                                      }

                                                                  ?>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                                 <tr>
                                                                      <?php 
                                                                      
                                                                      $year = date("Y")-2;    

                                                                      for($i=$year;$i < date("Y") + 1;$i++){

                                                                      ?>

                                                                      <td>
                                                                          
                                                                          <table class="table table-bordered">
                                                                              <thead>
                                                                                  <tr>
                                                                                      <th>Produk</th>
                                                                                      <th>Nilai</th>
                                                                                      <th>Jumlah</th>
                                                                                  </tr>
                                                                              </thead>
                                                                              <tbody>

                                                                                  <?php 

                                                                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("1");

                                                                                      foreach ($products as $a) {

                                                                                          if($a['slug']=="kredit"){
                                                                                              $label = "Debitur";
                                                                                          }else if ($a['slug']=="tabungan") {
                                                                                              $label = "Penabung";
                                                                                          }else if ($a['slug']=="deposito") {
                                                                                              $label = "Pendeposito";
                                                                                          }
                                                                                  
                                                                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);
                                                                                          echo "<tr>";
                                                                                          echo "<td>".$a['name']."</td>";
                                                                                          echo "<td> Rp. ".number_format($ast['value'],0,",",".")."</td>";
                                                                                          echo "<td>".$ast['saving_account_count']." ".$label."</td>";       
                                                                                          echo "</tr>";

                                                                                      }

                                                                                  ?>
                                                                                  
                                                                                  <tr>
                                                                                      <td colspan="3">&nbsp;</td>
                                                                                  </tr>

                                                                                  <?php 

                                                                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("0");

                                                                                      foreach ($products as $a) {
                                                                                  
                                                                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);

                                                                                        
                                                                                          echo "<tr>";
                                                                                          echo "<td>".$a['name']."</td>";
                                                                                          echo "<td colspan='2'> Rp. ".number_format($ast['value'],0,",",".")."</td>"; 
                                                                                          echo "</tr>";

                                                                                      }

                                                                                  ?>
                                                                              </tbody>
                                                                          </table>

                                                                     </td>

                                                                      <?php

                                                                      }

                                                                      ?>
                                                                     
                                                                 </tr>
                                                                
                                                             </tbody>
                                                         </table>
                                                         ***/ ?>
                                                      </div>
                                                    </div>
                                                </div> 

                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-red wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><i class="fa fa-users fa-50px" aria-hidden="true"></i> <br/> Direksi</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="col-md-12"> 
                                                    <div id="collapse3" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-users fa-5px" aria-hidden="true"></i> Direksi </h5>

                           

                                                      <div id="people"></div>

                                                     

                                                      </div>
                                                    </div>
                                                </div>

                                                <!-- Visible LG -->
                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" id="stockholder_tab" data-parent="#accordion" href="#collapse4"><i class="fa fa-pie-chart fa-50px" aria-hidden="true"></i> <br/> Pemegang Saham</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12  visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><i class="fa fa-map-marker fa-50px" aria-hidden="true"></i> <br/> Lokasi (Map)</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                
                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12  visible-lg visible-md hidden-sm hidden-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><i class="fa fa-phone-square fa-50px" aria-hidden="true"></i> <br/> Kontak Kami</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                <!-- / end -->

                                                <!-- hidden-lg hidden-md visible-sm visible-xs -->
                                                <div class="panel panel-default col-lg-4 col-md-12 col-sm-12 hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" id="stockholder_tab" data-parent="#accordion" href="#collapse4"><i class="fa fa-pie-chart fa-50px" aria-hidden="true"></i> <br/> Pemegang Saham</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="col-md-12"> 
                                                    <div id="collapse4" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-pie-chart fa-5px" ></i>  Pemegang Saham </h5> 

                                                        <div id="chartContainer" style="width: 100%;"></div>

                                                      </div>
                                                    </div>
                                                </div>

                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12  hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><i class="fa fa-map-marker fa-50px" aria-hidden="true"></i> <br/> Lokasi (Map)</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="col-md-12"> 
                                                   <div id="collapse5" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-map-marker fa-5px" aria-hidden="true"></i> Lokasi (Map) </h5> 
                                                        <?php if($bpr['latitude']!="" && $bpr['longitude']!=""){ ?>
                                                        <div id="map" style="width: 100%;height: 279px;"></div>
                                                        <?php }else{ ?>
                                                            <div class="alert alert-warning text-center">
                                                              <i class="fa fa-warning"></i> Belum ada data
                                                          </div>
                                                        <?php } ?>
                                                      </div>
                                                    </div>
                                                </div>


                                                 <div class="panel panel-default col-lg-4 col-md-12 col-sm-12  hidden-lg hidden-md visible-sm visible-xs">
                                                  <div class="panel-heading alert alert-warning alert-yellow wrap-boxmetro">
                                                    <h4 class="panel-title text-center">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><i class="fa fa-phone-square fa-50px" aria-hidden="true"></i> <br/> Kontak Kami</a>
                                                    </h4>
                                                  </div> 
                                                </div>
                                                 <div class="col-md-12"> 
                                                    <div id="collapse6" class="panel-collapse collapse in body-boxmetro"> 
                                                      <div class="panel-body"><h5><i class="fa fa-phone-square fa-5px" aria-hidden="true"></i> Kontak Kami </h5> 

                                                        <table class="table table-striped">
                                                            <tbody>
                                                                
                                                                <tr>
                                                                    <td>Alamat</td>
                                                                    <td>:</td>
                                                                    <td><?php ($bpr['address']!="") ? $address = $bpr['address']:$address="-";echo $address; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Telp</td>
                                                                    <td>:</td>
                                                                    <td><?php ($bpr['telp']!="") ? $telp = $bpr['telp']:$telp="-";echo $telp; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Fax</td>
                                                                    <td>:</td>
                                                                    <td><?php ($bpr['fax']!="") ? $fax = $bpr['fax']:$fax="-";echo $fax; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td>:</td>
                                                                    <td><?php ($bpr['email']!="") ? $email = $bpr['email']:$email="-";echo $email; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Website</td>
                                                                    <td>:</td>
                                                                    <td><?php ($bpr['website']!="") ? $website = $bpr['website']:$website="-";echo $website; ?></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>

                                                        <h5><i class="fa fa-address-book fa-5px" aria-hidden="true"></i> Kontak Person</h5>

                                                        <div class="panel-group" id="accordion_contact">

                                                        <?php foreach($contact_person as $cp){ ?>

                                                          <div class="panel panel-default panel-contact">
                                                            <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion_contact" href="#collapse_<?php echo $cp['id'];?>">
                                                                <?php echo ucfirst($cp['first_name']).' '.ucfirst($cp['last_name']);?></a>
                                                              </h4>
                                                            </div>
                                                            <div id="collapse_<?php echo $cp['id'];?>" class="panel-collapse collapse in">
                                                              <div class="panel-body">
                                                                <table class="table table-striped table-contact table-xs">
                                                                    
                                                                    <tr>
                                                                      <td>Email</td>
                                                                      <td>:</td>
                                                                      <td><?php echo $cp['email'];?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>No. HP</td>
                                                                      <td>:</td>
                                                                      <td><?php echo $cp['mobile_phone'];?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Telp</td>
                                                                      <td>:</td>
                                                                      <td><?php echo $cp['telp'];?></td>
                                                                    </tr>
                                                                     <tr>
                                                                      <td>Alamat</td>
                                                                      <td>:</td>
                                                                      <td><?php echo $cp['address'];?></td>
                                                                    </tr>
                                                                </table>

                                                              </div>
                                                            </div>
                                                          </div>

                                                         

                                                        <?php } ?>

                                                          </div>
                                                      </div>
                                                    </div>
                                                </div> 
                                              </div> 
                                        </div>
                                        <div class="tab-pane padding-12" id="branch_content" >  
                                              <section class="containerx">
                                                    <div class="block__box"> 
                                                        

                                                          <div class="rowx clearfix"> 
                                                          <?php foreach ($branch as $b) { ?>
                                                          <!-- Start Looping -->
                                                             <article class="col-md-12 padding-0">
                                                                <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                                                    <div class="row margin-0">
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                                                            <div class="item__content">
                                                                                 <div class="row margin-0">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                                                                                         <div class="head-cardx">
                                                                                           <div class="branch_map" style="width: 100%;height:257px;" id="map_<?php echo $b['id']; ?>" data-latitude="<?php echo $b['latitude']; ?>" data-longitude="<?php echo $b['longitude']; ?>"></div>
                                                                                        </div>
                                                                                    </div> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                                                             <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                                                                   <div class="text__content text--medium">
                                                                                      <h3><?php echo $b['name']; ?></h3>
                                                                                    </div> 
                                                                                       
                                                                                  <div class="clearfix padding-0">
                                                                                        <div class="text--medium text-left">Alamat Detail :</div>
                                                                                       <hr class="hr-xs">
                                                                                  </div> 
                                                                                  <div class="clearfik item__content">  
                                                                                      <div class="text--medium text__content mb10"> 
                                                                                      <?php echo $b['address']; ?>
                                                                                      </div> 
                                                                                     
                                                                                  </div>
                                                                                  <br>
                                                                                   <div class="clearfix padding-0">
                                                                                        <div class="text--medium text-left">Kontak :</div>
                                                                                       <hr class="hr-xs">
                                                                                  </div> 
                                                                                  <div class="clearfik item__content">  
                                                                                      <div class="text--medium text__content mb10"> 
                                                                                      <table>
                                                                                        <tr>
                                                                                          <td>Email</td>
                                                                                          <td>:</td>
                                                                                          <td><?php echo $b['email']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>Telp</td>
                                                                                          <td>:</td>
                                                                                          <td><?php echo $b['telp']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>Fax</td>
                                                                                          <td>:</td>
                                                                                          <td><?php echo $b['fax']; ?></td>
                                                                                        </tr>
                                                                                      </table>
                                                                                      </div> 
                                                                                     
                                                                                  </div>
                                                                                
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    showMap('<?php echo $b['latitude']; ?>','<?php echo $b['longitude']; ?>','map_<?php echo $b['id']; ?>');
                                                                </script>
                                                            </article>
                                                           <!-- End Looping --> 
                                                          <?php } ?>
                                                              
                                                        </div>  
                                                       
                                                    </div> 
                                                </section> 
                                        </div>
                                        <div class="tab-pane padding-12" id="product_content" >  
                                           <!--- DATA PRODUK --> 
                                            
                                                  <section class="containerx">
                                                    <div class="block__box"> 
                                                        <div class="rowx clearfix"> 
                                                              <form id="search_product">
                                                                <input type="hidden" name="bpr" value="<?php echo $bpr['id']; ?>">
                                                                <div class="col-md-6 pull-left clearfix">

                                                                    <div class="form-group">
                                                                        <select name="category" class="form-control select hasCustomSelect" >
                                                                            <option value="" selected="selected">Semua Kategori</option> 
                                                                            <?php 

                                                                              foreach ($product_category as $cat) {
                                                                                
                                                                                echo "<option value='".$cat['id']."'>".$cat['name']."</option>";
                                                                                $ch1 = $this->Mbprproductcategory->getBPRProductCategoryByParent($cat['bpr'],$cat['id']);

                                                                                foreach ($ch1 as $c1) {

                                                                                  echo "<option value='".$c1['id']."'>- ".$c1['name']."</option>";

                                                                                  $ch2 = $this->Mbprproductcategory->getBPRProductCategoryByParent($c1['bpr'],$c1['id']);

                                                                                  foreach ($ch2 as $c2) {

                                                                                      echo "<option value='".$c2['id']."'>-- ".$c2['name']."</option>";

                                                                                      $ch3 = $this->Mbprproductcategory->getBPRProductCategoryByParent($c2['bpr'],$c2['id']);

                                                                                      foreach ($ch3 as $c3) {

                                                                                         echo "<option value='".$c3['id']."'>--- ".$c3['name']."</option>";

                                                                                      }

                                                                                  }

                                                                                }


                                                                              }

                                                                            ?>
                                                                        </select> 
                                                                    </div>
                                                                </div>
                                                                  <div class="col-md-6 pull-right clearfix">
                                                                    <div class="form-group search__form"><i class="fa fa-search" title="search"></i>
                                                                        <input type="text" name="keyword" class="form-control" id="keyword" placeholder="cari produk">
                                                                    </div>
                                                                </div>
                                                              </form>   
                                                        </div>
                                                        <hr> 
                                                          <div class="rowx clearfix"> 
                                                          <!-- Start Looping -->
                                                            <div id="bprproduct"></div>
                                                           <!-- End Looping --> 
                                                           
                                                        </div> 

                                                        
                                                    </div> 
                                                </section> 

                                           <!-- END DATA BOX -->  
                                        </div>
                                         <div class="tab-pane padding-12" id="auction_content" > 
                                            <!--- DATA BLOGS -->


                                          <section class="data-list-blog">
                                              <div class="block__box"> 
                                                <div class="rowx clearfix"> 
                                                <form id="search_auction">
                                                  <input type="hidden" name="bpr" value="<?php echo $bpr['id']; ?>">
                                                  <div class="col-md-6 pull-left clearfix">
                                                    <div class="form-group">
                                                       <select name="category" id="auction_category" class="form-control select hasCustomSelect" >
                                                            <option value="" selected="selected">Semua Kategori</option> 
                                                            <?php 

                                                              foreach ($auction_category as $cat) {
                                                                
                                                                echo "<option value='".$cat['id']."'>".$cat['name']."</option>";


                                                              }

                                                            ?>
                                                        </select>  
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 pull-right clearfix">
                                                    <div class="form-group search__form"><i class="fa fa-search" title="search"></i>
                                                      <input type="text" name="asset_code" id="asset_code" class="form-control" placeholder="kode asset">
                                                    </div>
                                                  </div>
                                                  </form>   
                                                </div> 
                                                <hr> 
                                                  <div class="rowx clearfix"> 
                                                    <div id="bprauction"></div>
                                                  </div>   

                                            </div> 
                                          </section>  

                                            <!-- END DATA BOX -->  
                                        </div> 
                                        <div class="tab-pane padding-12" id="blog_content" > 
                                            <!--- DATA BLOGS -->


                                          <section class="data-list-blog">
                                              <div class="block__box"> 
                                                <div class="rowx clearfix"> 
                                                <form id="search_post">
                                                  <input type="hidden" name="bpr" value="<?php echo $bpr['id']; ?>">
                                                  <div class="col-md-6 pull-left clearfix">
                                                    <div class="form-group">
                                                       <select name="category" class="form-control select hasCustomSelect" >
                                                            <option value="" selected="selected">Semua Kategori</option> 
                                                            <?php 

                                                              foreach ($post_category as $cat) {
                                                                
                                                                echo "<option value='".$cat['id']."'>".$cat['name']."</option>";
                                                                $ch1 = $this->Mbprarticlecategory->getBPRArticleCategoryByParent($cat['bpr'],$cat['id']);

                                                                foreach ($ch1 as $c1) {

                                                                  echo "<option value='".$c1['id']."'>- ".$c1['name']."</option>";

                                                                  $ch2 = $this->Mbprarticlecategory->getBPRArticleCategoryByParent($c1['bpr'],$c1['id']);

                                                                  foreach ($ch2 as $c2) {

                                                                      echo "<option value='".$c2['id']."'>-- ".$c2['name']."</option>";

                                                                      $ch3 = $this->Mbprarticlecategory->getBPRArticleCategoryByParent($c2['bpr'],$c2['id']);

                                                                      foreach ($ch3 as $c3) {

                                                                         echo "<option value='".$c3['id']."'>--- ".$c3['name']."</option>";

                                                                      }

                                                                  }

                                                                }


                                                              }

                                                            ?>
                                                        </select>  
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 pull-right clearfix">
                                                    <div class="form-group search__form"><i class="fa fa-search" title="search"></i>
                                                      <input type="text" name="keyword" id="keyword2" class="form-control" placeholder="cari article">
                                                    </div>
                                                  </div>
                                                  </form>   
                                                </div> 
                                                <hr> 
                                                  <div class="rowx clearfix"> 
                                                    <div id="bprpost"></div>
                                                  </div>   

                                            </div> 
                                          </section>  

                                            <!-- END DATA BOX -->  
                                        </div> 
                                        <div class="tab-pane padding-12" id="report_content" >
                                            <!-- TAB LAPORAN -->  
                                          <h3><i class="now-ui-icons business_chart-bar-32"></i> Laporan Publikasi</h3> 
                                            <!-- END TAB SETTING -->  
                                            <div style="border-left: 3px solid #f96332;">  
                                                <div class="rowx clearfix">
                                                  <div class="col-md-12">

                                                  <?php 
                                                  if(count($report)>0 && $validate['is_publication_report_verified']=="1"){

                                                  foreach($report as $rp){ 
                                                    $x = explode(".", $rp['attachement']);
                                                    $ext = end($x);
                                                  ?>
                                                      <div class="well" style="overflow-x: auto;margin-bottom: 20px;"> 
                                                        <h2><?php echo $rp['name']; ?></h2>
                                                        <hr>

                                                        
                                                          

                                                          <?php if(strtolower($ext) == "pdf"){ ?>

                                                          <div id="pdfviewer_<?php echo $rp['id']; ?>" style="height: 800px"></div>

                                                          <script type="text/javascript">
                                                              $(document).ready(function(){

                                                                 PDFObject.embed("<?php echo base_url(); ?>upload/media/<?php echo $rp['attachement']; ?>", "#pdfviewer_<?php echo $rp['id']; ?>");


                                                              });
                                                          </script>

                                                          <?php }else{?>

                                                         
                                                          <iframe class="form-control" style="height: 700px;margin-top:20px;width: 100%;" src="https://view.officeapps.live.com/op/embed.aspx?src=<?php echo base_url(); ?>upload/media/<?php echo $rp['attachement']; ?>"></iframe>

                                                          

                                                          <?php } ?>
                                                      
                                                        <div class="error-actions">
                                                          <a href="<?php echo base_url(); ?>home/bpr/download_report/<?php echo $rp['attachement']; ?>" id="download_file_<?php echo $rp['id']; ?>" target="_blank" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Download</a>
                                                         
                                                        </div>
                                                      </div>
                                                  <?php } }else{?>

                                                        <div class="alert alert-warning text-center">
                                                              <i class="fa fa-warning"></i> Belum ada data
                                                        </div>
                                                  <?php } ?>

                                                  
                                                </div> 
                                            </div> 
                                             <!-- END LAPORAN -->   
                                        </div>  
                                     </div> <!-- / tab content --> 
                                  </div><!-- / Card body -->
                              </div> <!-- / Card -->
                        </div>
                    </div> 
                </section> 
        
                    </div> 
                </div>
            </div>
            
        </div>


<nav class="navbar navbar-expand-lg bg-dark fixed-bottom text-center hidden-lg hidden-md visible-sm visible-xs" style="z-index: 1029!important;">
  <div class="container-fluid">  
      <div class="scrollmenu text-center">
     
        <a class="nav-link" href="<?php echo $bpr['fb']; ?>"><i class="fa fa-facebook-official fa-lg fa-5px"></i>  <h5>Facebook</h5></a>
        <a class="nav-link" href="<?php echo $bpr['twt']; ?>"><i class="fa fa-twitter-square fa-lg fa-5px"></i>  <h5>Twitter</h5></a>
        <a class="nav-link" href="<?php echo $bpr['gplus']; ?>"><i class="fa fa-google-plus-official fa-lg fa-5px" ></i>  <h5>Google Plus</h5></a>
        <a class="nav-link" href="<?php echo $bpr['insta']; ?>"><i class="fa fa-instagram fa-lg fa-5px" ></i>  <h5>Instagram</h5></a>
       <a class="nav-link" href="tel:021-4261445 | 021-4261463"><i class="fa fa-phone-square fa-lg fa-5px" ></i>  <h5>Phone</h5></a> 
       
      </div>
     
  </div><!-- /.container-fluid -->
</nav>

<script type="text/javascript">
  
$(document).ready(function(){
   
   
    $("#auction_category").on("change",function(){

      var cat = $(this).val();

      $("#bprauction").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

      $.get(base_url+'home/bpr/auction/0/<?php echo $bpr['id']; ?>/'+cat, function(res){

        $("#bprauction").html(res);
        $(".lazy").lazy({
          effect: "fadeIn",
          effectTime: 300,
          threshold: 0
        });

      });

    });

    $("#asset_code").keypress(function(e) {

        if(e.which == 13) {

          var asset_code = $(this).val();
           
          $("#bprauction").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

          $.get(base_url+'home/bpr/auction/0/<?php echo $bpr['id']; ?>/-/'+asset_code, function(res){

            $("#bprauction").html(res);
            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });

          });

          return false;


        }

    });

    $('#accordion').on('shown.bs.collapse', function() {

         $.get(base_url+'home/bpr/get_stockholder/<?php echo $bpr['id']; ?>',function(data){

            if(data.length>0){

                
                $("#chartContainer").css("height","400px;");

                var plot1 = jQuery.jqplot ('chartContainer', [data], 
                { 
                  seriesDefaults: {
                    renderer: jQuery.jqplot.PieRenderer, 
                    rendererOptions: {
                      showDataLabels: true
                    }
                  }, 
                  legend: { show:true, location: 'e' }
                }
              );

             

            }else{
              $("#chartContainer").css("height","auto !important");
              $("#chartContainer").html("<div class='alert alert-warning text-center'><i class='fa fa-warning'></i> Belum ada data </div>");
            }
           

        }, 'json');
        

        if($("#map").length>0){

           initMap();

        }

        $.get(base_url+'home/bpr/getOrgChartData/<?php echo $bpr['id']; ?>',function(data){

            if(data.sources.length>0){
                var orgchart = new getOrgChart(document.getElementById("people"),{  
                    color: "orange",
                    theme: "tal",
                    enablePrint: true,
                    enableExportToImage: true,
                    enableSearch: false,
                    primaryFields: data.primaryFields,
                    enableEdit: false,                
                    photoFields: ["pic"],
                    dataSource: data.sources,
                   
                });
            }else{
                 $("#people").html('<div class="alert alert-warning text-center" style="margin: 10px;"><i class="fa fa-warning"></i> Belum ada data</div>');
            }
            

        },'json');

        

    });

   

    $.get(base_url+'home/bpr/product/0/<?php echo $bpr['id']; ?>', function(res){

      $("#bprproduct").html(res);
      $(".lazy").lazy({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 0
      });

    });

    $.get(base_url+'home/bpr/auction/0/<?php echo $bpr['id']; ?>', function(res){

      $("#bprauction").html(res);
      $(".lazy").lazy({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 0
      });

    });

    $.get(base_url+'home/bpr/post/0/<?php echo $bpr['id']; ?>', function(res){

      $("#bprpost").html(res);
      $(".lazy").lazy({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 0
      });

    });

    $("#keyword").keypress(function(e) {
        if(e.which == 13) {
           
          var data = $("#search_product").serialize();
          var target = base_url + 'search/product';

          $("#bprproduct").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

          $.post(target, data, function(res){

            $("#bprproduct").html(res);
            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });

          });

          return false;

        }
    });

    $("#keyword2").keypress(function(e) {
        if(e.which == 13) {
           
          var data = $("#search_post").serialize();
          var target = base_url + 'search/post';

          $("#bprproduct").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

          $.post(target, data, function(res){

            $("#bprpost").html(res);
            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });

          });

          return false;

        }
    });

     $(".lazy").lazy({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 0
      });


     /** Owl-Carosel **/

      $('.owl-carousel').owlCarousel({
            loop:true,
            // margin:10,
            responsiveClass:true,
            nav:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:false
                },
                500:{
                    items:1,
                    nav:true,
                    loop:false
                },
                700:{
                    items:1,
                    nav:true,
                    loop:false
                },
                800:{
                    items:1,
                    nav:true,
                    loop:false
                },
                900:{
                    items:1,
                    nav:true,
                    loop:false
                },
                1000:{
                    items:2,
                    nav:true,
                    loop:false
                }
            }
      });

      $(".owl-prev").html("<i class='fa fa-angle-double-left fa-lg'></i>");
      $(".owl-next").html("<i class='fa fa-angle-double-right fa-lg'></i>");


      
      /*** Animasi Tabs Content ***/
       $('#profile').click(function (e) {
            e.preventDefault()
            $("#profile_content").slideDown('slow');
            $("#branch_content").hide('slow');
            $("#product_content").hide('slow');
            $("#blog_content").hide('slow');
            $("#report_content").hide('slow'); 
            $("#auction_content").hide('slow'); 
        });

       $('#branch').click(function (e) {
            e.preventDefault()
            $("#profile_content").hide('slow');
            $("#branch_content").slideDown('slow');
            $("#product_content").hide('slow');
            $("#blog_content").hide('slow');
            $("#report_content").hide('slow');
            $("#auction_content").hide('slow'); 

            $(".branch_map").each(function(i, obj) {
                id = $(this).attr("id");
                lat = $(this).data("latitude");
                lon = $(this).data("longitude");

                showMap(lat, lon, id);
            }); 
        });

       $('#product').click(function (e) {
            e.preventDefault()
            $("#profile_content").hide('slow');
            $("#branch_content").hide('slow');
            $("#product_content").slideDown('slow');
            $("#blog_content").hide('slow');
            $("#report_content").hide('slow'); 
            $("#auction_content").hide('slow'); 
        });
        $('#lelang').click(function (e) {
            e.preventDefault()
            $("#profile_content").hide('slow');
            $("#branch_content").hide('slow');
            $("#product_content").hide('slow');
            $("#blog_content").hide('slow');
            $("#report_content").hide('slow'); 
            $("#auction_content").slideDown('slow'); 
        });
       $('#blog').click(function (e) {
            e.preventDefault()
            $("#profile_content").hide('slow');
            $("#branch_content").hide('slow');
            $("#product_content").hide('slow');
            $("#blog_content").slideDown('slow');
            $("#report_content").hide('slow');
            $("#auction_content").hide('slow');  
        });
       $('#report').click(function (e) {
            e.preventDefault()
            $("#profile_content").hide('slow');
            $("#branch_content").hide('slow');
            $("#product_content").hide('slow');
            $("#blog_content").hide('slow');
            $("#report_content").slideDown('slow'); 
            $("#auction_content").hide('slow');  
        });
      /** END **/

      $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
          e.preventDefault();
          $(this).siblings('a.active').removeClass("active");
          $(this).addClass("active");
          var index = $(this).index();
          $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
          $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
      });

});

function pagination(container,target){

  $("html, body").animate({ scrollTop: 0 }, "slow");

  $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");
  $("#"+container).load(target,function(){

    $(".lazy").lazy({
      effect: "fadeIn",
      effectTime: 300,
      threshold: 0
    });

  });


}

function initMap() {

    var loc = {lat: parseFloat("<?php echo $bpr['latitude']; ?>"), lng: parseFloat("<?php echo $bpr['longitude']; ?>")}

    var map = new google.maps.Map(document.getElementById('map'), {
        center: loc,
        zoom: 10
    });
    

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        position: loc,
        map: map
    });

}


</script>
