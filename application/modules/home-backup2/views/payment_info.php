 <div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">


        <section>
          <div class="row" style=" margin-top: -444px;" >
            <div class="col-md-8"> 
                <div class="card">  
                    <div  >      
                        <span class="label label-default">Bank Transfer</span> 
                        <div class="text-center padding-12" >
                            <h3 class="title titel-total margin-0"  >TOTAL PEMBAYARAN</h3>
                            <h6 class="description margin-0 text-orange" >Rp.21.365.553</h6>
                        </div>
                    </div>
                        <!-- <hr> --> 
                        <div class="panel panel-default">
                            <div class="text-center padding-12">
                                <div class="alert alert-warning">
                                    Harga sudah termasuk kode transfer Rp 553
                                Segera lakukan transfer dan konfirmasi pembayaran sebelum Jumat,<b> 6 Oktober 2017 pukul 10.22. </b> Jika tidak maka transaksi Agan akan dianggap batal.
                                </div>
                            </div>
                              <div class="panel-body">

                                    <div class="table-responsive padding-12">
                                        
                                        <table class="table table-striped border-b table-hovered">
                                            <tr>
                                                <td>
                                                    Mandiri
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    1240 0222 9998 7 (Cabang Kuningan)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Atas Nama
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                     PT Darta Media Indonesia
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                        <table class="table table-striped border-b table-hovered ">
                                            <tr>
                                                <td>
                                                    BCA
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    5425 1111 05 (Cabang Kuningan)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Atas Nama
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    PT Darta Media Indonesia
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="form-group text-center">
                                         <a href="#" class="btn btn-primary btn-simple btn-lg"  data-toggle="modal" data-target="#cekBayar">Cara bayar</a>
                                        <a href="#" class="btn btn-primary btn-lg">Detail Transaksi</a>
                                    </div>
                              </div>
                  </div>
              </div>
            </div>
            <div class="col-md-4">
                 <div class="card">     
                        <div class="kc-summary-details"> 
                          <div class="kc-summary-item-container">
                          <img class="kc-summary-item-image" src="https://3.bp.blogspot.com/-hEpBOGZz_MU/WMAzzPf8TGI/AAAAAAAACGc/dcPE93ztF5IUdHxI0vVTCrmtPXgKBMwNQCLcB/s1600/16939293_1170759089688952_5421678601934932490_n.jpg" role="presentation">
                              <div class="kc-summary-item">   
                                  <div class="title-ivent"> 
                                      <div class="listing-hero-body ">
                                          <h5 class="listing-hero-title-x" data-automation="listing-title">HAMERSONIC EDUCATION EXPO 2017 - Bandung</h5>
                                          <meta content="INTERNATIONAL EDUCATION EXPO 2017 - Bandung">
                                          <div class="l-mar-top-3">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <a href="#listing-organizer" class="js-d-scroll-to listing-organizer-name text-default" data-d-duration="1500" data-d-offset="-70" data-d-destination="#listing-organizer" dorsal-guid="c4b13822-b124-5d86-db1b-ecdd65393a23" data-xd-wired="scroll-to">
                                                          by Overseas Zone
                                                      </a>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      
                                  </div>
                               
                              </div>
                          </div>
                          <div class="kc-summary-info-container">

                          <table class="table">
                              <tr>
                                  <td>
                                      <span><span>Peserta</span></span>
                                  </td>
                                  <td>
                                     <i class="fa fa-users"></i>
                                  </td>
                                  <td class="text-left">
                                     1.000.000
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <span><span>Harga Ticket</span></span>
                                  </td>
                                  <td>
                                      Rp
                                  </td>
                                  <td class="text-left">
                                    20.000.000
                                  </td>
                              </tr>
                              
                              <tr>
                                  <td>
                                      <span><span ><b>Total Pembelian</b></span></span>
                                  </td>
                                  <td class="text-orange">
                                      Rp
                                  </td>
                                  <td class="text-left">
                                    <span class="text-orange">21.365.553</span>
                                  </td>
                              </tr>
                               
                          </table>

                               
                          </div>
                      </div>

                </div>
            </div>
          </div>
        </section>


        <section>
              <!-- Sart Modal -->
                <div class="modal fade" id="cekBayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header justify-content-center">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                </button>
                                <h4 class="title title-up">Cara Bayar - Bank Transfer</h4>
                            </div>
                            <div class="modal-body">
                                <div class="kc-modal-body">
                                    <div class="kc-modal-warning">
                                        <div class="kc-warning-container">
                                            <div class="kc-warning-text">
                                                <div><span class="text-primary">Harga sudah termasuk kode transfer Rp 553</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <ol class="kc-instruction-ol"><span><li>Lakukan pembayaran melalui transfer via Bank yang Agan pilih.</li><li>Masukkan kode bank + nomor rekening yang dituju.</li><li>Pastikan nominal yang Agan transfer tepat sesuai kode transfer demi kelancaran verifikasi pembayaran.</li><li>Kode transfer akan dikembalikan ke Saldo Kaspay setelah transaksi selesai.</li><li>Konfirmasi pembayaran Agan di bagian Konfirmasi Pembayaran.</li><li>Transfer dan konfirmasi pembayaran maksimal 24 jam, jika tidak maka transaksi Agan akan dianggap batal.</li><li>Pembayaran akan diproses dalam 2 hari kerja setelah konfirmasi dilakukan.</li><li>Untuk transfer dari bank yang berbeda, Agan mungkin akan dikenai biaya bank.</li></span></ol>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> 
                            </div>
                        </div>
                    </div>
                </div>
                <!--  End Modal -->
        </section>



            </div> 
        </div>
    </div>
</div>