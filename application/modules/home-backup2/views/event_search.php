 
<div class="wrapper margin-topfix">
          <div class="page-header page-header-small"  > 
                <div class="page-header-image lazy" data-parallax="true" data-src="http://perbarindo.tranyar.com/mocukup/Bghijau.png">
                </div> 
            </div>  
            <div class="section">
                <div class="container marketing">
 
                  <section>
                    <div class="row"> 
                        <div class="col-md-12 padding-0">
                          <!-- Start Page -->
 
  

     <section>  
          <div class="container marketing margin-topfxs">
              <div class="card"> 
                    <div class="search-travel">
                   <div class="seach-head text-center">
                       <h1 class="text-heading-epic homepage-header text-primary"  >
                          <i class="fa fa-search fa-lg"></i> Find Event
                      </h1>
                   </div>
                     <form action="<?php echo base_url(); ?>find/event" method="post">
                          <input type="hidden" name="street_number" id="street_number">
                          <input type="hidden" name="route" id="route">
                          <input type="hidden" name="locality" id="locality">
                          <input type="hidden" name="administrative_area_level_1" id="administrative_area_level_1">
                          <input type="hidden" name="postal_code" id="postal_code">
                          <input type="hidden" name="country" id="country">
                      <div class="row">
                         
                          <div class="col-sm-6 col-lg-3">
                              <div class="form-group">
                                  <input type="text" name="keyword" class="form-control" placeholder="Search events" title="Search events">
                              </div>
                          </div>
                          <div class="col-sm-6 col-lg-3">
                              <div class="form-group has-success">
                                  <input type="text" name="location" id="autocomplete" class="form-control form-control-success" placeholder="City or Location" title="City or Location">
                              </div>
                          </div>
                          <div class="col-sm-6 col-lg-3">
                              <div class="form-group has-ingo">
                                  <select name="date" class="form-control form-control-info">
                                       <option class="option-org" value="all" selected="selected">All Dates</option>
                                      <option class="option-org" value="today">Today</option>
                                      <option class="option-org" value="tomorrow">Tomorrow</option>
                                      <option class="option-org" value="this_week">This Week</option>
                                      <option class="option-org" value="next_week">Next Week</option>
                                      <option class="option-org" value="next_month">Next Month</option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-6 col-lg-3" style="margin-top: -7px;">
                              <div class="input-group"> 
                                  <button class="btn btn-primary btn-block btn-xs" type="submit">
                                      <i class="fa fa-seach"></i> SEARCH
                                  </button>
                              </div>
                          </div>
        
                         
                      </div>    
                   </form>           
             </div>
              </div> 
          </div> 
      </section>

       <section>  
          <div class="container marketing">
            <div class="card card-x" style="padding: 12px;">
               <?php if($post){ ?>
                
                  <div class="card"> 
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3 class="section-header l-align-left" aria-label="Popular Events in  Bandung, Indonesia">
                                <span data-automation="home-popular-events-header" aria-hidden="true">Found (<strong><?php echo count($events); ?></strong>) Result</span>
                               
                            </h3>
                        </div>
                    </div> 
                  </div> 
                <hr>
                <?php } ?> 

           <div class="row">  
             <?php foreach($events as $oe){ ?>

                  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                      <div class="card card-bx">
                          <div class="wrap-card poster-card">
                              <div class="head-card">
                                     <div class="first-slide img-responsive"  style="height:600px;background: url('<?php echo setEventImage($oe['cover']) ?>') no-repeat;background-size: auto 277px; "  alt="<?php echo $oe['name'] ?>" width="100%" > 
                           <!--        <img data-src="holder.js/100px280/thumb" alt="<?php echo $oe['name'] ?>" style="height: auto; width: auto; display: block;" src="<?php echo setEventImage($oe['cover']) ?>" data-holder-rendered="true"> -->
                                      </div>
                              </div>
                              <div class="body-card-px">
                                  <div class="poster-card__body">
                                        <?php
                                              $sdate=date_create($oe['start_date']);
                                              $sdaynum = date_format($sdate,"d");
                                              $sday = date_format($sdate,"l");
                                              $smonth = date_format($sdate,"F");
                                              $syear = date_format($sdate,"Y");

                                              $edate=date_create($oe['end_date']);
                                              $edaynum = date_format($edate,"d");
                                              $eday = date_format($edate,"l");
                                              $emonth = date_format($edate,"F");
                                              $eyear = date_format($edate,"Y");
                                      ?>                                    
                                      <time class="poster-card__date">

                                          <?php echo $sday.' , '.$smonth.' '.$sdaynum.' '.$syear ?>

                                      </time>
                                      <div class="poster-card__title">
                                         <a href="<?php echo base_url().'show/event/'.$oe['id']; ?>"> <?php echo $oe['name']; ?></a>
                                      </div>
                                      <div class="poster-card__venue">

                                         <?php echo $oe['city']; ?>

                                      </div>
            
                                  </div>   
                              </div>     
                              <div class="poster-card__footer"> 
                                  <div class="poster-card__tags"> 
                                         
                                  </div>
                                  <div class="poster-card__actions">
                                      <a class="js-share-event-card share-action" href="<?php echo base_url().'show/event/'.$oe['id']; ?>" data-eid="37867656179" title="Detail" aria-haspopup="true">
                                         <i class="fa fa-check"></i>
                                      </a>
                                      <a class="js-d-bookmark" data-eid="37867656179" data-bookmarked="false" title="show map" data-source-page="home:popular" href="http://maps.google.com/maps?q=<?php echo $oe['latitude']; ?>,<?php echo $oe['longitude']; ?>&z=17" aria-label="Save Event" dorsal-guid="1991d28d-4cdd-81ca-3e37-5ce9278b2747" data-xd-wired="bookmark" target="_blank">
                                          <i class="fa fa-map-marker"></i>
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div> 
              <?php } ?> 
              </div>


                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 class="section-header l-align-left" aria-label="Popular Events in  Bandung, Indonesia">
                            <span data-automation="home-popular-events-header" aria-hidden="true">Latest Event</span> 
                        </h4>
                    </div>
                </div> 
                <hr>
                <div class="row">


                   <?php foreach($latest_events as $oe){ ?>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                            <div class="card">
                                <div class="wrap-card poster-card">
                                    <div class="head-card frame"> 
                                         <img class="img-responsive lazy" src="<?php echo base_url().'assets/dist/img/preloader.gif'; ?>" data-src="<?php echo setEventImage($oe['cover']) ?>" alt="<?php echo $oe['name'] ?>" data-holder-rendered="true"> 
                                    </div>
                                    <div class="body-card">
                                        <div class="poster-card__body">
                                              <?php
                                                    $sdate=date_create($oe['start_date']);
                                                    $sdaynum = date_format($sdate,"d");
                                                    $sday = date_format($sdate,"l");
                                                    $smonth = date_format($sdate,"F");
                                                    $syear = date_format($sdate,"Y");

                                                    $edate=date_create($oe['end_date']);
                                                    $edaynum = date_format($edate,"d");
                                                    $eday = date_format($edate,"l");
                                                    $emonth = date_format($edate,"F");
                                                    $eyear = date_format($edate,"Y");
                                            ?>                                    
                                            <time class="poster-card__date">

                                                <?php echo $sday.' , '.$smonth.' '.$sdaynum.' '.$syear ?>

                                            </time>
                                            <div class="poster-card__title">
                                               <a href="<?php echo base_url().'show/event/'.$oe['id']; ?>"> <?php echo $oe['name']; ?></a>
                                            </div>
                                            <div class="poster-card__venue">

                                               <?php echo $oe['city']; ?>

                                            </div>
                  
                                        </div>   
                                    </div>     
                                    <div class="poster-card__footer"> 
                                        <div class="poster-card__tags"> 
                                               
                                        </div>
                                        <div class="poster-card__actions">
                                            <a class="js-share-event-card share-action" href="<?php echo base_url().'show/event/'.$oe['id']; ?>" data-eid="37867656179" title="Detail" aria-haspopup="true">
                                               <i class="fa fa-check"></i>
                                            </a>
                                            <a class="js-d-bookmark" data-eid="37867656179" data-bookmarked="false" title="show map" data-source-page="home:popular" href="http://maps.google.com/maps?q=<?php echo $oe['latitude']; ?>,<?php echo $oe['longitude']; ?>&z=17" aria-label="Save Event" dorsal-guid="1991d28d-4cdd-81ca-3e37-5ce9278b2747" data-xd-wired="bookmark" target="_blank">
                                                <i class="fa fa-map-marker"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                    <?php } ?>
                  
                 
                </div>



                    </div>
                  </div> 
              </section>


 


            <!-- End Page -->
                 </div>
                </div>
             </section> 
        </div>
      </div>

 
    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();
            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });
        });
    </script>
     <script>

          var placeSearch, autocomplete;
          var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
          };

          function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
          }

          function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
              }
            }
          }

          // Bias the autocomplete object to the user's geographical location,
          // as supplied by the browser's 'navigator.geolocation' object.
          function geolocate() {
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
              });
            }
          }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places&callback=initAutocomplete"
        async defer></script>