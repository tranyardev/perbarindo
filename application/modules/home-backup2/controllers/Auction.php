<?php

class Auction extends MX_Controller{


	var $preloader = "";

	function __construct(){

		parent::__construct();
    
    $this->load->library("Aauth");
    $this->load->model("Mbprauction");
    $this->load->model("Mbprauctioncategory");

    $this->preloader = base_url()."assets/dist/img/preloader.gif";
 

	}


	public function index(){


		$data['page'] = 'auction';
    $data['og_title'] = 'PERBARINDO | LELANG';
    $data['og_site_name'] = 'PERBARINDO LELANG';
    $data['og_site_url'] = base_url();
    $data['og_site_description'] = 'Lelang Aset BPR PERBARINDO';
    $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';

   	$data['body_class'] = 'profile-page';
    $data['navbar']['navbar_class'] = 'navbar-transparent'; 

    $data['auction_category'] = $this->Mbprauctioncategory->getBPRAuctionCategory();

    $this->load->view('layout/main-new',$data);

	}

  function get_auction($page=0, $selector = "auctionlist"){

        $alldata = $this->Mbprauction->getAllAuctionList();
        $page_config = $this->config->item('pagination');
        $page_config['per_page'] = 4;
        $page_config['uri_segment'] = 3;
        $page_config['total_rows'] = count($alldata);
        $page_config['base_url'] = "javascript:pagination('".$selector."','".base_url() . "/home/auction/get_auction/";
        $page_config['first_url'] = "javascript:pagination('".$selector."','".base_url() . "/home/auction/get_auction/0".$selector."')";
        $page_config['suffix'] = "/".$selector."')";

        $start=$page;

        if(empty($start)){

            $start=0;
        
        }

        $this->pagination->initialize($page_config);
        $events = $this->Mbprauction->getAuctionList($start,$page_config['per_page']);
        $paging = $this->pagination->create_links();

        $html = "<ul class='list-group ovhiden'>";

        foreach($events as $e){ 
 
          $html.= '
            <li class="list-group-item padding-0">
             <a href="'.base_url().'bpr/asset_code/'.$e['asset_code'].'" class="list-group-item list-group-item-action flex-column align-items-start">

             <div class="row">
              <div class="col col-lg-3 col-md-3 col-4 col-sm-4 col-xs-4 padding-xs">
                <div class="img-bxs frame-x">
                  <img src="http://perbarindo.tranyar.com/upload/media/'.$e['photo'].'" class="lazy img-responsive img-fluid"> 
                </div>
              </div>
              <div class="col col-lg-9 col-md-9 col-8 col-sm-8 col-xs-8 padding-xs">
                   <div class="d-flex w-100">
                      <h6 class="titlelistg text-ellipsis padding-0"><span>'.$e['auction_category_name'].'</span></h6> 
                  </div>
                  <p class="p-lgt padding-0">Rp.'.number_format($e['start_price']).'.</p> 
              </div>
             </div> 
            </a>

            </li>
          ';

        } 

        $html.= "</ul>";
        $html.= "<div class='text-center ml-auto pagning'>";
        $html.= $paging;
        $html.= "</div>";
               
        echo $html;

    }

	public function auction_list($page = 0, $category = null, $asset_code = null){
  
    $alldata = $this->Mbprauction->getBPRAuctionList($category, null, null, $asset_code);

    $container = "bprauction";
    $page_config = $this->config->item('pagination');
    $page_config['per_page'] = 10;
    $page_config['uri_segment'] = 4;
    $page_config['total_rows'] = count($alldata);
    $page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/auction/auction_list";
    $page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/auction/auction_list/0/".$category."')";
    $page_config['suffix'] = "/".$category."/".$asset_code."')";

    $start=$page;

    if(empty($start)){
        $start=0;
    }
    $this->pagination->initialize($page_config);

    $post = $this->Mbprauction->getBPRAuctionList($category, $start, $page_config['per_page'], $asset_code);
    $paging = $this->pagination->create_links();

       
        $html = "";


        if(count($post)>0){

          foreach($post as $p){ 

          if($p['photo'] != ""){

            $path = "upload/media";
                  $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                  $file = $upload_path.'/'.$p['photo'];

                  if(file_exists($file)){

                      $photo = base_url().$path.'/'.$p['photo'];

                  }else{

                      $photo = base_url().'public/assets/img/default_event_cover.png';

                  }

          }else{

            $photo = base_url().'public/assets/img/default_event_cover.png';

          }
          
          $content = strip_tags($p['description']);

          $content = $this->truncate($content,200);

          $date = date_create($p['auction_date']);

          $label_status = "";

          if($p['status']=="1"){
            $label_status = "<span class='sold'>(TERJUAL)</span>";
          }

              $html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$photo.'" class="lazy img-responsive" alt="'.$p['asset_code'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/asset_code/'.$p['asset_code'].'" rel="noopener noreferrer">'.$p['auction_category_name'].'-'.$p['asset_code'].' '.$label_status.'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                    <div class="price"> Rp. '.number_format($p['start_price'],0,",",".").'</div>
                                                  
                                                  <p>'.$p['city'].'</p>
                                                  <p><b>Lelang</b> : '.date_format($date,"d-m-Y").', '.$p['start_time'].'-'.$p['end_time'].'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/asset_code/'.$p['asset_code'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['auction_category_name'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';

                                  

          } 

        }else{

          $html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

      

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;

  }

	

	function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }


}