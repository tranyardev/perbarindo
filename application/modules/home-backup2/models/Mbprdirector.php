<?php

class Mbprdirector extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRDirector(){

        $result = $this->db->query("SELECT * FROM directors")->result_array();
        return $result;

    }
    function getBPRDirectorById($id){

        $result = $this->db->query("SELECT * FROM directors WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRDirectorByPosition($bpr,$position){

        $result = $this->db->query("SELECT a.*, b.name, b.photo FROM directors a 
                                    LEFT JOIN members b ON b.id = a.member_id 
                                    WHERE a.position = '".$position."' AND a.bpr='".$bpr."'")->result_array();
        return $result;

    }

    function getBPRDirectorBySlug($bpr,$slug){

        $result = $this->db->query("SELECT a.*, b.name, b.photo, c.name as position_name, c.position_title FROM directors a 
                                    LEFT JOIN members b ON b.id = a.member_id 
                                    LEFT JOIN director_positions c ON c.id = a.position
                                    WHERE c.slug = '".$slug."' AND a.bpr='".$bpr."'")->result_array();
        return $result;

    }

    function getBPRDirectorDetail($id){

        $result = $this->db->query("SELECT a.*, b.name,b.gender, b.email,b.no_hp, b.photo, c.name as position_name, c.position_title FROM directors a 
                                    LEFT JOIN members b ON b.id = a.member_id 
                                    LEFT JOIN director_positions c ON c.id = a.position
                                    WHERE a.id='".$id."'")->result_array();
        return @$result[0];

    }

    function getBPRChartOrg($bpr){

        $result = $this->db->query("select b.id, b.name as position, b.parent, c.name, c.photo from directors a 
                                    left join director_positions b on b.id = a.position 
                                    left join members c on c.id=a.member_id
                                    where a.bpr='".$bpr."' AND b.slug <> 'komisaris'")->result_array();
        return $result;

    }

    function getBPRChartOrgTopLevel($bpr){

        $result = $this->db->query("select b.id, b.name as position, b.parent, c.name, c.photo from directors a 
                                    left join director_positions b on b.id = a.position 
                                    left join members c on c.id=a.member_id
                                    where a.bpr='".$bpr."' and b.slug='komisaris'")->result_array();
        return $result;

    }
    
    
 
}