<?php

class Mdpd extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getDPDById($id){

        $result = $this->db->query("SELECT * FROM dpd WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getDPD(){

        $result = $this->db->query("SELECT * FROM dpd ORDER BY name ASC")->result_array();
        return $result;

    }
    function getDPDByName($name){

        $result = $this->db->query("SELECT * FROM dpd WHERE name = '$name'")->result_array();
        return $result;

    }
    function checkDpdUser($dpd,$username){

        $result = $this->db->query("SELECT * FROM aauth_users WHERE dpd = '$dpd' AND username = '$username'")->num_rows();
        return $result;

    }
}