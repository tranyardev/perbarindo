<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Custom_page extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MCustomPage");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'custom_page_view';
            $this->add_perm = 'custom_page_add';
            $this->edit_perm = 'custom_page_update';
            $this->delete_perm = 'custom_page_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "pages";
        $this->dttModel = "MDCustomPage";
        $this->pk = "page_id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-file'></i> Custom Page" => base_url()."pages/custom_page"
        );

        $data['page'] = 'custom_page';
        $data['page_title'] = 'Custom Page';
        $data['page_subtitle'] = 'Manage Page';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'custom_page';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(

                "title" => $this->input->post("title"),
                "content" => $this->input->post("content"),
                "permalink" => $this->input->post("permalink"),
                "meta_keywords" => $this->input->post("meta_keyword"),
                "meta_description" => $this->input->post("meta_description"),
                "status" => $this->input->post("status"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")
            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-file'></i> Custom Page" => base_url()."pages/custom_page",
                "Add New" => base_url()."pages/custom_page/addnew",
            );

            $data['page'] = 'custom_page_add';
            $data['page_title'] = 'Custom Page';
            $data['page_subtitle'] = 'Add New Custom Page';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'custom_page';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }



    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(

                "title" => $this->input->post("title"),
                "content" => $this->input->post("content"),
                "permalink" => $this->input->post("permalink"),
                "meta_keywords" => $this->input->post("meta_keyword"),
                "meta_description" => $this->input->post("meta_description"),
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );


            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-file'></i> Custom Page" => base_url()."pages/custom_page",
                "Edit" => base_url()."pages/custom_page/edit/".$id,
            );

            $data['page'] = 'custom_page_edit';
            $data['page_title'] = 'Custom Page';
            $data['page_subtitle'] = 'Edit Page';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/program/assets/css/program_category.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MCustomPage->getCustomPageById($id);
            $data['data']['parent_menu'] = 'custom_page';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}