<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/18/16
 * Time: 12:06
 */

 class Session extends MX_Controller{

     function __construct(){
         parent::__construct();
         $this->load->library("Aauth");
     }
     function login(){
         if(count($_POST)>0){

             $username = strip_tags($_POST['username']);
             $pass = $_POST['password'];

             $this->session->unset_userdata('picture');

             if($this->aauth->login($username, $pass)){

                 /* start session */

                if($this->session->userdata('picture') == ""){
                    $this->session->set_userdata('picture', base_url().'public/assets/img/default-avatar.png');
                }else{
                    if($this->session->userdata('picture') != base_url().'public/assets/img/default-avatar.png'){

                        $this->session->set_userdata('picture', base_url().'upload/photo/'.$this->session->userdata('picture'));
                        
                    }
                }

                 $data['granted'] = true;
             }else{
                 $data['granted'] = false;
             }

             echo json_encode($data);

         }else{
             echo "WARNING : Missing data params!";
         }
     }

     function logout(){
         $this->aauth->logout();
         /* destroy all session */

         echo '<script>window.location.href="'.base_url().'";</script>';
     }
     function create_user(){
         $this->aauth->create_user('indra.developer.web.id@gmail.com','admin','indra');
     }
     function tes(){
        echo "123";
     }
 }