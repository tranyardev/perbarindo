<div class="row" id="form_wrapper">
    <form role="form" id="form">

        <div class="col-md-6">
   
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $this->lang->line('dpd_edit'); ?></h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label for="dpd"><?php echo $this->lang->line('dpd'); ?></label>
                        <select class="form-control" name="dpd" id="dpd">
                            <?php
                                foreach ($dpd as $d) {

                                    if($d['id']==$dataedit['dpd']){
                                        $selected = "selected";
                                    }else{
                                        $selected = "";
                                    }
                                    
                                    echo "<option $selected value='".$d['id']."'>".$d['name']."</option>";

                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="corporate"><?php echo $this->lang->line('corporate'); ?></label>
                        <select class="form-control" name="corporate" id="corporate">
                             <?php
                                foreach ($corporates as $c) {

                                    if($c['id']==$dataedit['corporate']){
                                        $selected = "selected";
                                    }else{
                                        $selected = "";
                                    }
                                    
                                    echo "<option value='".$c['id']."'>".$c['name']."</option>";

                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name"><?php echo $this->lang->line('name'); ?></label>
                        <input type="text" name="name" required class="form-control validate[required]" id="name" placeholder="<?php echo $this->lang->line('name'); ?>" value="<?php echo $dataedit['name']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="email"><?php echo $this->lang->line('email'); ?></label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="<?php echo $this->lang->line('email'); ?>" value="<?php echo $dataedit['email']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="telp"><?php echo $this->lang->line('telp'); ?></label>
                        <input type="text" name="telp" required class="form-control validate[required]" id="telp" placeholder="<?php echo $this->lang->line('telp'); ?>" value="<?php echo $dataedit['telp']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="fax"><?php echo $this->lang->line('fax'); ?></label>
                        <input type="text" name="fax" class="form-control" id="fax" placeholder="<?php echo $this->lang->line('fax'); ?>" value="<?php echo $dataedit['fax']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="website"><?php echo $this->lang->line('website'); ?></label>
                        <input type="text" name="website" class="form-control" id="website" placeholder="<?php echo $this->lang->line('website'); ?>" value="<?php echo $dataedit['website']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="address"><?php echo $this->lang->line('address'); ?></label>
                        <textarea name="address" class="form-control" id="address" placeholder="<?php echo $this->lang->line('description'); ?>"><?php echo $dataedit['address']; ?></textarea>
                    </div>
                </div>
        

                <div class="box-footer">
                    &nbsp;<button type="submit" id="btn-submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default"><?php echo $this->lang->line('back'); ?></button>
                </div>

            </div>

        </div>
        
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                toastr.info("Updating data...", 'Loading');
                $("#btn-submit").button("loading");
                saveFormData();

                return false;

            }

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"master/bpr/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();

        $.post(target, data, function(res){

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            $("#btn-submit").button("reset");
            resetForm();

        },'json');

    }

    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>