<div class="box">
    <div class="box-header">
        <h3 class="box-title">Media</h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <ul class="nav nav-tabs">
          <?php 
            $i=0;
            foreach ($media_type as $mtype) {
                if($i==0){
                    $class = "active";
                }else{
                    $class = "";
                }

                echo ' <li class="'.$class.'"><a data-toggle="tab" href="#'.$mtype['type'].'">'.$mtype['type'].'</a></li>';
                $i++;
            }
          ?>
        </ul>

        <div class="tab-content">
            <?php 
                $i=0;
                $html='';
                foreach ($media_type as $mtype) {
                    if($i==0){
                        $class = "in active";
                    }else{
                        $class = "";
                    }

                    $html.='<div id="'.$mtype['type'].'" class="tab-pane fade '.$class.'">
                            <h3>List media '.$mtype['type'].'</h3>';

                    if($mtype['type']=="image"){

                        $images = $this->MMedia->getMediaByType('image');

                        if(count($images) > 0){

                            $html.='<div class="row text-center text-lg-left">';

                            foreach ($images as $img) {
                               
                                $html.='<div class="col-lg-3 col-md-4 col-xs-6">
                                          <a href="'.base_url().'master/media/detail/'.$img['media_id'].'" class="d-block mb-4 h-100">
                                            <img class="img-fluid img-thumbnail" src="'.base_url().$img['path'].$img['file'].'" alt="">
                                          </a>
                                        </div>';

                            }

                            $html.='</div>';

                        }

                    }else if($mtype['type']=="document"){


                        $docs = $this->MMedia->getMediaByType('document');



                        if(count($docs) > 0){

                            $html.='<div class="row text-center text-lg-left">';

                            $icon = '';

                            foreach ($docs as $doc) {

                                $title = $this->MMedia->getDocumentReference($doc['object_id']);


                                switch ($doc['format']) {
                                    case 'pdf':
                                        
                                        $icon = 'pdf_icon.png';
                                        break;

                                    case 'csv':
                                        
                                        $icon = 'excel_icon.png';
                                        break;

                                    case 'xls':
                                        
                                        $icon = 'excel_icon.png';
                                        break;

                                    case 'xlsx':
                                        
                                        $icon = 'excel_icon.png';
                                        break;

                                    case 'doc':
                                        
                                        $icon = 'word_icon.png';
                                        break;

                                    case 'docx':
                                        
                                        $icon = 'word_icon.png';
                                        break;

                                    case 'ppt':
                                        
                                        $icon = 'powerpoint_icon.png';
                                        break;

                                    case 'pptx':
                                        
                                        $icon = 'powerpoint_icon.png';
                                        break;
                                    
                                }
                               
                                $html.='<div class="col-lg-2 col-md-2 col-xs-4">
                                          <a href="'.base_url().'master/media/detail/'.$doc['media_id'].'" class="d-block mb-4 h-100">
                                            <img class="img-fluid img-thumbnail" src="'.base_url().'assets/dist/img/'.$icon.'" alt="">
                                            <p>'.$title['name'].'</p>
                                          </a>
                                        </div>';

                            }

                            $html.='</div>';

                        }


                    }

                    $html.='</div>';
                  
                    $i++;
                }
                echo $html;
            ?>
         
        </div>
    </div>
    </form>
    <!-- /.box-body -->
</div>

