<?php
    if($bpr['latitude']==""){
        $bpr['latitude']="0";
    }
    if($bpr['longitude']==""){
        $bpr['longitude']="0";
    }
?>

<style>
div.orgChart div.node {
    width: 158px;
    height: 223px;
}
div.orgChart div.node img {
    margin-top: 4px;
    border: 2px solid black;
    border-radius: 4px;
}
ul.list_people{

    list-style: none;

}
ul.list_people li ul{

    list-style: none;

}
ul.list_people li.add_more{

    margin-top: 10px;
    padding: 5px;
    border: dashed 1px #3c8dbc;
    text-align: center;

}
ul.list_people li{
    padding: 10px;
    border-bottom: dashed 1px #aaa;
    border-left: dashed 1px #aaa;
}
.bottom-dashes{
    border-bottom: dashed 1px #aaa;
}
.img-pop-prev{

    height:180px;
    margin-left: auto;
    margin-right: auto;
    border: solid 2px #aaa;

}
.verified_label{

    color: green;
    font-style: italic;

}
.unverified_label{

    color: red;
    font-style: italic;

}
</style>
<div class="row" id="form_wrapper">
    <form role="form" id="form">

        <div class="col-md-12">

            <?php if($bpr['is_notif_activated']=="1"){ ?>
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Notifikasi</strong> <br><br><?php echo $bpr['notif']; ?>
            </div>
            <?php } ?>
            <div class="row">
                    
                <div class="col-md-6">
                      <div class="box box-primary box-solid" id="box-org">
                <div class="box-header with-border" data-step="1" data-intro="Selamat Datang Admin, Mohon untuk mengisi data struktur organisasi terlebih dahulu. Ikuti langkah - langkah yang ditunjukan.">
                    <h3 class="box-title"><i class="fa fa-sitemap"></i> Struktur Organisasi</h3>
                    <div class="box-tools pull-right">

                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
             

                <div class="box-body">

                    
                    <div class="col-md-12">
                        
                        <h4 class="bottom-dashes">Pemegang Saham</h4>
                        <ul class="list_people">
                            <li><h5 data-step="2" data-intro="Pertama, yang harus diisi adalah data PSP(Pemegang Saham Pengendali)">Pemegang Saham Pengendali(PSP)</h5>
                                <ul id="psp_stockholder"></ul>
                            </li>
                             <li><h5 data-step="3" data-intro="Selanjutnya, yang harus diisi adalah data Pemegang Saham lainnya. Nilai saham yang diinput jika diakumulasikan harus 100%.">Pemegang Saham Biasa</h5>
                                <ul id="regular_stockholder"></ul>
                            </li>
                        </ul>

                        <h4 class="bottom-dashes">Komisaris</h4>
                        <ul class="list_people" data-step="4" data-intro="Kemudian, Silahkan isi data posisi Komisaris.">
                            <li>
                                <h5>Komisaris Utama</h5>
                                <ul id="komisaris_utama"></ul>
                            </li>
                             <li>
                                <h5>Komisaris</h5>
                                <ul id="komisaris"></ul>
                            </li>
                        </ul>


                        <h4 class="bottom-dashes" data-step="5" data-intro="Langkah Selanjutnya, Silahkan isi data posisi Direktur.">Pengurus</h4>
                        <ul class="list_people">

                            <li><h5 data-step="6" data-intro="Isikan data direktur utama.">Direktur Utama</h5>
                                <ul id="dirut_container"></ul>
                            </li>
                            <li><h5 data-step="7" data-intro="Kemudian, lengkapi data direktur lainnya.">Direktur</h5>
                                <ul id="dir_container"></ul>
                            </li>
                           
                        </ul>

                    </div>

                </div>
                <div class="box-footer" style="text-align: center">
                    
                    <?php 
                        $ret = '<a href="javascript:verify_bpr();" class="btn btn-primary">Ajukan Verifikasi Organisasi</a>';
                        if(count($validation)>0){ 
                            if($validation['is_verified']=='1'){
                                $ret = '';
                            }
                        }
                        print($ret);
                    ?>
                    
                </div>

            </div>
                </div>

                <div class="col-md-6">
                    
                    <div class="box box-warning box-solid">
                        <div class="box-header with-border" style="border-bottom: 0px;">
                            <h3 class="box-title"><i class="fa fa-sitemap"></i> Bagan Organisasi</h3>
                            <div class="box-tools pull-right">
                               
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                     

                        <div class="box-body" style="padding: 0px;">

                            <div id="people"></div>

                        </div>
                        
                        <div class="overlay lock-feature">
                          <i class="fa fa-lock"></i>
                        </div>

                    </div>
                    <div class="box box-danger box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-secret"></i> Pemegang Saham</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div> 
                        <div class="box-body table-responsive" align="center"> 
                            
                            <div id="chartContainer"></div>

                        </div> 

                         <div class="overlay lock-feature">
                          <i class="fa fa-lock"></i>
                        </div>

                    </div>
                       
           
                </div>

            </div>

        </div>
       
    </form>
</div>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-address-book"></i> Asset Keuangan</h3>
                <div class="box-tools pull-right">
                    <a href="javascript:edit_finance_asset();" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="owl-carousel owl-loaded owl-drag">
                     <?php 
                                                                      
                      $year = date("Y")-2;    

                      for($i=$year;$i < date("Y") + 1;$i++){

                      ?>

                   <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          
                          <table class="table table-bordered" width="100%">
                              <thead>
                                  <tr>
                                    <?php echo "<th colspan='3'>Desember ".$i."</th>"; ?> 
                                  </tr>
                                  <tr>
                                      <th>Produk</th>
                                      <th>Nilai</th>
                                      <th>Jumlah</th>
                                  </tr>
                              </thead>
                              <tbody>

                                  <?php 

                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("1");

                                      foreach ($products as $a) {

                                          if($a['slug']=="kredit"){
                                              $label = "Debitur";
                                          }else if ($a['slug']=="tabungan") {
                                              $label = "Penabung";
                                          }else if ($a['slug']=="deposito") {
                                              $label = "Pendeposito";
                                          }
                                  
                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);
                                          echo "<tr>";
                                          echo "<td>".$a['name']."</td>";
                                          echo "<td> Rp. ".number_format($ast['value'],0,",",".")."</td>";
                                          echo "<td>".$ast['saving_account_count']." ".$label."</td>";       
                                          echo "</tr>";

                                      }

                                  ?>
                                  
                                  <tr>
                                      <td colspan="3">&nbsp;</td>
                                  </tr>

                                  <?php 

                                      $products = $this->Mbprfinanceassettype->getBPRFinanceAssetTypeByIsProduct("0");

                                      foreach ($products as $a) {
                                  
                                          $ast = $this->Mbprfinanceasset->getBPRFinanceAssetByAssetType($bpr['id'],$a['id'], $i);

                                        
                                          echo "<tr>";
                                          echo "<td>".$a['name']."</td>";
                                          echo "<td colspan='2'> Rp. ".number_format($ast['value'],0,",",".")."</td>"; 
                                          echo "</tr>";

                                      }

                                  ?>
                              </tbody>
                          </table>

                    </div>      

                      <?php

                      }

                      ?> 

                     
                   
                </div>
               
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>

        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-6">

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-university"></i> BPR <?php echo $bpr['name']; ?></h3>
                <div class="box-tools pull-right">

                    <a href="<?php echo base_url(); ?>master/bprprofile/edit" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <div class="box-body">
                
                <div class="col-md-12">

                    <table class="table">
                        <tr class="hidden-lg visible-md visible-sm visible-xs">
                            <td>
                                <div class="bpr-logo" style="text-align: center;">
                                    <?php

                                    if($bpr['logo']!=null){
                                        $path = "upload/bpr";
                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                        $file = $upload_path.'/'.$bpr['logo'];

                                        if(file_exists($file)){
                                            $logo = base_url().$path.'/'.$bpr['logo'];
                                        }else{
                                            $logo = base_url().'public/assets/img/bpr_default.png';
                                        }
                                    }else{
                                        $logo = base_url().'public/assets/img/bpr_default.png';
                                    }
                                       


                                    ?>
                                    <img src="<?php echo $logo; ?>" class="img-rounded" alt="<?php echo $bpr['corp']; ?> <?php echo $bpr['name']; ?>" style="width:150px;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="visible-lg hidden-md hidden-sm hidden-xs">
                                <div class="bpr-logo" style="text-align: center;">
                                    <?php

                                    if($bpr['logo']!=null){
                                        $path = "upload/bpr";
                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                        $file = $upload_path.'/'.$bpr['logo'];

                                        if(file_exists($file)){
                                            $logo = base_url().$path.'/'.$bpr['logo'];
                                        }else{
                                            $logo = base_url().'public/assets/img/bpr_default.png';
                                        }
                                    }else{
                                        $logo = base_url().'public/assets/img/bpr_default.png';
                                    }


                                    ?>
                                    <img src="<?php echo $logo; ?>" class="img-rounded" alt="<?php echo $bpr['corp']; ?> <?php echo $bpr['name']; ?>" style="width:150px;">
                                </div>
                            </td>
                            <td>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['corp']; ?> <?php echo $bpr['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>DPD</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['dpd_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['address']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Telp</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['telp']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Fax</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['fax']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Website</td>
                                            <td>:</td>
                                            <td><?php echo $bpr['website']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                 
                </div>
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>

        </div>

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-address-book"></i> Kontak Person</h3>
                <div class="box-tools pull-right">
                    <a href="<?php echo base_url(); ?>master/bprcontactperson/add" class="btn btn-warning btn-xs"><i class="fa fa-plus"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php  if(count($contact_person)>0){ ?>
                <table class="table table-striped table-bordered">
                    
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Telp</th>
                        </tr>
                    </thead>

                    <tbody>
                        
                        <?php
                       
                            foreach ($contact_person as $cp) {
                                
                                echo "<tr>";
                                echo "<td><a href='".base_url()."master/bprcontactperson/edit/".$cp['id']."'>".$cp['first_name']." ".$cp['last_name']."</a></td>";
                                echo "<td>".$cp['email']."</td>";
                                echo "<td>".$cp['mobile_phone']."</td>";
                                echo "</tr>";

                            }
                       
                        ?>

                    </tbody>

                </table>
                <?php  
                    }else{
                        echo '<div class="alert alert-warning text-center"><i class="fa fa-warning"></i> Belum ada data</div>';
                    }
                ?>
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>

        </div>

    </div>
    <div class="col-md-6">

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-map-marker"></i> Lokasi</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body" style="padding: 0px;">
                 <div id="map" style="width: 100%;height: 279px;"></div>
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>
        </div>

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-share-alt-square"></i> Cabang Bpr</h3>
                <div class="box-tools pull-right">
                    <a href="<?php echo base_url(); ?>master/bprbranch/add" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php  if(count($branches)>0){ ?>
                <table class="table table-striped table-bordered">
                    
                    <thead>
                        <tr>
                            <th style="width: 130px;">Map</th>
                            <th>Detail</th>
                        </tr>
                    </thead>

                    <tbody>
                        
                        <?php
                       
                            foreach ($branches as $bc) {
                                
                                echo "<tr>";
                                echo "<td style='padding: 0px;'> <div class='branch_map' style='width: 100%;height:100px;' id='map_".$bc['id']."' data-latitude='".$bc['latitude']."' data-longitude='".$bc['longitude']."'></div></td>";
                                echo "<td>
                                        <b><a href='".base_url()."master/bprbranch/edit/".$bc['id']."'>".$bc['name']."</a></b><br>
                                        <i class='fa fa-map-marker'></i> ".$bc['address']."<br>
                                        <i class='fa fa-phone'></i> ".$bc['telp']."</td>";
                                echo "</tr>";

                            }
                       
                        ?>

                    </tbody>

                </table>
                <?php  
                    }else{
                        echo '<div class="alert alert-warning text-center"><i class="fa fa-warning"></i> Belum ada data</div>';
                    }
                ?>
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>

        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary box-solid">

            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-area-chart"></i> Laporan GCG</h3>
                <div class="box-tools pull-right">
                     <a href="<?php echo base_url(); ?>master/bprfinancepublicationreport/add" class="btn btn-warning btn-xs"><i class="fa fa-upload"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <?php  if(count($report)>0){ ?>
                <table class="table table-striped table-bordered">
                    
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>File</th>
                            <!-- <th>Status</th> -->
                        </tr>
                    </thead>

                    <tbody>
                        
                        <?php
                       
                            foreach ($report as $rpt) {
                                
                                $ex = explode(".", $rpt['attachement']);
                                $extension = end($ex);
                                
                                if($extension!="zip"){

                                    $view_report = "<a href='".base_url()."viewer/report/".$rpt['attachement']."' target='_blank'>Lihat</a>";

                                }

                                echo "<tr>";
                                echo "<td><a href='".base_url()."master/bprfinancepublicationreport/edit/".$rpt['id']."'>".$rpt['name']."</td>";
                                echo "<td>".$rpt['file_report']."&nbsp;|&nbsp;".$view_report."</td>";
                                // echo "<td>".$rpt['status']."</td>";
                                echo "</tr>";

                            }
                       
                        ?>

                    </tbody>

                </table>
                    <?php 
                        $ret = '<br><center><a href="javascript:verify_pub_report();" class="btn btn-primary">Ajukan Verifikasi Laporan Publikasi</a></center><br>';
                        if(count($validation)>0){ 
                            if($validation['is_publication_report_verified']=='1'){
                                $ret = '';
                            }
                        }
                        print($ret);
                    ?>
                <?php  
                    }else{
                        echo '<div class="alert alert-warning text-center"><i class="fa fa-warning"></i> Belum ada data</div>';
                    }
                ?>
            </div>

            <div class="overlay lock-feature">
              <i class="fa fa-lock"></i>
            </div>
         </div>
    </div>
</div>

<div class="modal" id="edit_asset">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-money"></i> Edit Aset Keuangan</h4>
            </div>
            <div class="modal-body">
                
                <form id="frm_save_asset">
                    <table class="table table-striped table-bordered table-responsive" id="finance_asset_container">
                       
                        <tr>
                            <td>Tahun</td>
                            <td>:</td>
                            <td>
                                <select name="asset_active_year" class="form-control" id="asset_active_year">
                                    <?php for($i=1990;$i < (date("Y") + 1);$i++){ ?>
                                    <option value="<?php echo $i; ?>" <?php ($asset_active_year==$i)? $selected="selected":$selected="";echo $selected;?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <?php foreach($assettype as $at){ ?>
                        <tr>
                            <td><?php echo $at['name']; ?></td>
                            <td>:</td>
                            <td>
                                <input type="text" id="<?php echo $at['slug']; ?>" name="<?php echo $at['slug']; ?>" class="form-control validate[required, custom[number]]" placeholder="Nilai <?php echo $at['name']; ?>">
                            </td>
                            <?php if($at['is_product']=="1"){ ?>

                                <td>Jumlah Tabungan</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="sac_<?php echo $at['slug']; ?>" name="sac_<?php echo $at['slug']; ?>" class="form-control validate[custom[number]]" placeholder="<?php echo $at['name']; ?>">
                                </td>

                            <?php }else{ ?>

                                <td colspan="3"></td>

                            <?php } ?>

                        </tr>
                        <?php } ?>
                    </table>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary" id="btn-save-asset" onclick="save_asset()">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="add_stackholder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-user-secret"></i> Tambah Pemegang Saham</h4>
            </div>
            <div class="modal-body">
                
                <form id="frm_add_stackholder">
                    <input type="hidden" id="level" name="level">
                    <div class="form-group">
                        <label for="name">*) Nama</label>
                        <input type="text" name="name" class="form-control validate[required]" placeholder="Nama">
                    </div>

                    <div class="form-group">
                        <label for="percentage">*) Nilai Saham</label>
                        <input type="text" name="percentage" class="form-control validate[required,custom[number]]" placeholder="misal 10 (tanpa %)">
                    </div>
                  
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-save-stackholder" onclick="save_stackholder()">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="edit_stackholder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-user-secret"></i> Edit Pemegang Saham</h4>
            </div>
            <div class="modal-body">
                
                <form id="frm_update_stackholder">

                    <input type="hidden" name="level" id="stockholder_level">
                    <div class="form-group">
                        <label for="name">*) Nama</label>
                        <input type="text" name="name" id="stockholder_name" class="form-control validate[required]" placeholder="Nama">
                    </div>

                    <div class="form-group">
                        <label for="percentage">*) Nilai Saham</label>
                        <input type="text" name="percentage" id="stockholder_percentage" class="form-control validate[required,custom[number]]" placeholder="misal 10 (tanpa %)">
                    </div>
                  
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-stackholder">Save</button>
                <button type="button" class="btn btn-danger" id="btn-delete-stackholder">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="add_director">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mdtitle"><i class="fa fa-user-secret"></i> Tambah Direksi</h4>
            </div>
            <div class="modal-body">
                
                <form id="frm_add_director">
                    <input type="hidden" name="position_slug" id="position_slug">
                    <div class="form-group">

                        <label for="photo">Foto</label>
                        <div class="img-prev" id="photo_preview" style="max-width: 100%"><h1>Photo Preview</h1></div>
                        <br>
                        <input type="file" class="form-control" name="photo" id="photo">

                    </div>

                    <div class="form-group" id="extra_field">
                        <label for="name">*) Nama</label>
                        <input type="text" maxlength="100" name="name" class="form-control validate[required]" placeholder="Name (Required)">
                    </div>

                    <div class="form-group">
                        <label>*) Valid Sampai</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="valid_date" class="form-control pull-right" id="valid_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label for="gender">*) Jenis Kelamin</label>
                        <select class="form-control" name="gender">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">*) Email</label>
                        <input type="text" maxlength="150" name="email" class="form-control validate[required]" placeholder="Email (Required)">
                    </div>
                    <div class="form-group">
                        <label for="no_hp">*) No Hp</label>
                        <input type="text" maxlength="15" name="no_hp" class="form-control validate[required]" placeholder="Mobile Phone (Required)">
                    </div>
                   
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-save-director" onclick="save_director()">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="edit_director">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mdetitle"><i class="fa fa-user-secret"></i> Edit Direksi</h4>
            </div>
            <div class="modal-body">
                
                <form id="frm_update_director">

                    <input type="hidden" name="director_id" id="director_id">
                    <input type="hidden" name="old_director_photo" id="director_photo">
                    <input type="hidden" name="member_id" id="director_member_id">

                    <div class="form-group">

                        <label for="photo">Foto</label>
                        <div class="img-prev" id="director_photo_preview" style="max-width: 100%"><h1>Photo Preview</h1></div>
                        <br>
                        <input type="file" class="form-control" name="photo" id="director_photo_file">

                    </div>

                    <div class="form-group" id="extra_field2">
                        <label for="name">*) Nama</label>
                        <input type="text" maxlength="100" name="name" class="form-control validate[required]" id="director_name" placeholder="Name (Required)">
                    </div>    

                    <div class="form-group">
                        <label>*) Valid Sampai</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="valid_date" class="form-control pull-right" id="director_valid_date">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label for="gender">*) Jenis Kelamin</label>
                        <select class="form-control" name="gender" id="director_gender">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email">*) Email</label>
                        <input type="text" maxlength="150" name="email" class="form-control validate[required]" id="director_email" placeholder="Email (Required)">
                    </div>

                    <div class="form-group">
                        <label for="no_hp">*) No Hp</label>
                        <input type="text" maxlength="15" name="no_hp" class="form-control validate[required]" id="director_no_hp" placeholder="Mobile Phone (Required)">
                    </div>
                   
                    
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-director">Save</button>
                <button type="button" class="btn btn-danger" id="btn-delete-director">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mtitle"></h4>
            </div>
            <div class="modal-body">
                <p id="mbody"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn_cancel" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                <button type="button" class="btn btn-primary" id="btn_action"><?php echo $this->lang->line('yes'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places"></script>
<script type="text/javascript">
    $(document).ready(function(){

        unlockFeature();

        initFormValidation();

        initMap();

        initNumeral();

        initStockholderPieChart();

        initChartOrg();

        initHelper();
        
        get_stockholder_list("PSP","psp_stockholder");

        get_stockholder_list("REGULAR","regular_stockholder");

        get_director_list("komisaris", "komisaris");
        
        get_director_list("komisaris_utama", "komisaris_utama");

        get_director_list("dirut", "dirut_container");

        get_director_list("-", "dir_container");

        initAssetFinanceOnChange();

        $('.owl-carousel').owlCarousel({
            // loop:true,
            // margin:10,
            responsiveClass:true,
            // nav:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
      });

      $(".owl-prev").html("<i class='fa fa-angle-double-left fa-lg'></i>");
      $(".owl-next").html("<i class='fa fa-angle-double-right fa-lg'></i>");

        <?php
            $ret = "introJs().start();";
            if(count($validation)>0){ 

                // ini yang buat presentasi
                if($validation['is_verified']=='1'){
                    $ret = '';
                }

                // ini yang buat production
                /*if(count($director_position)>0){ 
                    // $ret = '';
                }*/
            }
            print($ret);
        ?>

    });

    function initAssetFinanceOnChange(){

        $("#asset_active_year").on("change",function(){

            var year = $(this).val();
            var target = base_url + 'master/bprprofile/get_finance_asset_by_year/'+year;

            $.get(target, function(res){

                $("#finance_asset_container").html(res);

                initAssetFinanceOnChange();

            });

        });


    }

    function unlockFeature(){

        $(".lock-feature").remove();
        <?php
            // $ret = "return false";
            // if(count($validation)>0){ 
            //     if($validation['is_verified']=='1'){
            //         $ret = '$(".lock-feature").remove()';
            //     }
            // }
            // print($ret);
        ?>
    }

    function showMap(latitude, longitude, selector) {

      var loc = {lat: parseFloat(latitude), lng: parseFloat(longitude)}

      var map = new google.maps.Map(document.getElementById(selector), {
          center: loc,
          zoom: 10
      });
      

      var infowindow = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({
          position: loc,
          map: map
      });

    }

    function initHelper(){

        $('#valid_date').daterangepicker();

        $("#photo").change(function(){
            readURL(this, "#photo");
        });

        $("#director_photo_file").change(function(){
            readURL(this, "#director_photo");
        });

    }

    function initStockholderPieChart(){

        $.get(base_url+'master/bprprofile/get_stockholder',function(data){

            if(data.length > 0){

                $("#container").attr("style","height: 400px; width: 100%;");

                 var plot1 = jQuery.jqplot ('chartContainer', [data], 
                    { 
                      seriesDefaults: {
                       
                        renderer: jQuery.jqplot.PieRenderer, 
                        rendererOptions: {
                         
                          showDataLabels: true
                        }
                      }, 
                      legend: { show:true, location: 'e' }
                    }
                  );
             }else{
                $("#chartContainer").html('<div class="alert alert-warning text-center"><i class="fa fa-warning"></i> Belum ada data</div>');
             }

        }, 'json');

    }

    function initNumeral(){

        numeral.register('locale', 'id', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'k',
                million: 'm',
                billion: 'b',
                trillion: 't'
            },
            ordinal : function (number) {
                return number === 1 ? 'er' : 'ème';
            },
            currency: {
                symbol: 'Rp. '
            }
        });

        numeral.locale('id');

    }

    function initChartOrg(){

        $.get(base_url+'master/bprprofile/getOrgChartData',function(data){

            if(data.sources.length>0){
                var orgchart = new getOrgChart(document.getElementById("people"),{  
                    color: "orange",
                    theme: "tal",
                    enablePrint: true,
                    enableExportToImage: true,
                    enableSearch: false,
                    primaryFields: data.primaryFields,
                    enableEdit: false,                
                    photoFields: ["pic"],
                    dataSource: data.sources,
                   
                });
            }else{
                 $("#people").html('<div class="alert alert-warning text-center" style="margin: 10px;"><i class="fa fa-warning"></i> Belum ada data</div>');
            }
            

        },'json');

    }

    function initFormValidation(){

        $("#frm_add_stackholder").validationEngine();
        $("#frm_add_director").validationEngine();

    }

    function initMap() {

        var loc = {lat: parseFloat("<?php echo $bpr['latitude']; ?>"), lng: parseFloat("<?php echo $bpr['longitude']; ?>")}

        var map = new google.maps.Map(document.getElementById('map'), {
            center: loc,
            zoom: 13
        });
        

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: loc,
            map: map
        });

    }

    function get_stockholder_list(level, container){

        $.get('<?php echo base_url(); ?>/master/bprprofile/get_stockholder_list/'+level,function(res){

            $("#"+container).html(res);

        });

    }

    function get_director_list(slug, container){

        $.get('<?php echo base_url(); ?>/master/bprprofile/get_director_list/'+slug,function(res){

            $("#"+container).html(res);

        });

    }

    function delete_stockholder(id,url,msg){

        $("#mtitle").html("Konfirmasi");
        $("#mbody").html(msg);
        $("#btn_action").attr("onclick","do_delete_stockholder('"+id+"','"+url+"')");
        $('#modal_confirm').modal({
            backdrop: false,
            show: true
        });

    }

    function verify_bpr(){

        resetDialog();

        $("#mtitle").html("Konfirmasi");
        $("#mbody").html("Pastikan semua data yang anda input telah benar. Anda yakin ingin mengajukan verifikasi?");
        $("#btn_action").attr("onclick","do_request_verification()");
        $('#modal_confirm').modal({
            backdrop: false,
            show: true
        });

    }

    function verify_pub_report(){

        resetDialog();

        $("#mtitle").html("Konfirmasi");
        $("#mbody").html("Pastikan semua data yang anda input telah benar. Anda yakin ingin mengajukan verifikasi?");
        $("#btn_action").attr("onclick","do_request_verification_pub_report()");
        $('#modal_confirm').modal({
            backdrop: false,
            show: true
        });

    }    

    function resetDialog(){

        $("#btn_action").html("Ya");
        $("#btn_cancel").html("Tidak");
        $("#btn_action").show();
        $("#btn_cancel").show();

    }

    function delete_director(id,member_id,url,msg){

        $("#mtitle").html("Konfirmasi");
        $("#mbody").html(msg);
        $("#btn_action").attr("onclick","do_delete_director('"+id+"','"+member_id+"','"+url+"')");
        $('#modal_confirm').modal({
            backdrop: false,
            show: true
        });

    }

    function do_request_verification(){

        $("#btn_action").button("loading");

        $.get(base_url+'master/bprprofile/request_verification',function(res){

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
                $("#btn_action").button("reset");
                $('#modal_confirm').modal('hide');
            }else{

                var err_msg = res.error;
                var err = '<ul>';

                for(var i =0;i < err_msg.length;i++){

                    err+='<li>'+err_msg[i]+'</li>';

                }

                err+='</ul>';

                $("#btn_action").button("reset");

                $("#mtitle").html("Request Gagal");
                $("#mbody").html(err);
                $("#btn_action").hide();
                $("#btn_cancel").html("Ok");
                $('#modal_confirm').modal({
                    backdrop: false,
                    show: true
                });

                toastr.error(res.msg, 'Response Server');

                $("#box-org").effect( "shake", {times:4}, 1000 );
            }

        },'json');

    }

    function do_request_verification_pub_report(){

        $("#btn_action").button("loading");

        $.get(base_url+'master/bprprofile/request_verification_pub_report',function(res){

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
                $("#btn_action").button("reset");
                $('#modal_confirm').modal('hide');
            }else{

                var err_msg = res.error;
                var err = '<ul>';

                for(var i =0;i < err_msg.length;i++){

                    err+='<li>'+err_msg[i]+'</li>';

                }

                err+='</ul>';

                $("#btn_action").button("reset");

                $("#mtitle").html("Request Gagal");
                $("#mbody").html(err);
                $("#btn_action").hide();
                $("#btn_cancel").html("Ok");
                $('#modal_confirm').modal({
                    backdrop: false,
                    show: true
                });

                toastr.error(res.msg, 'Response Server');

                $("#box-org").effect( "shake", {times:4}, 1000 );
            }

        },'json');

    }

    function do_delete_stockholder(id,url){

        var data = { id : id }

        $.post(url,data, function(res){

            if(res.status=="1"){

                get_stockholder_list("PSP","psp_stockholder");
                get_stockholder_list("REGULAR","regular_stockholder"); 
                toastr.success(res.msg, 'Response Server');
                $('#edit_stackholder').modal('hide');
                $('#modal_confirm').modal('hide');

            }else{

            }

        },'json');

        
    }
    function do_delete_director(id,member_id,url){

        var data = { id : id, member_id : member_id }

        $.post(url,data, function(res){

            if(res.status=="1"){

                get_director_list("komisaris", "komisaris_position");
                get_director_list("dirut", "dirut_container");
                get_director_list("-", "dir_container");

                initChartOrg();

                toastr.success(res.msg, 'Response Server');
                $('#edit_director').modal('hide');
                $('#modal_confirm').modal('hide');

            }else{

            }

        },'json');

        
    }

    function edit_finance_asset(){

        $.get('<?php echo base_url(); ?>/master/bprprofile/get_finance_asset/',function(res){

            if(res.status=="1"){

                if(res.data.length > 0){

                    for(var i=0; i < res.data.length; i++){

                        var selector = res.data[i].type;
                        var value = res.data[i].value;
                        var saving_account_count = res.data[i].saving_account_count;

                        $("#"+selector).val(value);
                        $("#sac_"+selector).val(saving_account_count);
                        

                    }

                }

                $('#edit_asset').modal({
                    backdrop: false,
                    show: true
                });

            }

        },'json');

    }

    function edit_stockholder(id){

        $.get('<?php echo base_url(); ?>/master/bprprofile/edit_stockholder/'+id,function(res){

            if(res.status=="1"){

                var delete_url = '<?php echo base_url(); ?>/master/bprstackholder/remove';
                var msg = 'Hapus data ini?';

                $("#stockholder_level").val(res.data.level);
                $("#stockholder_name").val(res.data.name);
                $("#stockholder_percentage").val(res.data.percentage);
                $("#btn-update-stackholder").attr("onclick","update_stackholder("+res.data.id+");");
                $("#btn-delete-stackholder").attr("onclick","delete_stockholder("+res.data.id+",'"+delete_url+"','"+msg+"');");

                $('#edit_stackholder').modal({
                    backdrop: false,
                    show: true
                });

            }

        },'json');

    }

    function edit_director(id){

        $.get('<?php echo base_url(); ?>/master/bprprofile/edit_director/'+id,function(res){

            if(res.status=="1"){

                var delete_url = '<?php echo base_url(); ?>/master/bprprofile/remove_director';
                var msg = 'Hapus data ini?';

                $('#director_valid_date').daterangepicker({
                    locale: {
                        format: 'YYYY/MM/DD'
                    },
                    startDate: res.data.valid_start_date,
                    endDate: res.data.valid_end_date
                });

                $("#director_id").val(res.data.id);
                $("#director_name").val(res.data.name);
                $("#director_email").val(res.data.email);
                $("#director_gender").html(res.gender);
                $("#director_no_hp").val(res.data.no_hp);
                $("#director_photo").val(res.data.photo);
                $("#director_member_id").val(res.data.member_id);
                
                if($("#director_position_title2").length < 1){

                    var input = '<div class="form-group"><label for="name">*) Bidang</label><input type="text" maxlength="100" name="position_title"  class="form-control" id="director_position_title2" placeholder="misal: Pemasaran" value="'+res.data.position_title+'"></div><div class="form-group"><label for="name">*) Label</label><input type="text" maxlength="100" name="label"  value="'+res.data.position_name+'" class="form-control" placeholder="misal: Direktur I"></div><input type="hidden" name="position_parent_slug" value="dirut">';

                    $( input ).insertAfter( "#extra_field2" );

                }

               

                if(res.photo != ""){

                    $("#director_photo_preview").html("<img src='"+res.photo+"' class='img-responsive img-pop-prev' />");

                }

                $("#mdetitle").html('<i class="fa fa-user-secret"></i> Edit '+res.data.position_name);
                
                $("#btn-update-director").attr("onclick","update_director();");
                $("#btn-delete-director").attr("onclick","delete_director("+res.data.id+","+res.data.member_id+",'"+delete_url+"','"+msg+"');");

                $('#edit_director').modal({
                    backdrop: false,
                    show: true
                });

            }

        },'json');

    }

    function add_stackholder(level){

        $("#level").val(level);

        $('#add_stackholder').modal({
            backdrop: false,
            show: true
        });

    }

    function add_director(slug){

        if(slug != "dirut" && slug != "komisaris" && slug != "komisaris_utama"){

            if($("#director_position_title").length < 1){

                var input = '<div class="form-group"><label for="name">*) Bidang</label><input type="text" maxlength="100" name="position_title"  class="form-control" id="director_position_title" placeholder="misal: Pemasaran"></div><div class="form-group"><label for="name">*) Label</label><input type="text" maxlength="100" name="label"  class="form-control" placeholder="misal: Direktur I"></div><input type="hidden" name="position_parent_slug" value="dirut">';
                $( input ).insertAfter( "#extra_field" );

            }
           
        }

        if(slug=="dirut"){
            var title = "Direktur Utama";
        }else if(slug=="komisaris"){
            var title = "Komisaris";
        }else if(slug=="komisaris_utama"){
            var title = "Komisaris Utama";
        }else{
            var title = "Direktur";
        }

        $("#mdtitle").html('<i class="fa fa-user-secret"></i> Tambah '+title);
        $("#position_slug").val(slug);

        $('#add_director').modal({
            backdrop: false,
            show: true
        });

    }

    function save_stackholder(){

        var form = "frm_add_stackholder";
        var btn_submit = "btn-save-stackholder";
        var btn_cancel = "btn-cancel";
        var data = $("#"+form).serialize();
        var target = '<?php echo base_url(); ?>master/bprstackholder/add';


        if($("#"+form).validationEngine('validate')){

            $("#"+btn_submit).button("loading");
            $("."+btn_cancel).attr("disabled", "disabled");
            disableForm(form);

            toastr.info('Saving data.. Please wait', 'Response Server');

            $.post(target, data, function(res){

                if(res.status=="1"){

                    get_stockholder_list("PSP","psp_stockholder");
                    get_stockholder_list("REGULAR","regular_stockholder"); 
                    toastr.success(res.msg, 'Response Server');
                    formReset(form);


                }else{

                    toastr.error(res.msg, 'Response Server');
                
                }

                $("#"+btn_submit).button("reset");
                $("."+btn_cancel).removeAttr("disabled");
                enableForm(form);
                

            },'json');

        }

    }

    function save_director(){

        var form = "frm_add_director";
        var btn_submit = "btn-save-director";
        var btn_cancel = "btn-cancel";
        var target = '<?php echo base_url(); ?>master/bprprofile/add_director';
        var formData = new FormData($("#"+form)[0]);


        if($("#"+form).validationEngine('validate')){

            $("#"+btn_submit).button("loading");
            $("."+btn_cancel).attr("disabled", "disabled");
            disableForm(form);

            toastr.info('Saving data.. Please wait', 'Response Server');

            $.ajax({
                url: target,
                type: 'POST',
                data: formData,
                dataType: "json",
                async: true,
                xhr: function()
                {
                    var xhr = new window.XMLHttpRequest();
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                       
                        toastr.info("Uploading Image ... " + Math.round(percentComplete) + " %", 'Response Server');
                        
                        if(percentComplete==100){
                            toastr.info("File Uploaded!", 'Response Server');
                        }
                      }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with download progress
                        console.log(percentComplete);
                      }
                    }, false);
                    return xhr;
                },
                success: function (res) {
                    if(res.status=="1"){

                        get_director_list("komisaris", "komisaris_position");
                        get_director_list("dirut", "dirut_container");
                        get_director_list("-", "dir_container");

                        toastr.success(res.msg, 'Response Server');

                        resetFileUpload("#photo");
                        formReset(form);

                        initChartOrg();

                    }else{
                        toastr.error(res.msg, 'Response Server');
                    }

                    $("#"+btn_submit).button("reset");
                    $("."+btn_cancel).removeAttr("disabled");
                    enableForm(form);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

    }

    function save_asset(){

        var form = "frm_save_asset";
        var btn_submit = "btn-save-asset";
        var btn_cancel = "btn-cancel";
        
        var data = $("#"+form).serialize();
        var target = '<?php echo base_url(); ?>master/bprprofile/save_asset/';

        if($("#"+form).validationEngine('validate')){

            $("#"+btn_submit).button("loading");
            $("."+btn_cancel).attr("disabled", "disabled");
           
            disableForm(form);

            toastr.info('Saving data.. Please wait', 'Response Server');

            $.post(target, data, function(res){

                if(res.status=="1"){

                    toastr.success(res.msg, 'Response Server');

                    // if(res.data.length > 0){

                    //     for(var i=0; i < res.data.length; i++){

                    //         var selector = res.data[i].type;
                    //         var value = res.data[i].value;
                    //         var saving_account_count = res.data[i].saving_account_count;

                    //         $("#asset_"+selector).html(numeral(value).format("$0,0"));
                    //         $("#asset_sac_"+selector).html(saving_account_count);
                            

                    //     }

                    // }

                    setTimeout("window.location.reload();", 1000);

                }else{
                    toastr.error(res.msg, 'Response Server');
                }

                $("#"+btn_submit).button("reset");
                $("."+btn_cancel).removeAttr("disabled");
                enableForm(form);

            },'json');

        }

    }

    function update_stackholder(id){

        var form = "frm_update_stackholder";
        var btn_submit = "btn-update-stackholder";
        var btn_cancel = "btn-cancel";
        var btn_delete = "btn-delete-stackholder";
        var data = $("#"+form).serialize();
        var target = '<?php echo base_url(); ?>master/bprstackholder/edit/'+id;



        if($("#"+form).validationEngine('validate')){

            $("#"+btn_submit).button("loading");
            $("."+btn_cancel).attr("disabled", "disabled");
            $("#"+btn_delete).attr("disabled", "disabled");
            disableForm(form);

            toastr.info('Saving data.. Please wait', 'Response Server');

            $.post(target, data, function(res){

                if(res.status=="1"){

                    get_stockholder_list("PSP","psp_stockholder");
                    get_stockholder_list("REGULAR","regular_stockholder"); 

                    toastr.success(res.msg, 'Response Server');

                }else{
                    toastr.error(res.msg, 'Response Server');
                }

                $("#"+btn_submit).button("reset");
                $("."+btn_cancel).removeAttr("disabled");
                $("#"+btn_delete).removeAttr("disabled");
                enableForm(form);

            },'json');

        }

    }

    function update_director(){

        var form = "frm_update_director";
        var btn_submit = "btn-update-director";
        var btn_cancel = "btn-cancel";
        var btn_delete = "btn-delete-director";
        var target = '<?php echo base_url(); ?>master/bprprofile/update_director';
        var formData = new FormData($("#"+form)[0]);


        if($("#"+form).validationEngine('validate')){

            $("#"+btn_submit).button("loading");
            $("."+btn_cancel).attr("disabled", "disabled");
            $("#"+btn_delete).attr("disabled", "disabled");
            disableForm(form);

            toastr.info('Saving data.. Please wait', 'Response Server');

            $.ajax({
                url: target,
                type: 'POST',
                data: formData,
                dataType: "json",
                async: true,
                xhr: function()
                {
                    var xhr = new window.XMLHttpRequest();
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                       
                        toastr.info("Uploading Image ... " + Math.round(percentComplete) + " %", 'Response Server');
                        
                        if(percentComplete==100){
                            toastr.info("File Uploaded!", 'Response Server');
                        }
                      }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with download progress
                        console.log(percentComplete);
                      }
                    }, false);
                    return xhr;
                },
                success: function (res) {
                    if(res.status=="1"){

                        get_director_list("komisaris", "komisaris_position");
                        get_director_list("dirut", "dirut_container");
                        get_director_list("-", "dir_container");

                        initChartOrg();

                        toastr.success(res.msg, 'Response Server');

                    }else{
                        toastr.error(res.msg, 'Response Server');
                    }

                    $("#"+btn_submit).button("reset");
                    $("."+btn_cancel).removeAttr("disabled");
                    $("#"+btn_delete).removeAttr("disabled");
                    enableForm(form);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

    }

    function formReset(id){

        $('#'+id)[0].reset();

    }

    function disableForm(id){

        $('#'+id).find('input, textarea, select').attr('disabled','disabled');

    }

    function enableForm(id){

        $('#'+id).find('input, textarea, select').removeAttr('disabled','disabled');

    }

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive img-pop-prev"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Photo Preview</h1>");
        $(selector).val('');

    }

</script>

<?php

    foreach ($branches as $bc) {
        
       echo '<script type="text/javascript">
                showMap("'.$bc['latitude'].'","'.$bc['longitude'].'","map_'.$bc['id'].'");
            </script>';

    }

?>
