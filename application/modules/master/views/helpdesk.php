<div class="row">
<div class="col-md-6">
<div class="box box-primary box-solid">
    <div class="box-header">
        <h3 class="box-title">Pusat Bantuan</h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <div class="form-group">
            <label for="subject">Subject</label>
            <select class="form-control" name="subject" id="subject">
               <option value="Aplikasi Error">Aplikasi Error</option>
               <option value="Cara Penggunaan">Cara Penggunaan</option>
               <option value="Fungsi Menu">Fungsi Menu / Fitur</option>
               <option value="Lainnya">Lainnya</option>
            </select>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" class="form-control validate[required]" id="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="message">Detail Pesan</label>
            <textarea name="message" class="form-control validate[required]" id="message" placeholder="Detail Pesan"></textarea>
        </div>
        <div class="form-group">
            <label for="attachement">Attachement (.zip)</label>
            <input type="file" class="form-control" name="attachement" id="attachement" accept=".zip">
        </div>

        <div class="col-md-12 recaptcha-wrapper"><?php echo $this->recaptcha->render(); ?></div>

        <div style="display:none;" id="upload-progress">
            <div class="progress progress-sm active">
                <div id="progressbar" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">         
                </div>
            </div>
        </div>

    </div>
    <div class="box-footer">
        &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
        &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
    </div>
    </form>
    <!-- /.box-body -->
</div>
</div>
<div class="col-md-6">
<div class="box box-primary box-solid">
    <div class="box-header">
        <h3 class="box-title">Keterangan</h3>
    </div>
    <!-- /.box-header -->
   
    <div class="box-body">
        <p>Help Desk membantu anda untuk mengirim pesan ke developer perihal error aplikasi, cara penggunaan aplikasi, maupun menanyakan seputar fungsi fitur yang ada di SIP.</p>

        <ul>
            <li>
                <h4>Subject</h4>
                <p>Subject dari tiket yang akan dikirim. Seputar error aplikasi, cara penggunaan, fungsi dari modul SIP, maupun yang lainnya tentang SIP.</p>
            </li>
            <li>
                <h4>Email (wajib diisi)</h4>
                <p>Email tujuan untuk response yang akan kami kirim. Silahkan isi dengan email yang valid, agar kami bisa merespon ke email tersebut.</p>
            </li>
            <li>
                <h4>Pesan (wajib diisi)</h4>
                <p>Detail pesan yang ingin anda sampaikan.</p>
            </li>
            <li>
                <h4>Attachement (optional)</h4>
                <p>Attachement berisi gambar screenshot dari error aplikasi dalam bentuk hasil kompres ke format .zip</p>
            </li>
        </ul>
    </div>
   
    <!-- /.box-body -->
</div>
</div>
</div>

<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line('modal_delete_title'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo $this->lang->line('modal_delete_body'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('no'); ?></button>
                <button type="button" class="btn btn-primary" id="btn_action"><?php echo $this->lang->line('yes'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    

    $(document).ready(function(){

     $("#form").submit(function(){

        var target = base_url + 'master/helpdesk/sendTiket';
        var formData = new FormData($("#form")[0]);

        $.ajax({
            url: target,
            type: "POST",
            data: formData,
            dataType: "json",
            async: true,
            xhr: function()
              {
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress

                    $("#upload-progress").show();
                    $("#progressbar").attr("aria-valuenow", Math.round(percentComplete));
                    $("#progressbar").attr("style", "width:"+Math.round(percentComplete)+"%");

                  }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    // console.log(percentComplete);
                  }
                }, false);
                return xhr;
            },
            success: function (res) {

                // console.log(res);
                hideLoading();
                
                $("#upload-progress").hide();

                if(res.status=="1"){
                    
                    resetForm();
                    resetFileUpload("#attachement");
                    
                    toastr.success(res.msg, "Response Server");
                
                }else{
                
                    toastr.error(res.msg, "Response Server");
                
                }

            },
            error: function(xhr, textStatus, error){
                 
                  alert(textStatus + " : " + xhr.statusText);
                 
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;

     });

    });

    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();
        $("#btn-submit").button("reset");

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();
        $("#cover_preview").html('<h1>Cover Module</h1>');
        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }

</script>