<div class="row" id="form_wrapper">
    <form role="form" id="form">


        <div class="col-md-6">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $this->lang->line('corporate_add'); ?></h3>
                </div>
             

                <div class="box-body">
                    
                    <div class="form-group">
                        <label for="name"><?php echo $this->lang->line('name'); ?></label>
                        <input type="text" name="name" required class="form-control validate[required]" id="name" placeholder="<?php echo $this->lang->line('name'); ?>">
                    </div>

                    <div class="form-group">
                        <label for="description"><?php echo $this->lang->line('description'); ?></label>
                        <textarea name="description" class="form-control" id="description" placeholder="<?php echo $this->lang->line('description'); ?>"></textarea>
                    </div>
                </div>
        

                <div class="box-footer">
                    &nbsp;<button type="submit" id="btn-submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default"><?php echo $this->lang->line('back'); ?></button>
                </div>

            </div>


        </div>
        <div class="col-md-6">

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                toastr.info("Saving data...", 'Loading');
                $("#btn-submit").button("loading");
                saveFormData();

                return false;

            }

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"master/corporate/addnew";
        var data = $("#form").serialize();

        $.post(target, data, function(res){

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            $("#btn-submit").button("reset");
            resetForm();

        },'json');

    }

    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>