<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2><?php echo $this->lang->line('saving'); ?></h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Slider</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name"><?php echo $this->lang->line('name'); ?></label>
                        <input type="text" name="name" maxlength="100" class="form-control validate[required]" id="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" placeholder="Short Description"></textarea> 
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="cover">Picture</label>
                            <input type="file" class="form-control validate[required]" name="cover" id="cover">
                            <div class="img-prev" reqired id="cover_preview" style="display:none;"><h1>Picture Preview</h1></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="action_url">Action URL</label>
                        <input type="text" name="action_url" maxlength="100" class="form-control validate[required]" id="action_url" placeholder="http://domain.com">
                    </div>
                    <div class="form-group">
                        <label for="sequence">Sequence</label>
                        <input type="number" name="sequence" class="form-control validate[required,custom[number]]" id="sequence" placeholder="Sequence)" value="1">
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0">In Active</option>
                            <option value="1">Active</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default"><?php echo $this->lang->line('back'); ?></button>
                </div>

            </div>


        </div>
        <div class="col-md-6">

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#cover").change(function(){
            readURL(this, "#cover");
        });

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){



                $("#validate_content").hide();

                showLoading();

                setTimeout('saveFormData();',3000);


            }

            return false;

        });

        $("#form").validationEngine();

        //Date range picker
        $('#event_date').daterangepicker();

    });

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Cover Module</h1>");
        $(selector).val('');

    }

    function saveFormData(){

        var target = base_url+"master/slider/addnew";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }

                resetForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){


        $('#form')[0].reset();
        $("#cover_preview").html('<h1>Cover Module</h1>');
        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].setData('');

        }

    }
</script>