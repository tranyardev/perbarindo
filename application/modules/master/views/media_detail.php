<div class="box">
    <div class="box-header">
        <h3 class="box-title">Media Detail</h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">

                <?php

                    if($media_detail['type']=="image"){

                        $viewer_detail = base_url().$media_detail['path'].$media_detail['file'];

                        $thumb = base_url().$media_detail['path'].$media_detail['file'];

                    }else if($media_detail['type']=="document"){

                        $title = $this->MMedia->getDocumentReference($media_detail['object_id']);

                        $viewer_detail = base_url()."viewer/report/".$media_detail['file']; 

                        switch ($media_detail['format']) {
                            case 'pdf':
                                
                                $icon = 'pdf_icon.png';
                                break;

                            case 'csv':
                                
                                $icon = 'excel_icon.png';
                                break;

                            case 'xls':
                                
                                $icon = 'excel_icon.png';
                                break;

                            case 'xlsx':
                                
                                $icon = 'excel_icon.png';
                                break;

                            case 'doc':
                                
                                $icon = 'word_icon.png';
                                break;

                            case 'docx':
                                
                                $icon = 'word_icon.png';
                                break;

                            case 'ppt':
                                
                                $icon = 'powerpoint_icon.png';
                                break;

                            case 'pptx':
                                
                                $icon = 'powerpoint_icon.png';
                                break;
                            
                        }

                        $thumb = base_url().'assets/dist/img/'.$icon;

                    }

                ?>
      
                <a href="<?php echo base_url().'master/media/detail/'.$media_detail['media_id'] ?>" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="<?php echo $thumb; ?>" alt="">
                </a>
                            
            </div>
            <div class="col-md-8">
                <table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td>File</td>
                        <td>:</td>
                        <td><?php echo $media_detail['file']; ?></td>
                    </tr>
                    <tr>
                        <td>Link</td>
                        <td>:</td>
                        <td><a href='<?php echo  base_url().$media_detail['path'].$media_detail['file'] ?>' target='_blank'><?php echo  base_url().$media_detail['path'].$media_detail['file'] ?></a></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>:</td>
                        <td><?php echo $media_detail['type']; ?></td>
                    </tr>
                    <tr>
                        <td>Format</td>
                        <td>:</td>
                        <td><?php echo $media_detail['format']; ?></td>
                    </tr>
                    <tr>
                        <td>Module Reference</td>
                        <td>:</td>
                        <td><?php echo $media_detail['module']; ?></td>
                    </tr>
                    <tr>
                        <td>Link Reference</td>
                        <td>:</td>
                        <td>
                            <?php 

                                if($media_detail['module']=="slider"){
                                    echo "<a href='".base_url()."master/slider/edit/".$media_detail['object_id']."'>".base_url()."master/slider/edit/".$media_detail['object_id']."</a>";
                                }else if($media_detail['module']=="gcg report"){

                                     echo "<a href='".base_url()."master/bprfinancepublicationreport/edit/".$media_detail['object_id']."'>".base_url()."master/bprfinancepublicationreport/edit/".$media_detail['object_id']."</a>";

                                }else if($media_detail['module']=="event"){

                                     echo "<a href='".base_url()."event/event/edit/".$media_detail['object_id']."'>".base_url()."event/event/edit/".$media_detail['object_id']."</a>";
                                }else if($media_detail['module']=="member"){

                                     echo "<a href='".base_url()."member/member/edit/".$media_detail['object_id']."'>".base_url()."member/member/edit/".$media_detail['object_id']."</a>";

                                }else if($media_detail['module']=="user"){

                                     echo "<a href='".base_url()."user/user/edit/".$media_detail['object_id']."'>".base_url()."user/user/edit/".$media_detail['object_id']."</a>";

                                }

                            ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>Created At</td>
                        <td>:</td>
                        <td><?php echo $media_detail['created_at']; ?></td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>:</td>
                        <td><?php echo $media_detail['updated_at']; ?></td>
                    </tr>
                </table>
                <div class="btn-action">
                    <a href="<?php echo  base_url().'master/media/download/'.$media_detail['media_id'] ?>" class="btn btn-primary">Download</a>
                    <a href="<?php echo  $viewer_detail; ?>" class="btn btn-success" target='_blank'>Show Detail</a>
                    <a href="<?php echo  base_url() ?>master/media" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!-- /.box-body -->
</div>