<?php

class Mdbpr extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->load->model('Mdpk');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_delete');
        $this->allow_access_detail = $this->mcore->checkPermission($this->user_group, 'bpr_detail');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $detail = '';
        $str = array(

                "bpr" => "a.name",
                "corporate" => "c.name",
                "dpd" => "b.name",
                "ver_director" => "(select count(*) from directors where directors.bpr=a.id and is_valid='0')",
                "ver_stockholder" => "(select count(*) from stockholders where stockholders.bpr=a.id and is_valid='0')",

            );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($this->allow_access_detail){
            $detail = '<a class="btn btn-sm btn-warning" href="javascript:detail(\',a.id,\');"><i class="fa fa-eye"></i></a>&nbsp;';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$detail.$edit.$delete."')";
            $str = array(

                "op" => $op,
                "bpr" => "a.name",
                "corporate" => "c.name",
                "dpd" => "b.name",
                "ver_director" => "(select count(*) from directors where directors.bpr=a.id and is_valid='0')",
                "ver_stockholder" => "(select count(*) from stockholders where stockholders.bpr=a.id and is_valid='0')",

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr a";
    }

    public function joinArray(){
        return array(
             "dpd b|left" => "b.id=a.dpd",
             "corporates c|left" => "c.id=a.corporate",
        );
    }

    public function whereClauseArray(){

        if($this->user_group == "Admin DPD"){

            return array(

                "a.dpd" => $this->session->userdata('dpd')

            );    

        }else if($this->user_group == "Admin DPK"){


            // $dpd = $this->Mdpk->getDPDByDPK($this->session->userdata('dpk'));

            // return array(

            //     "a.dpd" => $dpd[0]['id']

            // ); 

        }else{

            return null;
                 
        }
       
    }


}