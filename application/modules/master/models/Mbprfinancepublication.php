<?php

class Mbprfinancepublication extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRFinancePublication($bpr){

        $result = $this->db->query("SELECT *, concat('<a href=\"".base_url()."upload/media/',attachement,'\" target=\"_blank\">Download</a>') as file_report FROM bpr_finance_publication_reports WHERE bpr='".$bpr."'")->result_array();
        return $result;

    }
    function getBPRFinancePublicationById($id){

        $result = $this->db->query("SELECT * FROM bpr_finance_publication_reports WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}