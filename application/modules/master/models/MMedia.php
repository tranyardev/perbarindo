<?php

class MMedia extends CI_Model
{
	function __construct(){
        
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getMediaById($id){

        $result = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->result_array();
        return @$result[0];

    }
    function getMedia(){

        $result = $this->db->query("SELECT * FROM media")->result_array();
        return $result;

    }
    function getMediaByType($type){

        $result = $this->db->query("SELECT * FROM media WHERE type='".$type."'")->result_array();
        return $result;

    }
    function getMediaType(){

        $result = $this->db->query("SELECT DISTINCT ON (type) type FROM media ORDER BY type ASC")->result_array();
        return $result;


    }
    function getDocumentReference($id){

        $result = $this->db->query("SELECT b.* FROM media a LEFT JOIN bpr_finance_publication_reports b ON b.id=a.object_id WHERE a.object_id='".$id."'")->result_array();
        return @$result[0];

    }
   
 
}