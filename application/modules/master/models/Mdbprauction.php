<?php

class Mdbprauction extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'bpr_auction_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'bpr_auction_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

                 "status" => "CASE WHEN a.status='1' THEN 'Terjual' ELSE 'Belum Terjual' END",

            );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op,
                 "status" => "CASE WHEN a.status='1' THEN 'Terjual' ELSE 'Belum Terjual' END",

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "bpr_auctions a";
    }

    public function joinArray(){
        return array(
            "bpr_auction_categories b|left" => "b.id=a.auction_category"
        );
    }

    public function whereClauseArray(){
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(
            "a.bpr" => $member['bpr']
        );
    }


}