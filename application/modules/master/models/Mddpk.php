<?php

class Mddpk extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'dpk_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'dpk_delete');
        $this->cl = $this->session->userdata('client');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(
            "dpd" => "b.name"
        );
        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op,
                "dpd" => "b.name"

            );

        }

        return $str;
    }

    public function fromTableStr() {
        return "dpk a";
    }

    public function joinArray(){
        return array(
            "dpd b|left" => "b.id=a.dpd"
        );
    }

    public function whereClauseArray(){
        
         if($this->user_group == "Admin DPD"){

            return array(

                "a.dpd" => $this->session->userdata('dpd')

            );    

        }else{

            return null;
                 
        }
       
    }


}