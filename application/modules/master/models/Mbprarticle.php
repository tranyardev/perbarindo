<?php

class Mbprarticle extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRArticle(){

        $result = $this->db->query("SELECT * FROM bpr_articles")->result_array();
        return $result;

    }
    function getCountBPRArticle($bpr){

        $result = $this->db->query("SELECT * FROM bpr_articles WHERE bpr='".$bpr."'")->num_rows();
        return $result;

    }
    function getBPRArticleById($id){

        $result = $this->db->query("SELECT * FROM bpr_articles WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}