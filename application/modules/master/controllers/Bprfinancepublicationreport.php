<?php

class Bprfinancepublicationreport extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbprfinancepublication");
        $this->load->model("Mbprvalidates");
        $this->load->model("Mbpr");
        $this->load->model("MConfiguration");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr_finance_publication_reports";
        $this->dttModel = "Mdbprfinancepublication";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Laporan Publikasi Keuangan";
        $data['page_subtitle'] = "Modul Laporan Publikasi Keuangan BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_report', 
            'submenu' => 'bpr_finance_publication_report' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_finance_publication_report_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Laporan Publikasi'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Laporan Publikasi</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Upload',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprfinancepublicationreport/add';
                }"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "report_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "report_year" => array(

                "data" => "a.year",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "report_attachement" => array(

                "data" => "$.attachement",
                "searchable" => false,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "name" => array(

                "label" => "Judul Laporan",
                "type" => "text",
                "placeholder" => "Nama/Judul Laporan",
                "class" => "form-control validate[required]",

            ),
            "year" => array(

                "label" => "Tahun",
                "type" => "text",
                "placeholder" => "Tahun",
                "class" => "form-control validate[required]",

            ),
            "attachement" => array(

                "label" => "File Lampiran",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/media/',
                "id" => "attachement",

            )


        );


    }

    public function add(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                    $_FILES["attachement"]["type"]=="application/vnd.ms-excel" ||
                    $_FILES["attachement"]["type"]=="application/excel" ||
                    $_FILES["attachement"]["type"]=="application/x-excel" ||
                    $_FILES["attachement"]["type"]=="application/x-msexcel" ||
                    $_FILES["attachement"]["type"]=="application/msword" ||
                    $_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                    $_FILES["attachement"]["type"]=="application/wps-office.xls" ||
                    $_FILES["attachement"]["type"]=="application/wps-office.xlsx" ||
                    $_FILES["attachement"]["type"]=="application/wps-office.doc" ||
                    $_FILES["attachement"]["type"]=="application/wps-office.docx" ||
                    $_FILES["attachement"]["type"]=="application/wps-office.ppt" ||
                    $_FILES["attachement"]["type"]=="text/csv" ||
                    $_FILES["attachement"]["type"]=="application/pdf" ||
                    $_FILES["attachement"]["type"]=="application/x-pdf"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                    $array = explode('.', $_FILES['attachement']['name']);
                    $extension = end($array);
                    $attachement = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["attachement"]["tmp_name"], $upload_path."/".$attachement)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    $_POST['attachement'] = $attachement;


                }else{


                    $res['msg'] = "Invalid attachement file! Should be XLS,XLSX,PDF,PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['data_return'] = true;
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $insert=$p1->process();

            if($insert){


                /* media */

                $media = array(

                    "file" => $attachement,
                    "path" => 'upload/media/',
                    "object_id" => $this->db->insert_id(),
                    "type" => "document",
                    "module" => "gcg report",
                    "format" => $extension,
                    "created_by" => $this->session->userdata('id'),
                    "created_at" => date("Y-m-d h:i:s"),
                    "bpr" => $member['bpr'],
                    "dpd" => $member['dpd']

                );

                $this->db->insert("media", $media);

                /* end media */


                /* sent notif */

                $_POST['id'] = $this->db->insert_id();
                $_POST['uploader'] = $member['member_name'];
                $_POST['bpr'] = $member['bpr'];
                $_POST['file'] = $attachement;

                $notif = $this->upload_success_notif($_POST);

                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = true;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $process = $p2->process();

                if($process){

                    echo json_encode(array("status" => "1", "msg" => "File berhasil diupload", "notif" => $notif));

                }else{

                    echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));
                
                }

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));
            }

        }else{

            $data['page_title'] = "Upload Laporan Publikasi";
            $data['page_subtitle'] = "Modul Laporan Publikasi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";
            $data['after_form'] = $this->_get_template_after_form();
            $data['columns_size'] = 2;
            $data['second_column'] = $this->_get_template_second_column();

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    function upload_success_notif($data){

        $bpr = $this->Mbpr->getBPRById($data['bpr']);

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Notif</title>
                <!-- Designed by https://github.com/kaytcat -->
                <!-- Header image designed by Freepik.com -->

                <style type="text/css">
                /* Take care of image borders and formatting */

                img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
                a img { border: none; }
                table { border-collapse: collapse !important; }
                #outlook a { padding:0; }
                .ReadMsgBody { width: 100%; }
                .ExternalClass {width:100%;}
                .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
                table td {border-collapse: collapse;}
                .ExternalClass * {line-height: 115%;}


                /* General styling */

                td {
                  font-family: Arial, sans-serif;
                  color: #5e5e5e;
                  font-size: 16px;
                  text-align: left;
                }

                body {
                  -webkit-font-smoothing:antialiased;
                  -webkit-text-size-adjust:none;
                  width: 100%;
                  height: 100%;
                  color: #5e5e5e;
                  font-weight: 400;
                  font-size: 16px;
                }


                h1 {
                  margin: 10px 0;
                }

                a {
                  color: #2b934f;
                  text-decoration: none;
                }


                .body-padding {
                  padding: 0 75px;
                }


                .force-full-width {
                  width: 100% !important;
                }

                .icons {
                  text-align: right;
                  padding-right: 30px;
                }

                .logo {
                  text-align: left;
                  padding-left: 30px;
                }

                .computer-image {
                  padding-left: 30px;
                }

                .header-text {
                  text-align: left;
                  padding-right: 30px;
                  padding-left: 20px;
                }

                .header {
                  color: #232925;
                  font-size: 24px;
                }

              ul{

                  list-style: none;

              }
              ul li ul{

                  list-style: none;

              }
              
              ul li{
                  padding: 10px;
                  border-bottom: dashed 1px #aaa;
                  border-left: dashed 1px #aaa;
              }


                </style>

                <style type="text/css" media="screen">
                    @media screen {
                      @import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
                      /* Thanks Outlook 2013! */
                      * {
                        font-family: "PT Sans", "Helvetica Neue", "Arial", "sans-serif" !important;
                      }
                    }
                </style>

              </head>
              <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
              <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                <tr>
                  <td align="center" valign="top" style="background-color:#ffffff" width="100%">

                  <center>
                    <table cellspacing="0" cellpadding="0" width="600" class="w320"  bgcolor="#232925">
                      <tr>
                        <td align="center" valign="top">

                          <table class="force-full-width" cellspacing="0" cellpadding="0" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925;padding-left: 30px;" class="logo">
                                <br>
                                <a href="#"><img src="http://perbarindo-event.tranyar.com/public/assets/img/logo.png" alt="Logo"></a>
                              </td>
                              <td class="icons">
                               
                              </td>
                            </tr>
                          </table>

                          <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td class="computer-image">
                                <br>
                                <br class="mobile-hide" />
                                <img style="display:block;" width="224" height="213" src="https://www.filepicker.io/api/file/CoMxXSlVRDuRQWNwnMzV" alt="hello">
                              </td>
                              <td style="color: #ffffff;" class="header-text">
                                <br>
                                <br>
                                <span style="font-size: 24px;">Hi Admin!</span><br>
                                Proses upload laporan GCG telah berhasil.
                                <br>
                                <br>
                                <a href="'.base_url().'viewer/report/'.$data['file'].'"
                                        style="background-color:#2b934f;color:#ffffff;display:inline-block;font-family:Helvetcia, sans-serif;font-size:16px;font-weight:light;line-height:40px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;width:100%;">Lihat</a>
                                <br>
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="30" bgcolor="#ebebeb">
                            <tr>
                              <td>
                                <table cellspacing="0" cellpadding="0" class="force-full-width">
                                  <tr>
                                    <td>
                                        <span style="color: #232925;font-size: 24px;">'.$bpr['corp']." " .$bpr['name'].'</span> 
                                        <br>
                                        '.@$bpr['address'].'<br>
                                        '.@$bpr['phone'].'<br>
                                        '.@$bpr['email'].'<br><br>
                                    </td>
                                    <td style="text-align:right; vertical-align:top;">
                                      
                                    </td>
                                  </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                  <tr>
                                    <td style="background-color:#dedede; padding: 5px; font-weight:bold; ">
                                      Laporan GCG
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="background-color:#f3f3f3; padding: 10px 5px;">
                                      <div class="col-md-12">
                                        <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                          <tr>
                                            <td style="padding: 5px;">Tanggal Upload</td>
                                            <td>:</td>
                                            <td>'.date("d-m-Y h:i:s").'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Judul</td>
                                            <td>:</td>
                                            <td>'.$data['name'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Tahun</td>
                                            <td>:</td>
                                            <td>'.$data['year'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Diupload Oleh</td>
                                            <td>:</td>
                                            <td>'.$data['uploader'].'</td>
                                          </tr>
                                        </table>
                                      </div>
                                    </td>
                                  <tr>
                                 
                                </table>
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="20" bgcolor="#2b934f" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925; color:#ffffff; font-size: 14px; text-align: center;">&copy; PERBARINDO 2017 All Rights Reserved
                              </td>
                            </tr>
                          </table>


                        </td>
                      </tr>
                    </table>

                  </center>
                  </td>
                </tr>
              </table>
              </body>
            </html>';

        $email = $bpr['email'];
        $subject = "PERBARINDO - NOTIFIKASI LAPORAN GCG";
        $from = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $company = "PERBARINDO";

        if($this->send_mail($email, $subject, $html, @$from["value"], $company)){
           
           return true;

        }else{
           
           return false;

        }


    }

    function update_success_notif($data){

        $bpr = $this->Mbpr->getBPRById($data['bpr']);

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Notif</title>
                <!-- Designed by https://github.com/kaytcat -->
                <!-- Header image designed by Freepik.com -->

                <style type="text/css">
                /* Take care of image borders and formatting */

                img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
                a img { border: none; }
                table { border-collapse: collapse !important; }
                #outlook a { padding:0; }
                .ReadMsgBody { width: 100%; }
                .ExternalClass {width:100%;}
                .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
                table td {border-collapse: collapse;}
                .ExternalClass * {line-height: 115%;}


                /* General styling */

                td {
                  font-family: Arial, sans-serif;
                  color: #5e5e5e;
                  font-size: 16px;
                  text-align: left;
                }

                body {
                  -webkit-font-smoothing:antialiased;
                  -webkit-text-size-adjust:none;
                  width: 100%;
                  height: 100%;
                  color: #5e5e5e;
                  font-weight: 400;
                  font-size: 16px;
                }


                h1 {
                  margin: 10px 0;
                }

                a {
                  color: #2b934f;
                  text-decoration: none;
                }


                .body-padding {
                  padding: 0 75px;
                }


                .force-full-width {
                  width: 100% !important;
                }

                .icons {
                  text-align: right;
                  padding-right: 30px;
                }

                .logo {
                  text-align: left;
                  padding-left: 30px;
                }

                .computer-image {
                  padding-left: 30px;
                }

                .header-text {
                  text-align: left;
                  padding-right: 30px;
                  padding-left: 20px;
                }

                .header {
                  color: #232925;
                  font-size: 24px;
                }

              ul{

                  list-style: none;

              }
              ul li ul{

                  list-style: none;

              }
              
              ul li{
                  padding: 10px;
                  border-bottom: dashed 1px #aaa;
                  border-left: dashed 1px #aaa;
              }


                </style>

                <style type="text/css" media="screen">
                    @media screen {
                      @import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
                      /* Thanks Outlook 2013! */
                      * {
                        font-family: "PT Sans", "Helvetica Neue", "Arial", "sans-serif" !important;
                      }
                    }
                </style>

              </head>
              <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
              <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                <tr>
                  <td align="center" valign="top" style="background-color:#ffffff" width="100%">

                  <center>
                    <table cellspacing="0" cellpadding="0" width="600" class="w320"  bgcolor="#232925">
                      <tr>
                        <td align="center" valign="top">

                          <table class="force-full-width" cellspacing="0" cellpadding="0" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925;padding-left: 30px;" class="logo">
                                <br>
                                <a href="#"><img src="http://perbarindo-event.tranyar.com/public/assets/img/logo.png" alt="Logo"></a>
                              </td>
                              <td class="icons">
                               
                              </td>
                            </tr>
                          </table>

                          <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td class="computer-image">
                                <br>
                                <br class="mobile-hide" />
                                <img style="display:block;" width="224" height="213" src="https://www.filepicker.io/api/file/CoMxXSlVRDuRQWNwnMzV" alt="hello">
                              </td>
                              <td style="color: #ffffff;" class="header-text">
                                <br>
                                <br>
                                <span style="font-size: 24px;">Hi Admin!</span><br>
                                Proses update laporan GCG telah berhasil.
                                <br>
                                <br>
                                <a href="'.base_url().'viewer/report/'.$data['file'].'"
                                        style="background-color:#2b934f;color:#ffffff;display:inline-block;font-family:Helvetcia, sans-serif;font-size:16px;font-weight:light;line-height:40px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;width:100%;">Lihat</a>
                                <br>
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="30" bgcolor="#ebebeb">
                            <tr>
                              <td>
                                <table cellspacing="0" cellpadding="0" class="force-full-width">
                                  <tr>
                                    <td>
                                       <span style="color: #232925;font-size: 24px;">'.$bpr['corp']." " .$bpr['name'].'</span> <br>
                                        '.$bpr['address'].'<br>
                                        '.$bpr['phone'].'<br>
                                        '.$bpr['email'].'<br><br>
                                    </td>
                                    <td style="text-align:right; vertical-align:top;">
                                      
                                    </td>
                                  </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                  <tr>
                                    <td style="background-color:#dedede; padding: 5px; font-weight:bold; ">
                                      Laporan GCG
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="background-color:#f3f3f3; padding: 10px 5px;">
                                      <div class="col-md-12">
                                        <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                          <tr>
                                            <td style="padding: 5px;">Tanggal Update</td>
                                            <td>:</td>
                                            <td>'.date("d-m-Y h:i:s").'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Judul</td>
                                            <td>:</td>
                                            <td>'.$data['name'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Tahun</td>
                                            <td>:</td>
                                            <td>'.$data['year'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Diupdate Oleh</td>
                                            <td>:</td>
                                            <td>'.$data['uploader'].'</td>
                                          </tr>
                                        </table>
                                      </div>
                                    </td>
                                  <tr>
                                 
                                </table>
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="20" bgcolor="#2b934f" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925; color:#ffffff; font-size: 14px; text-align: center;">&copy; PERBARINDO 2017 All Rights Reserved
                              </td>
                            </tr>
                          </table>


                        </td>
                      </tr>
                    </table>

                  </center>
                  </td>
                </tr>
              </table>
              </body>
            </html>';

        $email = $bpr['email'];
        $subject = "PERBARINDO - NOTIFIKASI LAPORAN GCG";
        $from = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $company = "PERBARINDO";

        if($this->send_mail($email, $subject, $html, @$from["value"], $company)){
           
           return true;

        }else{
           
           return false;

        }


    }

    function delete_success_notif($data){

        $bpr = $this->Mbpr->getBPRById($data['bpr']);

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Notif</title>
                <!-- Designed by https://github.com/kaytcat -->
                <!-- Header image designed by Freepik.com -->

                <style type="text/css">
                /* Take care of image borders and formatting */

                img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
                a img { border: none; }
                table { border-collapse: collapse !important; }
                #outlook a { padding:0; }
                .ReadMsgBody { width: 100%; }
                .ExternalClass {width:100%;}
                .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
                table td {border-collapse: collapse;}
                .ExternalClass * {line-height: 115%;}


                /* General styling */

                td {
                  font-family: Arial, sans-serif;
                  color: #5e5e5e;
                  font-size: 16px;
                  text-align: left;
                }

                body {
                  -webkit-font-smoothing:antialiased;
                  -webkit-text-size-adjust:none;
                  width: 100%;
                  height: 100%;
                  color: #5e5e5e;
                  font-weight: 400;
                  font-size: 16px;
                }


                h1 {
                  margin: 10px 0;
                }

                a {
                  color: #2b934f;
                  text-decoration: none;
                }


                .body-padding {
                  padding: 0 75px;
                }


                .force-full-width {
                  width: 100% !important;
                }

                .icons {
                  text-align: right;
                  padding-right: 30px;
                }

                .logo {
                  text-align: left;
                  padding-left: 30px;
                }

                .computer-image {
                  padding-left: 30px;
                }

                .header-text {
                  text-align: left;
                  padding-right: 30px;
                  padding-left: 20px;
                }

                .header {
                  color: #232925;
                  font-size: 24px;
                }

              ul{

                  list-style: none;

              }
              ul li ul{

                  list-style: none;

              }
              
              ul li{
                  padding: 10px;
                  border-bottom: dashed 1px #aaa;
                  border-left: dashed 1px #aaa;
              }


                </style>

                <style type="text/css" media="screen">
                    @media screen {
                      @import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
                      /* Thanks Outlook 2013! */
                      * {
                        font-family: "PT Sans", "Helvetica Neue", "Arial", "sans-serif" !important;
                      }
                    }
                </style>

              </head>
              <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
              <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                <tr>
                  <td align="center" valign="top" style="background-color:#ffffff" width="100%">

                  <center>
                    <table cellspacing="0" cellpadding="0" width="600" class="w320"  bgcolor="#232925">
                      <tr>
                        <td align="center" valign="top">

                          <table class="force-full-width" cellspacing="0" cellpadding="0" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925;padding-left: 30px;" class="logo">
                                <br>
                                <a href="#"><img src="http://perbarindo-event.tranyar.com/public/assets/img/logo.png" alt="Logo"></a>
                              </td>
                              <td class="icons">
                               
                              </td>
                            </tr>
                          </table>

                          <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#232925" style="width: 100%;">
                            <tr>
                              <td class="computer-image">
                                <br>
                                <br class="mobile-hide" />
                                <img style="display:block;" width="224" height="213" src="https://www.filepicker.io/api/file/CoMxXSlVRDuRQWNwnMzV" alt="hello">
                              </td>
                              <td style="color: #ffffff;" class="header-text">
                                <br>
                                <br>
                                <span style="font-size: 24px;">Hi Admin!</span><br>
                                Proses delete laporan GCG telah berhasil.
                                <br>
                                <br>
                             
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="30" bgcolor="#ebebeb">
                            <tr>
                              <td>
                                <table cellspacing="0" cellpadding="0" class="force-full-width">
                                  <tr>
                                    <td>
                                      <span style="color: #232925;font-size: 24px;">'.$bpr['corp']." " .$bpr['name'].'</span> <br>
                                    '.$bpr['address'].'<br>
                                      '.$bpr['phone'].'<br>
                                      '.$bpr['email'].'<br><br>
                                    </td>
                                    <td style="text-align:right; vertical-align:top;">
                                      
                                    </td>
                                  </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                  <tr>
                                    <td style="background-color:#dedede; padding: 5px; font-weight:bold; ">
                                      Laporan GCG
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="background-color:#f3f3f3; padding: 10px 5px;">
                                      <div class="col-md-12">
                                        <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                                          <tr>
                                            <td style="padding: 5px;">Tanggal Hapus</td>
                                            <td>:</td>
                                            <td>'.date("d-m-Y h:i:s").'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Judul</td>
                                            <td>:</td>
                                            <td>'.$data['name'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Tahun</td>
                                            <td>:</td>
                                            <td>'.$data['year'].'</td>
                                          </tr>
                                          <tr>
                                            <td style="padding: 5px;">Dihapus Oleh</td>
                                            <td>:</td>
                                            <td>'.$data['uploader'].'</td>
                                          </tr>
                                        </table>
                                      </div>
                                    </td>
                                  <tr>
                                 
                                </table>
                              </td>
                            </tr>
                          </table>


                          <table class="force-full-width" cellspacing="0" cellpadding="20" bgcolor="#2b934f" style="width: 100%;">
                            <tr>
                              <td style="background-color:#232925; color:#ffffff; font-size: 14px; text-align: center;">&copy; PERBARINDO 2017 All Rights Reserved
                              </td>
                            </tr>
                          </table>


                        </td>
                      </tr>
                    </table>

                  </center>
                  </td>
                </tr>
              </table>
              </body>
            </html>';

        $email = $bpr['email'];
        $subject = "PERBARINDO - NOTIFIKASI LAPORAN GCG";
        $from = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $company = "PERBARINDO";

        if($this->send_mail($email, $subject, $html, @$from["value"], $company)){
           
           return true;

        }else{
           
           return false;

        }


    }

    function send_mail($email, $subject, $message, $from, $company,$files=Array()){

        $smtp_host = $this->Mbpr->getConfigurationByKey("SMTP_HOST", "EMAIL");
        $smtp_port = $this->Mbpr->getConfigurationByKey("SMTP_PORT", "EMAIL");
        $smtp_user = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $smtp_pass = $this->Mbpr->getConfigurationByKey("SMTP_PASSWORD", "EMAIL");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => @$smtp_host['value'],
            'smtp_port' => @$smtp_port['value'],
            'smtp_user' => @$smtp_user['value'],
            'smtp_pass' => @$smtp_pass['value'],
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from($from, $company);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if(count($files)>0){
          
          for($i=0;$i<(count($files));$i++) {
            $this->email->attach($files[$i]);            
          }

        }

        if($this->email->send())
        {
          return true;
        }
         else
        {
          return false;
        }
    }    


    private function _get_template_after_form(){

        return "<div class='alert alert-info'>
                    <p><i class='fa fa-check'></i> Dengan ini kami menyatakan bahwa laporan GCG yang telah dikirimkan ke perbarindo adalah sama dengan laporan yang dikirimkan ke OJK</p>
                </div>";

    }

    private function _get_template_second_column(){

    
        $gcg_info = $this->MConfiguration->getConfigurationByKey("INFO","GCG");

        return '<div class="col-md-6">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                        <h6 class="box-title">Keterangan</h6>
                    </div>
                    <div class="box-body">
                        <span style="font-weight:bold;font-size:17px;margin-bottom:10px;">Input Laporan</span>
                        <ul style="margin-top:10px;">
                        <li><span style="color:red">*)</span> <strong>Judul Laporan</strong> : Judul dari laporan GCG yang akan diupload. Misal: "Laporan GCG Desember 2018"</li>
                        <li><span style="color:red">*)</span> <strong>Tahun</strong> : Tahun Laporan GCG</li>
                        <li><span style="color:red">*)</span> <strong>File Lampiran</strong> : File laporan GCG yang akan diupload. Format file yang dizinkan : PDF/DOC/PPT/DOCX</li>
                        </ul>

                        <div class="alert" style="border: dashed 1px #666;">
                            <span style="font-weight:bold;font-size:17px;margin-bottom:10px;">Note :</span>
                            <p><span style="color:red">*) wajib diisi</span></p>
                        </div>

                        '.$gcg_info['value'].'

                    </div>
                </div>';

    }

    public function edit($id){

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["attachement"]["name"]!=""){

                    if($_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                        $_FILES["attachement"]["type"]=="application/vnd.ms-excel" ||
                        $_FILES["attachement"]["type"]=="application/excel" ||
                        $_FILES["attachement"]["type"]=="application/x-excel" ||
                        $_FILES["attachement"]["type"]=="application/x-msexcel" ||
                        $_FILES["attachement"]["type"]=="application/msword" ||
                        $_FILES["attachement"]["type"]=="application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                        $_FILES["attachement"]["type"]=="application/wps-office.xls" ||
                        $_FILES["attachement"]["type"]=="application/wps-office.xlsx" ||
                        $_FILES["attachement"]["type"]=="application/wps-office.doc" ||
                        $_FILES["attachement"]["type"]=="application/wps-office.docx" ||
                        $_FILES["attachement"]["type"]=="application/wps-office.ppt" ||
                        $_FILES["attachement"]["type"]=="text/csv" ||
                        $_FILES["attachement"]["type"]=="application/pdf" ||
                        $_FILES["attachement"]["type"]=="application/x-pdf"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                        $array = explode('.', $_FILES['attachement']['name']);
                        $extension = end($array);
                        $attachement = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_attachement'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["attachement"]["tmp_name"], $upload_path."/".$attachement)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid attachement file! Should be XLS,XLSX,PDF,PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $attachement = $this->input->post("old_attachement");

                }

                $_POST['attachement'] = $attachement;

            }

            if(isset($_POST['old_attachement'])){
                unset($_POST['old_attachement']);
            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['data_return'] = true;
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $updated=$p1->process();

            if($updated){

                /* media */

                $media = array(

                    "file" => $attachement,
                    "type" => "document",
                    "format" => $extension,
                    "updated_by" => $this->session->userdata('id'),
                    "updated_at" => date("Y-m-d h:i:s"),
                );

                $this->db->where("object_id", $id);
                $this->db->where("module", "gcg report");
                $this->db->update("media", $media);

                /* end media */

                 /* sent notif */

                $_POST['id'] = $id;
                $_POST['uploader'] = $member['member_name'];
                $_POST['bpr'] = $member['bpr'];
                $_POST['file'] = $attachement;

                $notif = $this->update_success_notif($_POST);

                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = false;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $p2->process();

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }

        }else{

            $data['page_title'] = "Edit Laporan Publikasi";
            $data['page_subtitle'] = "Modul Laporan Publikasi BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['after_form'] = $this->_get_template_after_form();
            $data['columns_size'] = 2;
            $data['second_column'] = $this->_get_template_second_column();

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['data_return'] = true;
            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "attachement";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $query = $this->db->query("SELECT * FROM bpr_finance_publication_reports WHERE id=".$_POST['id'])->result_array();

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

             /* sent notif */

            $_POST['name'] = @$query[0]['name'];
            $_POST['year'] = @$query[0]['year'];
            $_POST['uploader'] = $member['member_name'];
            $_POST['bpr'] = $member['bpr']; 


            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $deleted=$p1->process();

            if($deleted){

                $this->delete_success_notif($_POST);

                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = false;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $p2->process();

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }

        }

    }

}