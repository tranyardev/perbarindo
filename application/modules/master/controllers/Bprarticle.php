<?php

class Bprarticle extends MX_Controller{

    var $bpr;
    var $validation;

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('cloudinarylib');
        $this->load->model("mcore");
        $this->load->model("Mbprarticle");
        $this->load->model("Mbprarticlecategory");
        $this->load->model("Mbprvalidates");
        
        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));
        $this->bpr = $member['bpr'];
        $this->validation = $this->validate_feature();


        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr_articles";
        $this->dttModel = "Mdbprarticle";
        $this->pk = "id";

    }

    function validate_feature(){


        $validate = $this->Mbprvalidates->getBPRValidatesByBPR($this->bpr);

        if($validate['is_verified']=="1"){
            return true;
        }else{
            return false;
        }


    }

    function index(){

       
        $data['page_title'] = "Daftar Artikel";
        $data['page_subtitle'] = "Modul Artikel BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        // if(!$this->validation){
        //     $data['lock_modul'] = true;    
        // }

        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'blog', 
            'submenu' => 'bpr_article' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_article_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_article_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_article_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_article_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Artikel'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Artikel</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprarticle/add';
                }"

            ),
            'colvis'

        );

    }

    private function _get_datatable_columns(){


        return array(

            "post_title" => array(

                "data" => "a.title",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "post_status" => array(

                "data" => "$.status",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "post_category" => array(

                "data" => "b.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "post_created_at" => array(

                "data" => "a.created_at",
                "searchable" => false,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(

            "title" => array(

                "label" => "Judul",
                "type" => "text",
                "placeholder" => "Judul",
                "class" => "form-control validate[required]",
                "event" => array(
                    "onkeyup" => "setPermalink('title','permalink');",
                    "onblur" => "setPermalink('title','permalink');",
                )

            ),
            "cdn_public_id" => array(

                "label" => "cdn_public_id",
                "type" => "text",
                "input_type" => "hidden",
                "id" => "Judul",

            ),
            "category" => array(

                "label" => "Kategori",
                "type" => "sourcequery",
                "class" => "form-control",
                "source" => $this->Mbprarticlecategory->getBPRArticleCategory($member['bpr']),

            ),
            "content" => array(

                "label" => "Konten",
                "type" => "editor",
                "id" => "content"

            ),
            "permalink" => array(

                "label" => "Permalink",
                "type" => "text",
                "placeholder" => "Permalink",
                "class" => "form-control validate[required]",
                "event" => array("onkeyup" => "filterPermalink('permalink');")

            ),
            "cover" => array(

                "label" => "Cover (*ukuran : 1366 x 768 px)",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/media/',
                "id" => "cover",

            ),
            "tags" => array(

                "label" => "Tag",
                "type" => "tags",
                "placeholder" => "Tag",
                "class" => "form-control",

            ),
            "meta_keyword" => array(

                "label" => "Meta Keyword",
                "type" => "tags",
                "placeholder" => "Meta Keyword",
                "class" => "form-control",

            ),
            "meta_description" => array(

                "label" => "Meta Description",
                "type" => "textarea",
                "placeholder" => "Meta Description",
                "class" => "form-control"
            ),
            "status" => array(

                "label" => "Status",
                "type" => "sourcearray",
                "source" => array("1" => "Publish", "0" => "Draft"),
                "class" => "form-control"
            )


        );


    }

    public function add(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                        /* cloudinary upload */

                        $cloud_upload = \Cloudinary\Uploader::upload($upload_path."/".$cover);

                        unset($cloud_upload['created_at']);
                        unset($cloud_upload['tags']);

                        $this->db->insert("cloudinary_medias",$cloud_upload);

                        $_POST['cdn_public_id'] = $cloud_upload['public_id'];

                        /* end cloudinary */

                       

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    $_POST['cover'] = $cover;


                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();

            if($insert){

                /* media */

                $media = array(

                    "file" => $cover,
                    "path" => 'upload/media/',
                    "object_id" => $this->db->insert_id(),
                    "type" => "image",
                    "module" => "article",
                    "format" => $extension,
                    "created_by" => $this->session->userdata('id'),
                    "created_at" => date("Y-m-d h:i:s"),
                    "bpr" => $member['bpr'],
                    "dpd" => $member['dpd']

                );

                $this->db->insert("media", $media);

                /* end media */

                echo json_encode(array("status" => "1", "msg" => "Data berhasil disimpan!"));

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }
            
                        

        }else{

            $data['page_title'] = "Tambah Article";
            $data['page_subtitle'] = "Modul Article BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["cover"]["name"]!=""){

                    $cloudinary = new \Cloudinary\Api();

                    if($_FILES["cover"]["type"]=="image/png" or
                        $_FILES["cover"]["type"]=="image/jpg" or
                        $_FILES["cover"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                        $array = explode('.', $_FILES['cover']['name']);
                        $extension = end($array);
                        $cover = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_cover'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                            $cloudinary->delete_resources(array(@$_POST['cdn_public_id']));


                        }


                        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                            $cloud_upload = \Cloudinary\Uploader::upload($upload_path."/".$cover);

                            $cloud_upload['created_at'] = date("Y-m-d h:i:s");

                            $cloud_upload['tags'] = implode(',',$cloud_upload['tags']);

                            $this->db->insert("cloudinary_medias",$cloud_upload);

                            $_POST['cdn_public_id'] = $cloud_upload['public_id'];
                            
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $cover = $this->input->post("old_cover");

                }

                $_POST['cover'] = $cover;

            }

            if(isset($_POST['old_cover'])){

                unset($_POST['old_cover']);
            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");

            $data['params']['data_return'] = true;
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $update = $this->cinta->process();

            if($update){

                /* media */

                $media = array(

                    "file" => $cover,
                    "type" => "image",
                    "format" => $extension,
                    "updated_by" => $this->session->userdata('id'),
                    "updated_at" => date("Y-m-d h:i:s"),
                );

                $this->db->where("object_id", $id);
                $this->db->where("module", "article");
                $this->db->update("media", $media);

                /* end media */

                echo json_encode(array("status" => "1", "msg" => "Data berhasil diupdate!"));

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }

        }else{

            $data['page_title'] = "Edit Article";
            $data['page_subtitle'] = "Modul Article BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "cover";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}