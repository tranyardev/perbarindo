<?php

class Bprproduct extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbprproduct");
        $this->load->model("Mbprproductcategory");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "bpr_products";
        $this->dttModel = "Mdbprproduct";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Produk";
        $data['page_subtitle'] = "Modul Produk BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_product_management', 
            'submenu' => 'bpr_product' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_product_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_product_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_product_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_product_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Produk BPR'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Produk BPR</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprproduct/add';
                }"

            ),
            'colvis'

        );

    }

    private function _get_datatable_columns(){


        return array(

            "product_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "product_status" => array(

                "data" => "$.status",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "product_category" => array(

                "data" => "$.category",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "product_created_at" => array(

                "data" => "a.created_at",
                "searchable" => false,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        return array(

            "name" => array(

                "label" => "Nama Produk",
                "type" => "text",
                "placeholder" => "Nama Produk",
                "class" => "form-control validate[required]",
                "event" => array(
                    "onkeyup" => "setPermalink('name','permalink');",
                    "onblur" => "setPermalink('name','permalink');",
                )

            ),

            "category" => array(

                "label" => "Kategori Produk",
                "type" => "sourcequery",
                "class" => "form-control",
                "source" => $this->Mbprproductcategory->getBPRProductCategory($member['bpr']),

            ),
            "description" => array(

                "label" => "Konten",
                "type" => "editor",
                "id" => "content"

            ),
            "permalink" => array(

                "label" => "Permalink",
                "type" => "text",
                "placeholder" => "Permalink",
                "class" => "form-control validate[required]",
                "event" => array("onkeyup" => "filterPermalink('permalink');")

            ),
            "cover" => array(

                "label" => "Cover (*ukuran : 1366 x 768 px)",
                "type" => "upload_file",
                "class" => "form-control",
                "file_path" => '/upload/media/',
                "id" => "cover",

            ),
            "status" => array(

                "label" => "Status",
                "type" => "sourcearray",
                "source" => array("1" => "Publish", "0" => "Draft"),
                "class" => "form-control"
            )


        );


    }

    public function add(){

        if(count($_POST)>0){

            if(count($_FILES)>0){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                    $_POST['cover'] = $cover;


                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();

            if($insert){

                /* media */

                $media = array(

                    "file" => $cover,
                    "path" => 'upload/media/',
                    "object_id" => $this->db->insert_id(),
                    "type" => "image",
                    "module" => "product",
                    "format" => $extension,
                    "created_by" => $this->session->userdata('id'),
                    "created_at" => date("Y-m-d h:i:s"),
                    "bpr" => $member['bpr'],
                    "dpd" => $member['dpd']

                );

                $this->db->insert("media", $media);

                /* end media */

                echo json_encode(array("status" => "1", "msg" => "Data berhasil disimpan!"));

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }

        }else{

            $data['page_title'] = "Tambah Produk";
            $data['page_subtitle'] = "Modul Produk BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){


            if(count($_FILES)>0){


                if($_FILES["cover"]["name"]!=""){

                    if($_FILES["cover"]["type"]=="image/png" or
                        $_FILES["cover"]["type"]=="image/jpg" or
                        $_FILES["cover"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                        $array = explode('.', $_FILES['cover']['name']);
                        $extension = end($array);
                        $cover = md5(uniqid(rand(), true)).".".$extension;

                        $old_cover = $upload_path.'/'.@$_POST['old_cover'];

                        if(file_exists($old_cover)){

                            @unlink($old_cover);

                        }


                        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    $cover = $this->input->post("old_cover");

                }

                $_POST['cover'] = $cover;

            }

            if(isset($_POST['old_cover'])){
                unset($_POST['old_cover']);
            }

            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;

            $this->load->library("Cinta",$data);
            $update = $this->cinta->process();

            if($update){

                /* media */

                $media = array(

                    "file" => $cover,
                    "type" => "image",
                    "format" => $extension,
                    "updated_by" => $this->session->userdata('id'),
                    "updated_at" => date("Y-m-d h:i:s"),
                );

                $this->db->where("object_id", $id);
                $this->db->where("module", "product");
                $this->db->update("media", $media);

                /* end media */

                echo json_encode(array("status" => "1", "msg" => "Data berhasil diupdate!"));

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            }

        }else{

            $data['page_title'] = "Edit Produk";
            $data['page_subtitle'] = "Modul Produk BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_field'] = "cover";
            $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}