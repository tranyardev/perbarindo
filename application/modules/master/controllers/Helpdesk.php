<?php

class Helpdesk extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('cloudinarylib');
        $this->load->model("mcore");
        $this->load->model("Mbpr");
       
        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }


    }

    function index(){

        $data['page'] = 'helpdesk';
        $data['page_title'] = "Help Desk";
        $data['page_subtitle'] = "Pusat Bantuan";
        $this->load->view('layout/body',$data);

    }

    public function sendTiket(){

        if(count($_POST) > 0){


            // Verify user's answer
            $response = $this->recaptcha->verifyResponse($_POST['g-recaptcha-response']);

            // Processing ...
            if ($response['success']) {

                if(count($_FILES) > 0){

                    if($_FILES["attachement"]["type"]=="application/zip" or
                        $_FILES["attachement"]["type"]=="application/x-rar-compressed"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';
                        $array = explode('.', $_FILES['attachement']['name']);
                        $extension = end($array);
                        $attachement = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["attachement"]["tmp_name"], $upload_path."/".$attachement)) {

                           

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                        $_POST['attachement'] = $attachement;


                    }else{


                        $res['msg'] = "Invalid file extension! Should be .zip / .rar";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                }

                
                $data = array(
                    "date" => date("d-m-Y h:i:s"),
                    "subject" => $_POST['subject'],
                    "email" => $_POST['email'],
                    "message" => $_POST['message'],
                );

                $template = $this->_get_template($data);
                $company = "PERBARINDO";
                $from = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
                // $dev_mail = "developer.tranyar@gmail.com";
                $dev_mail = "indra.developer.web.id@gmail.com";
                $files = array($upload_path."/".$attachement);
        
                if($this->send_mail($dev_mail, "Tiket Helpdesk - kategori : ".$_POST['subject'], $template, @$from["value"], $company, $files)){
                    
                    $res = array("status" => "1", "msg" => "Successfully add data!");
                  
                }else{

                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                
                } 

            }else{

                $res = array("status" => "0", "msg" => "Oop! Invalid Captcha");

            }

            

            echo json_encode($res);
        }

    }

    function send_mail($email, $subject, $message, $from, $company,$files=Array()){

        $smtp_host = $this->Mbpr->getConfigurationByKey("SMTP_HOST", "EMAIL");
        $smtp_port = $this->Mbpr->getConfigurationByKey("SMTP_PORT", "EMAIL");
        $smtp_user = $this->Mbpr->getConfigurationByKey("SMTP_USER", "EMAIL");
        $smtp_pass = $this->Mbpr->getConfigurationByKey("SMTP_PASSWORD", "EMAIL");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => @$smtp_host['value'],
            'smtp_port' => @$smtp_port['value'],
            'smtp_user' => @$smtp_user['value'],
            'smtp_pass' => @$smtp_pass['value'],
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from($from, $company);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if(count($files)>0){
          
          for($i=0;$i<(count($files));$i++) {
            $this->email->attach($files[$i]);            
          }

        }

        if($this->email->send())
        {
          return true;
        }
         else
        {
          return false;
        }

    } 

    private function _get_template($data){

$template = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Verifikasi Struktur Organisasi BPR</title>
    <!-- Designed by https://github.com/kaytcat -->
    <!-- Header image designed by Freepik.com -->

    <style type="text/css">
    /* Take care of image borders and formatting */

    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
    a img { border: none; }
    table { border-collapse: collapse !important; }
    #outlook a { padding:0; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass {width:100%;}
    .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
    table td {border-collapse: collapse;}
    .ExternalClass * {line-height: 115%;}


    /* General styling */

    td {
      font-family: Arial, sans-serif;
      color: #5e5e5e;
      font-size: 16px;
      text-align: left;
    }

    body {
      -webkit-font-smoothing:antialiased;
      -webkit-text-size-adjust:none;
      width: 100%;
      height: 100%;
      color: #5e5e5e;
      font-weight: 400;
      font-size: 16px;
    }


    h1 {
      margin: 10px 0;
    }

    a {
      color: #2b934f;
      text-decoration: none;
    }


    .body-padding {
      padding: 0 75px;
    }


    .force-full-width {
      width: 100% !important;
    }

    .icons {
      text-align: right;
      padding-right: 30px;
    }

    .logo {
      text-align: left;
      padding-left: 30px;
    }

    .computer-image {
      padding-left: 30px;
    }

    .header-text {
      text-align: left;
      padding-right: 30px;
      padding-left: 20px;
    }

    .header {
      color: #232925;
      font-size: 24px;
    }

  ul{

      list-style: none;

  }
  ul li ul{

      list-style: none;

  }
  
  ul li{
      padding: 10px;
      border-bottom: dashed 1px #aaa;
      border-left: dashed 1px #aaa;
  }


    </style>

    <style type="text/css" media="screen">
        @media screen {
          @import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
          /* Thanks Outlook 2013! */
          * {
            font-family: "PT Sans", "Helvetica Neue", "Arial", "sans-serif" !important;
          }
        }
    </style>

  </head>
  <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tr>
      <td align="center" valign="top" style="background-color:#ffffff" width="100%">

      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320"  bgcolor="#232925">
          <tr>
            <td align="center" valign="top">

              <table class="force-full-width" cellspacing="0" cellpadding="0" bgcolor="#232925" style="width: 100%;">
                <tr>
                  <td style="background-color:#232925;padding-left: 30px;" class="logo">
                    <br>
                    <a href="#"><img src="http://perbarindo-event.tranyar.com/public/assets/img/logo.png" alt="Logo"></a>
                  </td>
                  <td class="icons">
                   
                  </td>
                </tr>
              </table>

              <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#232925" style="width: 100%;">
                <tr>
                  <td class="computer-image">
                    <br>
                    <br class="mobile-hide" />
                    <img style="display:block;" width="224" height="213" src="https://www.filepicker.io/api/file/CoMxXSlVRDuRQWNwnMzV" alt="hello">
                  </td>
                  <td style="color: #ffffff;" class="header-text">
                    <br>
                    <br>
                    <span style="font-size: 24px;">Hi Developer!</span><br>
                    Silahkan cek email tiket berikut
                    <br>
                    <br>
                   
                  </td>
                </tr>
              </table>


              <table class="force-full-width" cellspacing="0" cellpadding="30" bgcolor="#ebebeb" style="width: 100%;">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="0" class="force-full-width" style="width: 100%;">
                        <tr>
                            <td style="padding: 5px;">Tanggal</td>
                            <td>:</td>
                            <td>'.$data['date'].'</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;">Subject</td>
                            <td>:</td>
                            <td>'.$data['subject'].'</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;">Email</td>
                            <td>:</td>
                            <td>'.$data['email'].'</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;">Pesan</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 5px;">
                                <hr>
                                <p>'.$data['message'].'</p>
                            </td>
                        </tr>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td>
                   
                  </td>
                </tr>

              </table>


              <table class="force-full-width" cellspacing="0" cellpadding="20" bgcolor="#2b934f" style="width: 100%;">
                <tr>
                  <td style="background-color:#232925; color:#ffffff; font-size: 14px; text-align: center;">&copy; PERBARINDO 2017 All Rights Reserved
                  </td>
                </tr>
              </table>


            </td>
          </tr>
        </table>

      </center>
      </td>
    </tr>
  </table>
  </body>
</html>';

    return $template;

    }   

}