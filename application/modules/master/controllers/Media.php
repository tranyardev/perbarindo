<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Media extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MMedia");
        $this->load->helper('download');

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'slider_view';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "media";
        $this->pk = "id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> Media " => base_url()."master/media"
        );

        $data['page'] = 'media';
        $data['page_title'] = 'Media';
        $data['page_subtitle'] = 'Manage Media';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'media';
        $data['data']['submenu'] = '';
        $data['data']['media_type'] = $this->MMedia->getMediaType();
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

    function detail($id){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> Media Detail " => base_url()."master/media/detail/".$id
        );

        $data['page'] = 'media_detail';
        $data['page_title'] = 'Media';
        $data['page_subtitle'] = 'Media Detail';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'media';
        $data['data']['submenu'] = '';
        $data['data']['media_detail'] = $this->MMedia->getMediaById($id);
        $this->load->view('layout/body',$data);


    }

    function download($id){

     
        $media = $this->MMedia->getMediaById($id);

        if(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$media['path'].$media['file'])){

            force_download($_SERVER['DOCUMENT_ROOT'].'/'.$media['path'].$media['file'], NULL);

        }

    }


}