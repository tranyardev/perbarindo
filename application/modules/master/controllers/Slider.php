<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Slider extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('googlemaps');
        $this->load->model("mcore");
        $this->load->model("MSlider");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'slider_view';
            $this->add_perm = 'slider_add';
            $this->edit_perm = 'slider_update';
            $this->delete_perm = 'slider_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "sliders";
        $this->dttModel = "MDSlider";
        $this->pk = "slider_id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> Slider " => base_url()."event/event"
        );

        $data['page'] = 'slider';
        $data['page_title'] = 'Slider';
        $data['page_subtitle'] = 'Manage Slider';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'slider';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){


            /* upload cover */

            if($_FILES["cover"]["name"]!=""){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/slider/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }


            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "action_url" => $this->input->post("action_url"),
                "picture" => $cover,
                "sequence" => $this->input->post("sequence"),
                "status" => $this->input->post("status"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")

            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){


                /* media */

                $media = array(

                    "file" => $cover,
                    "path" => 'upload/slider/',
                    "object_id" => $this->db->insert_id(),
                    "type" => "image",
                    "module" => "slider",
                    "format" => $extension,
                    "created_by" => $this->session->userdata('id'),
                    "created_at" => date("Y-m-d h:i:s"),

                );

                $this->db->insert("media", $media);

                /* end media */

                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> Slider" => base_url()."xconfiguration/slider",
                "Add New" => base_url()."xconfiguration/slider/addnew",
            );

            $data['page'] = 'slider_add';
            $data['page_title'] = 'Slider';
            $data['page_subtitle'] = 'Add New Slider';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'home_efa_config';
            $data['data']['submenu'] = 'slider';
            $this->load->view('layout/body',$data);

        }



    }

    function edit($id){

        if(count($_POST)>0){

            /* upload cover */

            if($_FILES["cover"]["name"]!=""){

                if($_FILES["cover"]["type"]=="image/png" or
                    $_FILES["cover"]["type"]=="image/jpg" or
                    $_FILES["cover"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/slider/';
                    $array = explode('.', $_FILES['cover']['name']);
                    $extension = end($array);
                    $cover = md5(uniqid(rand(), true)).".".$extension;

                    $old_cover = $upload_path.'/'.@$_POST['old_cover'];

                    if(file_exists($old_cover)){

                        @unlink($old_cover);

                    }


                    if (move_uploaded_file($_FILES["cover"]["tmp_name"], $upload_path."/".$cover)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $cover = $this->input->post("old_cover");

            }

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "action_url" => $this->input->post("action_url"),
                "picture" => $cover,
                "sequence" => $this->input->post("sequence"),
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")

            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){

                /* media */

                $media = array(

                    "file" => $cover,
                    "type" => "image",
                    "format" => $extension,
                    "updated_by" => $this->session->userdata('id'),
                    "updated_at" => date("Y-m-d h:i:s"),
                );

                $this->db->where("object_id", $id);
                $this->db->where("module", "slider");
                $this->db->update("media", $media);

                /* end media */

                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-calendar'></i> Event" => base_url()."xconfiguration/slider",
                "Edit" => base_url()."xconfiguration/slider/edit/".$id,
            );

            $data['page'] = 'slider_edit';
            $data['page_title'] = 'Slider';
            $data['page_subtitle'] = 'Edit Slider';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css"
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MSlider->getSliderById($id);
            $data['data']['parent_menu'] = 'home_efa_config';
            $data['data']['submenu'] = 'slider';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $slider = $this->MSlider->getSliderById($id);

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/slider/';

            $picture = $upload_path.'/'.$slider['picture'];

            if(file_exists($picture)){

                @unlink($picture);

            }

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}