<?php

class Bprstackholder extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprvalidates");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "stockholders";
        $this->dttModel = "Mdbprstackholder";
        $this->pk = "id";

    }

    function index(){

       
        $data['page_title'] = "Daftar Pemegang Saham";
        $data['page_subtitle'] = "Modul Pemegang Saham BPR";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_stackholder', 
            'submenu' => '' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_stackholder_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_stackholder_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_stackholder_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_stackholder_delete"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar Produk Kategori'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar Pemegang Saham</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ) {
                    window.location.href = '".base_url()."master/bprstackholder/add';
                }"

            ),


        );

    }

    private function _get_datatable_columns(){


        return array(

            "stockholder_name" => array(

                "data" => "a.name",
                "searchable" => true,
                "orderable" => true,
                "class" => "",
                "id" => "",

            ),
            "stockholder_percentage" => array(

                "data" => "$.percentage",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            ),
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "class" => "",
                "id" => "",

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "name" => array(

                "label" => "Nama Pemegang Saham",
                "type" => "text",
                "placeholder" => "Pemegang Saham",
                "class" => "form-control validate[required]",

            ),
            "percentage" => array(

                "label" => "Nilai Saham (persen)",
                "type" => "text",
                "input_type" => "number",
                "placeholder" => "Nilai Saham",
                "class" => "form-control validate[required]",

            )

        );


    }

    public function add(){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['created_by'] = $this->session->userdata("id");
            $_POST['created_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $sum_percentage = $this->Mbprstackholder->getBPRStackholderSumPercentage($member['bpr']);
            $total = $_POST['percentage'] + $sum_percentage['total'];

            if($total > 100){

                echo json_encode(array("status" => "0", "msg" => "Jumlah nilai melebihi persentase data yang ada."));exit;

            }

            $data['params']['data_return'] = true;
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $insert=$p1->process();

            if($insert){

                $update['is_stockholder_data_completed'] = "0";
                $update['is_org_level_1_completed'] = "0";
                $update['is_org_level_2_completed'] = "0";
                $update['is_verified'] = "0";
                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = false;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $p2->process();

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));
            }

        }else{

            $data['page_title'] = "Tambah Pemegang Saham";
            $data['page_subtitle'] = "Modul Pemegang Saham BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

            $_POST['updated_by'] = $this->session->userdata("id");
            $_POST['updated_at'] = date("Y-m-d h:i:s");
            $_POST['bpr'] = $member['bpr'];

            $data['params']['data_return'] = true;
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $updated = $p1->process();

            if($updated){

                $update['is_stockholder_data_completed'] = "0";
                $update['is_org_level_1_completed'] = "0";
                $update['is_org_level_2_completed'] = "0";
                $update['is_verified'] = "0";
                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = false;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $p2->process();

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));
            }

        }else{

            $data['page_title'] = "Edit Pemegang Saham";
            $data['page_subtitle'] = "Modul Pemegang Saham BPR";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['data_return'] = true;
            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            // $data['params']['file_field'] = "cover";
            // $data['params']['file_path'] = $_SERVER['DOCUMENT_ROOT'].'/upload/media/';

            $this->load->library("Cinta");
            $p1 = new Cinta($data);
            $deleted = $p1->process();

            if($deleted){
                
                $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

                $update['is_stockholder_data_completed'] = "0";
                $update['is_org_level_1_completed'] = "0";
                $update['is_org_level_2_completed'] = "0";
                $update['is_verified'] = "0";
                $update['is_publication_report_verified'] = "0";

                $validation_data = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);
                $data_vd['params']['data_return'] = false;
                $data_vd['params']['action'] = "update";
                $data_vd['params']['table'] = "bpr_validates";
                $data_vd['params']['pk'] = "id";
                $data_vd['params']['id'] = $validation_data['id'];
                $data_vd['params']['post'] = $update;

                $p2 = new Cinta($data_vd);
                $p2->process();

            }else{

                echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));
            }

        }

    }

}