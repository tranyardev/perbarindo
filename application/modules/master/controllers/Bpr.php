<?php

class Bpr extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('excel');
        $this->load->model("mcore");
        $this->load->model("Mbpr");
        $this->load->model("Mdpd");
        $this->load->model("Mcorporate");
        $this->load->model("Mbprstackholder");
        $this->load->model("Mbprfinanceassettype");
        $this->load->model("Mbprfinanceasset");
        $this->load->model("Mbprdirectorposition");
        $this->load->model("Mbprdirector");
        $this->load->model("MCorporate");
        $this->load->model("Memployee");
        $this->load->model("Mbprcontactperson");
        $this->load->model("Mbprfinancepublication");
        $this->load->model("Mbprvalidates");


        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'bpr_view';
            $this->add_perm = 'bpr_add';
            $this->edit_perm = 'bpr_update';
            $this->delete_perm = 'bpr_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "bpr";
        $this->dttModel = "Mdbpr";
        $this->pk = "id";

    }



    function index(){

        $breadcrumb = array(
            "<i class='fa fa-cogs'></i> bpr " => base_url()."bpr"
        );

        $data['page'] = 'bpr';
        $data['page_title'] = $this->lang->line('master_data');
        $data['page_subtitle'] = $this->lang->line('bpr_list');
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'bpr';
        $data['data']['submenu'] = '';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }


    function detail($id=null){

        if($id!=null){

            $data['bpr'] = $this->Mbpr->getBPRById($id);
            $data['director_position'] = $this->Mbprdirectorposition->getBPRDirectorPositionByBPR($id,"parent");
            $data['assettype'] = $this->Mbprfinanceassettype->getBPRFinanceAssetType();
            $data['asset'] = $this->Mbprfinanceasset->getBPRFinanceAssetByBPR($id);
            $data['contact_person'] = $this->Mbprcontactperson->getBPRContactPerson($id);
            $data['branches'] = $this->Mbpr->getBPRBranches($id);
            $data['report'] = $this->Mbprfinancepublication->getBPRFinancePublication($id);
            $data['validation'] = $this->Mbprvalidates->getBPRValidatesByBPR($id);

            if($data['bpr']['asset_active_year']==""){
              $data['asset_active_year'] = date("Y");
            }else{
              $data['asset_active_year'] = $data['bpr']['asset_active_year'];
            }
           
            $data['page'] = "bpr_profile_admin";
            $data['page_title'] = "Profile BPR ".$data['bpr']['name'];
            $data['page_subtitle'] = "Profile BPR";
      
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            // $data['permissions'] = $this->_get_permissions();
            // $data['active_menu'] = $this->_get_active_menu();  
        
            $this->load->view('layout/body',$data);

        }

    }

    public function getOrgChartData($bpr){

        $org = $this->Mbprdirector->getBPRChartOrg($bpr);
        $orgTopLevel = $this->Mbprdirector->getBPRChartOrgTopLevel($bpr);

        $chart = array();


        $primaryFields[] = "Name";
        $primaryFields[] = "Position";

        $i=1;
        foreach ($orgTopLevel as $otl) {

            $primaryFields[] = "Komisaris_".$i;
            $extraField["Komisaris_".$i] = $i.". ".$otl['name'];   
            $i++;

        }


        foreach ($org as $o) {

            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            if($o['photo'] != ""){

                if(file_exists($path.$o['photo'])){

                    $img = base_url().'upload/photo/'.$o['photo'];

                }else{

                    $img = base_url().'public/assets/img/default-avatar.png';

                }

            }else{

                $img = base_url().'public/assets/img/default-avatar.png';

            }
            
            array_push($chart, array("id" => $o['id'], "parentId" => $o['parent'], "Name" => $o['name'],"Position" => $o['position'], "pic" => $img));

        }

        if(count($orgTopLevel)>0){


            array_push($chart, array_merge($extraField,array("id" => @$orgTopLevel[0]['id'], "parentId" => null, "Name" => null, "Position" => @$orgTopLevel[0]['position'], "pic" => base_url().'public/assets/img/ceo-512.png')));

        }

        $res = array(
            "primaryFields" => $primaryFields,
            "sources" => $chart,
        );

        echo json_encode($res);

    }

    public function get_stockholder($bpr){

        $stockholder = $this->Mbprstackholder->getBPRStockholderByBPR($bpr);

        $res = array();

        foreach ($stockholder as $s){
                
            $a = array( $s['name'], (float)$s['percentage']);
            
            array_push($res, $a);

        }

        echo json_encode($res);

    }

    public function get_stockholder_list($level,$bpr,$json=true){


        $stockholder = $this->Mbprstackholder->getBPRStockholderByLevel($bpr,$level);

        $html = "";
        
        if($json){
          
          foreach ($stockholder as $list) {

              if($list['is_valid']=="1"){
                $checked = "checked";
                $class = "verified_label";
                $verified_label = "<i class='fa fa-check-circle-o'></i> Verified";
              }else{
                $checked = "";
                $class = "unverified_label";
                $verified_label = "<i class='fa fa-exclamation-circle'></i> Unverified";
              }  
              
              $html.= "<li><input type='checkbox' ".$checked." id='stockholder_".$list['id']."' onclick='verify_stockholder(\"".$list['id']."\");'><a href='javascript:edit_stockholder(".$list['id'].");'>".$list['name']."</a> (".$list['percentage']."%) <span class='".$class."' id='verified_stockholder_".$list['id']."'>".$verified_label."</span></li>";

          }


          echo $html;

        }else{
          
          foreach ($stockholder as $list) {
              
              $html.= "<li>".$list['name']." (".$list['percentage']."%)</li>";

          }

          return $html;
        }        

    }

    public function verify_stockholder(){

        if(count($_POST)>0){

            $data = array(
                "is_valid" => $_POST['is_valid']
            );

            $this->db->where("id", $_POST['id']);
            $update = $this->db->update("stockholders", $data);

            if($update){
                $res = array("status" => "1", "verified" => $_POST['is_valid']);
            }else{
                $res = array("status" => "0", "verified" => $_POST['is_valid']);
            }

            echo json_encode($res);

        }

    }

    public function get_director_list($slug,$bpr, $json=true){

        $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

        $director = $this->Mbprdirector->getBPRDirectorBySlug($bpr,$slug);

        $extend_label = ""; 

        $html = "";

        if($json){

          foreach ($director as $list) {

            if($list['is_valid']=="1"){
                $checked = "checked";
                $class = "verified_label";
                $verified_label = "<i class='fa fa-check-circle-o'></i> Verified";
            }else{
                $checked = "";
                $class = "unverified_label";
                $verified_label = "<i class='fa fa-exclamation-circle'></i> Unverified";
            }  
            
            if($slug=="-"){
                $extend_label = " - ".$list['position_name']." - ".$list['position_title'];
            }

            $html.= "<li><input type='checkbox' ".$checked." id='director_".$list['id']."' onclick='verify_director(\"".$list['id']."\");'><a href='javascript:edit_director(".$list['id'].");'>".$list['name'].$extend_label." <span class='".$class."' id='verified_director_".$list['id']."'>".$verified_label."</span></a></li>";

          }


          echo $html;

        }else{

          foreach ($director as $list) {
            
            if($slug=="-"){
                $extend_label = " - ".$list['position_name']." - ".$list['position_title'];
            }

            $html.= "<li>".$list['name'].$extend_label."</li>";

          }

          return $html;
        }        

    }

    public function verify_director(){

        if(count($_POST)>0){

            $data = array(
                "is_valid" => $_POST['is_valid']
            );

            $this->db->where("id", $_POST['id']);
            $update = $this->db->update("directors", $data);

            if($update){
                $res = array("status" => "1", "verified" => $_POST['is_valid']);
            }else{
                $res = array("status" => "0", "verified" => $_POST['is_valid']);
            }

            echo json_encode($res);

        }

    }

    public function update_bpr_notif(){

        if(count($_POST)>0){

            $data = array(
                "notif" => trim($_POST['notif_message'])
            );

            $this->db->where("id", $_POST['bpr']);
            $update = $this->db->update("bpr", $data);

            if($update){
                $res = array("status" => "1", "msg" => "Notifikasi berhasil diupdate!");
            }else{
                $res = array("status" => "0", "msg" => "Oops! Telah terjadi kesalahan.");
            }

            echo json_encode($res);

        }


    }
    public function update_bpr_notif_status(){

        if(count($_POST)>0){

            $data = array(
                "is_notif_activated" => $_POST['is_notif_activated']
            );

            $this->db->where("id", $_POST['bpr']);
            $update = $this->db->update("bpr", $data);

            if($update){
                $res = array("status" => "1", "msg" => "Notifikasi diaktifkan!");
            }else{
                $res = array("status" => "0", "msg" => "Oops! Telah terjadi kesalahan.");
            }

            echo json_encode($res);

        }


    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'bpr_management', 
            'submenu' => 'bpr' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            
            "read_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_update"),
            "add_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_add"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "bpr_employee_delete"),

        );

    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(

                "name" => $this->input->post("name"),
                "address" => $this->input->post("address"),
                "dpd" => $this->input->post("dpd"),
                "email" => $this->input->post("email"),
                "telp" => $this->input->post("telp"),
                "fax" => $this->input->post("fax"),
                "website" => $this->input->post("website"),
                "corporate" => $this->input->post("corporate")

            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('bpr_list') => base_url()."master/bpr",
                "Add New" => base_url()."master/bpr/addnew",
            );

            $data['page'] = 'bpr_add';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('bpr_add');
            $data['dpd'] =  $this->Mdpd->getDPD();
            $data['corporates'] =  $this->Mcorporate->getCorporate();
        
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'bpr';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){


            $data = array(

                "name" => $this->input->post("name"),
                "address" => $this->input->post("address"),
                "dpd" => $this->input->post("dpd"),
                "email" => $this->input->post("email"),
                "telp" => $this->input->post("telp"),
                "fax" => $this->input->post("fax"),
                "website" => $this->input->post("website"),
                "corporate" => $this->input->post("corporate")
            
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-cogs'></i> ".$this->lang->line('bpr_list') => base_url()."master/bpr",
                "Edit" => base_url()."master/bpr/edit/".$id,
            );

            $data['page'] = 'bpr_edit';
            $data['page_title'] =  $this->lang->line('master_data');
            $data['page_subtitle'] =  $this->lang->line('bpr_edit');
            
            $data['dpd'] =  $this->Mdpd->getDPD();
            $data['corporates'] =  $this->Mcorporate->getCorporate();

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->Mbpr->getBPRById($id);
            $data['data']['parent_menu'] = 'bprs';
            $data['data']['submenu'] = '';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }
    function import(){


        if($_FILES["excel"]["name"]!=""){

            if($_FILES["excel"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
                $_FILES["excel"]["type"]=="application/vnd.ms-excel" or
                $_FILES["excel"]["type"]=="application/x-msexcel" or
                $_FILES["excel"]["type"]=="application/x-ms-excel" or
                $_FILES["excel"]["type"]=="application/x-excel" or
                $_FILES["excel"]["type"]=="application/x-dos_ms_excel" or
                $_FILES["excel"]["type"]=="application/xls" or
                $_FILES["excel"]["type"]=="application/wps-office.xlsx" or
                $_FILES["excel"]["type"]=="application/x-xls" or
                $_FILES["excel"]["type"]=="application/msexcel"){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/excel';
                $array = explode('.', $_FILES['excel']['name']);
                $extension = end($array);
                $file = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["excel"]["tmp_name"], $upload_path."/".$file)) {

                    $import = $this->import_from_excel($upload_path."/".$file); 

                    // if($import){

                    //     @unlink($upload_path."/".$file);

                    // }

                    $res = array("status" => "1", "msg" => "Data imported Successfully");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Something went wrong while uploading file");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid file extention. Must be .xls or .xlsx");

            }

            echo json_encode($res);

        }

    }

    function test(){

        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/excel/61dce467af18790d979eab6ddbe66a71.xlsx';

        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

        echo $totalrows.'<br>';                
          
        for($i=2;$i<=$totalrows;$i++)
        {

                $corporate= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
                $name= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                $address= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                $telp= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                $email= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                $fax= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 
                $dpd= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                $website= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();

                $dpd = $this->Mdpd->getDPDByName(trim($dpd));
                $corporate = $this->Mcorporate->getCorporateByName(trim($corporate));


                if(count($corporate)< 1){

                    $corporate= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue(); 

                    $ex_corp = explode(" ", $corporate);
                    
                    if(count($ex_corp) > 1){

                        if($ex_corp[0]=="PD."){

                            $corporate = $this->Mcorporate->getCorporateByName("PD");

                        }else if($ex_corp[0]=="PT."){

                            $corporate = $this->Mcorporate->getCorporateByName(trim($ex_corp[1]));

                        }

                    }else{

                        $corporate = $this->Mcorporate->getCorporateByName("BPR");

                    }

                }

                $data = array(
                    "name" => trim($name), 
                    "corporate" => @$corporate[0]['id'],
                    "dpd" => @$dpd[0]['id'],
                    "address" => trim($address),
                    "telp" => trim($telp),
                    "email" => trim($email),
                    "fax" => trim($fax),
                    "website" => trim($website),
                );

                $exist = $this->db->get_where($this->table, array('name' => trim($name)));

                if($exist->num_rows()<1){

                    echo trim($name).'no<br>';

                }else{

                    echo trim($name).'exist<br>';

                }

        }   

       
        
    }

    function import_from_excel($file){

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          
        for($i=2;$i<=$totalrows;$i++)
        {

                $corporate= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
                $name= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                $address= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                $telp= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                $email= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                $fax= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 
                $dpd= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                $website= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();

                $dpd = $this->Mdpd->getDPDByName(trim($dpd));
                $corporate = $this->Mcorporate->getCorporateByName(trim($corporate));


                // if(count($corporate)< 1){

                //     $corporate= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue(); 

                //     $ex_corp = explode(" ", $corporate);
                    
                //     if(count($ex_corp) > 1){

                //         if($ex_corp[0]=="PD."){

                //             $corporate = $this->Mcorporate->getCorporateByName("PD");

                //         }else if($ex_corp[0]=="PT."){

                //             $corporate = $this->Mcorporate->getCorporateByName(trim($ex_corp[1]));

                //         }

                //     }else{

                //         $corporate = $this->Mcorporate->getCorporateByName("BPR");

                //     }

                // }

                $data = array(
                    "name" => trim($name), 
                    "corporate" => @$corporate[0]['id'],
                    "dpd" => @$dpd[0]['id'],
                    "address" => trim($address),
                    "telp" => trim($telp),
                    "email" => trim($email),
                    "fax" => trim($fax),
                    "website" => trim($website),
                );

                $exist = $this->db->get_where($this->table, array('name' => trim($name)));

                if($exist->num_rows()<1){

                    $this->db->insert($this->table,$data);

                }else{

                    $data = array(
                        
                        "corporate" => @$corporate[0]['id'],
                        "dpd" => @$dpd[0]['id'],
                        "address" => trim($address),
                        "telp" => trim($telp),
                        "email" => trim($email),
                        "fax" => trim($fax),
                        "website" => trim($website),

                    );

                    $row = $exist->result_array();
                    $this->db->where($this->pk, @$row[0]['id']);
                    $this->db->update($this->table,$data);

                }

        }   

        return true;

    }

}