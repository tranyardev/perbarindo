<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

 class Dashboard extends MX_Controller{

     function __construct(){
         parent::__construct();
         $this->load->library("Aauth");
         $this->load->library('email');
         $this->load->model("MDashboard");
         $this->load->model("master/Mbprbranch");
         $this->load->model("master/Mbprproduct");
         $this->load->model("master/Mbprarticle");
         $this->load->model("master/Mbprvalidates");
         $this->load->model("mcore");

     }

     function index(){

         if($this->aauth->is_loggedin()){
            // print_r($this->session->userdata);exit;

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

            $data['introjs'] = "";

            if($this->user_group == "Admin Bpr"){

                $member = $this->mcore->getMemberByUser($this->session->userdata('id'));

                $data['upcoming_events'] = $this->MDashboard->getUpcomingEvent();
                $validation = $this->Mbprvalidates->getBPRValidatesByBPR($member['bpr']);

               
                $data['introjs'] = "introJs().start();";

                if(count($validation)>0){ 

                    // ini yang buat presentasi
                    if($validation['is_verified']=='1'){
                        $data['introjs'] = '';
                    }

                    // ini yang buat production
                    /*if(count($director_position)>0){ 
                        // $ret = '';
                    }*/
                }


                 

                $data['page_title'] = $member['corp']." ".$member['bpr_name'];
                $data['page_subtitle'] = 'Dashboard';
                $data['page'] = 'dashboard_bpr';
                $data['data']['bpr'] = $member;
                $data['data']['branch'] = $this->Mbprbranch->getCountBPRBranch($member['bpr']);
                $data['data']['product'] = $this->Mbprproduct->getCountBPRProduct($member['bpr']);
                $data['data']['article'] = $this->Mbprarticle->getCountBPRArticle($member['bpr']);

            }else if($this->user_group == "Admin DPD"){

                redirect('master/bpr');

            }else{

                $data['page_title'] = 'Dashboard';
                $data['page_subtitle'] = 'Data Summary';
                $data['page'] = 'dashboard';

             }
             
            
             $data['data']['parent_menu'] = 'dashboard';
             $data['data']['submenu'] = '';
           
             $data['data']['lastest_registered_users'] = $this->MDashboard->getLatestRegisteredUsers();
             $this->load->view('layout/body',$data);
         }else{
            // $data['data']['username'] = $_POST['username'];
            // $data['data']['password'] = $_POST['password'];

            $data['data']['username'] = "pass";
            $data['data']['password'] = "word";
             $this->load->view('layout/login', $data);
         }
     }

     function upload_file_tmp(){


        if(count($_FILES)>0){

            $tmp_name = $_POST['tmp_name'];
            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/tmp/';
            $array = explode('.', $_FILES[$tmp_name]['name']);
            $extension = end($array);
            $attachement = md5(uniqid(rand(), true)).".".$extension;

            if (move_uploaded_file($_FILES[$tmp_name]["tmp_name"], $upload_path."/".$attachement)) {

                $res['msg'] = "Loading Preview";
                $res['url'] = base_url().'upload/tmp/'.$attachement;
                $res['status'] = "1";

            }else{

                $res['msg'] = "Oops! Something went wrong!";
                $res['status'] = "0";

            }

            echo json_encode($res);

        }   


     }

     function register(){

        if(count($_POST)>0){

            // Verify user's answer
            $response = $this->recaptcha->verifyResponse($_POST['g-recaptcha-response']);

            // Processing ...
            if ($response['success']) {
                
                $this->aauth->create_user($_POST['email'],$_POST['password'],$_POST['username']);

                $last_insert_id = $this->db->insert_id();

                if($last_insert_id!=null){

                    $ver_key = "cInT4TAKH4RUSM3MILIKI";
                    $ver_code = md5($_POST['email'].$ver_key);
                    $activation_link = base_url().'verify/user/'.$ver_code;

                    $update = array(
                        "first_name" => $_POST['first_name'], 
                        "last_name" => $_POST['last_name'],
                        "verification_code" => $ver_code,
                        "status" => "0"
                    );

                    $this->db->where("id", $last_insert_id);
                    $this->db->update("aauth_users", $update);

                    $_POST['activation_link'] = $activation_link;

                    $this->send_verification($_POST);

                }

                $data['granted'] = true;

            } else {
                
                $data['granted'] = false;

            }

            echo json_encode($data);
            

        }else{
            $this->load->view('layout/register');     
        }

     }

     function send_verification($data){

     

        $mail_template = "
        <html>
            <head>
            <style>

            </style>
            </head>
            <body>
              <p>Hi, ".$data['first_name']." ".$data['last_name']."</p>
              <br>
              <br>
              <p>Satu langkah lagi untuk dapat menjadi developer tranyar! Silahkan aktifasi akun anda dengan mengeklik tautan berikut ini :</p>

              <br>
              <br>
              <br>
              ".$data['activation_link']."

              <br>
              <br>
              <br>
              <p>Abaikan email ini apabila anda merasa tidak pernah daftar sebagi developer di tranyar.</p>
            </body>
        </html>
        ";

        $config = array(
            "to" => $data['email'],
            "subject" => "Tranyar Developer Activation Account",
            "body" => $mail_template
        );

        if($this->sendMail($config)){
            return true;
        }else{
            return false;
        }


     }

     function sendMail($data){


        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "developer.tranyar@gmail.com"; 
        $config['smtp_pass'] = "devtranyar@4k535";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->email->initialize($config);

        $this->email->from('indraespada02@gmail.com', 'Developer');
        $this->email->to($data['to']);
        $this->email->subject($data['subject']);
        $this->email->message($data['body']);

        if($this->email->send()){
            return true;
        }else{
            return false;
        }


     }
     function verify_user($vercode){

        $user = $this->db->query("SELECT * FROM aauth_users WHERE status = '0' AND verification_code='".$vercode."'")->result_array();

        if(count($user)>0){

            $this->db->where("id", @$user[0]['id']);
            $update = $this->db->update("aauth_users", array("status" => '1'));

            if($update){

                $data['status'] = 1;
                $data['response'] = "Akun telah berhasil diaktifkan! Terimakasih telah bergabung sebagai tranyar Developer. Silahkan <a href='".base_url()."'>Login</a> untuk mulai berkontribusi.";

            }else{

                $data['status'] = 0;
                $data['response'] = "Oops! Telah terjadi kesalahan. Mohon kontak developer kami bila anda membaca pesan ini.";

            }

        }else{

            $data['status'] = 0;
            $data['response'] = "Oops! Tautan sudah tidak valid.";

        }

        $this->load->view("layout/verification", $data);

     }

     function manage_menus(){

         $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
         $this->read_perm = 'cms_menus_view';
         $this->add_perm = 'cms_menus_add';
         $this->edit_perm = 'cms_menus_update';
         $this->delete_perm = 'cms_menus_delete';

         $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

         $data['page'] = 'layout/manage_menu';
         $data['page_title'] = 'CMS Menus';
         $data['page_subtitle'] = 'Manage Backend Menus';
         $data['data']['parent_menu'] = 'developer';
         $data['data']['submenu'] = 'cms_menu';
         $data['data']['permission'] = $this->mcore->getPermission();
         $data['data']['menus'] = $this->mcore->getMenus();
         $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
         $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
         $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
         $this->load->view('layout/body',$data);


     }

     function rearrange_menu_position(){

         if(count($_POST)>0){

             $a = array_keys($_POST['menuItem']);
             $b = count($a);

             $parent_seq = 1;
             $child_seq = 1;

             for($i=0;$i < $b;$i++){

                 $menu_id = $a[$i];

                 $menu_parent = $_POST['menuItem'][$menu_id];

                 if($menu_parent!="null"){

                     $this->mcore->setParentMenu($menu_id, $menu_parent, $child_seq);

                     $child_seq++;

                 }else{

                     $child_seq = 1;

                     $this->mcore->setParentMenu($menu_id, $menu_parent, $parent_seq);

                     $parent_seq++;

                 }
             }


             echo json_encode(array("status" => 1, "msg" => "Menus position successfully updated!"));
         }

     }

     function insert_menu(){

         if(count($_POST)>0){


             if($_POST['parent']!=""){

                 $last_squence = $this->mcore->getMenuLastSquence($_POST['parent']);

                 $seq = $last_squence + 1;

                 $insert = array(

                     "name" => $_POST['name'],
                     "display_name" => $_POST['display_name'],
                     "parent" => $_POST['parent'],
                     "fa_icon" => $_POST['fa_icon'],
                     "sequence" => $seq,
                     "path" => $_POST['path'],
                     "slug" => $_POST['slug'],
                     "permission" => $_POST['permission'],
                     "status" => $_POST['status']

                 );


             }else{

                 $last_squence = $this->mcore->getMenuLastSquence();

                 $seq = $last_squence + 1;

                 $insert = array(

                     "name" => $_POST['name'],
                     "display_name" => $_POST['display_name'],
                     "fa_icon" => $_POST['fa_icon'],
                     "sequence" => $seq,
                     "path" => $_POST['path'],
                     "slug" => $_POST['slug'],
                     "permission" => $_POST['permission'],
                     "status" => $_POST['status']

                 );


             }

             $result = $this->db->insert("menus", $insert);

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully add menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function update_menu(){

         if(count($_POST)>0){


             $update = array(

                 "name" => $_POST['name'],
                 "display_name" => $_POST['display_name'],
                 "fa_icon" => $_POST['fa_icon'],
                 "path" => $_POST['path'],
                 "permission" => $_POST['permission'],
                 "status" => $_POST['status']

             );

             $this->db->where("menu_id", $_POST['menu_id']);
             $result = $this->db->update("menus", $update);

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully update menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function delete_menu(){

         if(count($_POST)>0){

             $this->db->where("menu_id", $_POST['menu_id']);
             $result = $this->db->delete("menus");

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully delete menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function change_skin(){

         if(count($_POST)>0){

             $this->db->where("scope","LAYOUT");
             $this->db->where("key","SKIN");
             $this->db->update("configurations",array(
                 "value" => $_POST['skin']
             ));

         }

     }

     function getEventDateOnCurrentMonth(){

         // $dates = $this->MDashboard->getEventDateOnCurrentMonth();
         // echo json_encode($dates);

     }

     function getCurrenYearVisitorStat(){

         $current_year = date("Y");
         $months = array("01","02","03","04","05","06","07","08","09","10","11","12");

         for($i=0;$i < count($months);$i++){

             $month =  $current_year.'-'.$months[$i].'-01';

             $y = $this->MDashboard->getCurrentYearVisitorStat($month);
             $x = $current_year.'-'.$months[$i];

             $data[] = array( "m" => $x, "a" => $y );

         }

         echo json_encode($data);


     }

     function getVisitorStatByCity(){

         $data = $this->MDashboard->getgetVisitorStatByCity();
         echo json_encode($data);

     }

     function update_profile(){

         if(count($_POST)>0){


             /* upload cover */

             if($_FILES["photo"]["name"]!=""){

                 if($_FILES["photo"]["type"]=="image/png" or
                     $_FILES["photo"]["type"]=="image/jpg" or
                     $_FILES["photo"]["type"]=="image/jpeg"){

                     $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                     $array = explode('.', $_FILES['photo']['name']);
                     $extension = end($array);
                     $photo = md5(uniqid(rand(), true)).".".$extension;

                     $old_photo = $upload_path.'/'.@$_POST['old_pp'];

                     if(file_exists($old_photo)){

                         @unlink($old_photo);

                     }


                     if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                     }else{


                         $res['msg'] = "Oops! Something went wrong!";
                         $res['status'] = "0";

                         echo json_encode($res);
                         exit;

                     }

                 }else{


                     $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                     $res['status'] = "0";

                     echo json_encode($res);
                     exit;

                 }

             }else{

                 $photo = $this->input->post("old_pp");

             }

             $data = array(
                 "first_name" => $this->input->post("first_name"),
                 "last_name" => $this->input->post("last_name"),
                 "picture" => $photo,
             );

             $this->db->where("id", $this->session->userdata('id'));
             $update = $this->db->update("aauth_users", $data);

             if($update){

                 $newdata = array(
                     "first_name" => $this->input->post("first_name"),
                     "last_name" => $this->input->post("last_name"),
                     "picture" => $photo,
                 );

                 $this->session->set_userdata($newdata);

                 $res = array("status" => "1", "msg" => "Successfully update data!");

             }else{

                 $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

             }

             echo json_encode($res);

         }

     }

     function update_password(){

         if(count($_POST)>0){

             if($this->aauth->login($this->session->userdata("username"), $this->input->post("old_password"))){

                 $update = $this->aauth->update_user($this->session->userdata("id"), $this->session->userdata("email"), $this->input->post("new_password"), $this->session->userdata("username"));

                 if($update){

                     $res = array("status" => "1", "msg" => "Successfully update data!");

                 }else{

                     $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

                 }

             }else{

                 $res = array("status" => "0", "msg" => "Invalid old password ! Please try again.");

             }

             echo json_encode($res);

         }

     }

     function update_widget_position(){

         if(count($_POST)>0){

             $data = array(
                "section" => $this->input->post('section'),
                "position" => $this->input->post('position'),
             );
             $this->db->where("widget_id",$this->input->post('id'));
             $this->db->update("widgets",$data);

         }


     }

     function test(){

        $data = $this->tportal->getAddonStore();

        echo "<pre>";
        print_r($data);
        echo "</pre>";

     }


 }