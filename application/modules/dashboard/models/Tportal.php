<?php


class Tportal extends CI_Model{

	function __construct(){
		
		parent::__construct();

		$this->db_portal = $this->load->database('tranyar_portal', TRUE);


	}

	function getAddonStore(){

		$query = $this->db_portal->query("SELECT * FROM tranyar_portal.addons_store")->result_array();

		return $query;

	}

	function insertModule($data){

		$result = $this->db_portal->insert("addons_store", $data);

		return $result;

	}

}