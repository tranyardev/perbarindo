<?php
    if($bpr['latitude']==""){
        $bpr['latitude']="0";
    }
    if($bpr['longitude']==""){
        $bpr['longitude']="0";
    }
?>
<div class="row">
    <section class="col-md-6">

      <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">BPR INFO</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="row"> 
                    <div class="col-lg-12 col-md-12">
                        <table class="table">
                            <tr class="hidden-lg visible-md visible-sm visible-xs">
                                <td>
                                   <div class="bpr-logo" style="text-align: center">
                                          <?php

                                            if($bpr['logo']!=null){
                                                $path = "upload/bpr";
                                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                                $file = $upload_path.'/'.$bpr['logo'];

                                                if(file_exists($file)){
                                                    $logo = base_url().$path.'/'.$bpr['logo'];
                                                }else{
                                                    $logo = base_url().'public/assets/img/bpr_default.png';
                                                }
                                            }else{
                                                $logo = base_url().'public/assets/img/bpr_default.png';
                                            }
                                               


                                            ?>
                                        <img src="<?php echo $logo; ?>" class="img-rounded" alt="<?php echo $bpr['corp']; ?> <?php echo $bpr['bpr_name']; ?>" style="width:150px;">
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td class="visible-lg hidden-md hidden-sm hidden-xs">
                                   <div class="bpr-logo" style="text-align: center">
                                          <?php

                                            if($bpr['logo']!=null){
                                                $path = "upload/bpr";
                                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                                $file = $upload_path.'/'.$bpr['logo'];

                                                if(file_exists($file)){
                                                    $logo = base_url().$path.'/'.$bpr['logo'];
                                                }else{
                                                    $logo = base_url().'public/assets/img/bpr_default.png';
                                                }
                                            }else{
                                                $logo = base_url().'public/assets/img/bpr_default.png';
                                            }
                                               


                                            ?>
                                        <img src="<?php echo $logo; ?>" class="img-rounded" alt="<?php echo $bpr['corp']; ?> <?php echo $bpr['bpr_name']; ?>" style="width:150px;">
                                    </div> 
                                </td>
                                <td>
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Nama</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['corp']; ?> <?php echo $bpr['bpr_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>DPD</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['dpd_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['bpr_address']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Telp</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['telp']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Fax</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['fax']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['bpr_email']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Website</td>
                                                <td>:</td>
                                                <td><?php echo $bpr['website']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
               
                
            </div>
           
        </div>

    </section>
    <section class="col-md-6">

     <div class="box box-danger box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">LOKASI</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
               
               <div id="map" style="width: 100%;height: 279px;"></div>
                
            </div>
    
        </div>

    </section>
</div>
<div class="row">
    <section class="col-md-6">

     <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">PERBARINDO UPCOMING EVENT</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
               
                    <?php $upcoming_events = $this->MDashboard->getLastUpcomingEvent(); ?>

                        <ul class="products-list product-list-in-box">

                        <?php 
                            if(count($upcoming_events)>0){

                                foreach($upcoming_events as $event){

                                    $s_date=date_create($event['start_date']);
                                    $start_date = date_format($s_date,"d-m-Y");

                                    $e_date=date_create($event['end_date']);
                                    $end_date = date_format($e_date,"d-m-Y");

                                    if($event['cover'] != ""){

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                                        $file = $upload_path.'/'.$event['cover'];

                                        if(file_exists($file)){

                                            $path = base_url().'upload/event/'.$event['cover'];

                                        }else{

                                            $path = base_url().'assets/dist/img/default_img.jpg';

                                        }
                                    }else{

                                    }

                                    echo '
                                            <li class="item">
                                                <div class="product-img">
                                                    <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                </div>
                                                <div class="product-info">
                                                    <a href="'.base_url().'show/event/'.$event['id'].'" target="_blank" class="product-title">'.$event['name'].'</a>
                                                    <span class="product-description">
                                                        <span><i class="fa fa-user"></i> '.$event['author'].'</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> '.$start_date.' - '.$end_date.'</span>
                                                    </span>
                                                </div>
                                            </li>
                                        ';

                                }

                            }else{

                               echo "<p class='no-data' style='padding: 120px;'>No data available</p>";

                            }
                    ?>        

                    </ul>
                
             
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                
            </div>
            <!-- /.box-footer -->
        </div>

    </section>
    <section class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo $product;?></h3>

                        <p>Produk</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-earth"></i>
                    </div>
                    <a href="<?php echo base_url();?>master/bprproduct" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-12">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo $article;?></h3>

                        <p>Artikel</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-paper-outline"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>master/bprarticle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
           
        </div>
        <div class="row">
            
            <div class="col-md-12">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo $branch;?></h3>

                        <p>Cabang</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-merge"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>master/bprbranch" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
           
        </div>
    </section>
</div>
<div class="modal" id="modal_notif_event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Event Baru</h4>
            </div>
            <div class="modal-body">
                <?php
                     $html = ' 
                                    <ul class="products-list product-list-in-box">';

                                        if(count($upcoming_events)>0){

                                            foreach($upcoming_events as $event){

                                                $s_date=date_create($event['start_date']);
                                                $start_date = date_format($s_date,"d-m-Y");

                                                $e_date=date_create($event['end_date']);
                                                $end_date = date_format($e_date,"d-m-Y");

                                                if($event['cover'] != ""){

                                                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                                                    $file = $upload_path.'/'.$event['cover'];

                                                    if(file_exists($file)){

                                                        $path = base_url().'upload/event/'.$event['cover'];

                                                    }else{

                                                        $path = base_url().'assets/dist/img/default_img.jpg';

                                                    }
                                                }else{

                                                }

                                                $html.='<li class="item">
                                                            <div class="product-img">
                                                                <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                            </div>
                                                            <div class="product-info">
                                                                <a href="'.base_url().'show/event/'.$event['id'].'" target="_blank" class="product-title">'.$event['name'].'</a>
                                                                <span class="product-description">
                                                                    <span><i class="fa fa-user"></i> '.$event['author'].'</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> '.$start_date.' - '.$end_date.'</span>
                                                                </span>
                                                            </div>
                                                        </li>';

                                            }

                                        }else{

                                            $html.="<p class='no-data'>No data available</p>";

                                        }


                    $html.='</ul>
                               ';

                    echo $html;
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn_cancel" data-dismiss="modal">Tutup</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        initMap();

        <?php echo $introjs; ?>

        var new_event = '<?php echo count($upcoming_events); ?>';

        if(parseInt(new_event) > 0){

            $('#modal_notif_event').modal({
                backdrop: false,
                show: true
            });

        }
    });

    function initMap() {

        var loc = {lat: parseFloat('<?php echo $bpr['latitude']; ?>'), lng: parseFloat('<?php echo $bpr['longitude']; ?>')}

        var map = new google.maps.Map(document.getElementById('map'), {
            center: loc,
            zoom: 13
        });
        

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: loc,
            map: map
        });

    }
    function bookEvent(event_id){

        window.location.href=base_url + 'event/registration/' + event_id;

    }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places"></script>


