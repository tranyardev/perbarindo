 
<div class="wrapper margin-topfix">
          <div class="page-header page-header-small" filter-color="green" > 
                <div class="page-header-image" data-parallax="true" style="background-image: url('http://perbarindo.tranyar.com/mocukup/Bghijau.png');">
                </div> 
            </div>  
            <div class="section">
                <div class="container">
                <br>
                <section>
                    <div class="row margin margin-topfxsxx" style="margin-top: -435px"> 
                        <div class="col-md-12">
                            <section>
                                <div class="card">
                                    <div class="bg-white">
                                        <div class="card-head row">
                                            <div class="card-img col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <img src="<?php echo setEventImage($event['cover']) ?>" class="lazy img-fluid img-responsive" width="100%">
                                            </div>
                                            <div class="card-heading col-md-4 col-sm-12 col-sm-12">  
                                                 <div class="l-pad-top-6 l-pad-bot-2 l-lg-pad-bot-4 l-pad-hor-6">
                                                    <div class="listing-hero-header hide-small ">
                                                        <time class="listing-hero-date" datetime="2017-10-21">
                                                            <span class="listing-hero-image--month" style="font-size: 20px;">
                                                                <?php
                                                                    $date=date_create($event['start_date']);
                                                                    echo date_format($date,"M");
                                                                 ?>
                                                            </span>
                                                            <span class="listing-hero-image--day">
                                                                <?php
                                                                    $date=date_create($event['start_date']);
                                                                    echo date_format($date,"d");
                                                                 ?>
                                                            </span>
                                                        </time>
                                                    </div>
                                                    <div class="listing-hero-body ">
                                                        <h1 class="listing-hero-title" data-automation="listing-title"><?php echo $event['name']; ?></h1>
                                                        <meta content="<?php echo $event['name']; ?>">
                                                        <div class="l-mar-top-3">
                                                            <div class="l-media clrfix listing-organizer-title">
                                                                <div class="l-align-left">
                                                                    <a href="#listing-organizer" class="js-d-scroll-to listing-organizer-name text-default" data-d-duration="1500" data-d-offset="-70" data-d-destination="#listing-organizer" dorsal-guid="c4b13822-b124-5d86-db1b-ecdd65393a23" data-xd-wired="scroll-to">
                                                                by <?php echo $event['first_name']; ?>
                                                            </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="js-display-price-container listing-hero-footer hide-small hide-medium" data-automation="micro-ticket-box-price">
                                                        <div class="js-display-price">
                                                            <i class="fa fa-map-marker"></i> <?php echo $event['city']; ?>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body border-min-height">
                                         <div class="row">
                                             <div class="col-md-8 ">
                                                 <div class="btn--register text-left">
                                                    
                                                 
                                                     <button id="bookmarkme" class="btn"><i class="fa fa-bookmark-o fa-2x" aria-hidden="true"></i></button>
                                                 </div>
                                             </div> 
                                             <div class="col-md-4">
                                                 <div class="btn--register text-right">
                                                    <?php 
                                                        $current_date = date('Y-m-d');
                                                        $current_date = date('Y-m-d', strtotime($current_date));;
                                                    
                                                        $reg_start_date = date('Y-m-d', strtotime($event['registration_start_date']));
                                                        $reg_end_date = date('Y-m-d', strtotime($event['registration_end_date']));

                                                        if (($current_date > $reg_start_date) && ($current_date < $reg_end_date))
                                                        {
                                                          $disabled = "";
                                                        }
                                                        else
                                                        {
                                                          $disabled = "disabled";
                                                        }

                                                    ?>
                                                     <button <?php echo $disabled; ?> class="btn btn-warning btn-lg btn-block" onclick="bookEvent('<?php echo $id; ?>')">REGISTER</button>
                                                 </div>
                                             </div>
                                         </div>
                                    </div> 
                                </div>
                            </section>
                        </div>

                         <div class="col-md-12">
                            <section>
                                <div class="card" >
                                   <div class="row">
                                        <div class="card-content col-md-8">
                                            <!-- ============= CONTENCT CENTER =====================--> 
                                            <article >
                                                <h3 class="label-primary">Description</h3>
                                                <div class="has-user-generated-content js-d-read-more read-more js-read-more read-more--medium-down read-more--expanded" dorsal-guid="004aa28b-5df8-1095-fb8e-f648f8b1299b" data-xd-wired="read-more">
                                                    <div class="js-xd-read-more-toggle-view read-more__toggle-view">
                                                        <div class="js-xd-read-more-contents l-mar-top-3" data-automation="listing-event-description">
                                                            <p><span><span><strong><?php echo $event['name']; ?></strong><br></span></span>
                                                            </p>
                                                            <p><?php echo $event['description']; ?></p>
                                                            <p><span><span></span></span>
                                                                <br><span><span></span></span>
                                                            </p>
                                                            <p><span><span> <br></span></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                               
                                                </div>
                                            </article> 

                                            <div class="box-share">
                                                <div class="listing-info__footer clrfix">
                                                    <div class="share-wrapper l-sm-mar-bot-4">
                                                        <h3 class="label-primary l-sm-align-left">Share with friends</h3>
                                                        <!-- Listing Share Options -->
                                                        <div class="js-share-wrapper l-sm-align-left">
                                                            <section>
                                                                <section>
                                                                    <ul class="grouped-ico grouped-ico--share-links js-social-share-list fx--fade-in" data-automation="social-share-inline">
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round " href="https://www.facebook.com/sharer.php?u=<?php echo base_url().'show/event/'.$id;?>" target="_blank" title="Share this page on Facebook">
                                                                                <i class="ico-facebook-badge ico--color-understated-link ico--xlarge" data-automation="facebook-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-facebook-official"></i> Facebook</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://www.linkedin.com/shareArticle?url=<?php echo base_url().'show/event/'.$id;?>&title=<?php echo $event['name']; ?>" target="_blank" title="Share this page on LinkedIn">
                                                                                <i class="ico-linkedin-badge ico--color-understated-link ico--xlarge" data-automation="linkedin-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-linkedin-square"></i> LinkedIn</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://twitter.com/intent/tweet?url=<?php echo base_url().'show/event/'.$id;?>&text=<?php echo $event['name']; ?>&via=<?php echo base_url();?>&hashtags=<?php echo str_replace(" ","_",$event['name']); ?>" target="_blank" title="Share this page on Twitter">
                                                                                <i class="ico-twitter-badge ico--color-understated-link ico--xlarge" data-automation="twitter-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-twitter-square"></i> Twitter</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://plus.google.com/share?url=<?php echo base_url().'show/event/'.$id;?>" target="_blank" title="Share this page on google">
                                                                                <i class="ico-google-badge ico--color-understated-link ico--xlarge" data-automation="google-share-link">
                                                                                 </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-google-plus-official"></i> Google Plus</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </section>
                                                            </section>
                                                        </div>
                                                    </div>
                                                    <!-- Fallback anchor for the link to the ticket area -->
                                                    <span id="listing-ticket"></span>
                                                </div>
                                            </div>

                                            <!-- ============= END CONTENT CONTER ==================-->
                                        </div>
                                       <div class="card-content-right col-md-4">
                                        
                                            <div class="event-details l-lg-mar-left-6 l-lg-pad-left-6">
                                                <h3 class="label-primary l-mar-bot-2" data-automation="listing-info-language">Date</h3>
                                                <div class="event-details-data">
                                                    <meta content="2017-10-21T13:00:00+07:00">
                                                    <meta content="2017-10-21T17:00:00+07:00">
                                                    <time class="clrfix" data-automation="event-details-time">
                                                        <?php
                                                                $sdate=date_create($event['start_date']);
                                                                $sdaynum = date_format($sdate,"d");
                                                                $sday = date_format($sdate,"l");
                                                                $smonth = date_format($sdate,"F");
                                                                $syear = date_format($sdate,"Y");

                                                                $edate=date_create($event['end_date']);
                                                                $edaynum = date_format($edate,"d");
                                                                $eday = date_format($edate,"l");
                                                                $emonth = date_format($edate,"F");
                                                                $eyear = date_format($edate,"Y");
                                                        ?>
                                                        <p>Start : <?php echo $sday.' , '.$smonth.' '.$sdaynum.' '.$syear ?></p>
                                                        <p>End : <?php echo $eday.' , '.$emonth.' '.$edaynum.' '.$eyear ?></p>
                                                       
                                                    </time>
                                                </div>
                                                <h3 class="label-primary l-mar-bot-2">Location</h3>
                                                <div class="event-details-data">
                                                    <p> <?php echo $event['address'] ?></p>
                                                    <p> <?php echo $event['city'] ?> </p>
                                                    <p> <?php echo $event['country'] ?> </p>
                                                    
                                                </div>
                                                <h3 class="label-primary l-mar-bot-2">Price</h3>
                                                <div class="event-details-data">
                                                    
                                                    <ul>
                                                        <?php foreach($event_packages as $package){ ?>

                                                            <li><?php echo $package['name']; ?> - Rp <?php echo number_format($package['price'],2,",","."); ?></li>
                                                        
                                                        <?php } ?>
                                                    </ul>
                                                    
                                                </div>
                                            </div> 
                                       </div>
                                   </div>
                                   <div class="row">
                                       <div class="col-md-12">
                                       
                                            <section class="list-timeline" id="list-timeline">
                                                <div class="timeline-body"> 
                                                    <article class="border-timline">
                                                        <div class="text-center">
                                                            <button data-toggle="collapse" data-target="#timline" class="btn btn-primary btn-simple btn-round fa-lg"><i class="fa  fa-history fa-lg"></i> Event Activity</button> 
                                                        </div>
                                                        <div id="timline" class="collapse show">
                                                        <div class="timeline-content">
                                                        <hr style="margin-top: 0rem;">
                                                            <div class="head-timeline text-center">
                                                                <h2 class="margin-b-0">SUSUNAN ACARA</h2>
                                                                <h4 class="margin-b-0"><?php echo $event['name']; ?></h4>
                                                                <h3 class="margin-b-0">Timeline Event</h3>
                                                            </div>
                                                            <div class="time-line-event">


                                                                    <div class="container"> 
                                                                        <ul class="timeline">

                                                                            <?php 
                                                                                $i=1;
                                                                                foreach($event_activity_dates as $ad){ 

                                                                                    $d=date_create($ad['date']);
                                                                                    $daynum = date_format($d,"d");
                                                                                    $day = date_format($d,"l");
                                                                                    $month = date_format($d,"F");
                                                                                    $year = date_format($d,"Y");

                                                                                    if($i%2==0){
                                                                            ?>
                                                                             <li class="timeline-inverted">
                                                                              <div class="timeline-badge warning"><i class="fa fa-history fa-lg"></i></div>
                                                                              <div class="timeline-panel">
                                                                                <div class="timeline-heading head-timeline">
                                                                                  <h4 class="timeline-title"> <?php echo $day.' , '.$month.' '.$daynum.' '.$year ?></h4>
                                                                                </div>
                                                                                <div class="timeline-body">
                                                                                    <table class="table table-timline">
                                                                                    <?php 
                                                                                        $activities = getEventActivities($id, $ad['date']);
                                                                                        foreach($activities as $act){
                                                                                    ?>
                                                                                        <tr class="row">
                                                                                             <td class="col-lg-3 col-md-4 col-xs-12 date-timeline"><?php echo $act['start_time']; ?> - <?php echo $act['end_time']; ?></td>
                                                                                             <td class="col-lg-9 col-md-8 col-xs-12"><?php echo $act['activity']; ?></td>
                                                                                        </tr>

                                                                                    <?php } ?>
                                                                                    </table>
                                                                                </div>
                                                                              </div>
                                                                            </li>

                                                                            <?php }else{ ?>

                                                                            <li>
                                                                              <div class="timeline-badge danger  "><i class="fa fa-history fa-lg"></i></div>
                                                                              <div class="timeline-panel">
                                                                                <div class="timeline-heading head-timeline">
                                                                                  <h4 class="timeline-title"><?php echo $day.' , '.$month.' '.$daynum.' '.$year ?></h4>
                                                                                   
                                                                                </div>
                                                                                <div class="timeline-body table-responsive">
                                                                                     <table class="table table-timline">
                                                                                        <?php 
                                                                                           
                                                                                            $activities = getEventActivities($id, $ad['date']);
                                                                                            foreach($activities as $act){
                                                                                        ?>
                                                                                            <tr class="row">
                                                                                                 <td class="col-lg-3 col-md-4 col-xs-12 date-timeline"><?php echo $act['start_time']; ?> - <?php echo $act['end_time']; ?></td>
                                                                                                 <td class="col-lg-9 col-md-8 col-xs-12"><?php echo $act['activity']; ?></td>
                                                                                            </tr>

                                                                                        <?php } ?>
                                                                                     </table>
                                                                                </div>
                                                                              </div>
                                                                            </li>

                                                                            <?php 
                                                                                    }

                                                                                    $i++;
                                                                                }

                                                                            ?>

                                                                           
                                                                        </ul> 
                                                                    </div>  
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </article>
                                                </div>
                                            </section>
                                            <section class="map-maker" id="map-target">
                                                <div class="map-body"> 
                                                    <article class="border-map">
                                                        <div class="text-center">
                                                            <button data-toggle="collapse" data-target="#mapmapker" class="btn btn-primary btn-simple btn-round fa-lg"><i class="fa fa-map-marker fa-lg"></i> Map Location</button> 
                                                        </div>
                                                        <div id="mapmapker" class="collapse show">
                                                            <div id="map" style="width: 100%;height: 400px;"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </section>
                                          

                                       </div>
                                   </div>
                                </div>
                            </section>
                            
                        </div>
                        <div class="col-md-12">
                                
                            <section>
                                <div class="albumx text-muted">
                                     <div class="container">
                                        <br/><br/> 

                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <h3 class="section-header l-align-left" aria-label="Popular Events in  Bandung, Indonesia">
                                            <span data-automation="home-popular-events-header" aria-hidden="true">Other Events You May Like </span>
                                            <hr>
                                        </h3>
                                    </div>
                                </div> 

                                <div class="row">

                                <?php foreach($other_events as $oe){ ?>

                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                        <div class="card">
                                            <div class="wrap-card poster-card">
                                                <div class="head-card framex"> 
                                                     <a href="<?php echo base_url().'show/event/'.$oe['id']; ?>"> 
                                                        <img class="img-responsive img-block lazy img-fluid"  data-src="holder.js/100px280/thumb" alt="100%x280"   src="<?php  echo setEventImage($oe['cover']) ?>" data-holder-rendered="true"> 
                                                     </a>
                                                </div>
                                                <div class="body-card">
                                                    <div class="poster-card__body">
                                                          <?php
                                                                $sdate=date_create($oe['start_date']);
                                                                $sdaynum = date_format($sdate,"d");
                                                                $sday = date_format($sdate,"l");
                                                                $smonth = date_format($sdate,"F");
                                                                $syear = date_format($sdate,"Y");

                                                                $edate=date_create($oe['end_date']);
                                                                $edaynum = date_format($edate,"d");
                                                                $eday = date_format($edate,"l");
                                                                $emonth = date_format($edate,"F");
                                                                $eyear = date_format($edate,"Y");
                                                        ?>                                    
                                                        <time class="poster-card__date">

                                                            <?php echo $sday.' , '.$smonth.' '.$sdaynum.' '.$syear ?>

                                                        </time>
                                                        <div class="poster-card__title">
                                                           <a href="<?php echo base_url().'show/event/'.$oe['id']; ?>"> <?php echo $oe['name']; ?></a>
                                                        </div>
                                                        <div class="poster-card__venue">

                                                           <?php echo $oe['city']; ?>

                                                        </div>
                              
                                                    </div>   
                                                </div>     
                                                <div class="poster-card__footer"> 
                                                    <div class="poster-card__tags"> 
                                                           
                                                    </div>
                                                    <div class="poster-card__actions">
                                                        <a class="js-share-event-card share-action" href="<?php echo base_url().'show/event/'.$oe['id']; ?>" data-eid="37867656179" title="Detail" aria-haspopup="true">
                                                           <i class="fa fa-check"></i>
                                                        </a>
                                                        <a class="js-d-bookmark" data-eid="37867656179" data-bookmarked="false" title="show map" data-source-page="home:popular" href="http://maps.google.com/maps?q=<?php echo $oe['latitude']; ?>,<?php echo $oe['longitude']; ?>&z=17" aria-label="Save Event" dorsal-guid="1991d28d-4cdd-81ca-3e37-5ce9278b2747" data-xd-wired="bookmark" target="_blank">
                                                            <i class="fa fa-map-marker"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  
                                <?php } ?>

                                </div>

                              </div>
                            </div>
                            </section> 
                        </div>
                       
                    </div>
                </section>


                <section>
                      <!-- Sart Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header justify-content-center">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                        <h4 class="title title-up">Add BPR Name</h4>
                                    </div>
                                    <div class="modal-body">
                                       <form>
                                          <div class="form-group">
                                            <label for="email">BPR Name</label>
                                            <input type="email" class="form-control" id="email">
                                          </div>
                                          
                                          <div class="checkbox">
                                            <label><input type="checkbox"> Remember me</label>
                                          </div> 
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-default">Nice Button</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  End Modal -->
                </section>
 
                    </div> 
                </div>
            </div>
            
        </div>

<script type="text/javascript">
    
    $(document).ready(function(){
        initMap();

        $("#bookmarkme").click(function() {
            if (window.sidebar) { // Mozilla Firefox Bookmark
              window.sidebar.addPanel(location.href,document.title,"");
            } else if(window.external) { // IE Favorite
              window.external.AddFavorite(location.href,document.title); }
            else if(window.opera && window.print) { // Opera Hotlist
              this.title=document.title;
              return true;
            }
        });
    });

    function initMap() {

        var loc = {lat: parseFloat('<?php echo $event['latitude']; ?>'), lng: parseFloat('<?php echo $event['longitude']; ?>')}

        var map = new google.maps.Map(document.getElementById('map'), {
            center: loc,
            zoom: 13
        });
        

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: loc,
            map: map
        });

    }
    function bookEvent(event_id){

        window.location.href=base_url + 'event/registration/' + event_id;

    }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places"></script>