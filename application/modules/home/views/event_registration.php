<div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">

        <section>
            <div class="card rowz" style=" margin-top: -444px;">
                <div class="col-md-12">
                          

                        <div class="text-center padding-12">
                            <h3 class="title margin-0" >FORMULIR PENDAFTARAN EVENT</h3>
                            <h6 class="description margin-0"><?php echo $event['name']; ?></h6>
                        </div>

                        <hr>

                        <div class="panel panel-default">
                            <div class="text-center">
                                <div class="panel-heading">Silahkan isi Form di bawah ini</div>
                               
                            </div>
                              <div class="panel-body">




                              <!--==== BIGEN FORM ====-->

                                <div class="padding-12">
                                    <form id="frm-registration">
                                      <input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
                                      <h4>Data Perusahaan/BPR</h4>
                                      <hr>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">DPD :</label></div>
                                        <div class="col-md-9">
                                          
                                            <select class="form-control select2" name="dpd" id="dpd" autocomplete="off">

                                              <option value="">-- Pilih DPD --</option>

                                              <?php foreach($dpd as $d){ ?>
                                              <option value="<?php echo $d['id']; ?>"><?php echo $d['name']; ?></option>
                                              <?php } ?>
                                        
                                            </select>

                                        </div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">BPR :</label></div>
                                        <div class="col-md-9">
                                          
                                            <select disabled name="bpr" class="form-control select2" id="bpr" autocomplete="off">

                                              <option value="">-- Pilih BPR --</option>
                                        
                                            </select>
                                            
                                        </div>
                                      </div>
                                      <div class="row form-group" id="new_bpr" style="display: none">
                                        <div class="col-md-3">&nbsp;</div>
                                        <div class="col-md-9"><input type="text" name="new_bpr"  placeholder="BPR" class="form-control"></div>
                                      </div>

                                       <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">TELP :</label></div>
                                        <div class="col-md-9"><input type="text" name="bpr_telp" class="form-control" id="telp"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">FAX :</label></div>
                                        <div class="col-md-9"><input type="text" name="bpr_fax" class="form-control" id="fax"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">EMAIL :</label></div>
                                        <div class="col-md-9"><input type="text" name="bpr_email" class="form-control" id="email"></div>
                                      </div>
                                      
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">ALAMAT :</label></div>
                                        <div class="col-md-9"><textarea class="form-control" name="bpr_address" id="address"></textarea></div>
                                      </div>

                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">WEBSITE :</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" name="bpr_website" id="website"></div>
                                      </div>

                                      <h4>Data PIC</h4>
                                      <hr>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">NAMA :</label></div>
                                        <div class="col-md-9">
                                          
                                            <select class="form-control select2" name="registrar_name" id="registrar" autocomplete="off">

                                              <option value="">-- Pilih Nama Pendaftar --</option>
                                              <option value="0">belum terdaftar</option>
                                          
                                            </select>
                                            
                                        </div>
                                      </div>
                                      <div class="row form-group" id="new_name" style="display: none">
                                        <div class="col-md-3">&nbsp;</div>
                                        <div class="col-md-9"><input type="text" name="registrar_new_name"  placeholder="Nama" class="form-control"></div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">JABATAN :</label></div>
                                        <div class="col-md-9">
                                          
                                            <select class="form-control select2" name="registrar_job"  id="job_position" autocomplete="off">

                                              <option value="">-- Jabatan --</option>

                                              <?php foreach($job_positions as $job){ ?>
                                              <option value="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></option>
                                              <?php } ?>
                                        
                                            </select>
                                            
                                        </div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">JENIS KELAMIN :</label></div>
                                        <div class="col-md-9">
                                          
                                            <select class="form-control select2" name="registrar_gender"  id="gender" autocomplete="off">

                                              <option value="">-- Jenis Kelamin --</option>
                                              <option value="L">Laki - laki</option>
                                              <option value="P">Perempuan</option>
                                              
                                        
                                            </select>
                                            
                                        </div>
                                      </div>
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">HP :</label></div>
                                        <div class="col-md-9"><input type="text" name="registrar_hp" class="form-control" id="no_hp"></div>
                                      </div>
                                     
                                      <div class="row form-group">
                                        <div class="col-md-3"> <label for="text">EMAIL :</label></div>
                                        <div class="col-md-9"><input type="text" name="registrar_email"  class="form-control" id="email_member"></div>
                                      </div>
                                      
                                      <h4>Data Peserta Event</h4>
                                      <hr>
                                         
                                       <div class="rowx form-groupx ">
                                            <div class="table-responsive">
                                            <table class="table table-bordred table-hovered"  style="width: 100%;">
                                            <thead> 
                                              <tr>
                                               
                                                <th >Nama</th>
                                                <th >Jenis Kelamin</th>
                                                <th >No&nbsp;Hp</th>
                                                <th >Jabatan</th>
                                                <th style="width:30%">Paket</th>
                                              </tr>
                                            </thead>
                                            <tbody class="tabpeserta" id="tabpeserta">
                                              <tr class="member_row">
                                                
                                                <td>
                                                    <select class="form-control member" name="member1_name[]">
                                                       <option value="">-- Nama ---</option> 
                                                       
                                                    </select>

                                                    <div class="member_new_name" style="padding-top: 10px;">
                                                      <span>Belum terdaftar? :</span><br>
                                                      <input type="text" class="form-control" name="member2_name[]" placeholder="Nama">
                                                    </div>
                                                </td>
                                                <td>
                                                    <select class="form-control jkel" name="member_gender[]">
                                                       <option value="L">Laki-Laki</option> 
                                                       <option value="P">Perempuan</option> 
                                                    </select>
                                                </td>
                                                <td><input type="text" class="form-control" placeholder="No Hp" name="member_no_hp[]">

                                                </td> 
                                                <td>
                                                    <select class="form-control position" name="member_position[]">
                                                        <?php foreach($job_positions as $job){ ?>
                                                        <option value="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control package" name="member_package[]">
                                                       <option value="">Pilih Paket</option> 
                                                        <optgroup label="<?php echo $event['name']; ?> :">
                                                            <?php 
                                                              foreach($event_packages as $package){
                                                            ?>
                                                            <?php if($package['twin_sharing']!="1"){ ?>
                                                             <option value="<?php echo $package['id']; ?>-<?php echo $package['price']; ?>"><?php echo $package['name']; ?> Rp <?php echo number_format($package['price'],2,",","."); ?> / Peserta</option> 
                                                            <?php } } ?>
                                                        </optgroup>
                                                    </select>

                                                </td>
                                              </tr>

                                               
                                               
                                            </tbody>
                                            <tfoot>
                                                
                                            </tfoot>
                                          </table>
                                           <button id="add_attribute" class="btn btn-default btn-xs" type="button">
                                                <i class="fa fa-plus"></i>
                                              </button>

                                              <button id="remove_attribute" disabled class="btn btn-danger btn-xs" type="button">
                                                <i class="fa fa-remove"></i>
                                              </button>
                                          </div>    
                                       </div>

                                      <h4>Peserta Twin Sharing</h4>
                                      <hr>
                                      <div id="tabpesertatwinsharing">
                                        
                                        <div class="rowx form-groupx member_twin_sharing_row">
                                            <div class="table-responsive">
                                            <table class="table table-bordred table-hovered"  style="width: 100%;">
                                            <thead> 
                                              <tr>
                                               
                                                <th >Nama</th>
                                                <th >Jenis Kelamin</th>
                                                <th >No&nbsp;Hp</th>
                                                <th >Jabatan</th>
                                                <th style="width:30%">Paket</th>
                                              </tr>
                                            </thead>
                                            <tbody class="tabpeserta">
                                     
                                                <tr>
                                                  
                                                  <td>
                                                      <select class="form-control member" name="member1_name_twin[]">
                                                         <option value="">-- Nama ---</option> 
                                                      </select>
                                                      <div class="member_new_name" style="padding-top: 10px;">
                                                        <span>Belum terdaftar? :</span><br>
                                                        <input type="text" class="form-control" name="member2_name_twin[]" placeholder="Nama">
                                                      </div>
                                                  </td>
                                                  <td>
                                                      <select class="form-control jkel" name="member_gender_twin[]">
                                                         <option value="L">Laki-Laki</option> 
                                                         <option value="P">Perempuan</option> 
                                                      </select>
                                                  </td>
                                                  <td><input type="text" class="form-control" name="member_no_hp_twin[]" placeholder="No Hp">
                                                    
                                                  </td> 
                                                  <td>
                                                      <select class="form-control position" name="member_job_twin[]">
                                                          <?php foreach($job_positions as $job){ ?>
                                                          <option value="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></option>
                                                          <?php } ?>  
                                                      </select>
                                                  </td>
                                                  <td>
                                                      <select class="form-control package" name="member_package_twin[]">
                                                         <option value="">Pilih Paket</option> 
                                                          <optgroup label="<?php echo $event['name']; ?> :">
                                                              <?php 
                                                                foreach($event_packages as $package){
                                                                  
                                                              ?>
                                                              <?php if($package['twin_sharing']=="1"){ ?>
                                                               <option value="<?php echo $package['id']; ?>-<?php echo $package['price']; ?>"><?php echo $package['name']; ?> Rp <?php echo number_format($package['price'],2,",","."); ?> / Peserta</option> 
                                                              
                                                              <?php } }?>
                                                          </optgroup>
                                                      </select>

                                                  </td>
                                                </tr>
                                                <tr>
                                                  
                                                  <td>
                                                      <select class="form-control member" name="member1_name_twin_with[]">
                                                         <option value="">-- Nama ---</option> 
                                                      </select>
                                                      <div class="member_new_name" style="padding-top: 10px;">
                                                        <span>Belum terdaftar? :</span><br>
                                                        <input type="text" class="form-control" name="member2_name_twin_with[]" placeholder="Nama">
                                                      </div>
                                                  </td>
                                                  <td>
                                                      <select class="form-control jkel" name="member_gender_twin_with[]">
                                                         <option value="">Laki-Laki</option> 
                                                         <option value="">Perempuan</option> 
                                                      </select>
                                                  </td>
                                                  <td><input type="text" class="form-control" name="member_no_hp_twin_with[]" placeholder="No Hp">
                                                    
                                                  </td> 
                                                  <td>
                                                      <select class="form-control position" name="member_job_twin_with[]">
                                                          <?php foreach($job_positions as $job){ ?>
                                                          <option value="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></option>
                                                          <?php } ?>  
                                                      </select>
                                                  </td>
                                                  <td>&nbsp;</td>
                                                </tr>
                                                
                                             
                                            </tbody>
                                            <tfoot>
                                               
                                            </tfoot>
                                          </table>
                                           
                                          </div>
                                        </div>

                                      </div>
                                      
                                            <button id="add_attribute_twin_sharing" class="btn btn-default btn-xs" type="button">
                                              <i class="fa fa-plus"></i>
                                            </button>

                                            <button id="remove_attribute_twin_sharing" disabled class="btn btn-danger btn-xs" type="button">
                                              <i class="fa fa-remove"></i>
                                            </button>
                                       
                                        <div class="pull-right">
                                            
                                            <span class="text-right">Total</span> <br> <span id="cost" style="font-weight: bold"></span>
                                            <input type="hidden" name="total_cost" id="total_cost">

                                        </div>  
                                        <div class="form-group text-center">
                                              <button type="submit" class="btn btn-primary fa-lg"> Submit</button>
                                        </div>

                                        
                                    </form>
                                </div>
                                   
                              <!--==== END FORM ====--> 
                </div>
            </div>
        </section>


        <section>
              <!-- Sart Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header justify-content-center">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                </button>
                                <h4 class="title title-up">Add BPR Name</h4>
                            </div>
                            <div class="modal-body">
                               <form>
                                  <div class="form-group">
                                    <label for="email">BPR Name</label>
                                    <input type="email" class="form-control" id="email">
                                  </div>
                                  
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Remember me</label>
                                  </div> 
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-default">Nice Button</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  End Modal -->
        </section>



            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');

    var cost = 0;

    $('#cost').html(numeral(cost).format("$0,0"));
    $('.member').select2();
    $('.jkel').select2();
    $('.position').select2();
    $('.package').select2();
    $('#job_position').select2();
    $('#gender').select2();
    $('#dpd').select2();
    $('#dpd').on('select2:selecting', function(e) {
       var id = e.params.args.data.id;
       var target = base_url + 'autocomplete/bpr/'+id;

       $.get(target, function(res){

          $('#bpr').html(res);

            $('#bpr').removeAttr("disabled");
            $('#bpr').select2();
            $('#bpr').on('select2:selecting', function(e) {
               var id = e.params.args.data.id;
              
                if(id=="0"){

                  $("#new_bpr").slideDown();

                }else{

                  $("#new_bpr").slideUp();
                  
                  var target = base_url + 'autocomplete/member/'+id; 

                  $.get(target, function(data){

                      $('.member').html(data.member);
                      $('#registrar').html(data.member);
                      $('#registrar').select2();
                      $('#registrar').on('select2:selecting', function(e) {

                         var id = e.params.args.data.id;

                         if(id=="0"){

                            $("#new_name").slideDown();

                         }else{

                            $("#new_name").slideUp(); 

                            var target = base_url + 'member/get/' + id;

                            $.get(target,function(member){

                              $("#no_hp").val(member[0].no_hp);
                              $("#email_member").val(member[0].email);
                              $("#job_position").val(member[0].job_position).trigger('change');
                              $("#gender").val(member[0].gender).trigger('change');

                            },'json');

                         }

                      });

                      $("#telp").val(data.bpr.telp);
                      $("#fax").val(data.bpr.fax);
                      $("#email").val(data.bpr.email);
                      $("#address").val(data.bpr.address);
                      $("#website").val(data.bpr.website);


                  },'json'); 

                }

            });

       });


    });
    $('#bpr').select2();
    $('#registrar').select2();
    $('#registrar').on('select2:selecting', function(e) {

       var id = e.params.args.data.id;

       if(id=="0"){

          $("#new_name").slideDown();

       }else{

          $("#new_name").slideUp();

       }

    });

    var row_attr = $(".member_row");
    var container_attr = $("#tabpeserta");

    $("#add_attribute").click(function(){

      $('.member').select2('destroy');
      $('.jkel').select2('destroy');
      $('.position').select2('destroy');
      $('.package').select2('destroy');
      
      row_attr.clone(true, true).appendTo(container_attr);

      $('.member').select2();
      $('.jkel').select2();
      $('.position').select2();
      $('.package').select2();

      

      if($("#tabpeserta .member_row").length > 1){

        $("#remove_attribute").removeAttr("disabled");

      }


    });

    $("#remove_attribute").click(function(){

      if($("#tabpeserta .member_row").length > 1){

        
        $("#tabpeserta .member_row:last-child").remove();

        var cost = 0;
        
        $(".package").each(function(i, val){

            var value = $(this).val();

            if(value!=""){

              var x = value.split("-");

              cost = parseInt(cost) + parseInt(x[1]); 

            }

        });

        $("#cost").html(numeral(cost).format("$0,0"));
        $("#total_cost").val(cost);


      }

      if($("#tabpeserta .member_row").length < 2){

        $("#remove_attribute").attr("disabled","disabled");

      }

    });

    $(".package").on('change', function(e) {

        var cost = 0;

        $(".package").each(function(i, val){

            var value = $(this).val();

            if(value!=""){

              var x = value.split("-");

              cost = parseInt(cost) + parseInt(x[1]); 

            }

        });

        $("#cost").html(numeral(cost).format("$0,0"));
        $("#total_cost").val(cost);

    });  


    var row_attr_twin_sharing = $(".member_twin_sharing_row");
    var container_attr_twin_sharing = $("#tabpesertatwinsharing");

    $("#add_attribute_twin_sharing").click(function(){

      $('.member').select2('destroy');
      $('.jkel').select2('destroy');
      $('.position').select2('destroy');
      $('.package').select2('destroy');
      
      row_attr_twin_sharing.clone(true, true).appendTo(container_attr_twin_sharing);

      $('.member').select2();
      $('.jkel').select2();
      $('.position').select2();
      $('.package').select2();

      

      if($("#tabpesertatwinsharing .member_twin_sharing_row").length > 1){

        $("#remove_attribute_twin_sharing").removeAttr("disabled");

      }


    });

    $("#remove_attribute_twin_sharing").click(function(){

      if($("#tabpesertatwinsharing .member_twin_sharing_row").length > 1){

        
        $("#tabpesertatwinsharing .member_twin_sharing_row:last-child").remove();

        var cost = 0;
        
        $(".package").each(function(i, val){

            var value = $(this).val();

            if(value!=""){

              var x = value.split("-");

              cost = parseInt(cost) + parseInt(x[1]); 

            }

        });

        $("#cost").html(numeral(cost).format("$0,0"));
        $("#total_cost").val(cost);


      }

      if($("#tabpesertatwinsharing .member_twin_sharing_row").length < 2){

        $("#remove_attribute_twin_sharing").attr("disabled","disabled");

      }

    });
   

    $("#frm-registration").submit(function(){
      	
      	var data = $(this).serialize();
      	var target = base_url + 'submit/registration';

       	var $loading = $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });
     
      	$.post(target,data,function(res){

      		if(res.status=="1"){

      			$loading.hide();

      			window.location.href=base_url + 'checkout/payment/' + res.reg_code;
      			
      		}else{

      		}
        	

      	},'json');

      	return false;

    });
  
  });

 
</script>