 
     <!-- Start Page -->
        <section class="bg-crimeimgx" style="padding-top: -29px;"> 

            <div class="container marketing">
                <div class="seach-head text-center dex-aboutus"  style="margin-top: -29px;">
                    <h1 class="text-heading" style="margin-top: 64px;">
                        <?php echo @$content['title']; ?><br/>
                        <?php
                          if($content['title']=="About Us"){
                        ?>

                        Sejarah BPR

                        <?php } ?>
                    </h1> 
                 </div>
                <div class="cardx bg-crimex"> 
                   <div class="search-travel">
                         
                          
                        <div class="row">
                             
                          <div class="col-sm-12 col-lg-12 padding-0"> 
                              <?php
                                if($content['title']=="About Us"){
                              ?>

                                  <div class="row">
                                      <div class="col col-lg-6 col-md-6 col-12 col-sm-12 col-xs-12">
                                        <div class="img-aboutus"> 
                                            <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/2.png" class="img-responsive img-fluid lazy">
                                        </div>
                                      </div>
                                       <div class="col col-lg-6 col-md-6 col-12 col-sm-12 col-xs-12">
                                         <div class="dex-aboutus">
                                            <h1>Jaman Kolonial Belanda</h1>
                                            <p>
                                              Perkreditan Rakyat di Indonesia dimulai sejak abad ke 19 pada masa penjajahan Belanda dengan berdirinya Bank Kredit Rakyat dan Lumbung Desa yang dibangun dengan tujuan membantu para petani, pegawai dan buruh agar dapat melepaskan diri dari lintah darat (rentenir) yang membebani dengan bunga yang sangat tinggi 

                                              Pada masa pemerintahan kolonial Belanda, perkreditan rakyat dikenal masyarakat dengan istilah Lumbung Desa, Bank desa, Bank Tani, dan Bank Dagang Desa, yang saat itu hanya terdapat di Jawa dan Bali. 

                                              <br><br> Tahun 1929 didirikan pula badan yang menangani kredit di pedesaan, yaitu Badan Kredit Desa (BKD) yang terdapat di pulau Jawa dan Bali. Sementara untuk pengawasan dan pembinaan Pemerintah Kolonial Belanda membentuk kas pusat dan Dinas Perkreditan Rakyat. Mengingat kesatuan dan keseragaman dalam pembinaan bank diperlukan, maka pada 1927 Dinas Perkreditan Rakyat dilebur ke satu instansi yaitu Instansi Kas Pusat (IKP).
                                            </p>
                                         </div>

                                      </div>
                                  </div>

                                  <div class="row">
                                     <div class="col col-lg-6 col-md-6 col-12 col-sm-12 col-xs-12 hidden-lg hidden-md visible-sm visible-xs">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/1.png" class="img-responsive 2mg-fluid lazy">
                                          </div>
                                      </div>
                                      <div class="col col-lg-6 col-md-6 col-12 col-sm-12 col-xs-12">
                                          
                                         <div class="dex-aboutus">
                                            <h1>Setelah Indonesia Merdeka</h1>
                                            <p>
                                              Setelah Indonesia merdeka, pemerintah RI mendorong pendirian bank-bank pasar yang terutama sangat dikenal karena didirikan di lingkungan pasar dan bertujuan untuk memberikan pelayanan jasa keuangan kepada para pedagang pasar. Bank-bank pasar tersebut kemudian berdasarkan pakto 1988 dikukuhkan menjadi Bank Perkreditan Rakyat, sejak itu BPR di Indonesia tumbuh dengan subur. <br><br>
                                              Bank-bank yang didirikan antara 1950-1970 didaftarkan sebagai Perseroan Terbatas (PT), CV, Koperasi, Maskapai Andil Indonesia (MAI), Yayasan dan Perkumpulan. Pada masa tersebut berdiri beberapa lembaga keuangan yang dibentuk oleh pemerintah daerah seperti Bank Karya Produksi Desa (BPKD) di Jawa Barat, Bank Kredit Kecamatan (BKK) di Jawa Tengah, Kredit Usaha Rakyat Kecil (KURK) di Jawa Timur, Lumbung Pitih Nagari (LPN) di Sumatera Barat dan Lembaga Perkreditan Desa (LPD) di Bali.  <br><br>
                                              Pada Oktober 1988 pemerintah menetapkan kebijakan deregulasi perbankan yang dikenal sebagai pakto 88 (Paket Oktober 1988). Sebagai kelanjutan pakto 1988, pemerintah mengeluarkan beberapa paket ketentuan di bidang perbankan yang merupakan penyempurnaan ketentuan sebelumnya. Sejalan dengan itu, pemerintah menyempurnakan undang-undang no. 14 tahun 1967 tentang pokok-pokok perbankan dengan mengeluarkan undang-undang no. 7 tahun 1992 tentang perbankan. Undang-undang tersebut disempurnakan lebih lanjut dalam undang-undang no. 10 tahun 1998. Dalam undang-undang ini secara tegas ditetapkan bahwa jenis bank di Indonesia adalah Bank Umum dan Bank Perkreditan Rakyat.                                              
                                            </p>
                                         </div>

                                      </div>
                                       <div class="col col-lg-6 col-md-6 col-12 col-sm-12 col-xs-12 visible-lg visible-md hidden-sm hidden-xs">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/1.png" class="img-responsive 2mg-fluid lazy">
                                          </div>
                                      </div>
                                  </div>  
                                  <br>
                                  <div class="row"> 
                                       <div class="col-12 col-lg-12">
                                          <div class="dex-aboutus text-center">
                                            <h1>Sejarah Pendirian Perbarindo</h1>
                                          </div>
                                      </div>

                                      <div class="col col-lg-4 col-md-4 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus-x"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/Icon-1.png" class="img-responsive 2mg-fluid lazy">
                                          </div>
                                         
                                         <div class="dex-aboutus">
                                            <h1 class="text-purple">29 Juni 1981</h1>
                                            <p>Didirikan organisasi Badan Kerjasama Perhimpunan Bank Perkreditan Rakyat Indonesia (BKSP BAPRI) di Jawa timur</p>
                                         </div>
                                      </div>
                                      <div class="col col-lg-4 col-md-4 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus-x"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/Icon-2.png" class="img-responsive 2mg-fluid lazy">
                                          </div>
                                          <div class="dex-aboutus">
                                            <h1 class="text-orange">3 Agustus 1984</h1>
                                            <p>Terbentuknya Federasi Perhimpunan Bank Perkreditan Rakyat Indonesia (PERBARI) di Bandung</p>
                                         </div>
                                      </div> 
                                      <div class="col col-lg-4 col-md-4 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus-x"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/Icon-3.png" class="img-responsive 2mg-fluid lazy">
                                          </div>
                                          <div class="dex-aboutus">
                                             <h1 class="text-yellow">24 Januari 1995</h1>
                                             <p>Pembentukan organisasi asosiasi Perhimpunan Bank Perkreditan Rakyat Indonesia (PERBARINDO) Sejarah BPR</p>
                                         </div>
                                      </div> 
                                  </div>



                                  <div class="row">
                                      <div class="col-12 col-lg-12">
                                          <div class="dex-aboutus text-center">
                                            <h1>Penghargaan</h1>
                                          </div>
                                      </div>
                                      <div class="col col-lg-3 col-md-3 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/1f.png" class="img-responsive 2mg-fluid lazy">
                                          </div> 
                                      </div> 
                                      <div class="col col-lg-3 col-md-3 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/2f.png" class="img-responsive 2mg-fluid lazy">
                                          </div> 
                                      </div>
                                      <div class="col col-lg-3 col-md-3 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/3f.png" class="img-responsive 2mg-fluid lazy">
                                          </div> 
                                      </div>
                                      <div class="col col-lg-3 col-md-3 col-12 col-sm-12 col-xs-12">
                                          <div class="img-aboutus"> 
                                              <img src="http://perbarindo.tranyar.com/mocukup/aboutusfile/4f.png" class="img-responsive 2mg-fluid lazy">
                                          </div> 
                                      </div>
                                  </div>
                              <?php
                                }else{
                                  echo @$content['content']; 
                                }
                              ?>
                          </div>  
                        </div>            
                   </div>
                </div> 
            </div> 
        </section>
  <!-- End Page -->

    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();

        });
    </script>