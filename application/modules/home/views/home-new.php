 

<section>
        
<div id="myCarousel" class="carousel slide  margin-topfix" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">

    <?php
      $i = 0;
      foreach ($sliders as $sl) {
    ?>

      <div class="carousel-item <?php ($i==0)? $active = 'active': $active = '';echo $active; ?>">
      <div class="overlay-item-layer-white"></div> 
      <div class="first-slide" >   


          <img class="first-slide img-responsive lazy" src="<?php echo setSliderImage($sl['picture']); ?>" alt="<?php echo $sl['name']; ?>" alt="<?php echo $sl['name']; ?>" width="100%">  
          <div class="container">
            <div class="carousel-caption d-nonex d-md-blockx text-left">
              <?php /**
               <h1 class="demo3"><?php echo $sl['name']; ?></h1>
                <span class="visible-lg visible-md visible-sm hidden-xs demo3"><?php echo $sl['description']; ?></span>
                <?php if($sl['action_url']!=""){ ?>
                <p class="visible-lg visible-md visible-sm hidden-xs"><a class="btn btn-lg btn-primary" href="<?php echo $sl['action_url']; ?>" role="button">Detail</a></p>
                <?php } ?>
                <a class="btn btn-xs btn-primary hidden-lg hidden-md hidden-sm visible-xs" href="<?php echo base_url().'show/event/'.$sl['slider_id']; ?>" role="button">Detail</a>
                **/ ?>
            </div>
          </div> 
       </div> 
    </div>

    <?php
        $i++;    
      }
    ?>
    
    
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</section>


<section style="background-size: 1604px 895px;background-image: url('http://perbarindo.tranyar.com/mocukup/Bg-New.png');padding-bottom: 0px;">
 

        <!-- TYPE 2 -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center padding-0"> 
                   <div class="view-header visible-lg visible-md hidden-sm hidden-xs"> 
                        <center>
                            <br>
                            <h3>Sistem Informasi <span class="demo2 title">Perbarindo</span></h3>
                            <!-- <p class="demo1">Berikut adalah list kegiatan-kegiatan yang akan berlangsung dalam beberapa waktu dekat ini</p> -->
                        </center>  

                         <div style="margin-top:36px;"></div>
                    </div> 
                    <div class="view-header padding-0 hidden-lg hidden-md visible-sm visible-xs"> 
                        <br/>
                        <center> 
                            <h5>Sistem Informasi  <span class="demo2 title">Perbarindo</span></h5>
                            <!-- <p class="demo1">Berikut adalah list kegiatan-kegiatan yang akan berlangsung dalam beberapa waktu dekat ini</p> -->
                        </center>  


                        <div style="margin-top:-22px;"></div>
                    </div>
                </div>
            </div>  

            <!-- DISPLAY DEKSTOP - LG - MD --> 

            <div class="rowx owl-carousel visible-lg visible-md hidden-sm hidden-xs">
                  
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                       <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/1.png">
                            </div> 
                            <div class="head-cards bg-purplegr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>BALAI LELANG</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/11.png">
                            </div>
                       </div>

                        <div class="card card-radius effect2">
                            <div class="wrap-card poster-card card-lelang"> 

                                
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <p>Daftar lelang yang diselegarakan oleh PERBARINDO.</p> 
                                        <h6>List Lelang</h6>
                                        <div id="auctionlist" class="tcarouse paging-purple">
                                          
                                        </div>
                                         <div class="poster-card__datex text-center"> 
                                            
                                             <a href="<?php echo base_url(); ?>auction" class="btn btn-warning bg-purplegr btn-block btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a>
                                        </div>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article> 
                  <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/2.png">
                            </div> 
                            <div class="head-cards bg-orangegr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>EVENT</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/22.png">
                            </div>
                       </div>


                        <div class="card card-radius effect2">
                            <div class="wrap-card poster-card card-lelang">  
                                <div class="body-card">
                                    <div class="poster-card__body">        

                                        <p>List event terbaru yang dilaksanakan oleh PERBARINDO.</p> 
                                        <!-- <p>Event - event PERBARINDO terbaru.</p> --> 
                                        <h6>List Event</h6>
                                        <div id="eventlist" class="tcarousel paging-orange"></div>
                                         <div class="poster-card__datex text-center"> 
                                             <a href="<?php echo base_url(); ?>find/event" class="btn btn-warning bg-orangegr btn-block btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a> 
                                        </div>
                                        
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article>  
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                         <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/3.png">
                            </div> 
                            <div class="head-cards bg-greengr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>SIP</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/33.png">
                            </div>
                       </div>

                        <div class="card card-radius effect2">
                            <div class="wrap-card poster-card card-lelang"> 
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <div class="poster-card__datex text-center"> 
                                             
                                        </div> 
                                        <?php 

                                        if($this->aauth->is_loggedin()){

                                            $user_group = $this->Mcore->getUserGroup($this->session->userdata("id"));

                                        ?>
                                          <h6>Login SIP As</h6>
                                        <div class="tlogin tloginlg table-responsive" >
                                           <table class="table table-striped table-bordered" align="center">
                                                <tr>
                                                   <td><img style="width: 50px;height: auto;right-bottom: 20px;" <?php ($this->session->userdata('picture')!="")? $avatar=$this->session->userdata('picture'):$avatar=base_url().'public/assets/images/default-avatar.png';?> src="<?php echo $avatar; ?>" alt="Circle Image" class="rounded-circle"></td>
                                                   <td><span class="user_profile"><b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                </tr> 
                                                <tr>
                                                 <!--   <td><span class="user_profile"><b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td> -->
                                                </tr>
                                                <tr>
                                                   <td colspan="2"><span class="user_profile"> <?php echo $this->session->userdata('email');?></span></td>
                                                </tr>

                                                <tr>
                                                   <td colspan="2"><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                          <div>
                                          <a href="<?php echo base_url(); ?>admin" class="btn btn-warning btn-block btn-round bg-greengr"><i class="fa fa-cog"></i> Dashboard </a>

                                          <a href="<?php echo base_url(); ?>logout" class="btn btn-warning btn-block btn-round bg-greengr"><i class="fa fa-sign-out"></i> Logout </a>
                                          </div>
                                        
                                        <?php }else{ ?>


                                         <p>Login Sistem Informasi PERBARINDO (SIP) untuk BPR yang terdaftar di PERBARINDO.</p>
                                          <!-- <p>Login area untuk BPR yang terdaftar di PERBARINDO.</p> --> 

                                          <h6>Login SIP</h6>

                                          <form class="form" id="login">
                                            <div class="tcarousel">
                                                <div class="header header-primary text-center">
                                                  
                                                </div>
                                                <div class="content">
                                                  <div class="input-group form-group-no-border input-lg">

                                                     <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon bg-greengr">
                                                            <i class="fa fa-map-marker"></i>
                                                        </span>
                                                       <select class="form-control select2" name="dpd" id="dpd" autocomplete="off" style="width: 100%">
                                                           <option value="">-- Pilih DPD --</option>

                                                            <?php 
                                                            foreach($dpd as $d){
                                                            
                                                             
                                                                  echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';
                                                              
                                                            } 
                                                            ?> 
                                                       </select>
                                                    </div>
                                                      
                                                    </div>
                                                    <div class="input-group form-group-no-border input-lg">

                                                       <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon bg-greengr">
                                                            <i class="fa fa-university "></i>
                                                        </span> 

                                                           <select class="form-control select2" name="bpr" id="bpr" autocomplete="off" style="width: 100%;z-index: 9999;cursor: pointer!important;" disabled="disabled">
                                                              <option value="">-- Pilih BPR --</option>
                                                           </select>
                                                      </div>

                                                      
                                                     </div> 
                                                    <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon bg-greengr">
                                                            <i class="now-ui-icons users_circle-08"></i>
                                                        </span>
                                                        <input type="text" disabled data-prompt-position="bottomLeft"  name="username" id="username" class="form-control validate[required]" placeholder="Username">
                                                    </div>
                                                    <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon bg-greengr">
                                                            <i class="now-ui-icons objects_key-25"></i>
                                                        </span>
                                                        <input type="password" disabled data-prompt-position="bottomLeft"  name="password" id="password" placeholder="Password" class="form-control validate[required]" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                 <button id="btn-submit" class="btn btn-warning btn-block btn-round bg-greengr"><i class="fa fa-sign-in"></i> Login SIP</button>
                                            </div>
                                          </form>

                                        <?php } ?>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article>


            </div>  

            <br>

            <!-- DISPLAY MOBILE - SM - XS -->
            <div class="rowx  hidden-lg hidden-md visible-sm visible-xs">
                  

                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/3.png">
                            </div> 
                            <div class="head-cards bg-greengr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>SIP</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/33.png">
                            </div>
                       </div>

                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
 
                                <div class="body-card">
                                    
                                    <div class="poster-card__body">                             
                                        <div class="poster-card__datex text-center"> 
                                             
                                        </div>
                                        <?php 

                                        if($this->aauth->is_loggedin()){

                                            $user_group = $this->Mcore->getUserGroup($this->session->userdata("id"));

                                        ?>
                                          <h6>Login SIP As</h6>
                                          
                                          <div class="tlogin tloginsm hidden-lg hidden-md visible-sm hidden-xs table-responsive"> 
                                                <table class="table table-striped table-bordered" align="center" width="100%">
                                                    <tr>
                                                       <td align="center">
                                                          <center>
                                                          <img style="width: 90px;height: 90px;margin-bottom: 20px;" <?php ($this->session->userdata('picture')!="")? $avatar=$this->session->userdata('picture'):$avatar=base_url().'public/assets/images/default-avatar.png';?> src="<?php echo $avatar; ?>" alt="Circle Image" class="rounded-circle img-responsive img-fluid">
                                                          </center>
                                                        </td>
                                                        <td style="padding: 0px;">
                                                          <table width="100%">
                                                            <tr>
                                                               <td><span class="user_profile">  <b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                            </tr>

                                                            <tr>
                                                               <td><span class="user_profile">  <?php echo $this->session->userdata('email');?></span></td>
                                                            </tr>

                                                            <tr>
                                                               <td><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                    </tr>  

                                                    
                                                </table> 
                                          </div>
                                          <div class="tlogin tloginsm hidden-lg hidden-md hidden-sm visible-xs table-responsive"> 
                                                <table class="table table-striped table-bordered" align="center" width="100%">
                                                    <tr>
                                                       <td align="center">
                                                          <center>
                                                          <img style="width: 90px;height: 90px;margin-bottom: 20px;" src="<?php echo $this->session->userdata('picture'); ?>" alt="Circle Image" class="rounded-circle img-responsive img-fluid">
                                                          </center>
                                                        </td>
                                                    </tr>  

                                                    <tr>
                                                       <td><span class="user_profile">  <b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                    </tr>

                                                    <tr>
                                                       <td><span class="user_profile">  <?php echo $this->session->userdata('email');?></span></td>
                                                    </tr>

                                                    <tr>
                                                       <td><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                    </tr>
                                                </table> 
                                          </div>


                                          <div>
                                          <a href="<?php echo base_url(); ?>admin" class="btn btn-warning btn-block  bg-greengr"><i class="fa fa-cog"></i> Dashboard </a>

                                          <a href="<?php echo base_url(); ?>logout" class="btn btn-warning btn-block  bg-greengr"><i class="fa fa-sign-out"></i> Logout </a>
                                          </div>
                                        
                                        <?php }else{ ?>
                                         <p>Login Sistem Informasi PERBARINDO (SIP) untuk BPR yang terdaftar di PERBARINDO.</p>
                                         <!--  <p>Login area untuk BPR yang terdaftar di PERBARINDO.</p> -->
                                          <h6>Login SIP</h6>

                                          <form class="form" id="login_m">
                                            <div class="header header-primary text-center">
                                              
                                            </div>
                                            <div class="content">
                                              <div class="input-group form-group-no-border input-lg">

                                                 <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon bg-greengr">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                   <select class="form-control select2" name="dpd" id="dpd_m" autocomplete="off" style="width: 100%">
                                                       <option value="">-- Pilih DPD --</option>

                                                        <?php 
                                                        foreach($dpd as $d){
                                                        
                                                         
                                                              echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';
                                                          
                                                        } 
                                                        ?> 
                                                   </select>
                                                </div>
                                                  
                                                </div>
                                                <div class="input-group form-group-no-border input-lg">

                                                   <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon bg-greengr">
                                                        <i class="fa fa-university "></i>
                                                    </span> 

                                                       <select class="form-control select2" name="bpr" id="bpr_m" autocomplete="off" style="width: 100%;z-index: 9999;cursor: pointer!important;" disabled="disabled">
                                                          <option value="">-- Pilih BPR --</option>
                                                       </select>
                                                  </div>

                                                  
                                                 </div> 
                                                <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon bg-greengr">
                                                        <i class="now-ui-icons users_circle-08"></i>
                                                    </span>
                                                    <input type="text" disabled data-prompt-position="bottomLeft"  name="username" id="username_m" class="form-control validate[required]" placeholder="Username">
                                                </div>
                                                <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon bg-greengr">
                                                        <i class="now-ui-icons objects_key-25"></i>
                                                    </span>
                                                    <input type="password" disabled data-prompt-position="bottomLeft"  name="password" id="password_m" placeholder="Password" class="form-control validate[required]" />
                                                </div>
                                            </div>
                                            <div class="footer text-center">
                                                 <button id="btn-submit" class="btn btn-warning btn-blockx btn-round  bg-greengr"><i class="fa fa-sign-in"></i> Login SIP</button>
                                            </div>
                                            
                                          </form>

                                        <?php } ?>
                                    </div>   
                                </div>     
                          </div>
                      </div>

                  </article>
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                      <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/1.png">
                            </div> 
                            <div class="head-cards bg-purplegr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>BALAI LELANG</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/11.png">
                            </div>
                       </div>

                        <div class="card">
                            <div class="wrap-card poster-card card-lelang"> 
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <p>Daftar lelang yang diselegarakan oleh PERBARINDO.</p> 
                                        <h6>List Lelang</h6>
                                        <div id="auctionlist_m" class="paging-purple"></div>
                                         <div class="poster-card__datex text-center"> 
                                            
                                             <a href="<?php echo base_url(); ?>auction" class="btn btn-warning btn-blockx btn-round bg-purplegr"><i class="fa fa-eye"></i> Tampilkan Semua</a>
                                        </div>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                      <br/>
                  </article> 
                  <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                       <div class="card-head">
                           <div class="iconx icon-xs" align="center">
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/2.png">
                            </div> 
                            <div class="head-cards bg-orangegr head-homes pull-center"> 
                                <div class="title font-carter">
                                    <h3>EVENT</h3>
                                </div>  
                            </div> 
                             <div class="spin" align="center"> 
                                <img class="lazy" src="<?php echo base_url(); ?>/mocukup/22.png">
                            </div>
                       </div>

                        <div class="card">
                            <div class="wrap-card poster-card card-lelang"> 
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                       <!--  <p>Event - event PERBARINDO terbaru.</p> -->
                                        <p>List event terbaru yang dilaksanakan oleh PERBARINDO.</p>
                                        <br>
                                        <h6>List Event</h6>
                                        <div id="eventlist_m" class="paging-orange"></div>
                                         <div class="poster-card__datex text-center"> 
                                             <a href="<?php echo base_url(); ?>find/event" class="btn btn-warning btn-blockx btn-round bg-orangegr"><i class="fa fa-eye"></i> Tampilkan Semua</a> 
                                        </div>
                                        
                                    </div>   
                                </div>     
                          </div>
                      </div>
                      <br/><br/><br/>
                  </article> 
                  



            </div>  
        </div> 
</section>


         
<div class="section register-perbarindo" style="background-size:cover;background-color: #fffbf0;
    padding-top: 1px!important;padding-bottom: 1px!important;margin-top: -45px;">
        <div class="container">

            <div class="text-center" align="center">
                <h4 class="title-rgs">DAFTAR PERBARINDO</h4> 
            </div>

            <article class="visible-lg visible-md hidden-sm hidden-xs">
                <div class="row">  
                  <div class="col col-lg-3 col-md-6 col-sm-6 col-6 ">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Dewan Perwakilan Daerah (DPD)" data-placement="bottom"> 
                          <h2 class="title-md title-br"><i class="fa fa-home fa-lg 5px"></i><b>24</b></h2> 
                          <div class="description-rgs bg-orange-md">
                            <p class="description-md">
                                <h3>DPD</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Dewan Perwakilan Komisariat (DPK)" data-placement="bottom"> 
                          <h2 class="title-md title-br"><i class="fa fa-trello fa-lg 5px"></i><b>60</b></h2>
                          <div class="description-rgs bg-yellow-md">
                            <p class="description-md">
                                <h3>DPK</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                  <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Bank Perkreditan Rakyat (BPR)" data-placement="bottom"> 
                          <h2 class="title-md title-br"><i class="fa fa-building-o fa-lg 5px" aria-hidden="true"></i> <b>1576</b></h2>
                           <div class="description-rgs bg-purple-md"> 
                            <p class="description-md">
                                <h3>BPR</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Bank Perkreditan Rakyat Syariah (BPRS)" data-placement="bottom"> 
                          <h2 class="title-md title-br"><i class="fa fa-hospital-o fa-lg 5px" aria-hidden="true"></i> <b>51</b></h2>
                           <div class="description-rgs bg-ungu-md">
                            <p class="description-md">
                                <h3>BPRS</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   
              </div>
            </article> 


            <article class="hidden-lg hidden-md visible-sm visible-xs">
                <div class="row">  
                  <div class="col col-lg-3 col-md-6 col-sm-6 col-6 ">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Dewan Perwakilan Daerah (DPD)" data-placement="bottom"> 
                          <h2 class="title-md title-br"> <b>24</b></h2> 
                          <div class="description-rgs bg-orange-md">
                            <p class="description-md">
                                <h3>DPD</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Dewan Perwakilan Komisariat (DPK)" data-placement="bottom"> 
                          <h2 class="title-md title-br"> <b>60</b></h2>
                          <div class="description-rgs bg-yellow-md">
                            <p class="description-md">
                                <h3>DPK</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Bank Perkreditan Rakyat (BPR)" data-placement="bottom"> 
                          <h2 class="title-md title-br"> <b>1576</b></h2>
                           <div class="description-rgs bg-purple-md"> 
                            <p class="description-md">
                                <h3>BPR</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                       
                      <div class="card card-service text-center" align="center" rel="tooltip" title="Bank Perkreditan Rakyat Syariah (BPRS)" data-placement="bottom"> 
                          <h2 class="title-md title-br"> <b>51</b></h2>
                           <div class="description-rgs bg-ungu-md">
                            <p class="description-md">
                                <h3>BPRS</h3>
                            </p>
                          </div>
                      </div>

                  </div>
                   
              </div>
            </article> 


        </div>
    </div>  




 <script type="text/javascript">
   $(document).ready(function(){

      $('#dpd').select2();
      $('#dpd').on('select2:selecting', function(e) {
         var id = e.params.args.data.id;
         var target = base_url + 'login_autocomplete/bpr/'+id;

         $.get(target, function(res){

              $('#bpr').html(res);
              $('#bpr').removeAttr("disabled");
              $('#bpr').select2();

              $('#bpr').on('select2:selecting', function(e) {

                $("#username").removeAttr("disabled");
                $("#password").removeAttr("disabled");
             
            });

         });


      });
      $('#bpr').select2();

        $("#login").validationEngine();

        $("#login").submit(function(){

          if($(this).validationEngine("validate")){

              var target = base_url + 'login_bpr';
              var data = $(this).serialize();

              $("body").waitMe({
                      effect: 'pulse',
                      text: 'Authenticating...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'
                  });
              
              $.post(target, data, function(res){

                  if(res.status == "1"){

                      $("body").waitMe({
                          effect: 'pulse',
                          text: 'Redirecting...',
                          bg: 'rgba(255,255,255,0.90)',
                          color: '#555'
                      });

                      toastr.success(res.msg, 'Response Server');
                     
                      setTimeout('window.location.href = base_url+"admin";',3000);
                      
                     
                  }else{
                      
                      $("body").waitMe("hide");
                      toastr.error(res.msg, 'Response Server');
                  
                  }

                 

              },'json');

              return false;

          }

      });

      $('#dpd_m').select2();
      $('#dpd_m').on('select2:selecting', function(e) {
         var id = e.params.args.data.id;
         var target = base_url + 'login_autocomplete/bpr/'+id;

         $.get(target, function(res){

              $('#bpr_m').html(res);
              $('#bpr_m').removeAttr("disabled");
              $('#bpr_m').select2();

              $('#bpr_m').on('select2:selecting', function(e) {

                $("#username_m").removeAttr("disabled");
                $("#password_m").removeAttr("disabled");
             
            });

         });


      });
      $('#bpr_m').select2();

        $("#login_m").validationEngine();

        $("#login_m").submit(function(){

          if($(this).validationEngine("validate")){

              var target = base_url + 'login_bpr';
              var data = $(this).serialize();

              $("body").waitMe({
                      effect: 'pulse',
                      text: 'Authenticating...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'
                  });
              
              $.post(target, data, function(res){

                  if(res.status == "1"){

                      $("body").waitMe({
                          effect: 'pulse',
                          text: 'Redirecting...',
                          bg: 'rgba(255,255,255,0.90)',
                          color: '#555'
                      });

                      toastr.success(res.msg, 'Response Server');
                     
                      setTimeout('window.location.href = base_url+"admin";',3000);
                      
                     
                  }else{
                      
                      $("body").waitMe("hide");
                      toastr.error(res.msg, 'Response Server');
                  
                  }

                 

              },'json');

              return false;

          }

      });

      $.get(base_url+"home/get_events/0/eventlist", function(res){

        $("#eventlist").html(res);

      });

      $.get(base_url+"home/get_events/0/eventlist_m", function(res){

        $("#eventlist_m").html(res);

      });

      $.get(base_url+"home/auction/get_auction/0/auctionlist", function(res){

        $("#auctionlist").html(res);

      });

      $.get(base_url+"home/auction/get_auction/0/auctionlist_m", function(res){

        $("#auctionlist_m").html(res);

      });


      $('.owl-carousel').owlCarousel({
            // loop:true,
            // margin:10,
            responsiveClass:true,
            // nav:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:false
                },
                600:{
                    items:2,
                    nav:true,
                    loop:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
      });

      $(".owl-prev").html("<i class='fa fa-angle-double-left fa-lg'></i>");
      $(".owl-next").html("<i class='fa fa-angle-double-right fa-lg'></i>");

    });
    function pagination(container,target){

      $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");
      $("#"+container).load(target);

    }
</script>