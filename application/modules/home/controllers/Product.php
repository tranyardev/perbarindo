<?php

class Product extends MX_Controller{


	var $preloader = "";

	function __construct(){

		parent::__construct();

    $this->load->library("Aauth");
    $this->load->model("Mbprproduct");

    $this->preloader = base_url()."assets/dist/img/preloader.gif";
 

	}


	public function index(){


		$data['page'] = 'product';
    $data['og_title'] = 'Perbarindo | Sistem Informasi Perbarindo';
    $data['og_site_name'] = 'PERBARINDO BPR PRODUK';
    $data['og_site_url'] = base_url();
    $data['og_site_description'] = 'Produk Bpr yang tergabung di PERBARINDO';
    $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';

   	$data['body_class'] = 'profile-page';
    $data['navbar']['navbar_class'] = 'navbar-transparent'; 

    $this->load->view('layout/main',$data);

	}

	public function product_list($page = 0, $keyword = null){
	
		$alldata = $this->Mbprproduct->getBPRProductList(null, null, $keyword);

		$container = "bprproduct";
		$page_config = $this->config->item('pagination');
		$page_config['per_page'] = 10;
		$page_config['uri_segment'] = 4;
		$page_config['total_rows'] = count($alldata);
		$page_config['base_url'] = "javascript:pagination('".$container."','".base_url() . "/home/product/product_list";
		$page_config['first_url'] = "javascript:pagination('".$container."','".base_url() . "/home/product/product_list/0')";
		$page_config['suffix'] = "/".$keyword."')";

		$start=$page;

		if(empty($start)){
		    $start=0;
		}
		$this->pagination->initialize($page_config);

		$product = $this->Mbprproduct->getBPRProductList($start, $page_config['per_page'], $keyword);
		$paging = $this->pagination->create_links();

       
        $html = "";


        if(count($product)>0){

        	foreach($product as $p){ 

		  		if($p['cover'] != ""){

		  			$path = "upload/media";
	                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
	                $file = $upload_path.'/'.$p['cover'];

	                if(file_exists($file)){

	                    $cover = base_url().$path.'/'.$p['cover'];

	                }else{

	                    $cover = base_url().'public/assets/img/default_event_cover.png';

	                }

		  		}else{

		  			$cover = base_url().'public/assets/img/default_event_cover.png';
		  		}
		  		
		  		$content = strip_tags($p['description']);

		  		$content = $this->truncate($content,200);


	           	$html.=' <article class="col-md-12 padding-0">
                            <div class="card block__box blefetHover padding-0 updated margin-b-0">
                                <div class="row margin-0">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-0">
                                        <div class="item__content">
                                             <div class="row margin-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-12">
                                                     <div class="item__image">
                                                        <figure> <img src="'.$this->preloader.'" data-src="'.$cover.'" class="lazy img-responsive" alt="'.$p['name'].'"></figure>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 bordered-left ">
                                         <div class="status__content padding-12 bordered-dotted" user-type="buyer"> 

                                               <div class="text__content text--medium">
                                                  <h3><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'" rel="noopener noreferrer">'.$p['name'].'</a></h3>
                                                </div> 
                                                   
                                              <div class="clearfix padding-0">
                                                    <div class="text--medium text-left">Deskripsi:</div>
                                                   <hr class="hr-xs">
                                              </div> 
                                              <div class="clearfik item__content">  
                                                  <div class="text--medium text__content mb10"> 
                                                 	<p>'.$content.'</p>
                                                  </div> 
                                                  <span class="readmore text--medium"><a href="'.base_url().'bpr/product/'.$p['bpr'].'/'.$p['permalink'].'">Lihat Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                              </div>
                                             <div class="text__content text--medium">

                                                 <br> 

                                                 <span class="kategori"><i class="fa fa-tag" aria-hidden="true"></i> '.$p['product_category'].'</span>
                                              </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </article>';

	                                

	        } 

        }else{

        	$html.='<div class="alert alert-warning text-center">
                       <i class="fa fa-warning"></i> Data Tidak di temukan
                    </div>';

        }

	  	

        $html.='<div class="form-group text-center text-center center-block"><br/>'.$paging.'</div>';
                             
        echo $html;

	}

	

	function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }


}