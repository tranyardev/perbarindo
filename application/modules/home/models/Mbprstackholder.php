<?php

class Mbprstackholder extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRStackholder(){

        $result = $this->db->query("SELECT * FROM stockholders")->result_array();
        return $result;

    }
    function getBPRStackholderById($id){

        $result = $this->db->query("SELECT * FROM stockholders WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRStockholderByBPR($bpr){

        $result = $this->db->query("SELECT * FROM stockholders WHERE bpr='".$bpr."'")->result_array();
        return @$result;

    }
    
 
}