<?php

if($edit_perm == "1"){
    $disabled = "";
}else{
    $disabled = "disabled";
}

?>
<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Payment Expired Config</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">

                    <?php foreach($configs as $cfg){ ?>
                    <div class="form-group">
                        <label for="description"><?php echo $cfg['display_name']; ?></label>
                        <div class="input-group">                            
                            <input type="text" name="<?php echo $cfg['key']; ?>" class="form-control validate[required,custom[number]]" id="<?php echo $cfg['key']; ?>" value="<?php echo $cfg['value']; ?>"> <span class="input-group-addon">Hours</span>
                        </div>
                    </div>
                    <?php } ?>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" <?php echo $disabled ?> class="btn btn-primary">Save</button>
                </div>

            </div>
            <!-- /.box -->
        </div>

    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    function updateConfig(form){

        var target = base_url+"configuration/paymentexpired/update";
        var data = $("#"+form).serialize();

        showLoading();

        $.post(target,data,function(res){

            hideLoading();
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');
        

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }

    $(document).ready(function(){
        
        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                $("#validate_content").hide();

                showLoading();

                setTimeout('saveFormData();',3000);

            }

            return false;

        });

        $("#form").validationEngine();

    });

   
    function saveFormData(){

        var target = base_url+"configuration/paymentexpired/update";
        var data = $("#form").serialize();

        showLoading();
        
        $.post(target,data,function(res){

            hideLoading();
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

</script>