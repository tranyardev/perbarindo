<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Gcg_info extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MConfiguration");

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'gcg_info_view';
            $this->edit_perm = 'gcg_info_update';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "configurations";
        $this->scope = "GCG";

    }


    function index(){

        $breadcrumb = array(
            "<i class='fa fa-file'></i> GCG" => base_url()."configuration/gcg_info"
        );

        $data['page'] = 'gcg_info';
        $data['page_title'] = 'GCG Config';
        $data['page_subtitle'] = 'Edit Config';
        $data['custom_css'] = array(
            "program_category" => base_url()."assets/modules/event/assets/css/event.css",
        );

        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['configs'] = $this->MConfiguration->getConfiguration($this->scope);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['parent_menu'] = 'gcg_info';
        $data['data']['submenu'] = '';
        $this->load->view('layout/body',$data);


    }
    function update(){


        if(count($_POST)>0){


            $keys = array_keys($_POST);
            $a = count($keys);

            for($i=0;$i<$a;$i++){

                $key = $keys[$i];
                $value = $_POST[$key];

                $data = array(
                    "value" =>  $value
                );

                $this->db->where("scope", $this->scope);
                $this->db->where("key", $key);
                $this->db->update($this->table, $data);

            }

            $res = array("status" => "1", "msg" => "Successfully update data!");


            echo json_encode($res);

        }


    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }


}