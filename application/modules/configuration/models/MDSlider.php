<?php

class MDSlider extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'slider_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'slider_delete');

    }
    public function appendToSelectStr() {

        $edit = '';
        $delete = '';
        $str = array(

            "picture" => "concat('<div class=\"image-container\"><img src=\"".base_url()."upload/slider/',a.picture,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')",
            "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",


        );

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.slider_id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.slider_id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "picture" => "concat('<div class=\"image-container\"><img src=\"".base_url()."upload/slider/',a.picture,'\" class=\"scale\" data-scale=\"best-fill\" data-align=\"center\"></div>')",
                "status" => "CASE WHEN a.status = '0' THEN '<span class=\"label label-danger\">In Active</lable>' ELSE '<span class=\"label label-success\">Active</span>' END",
                "op" => $op

            );


        }

        return $str;


    }

    public function fromTableStr() {
        return "sliders a";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}