<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class MConfiguration extends CI_Model
{
    function getConfigurationById($id){

        $result = $this->db->query("SELECT * FROM configurations WHERE configuration_id='".$id."'  ORDER BY configuration_id ASC")->result_array();
        return @$result[0];

    }
    function getConfiguration($scope){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope='".$scope."'  ORDER BY configuration_id ASC")->result_array();
        return $result;

    }

    function getConfigurationByKey($key, $scope){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope='".$scope."' and key='".$key."'  ORDER BY configuration_id ASC")->result_array();
        return @$result[0];

    }
}