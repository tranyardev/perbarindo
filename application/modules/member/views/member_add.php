<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Member</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <div class="img-prev" id="photo_preview"><h1>Photo Preview</h1></div>
                                <br>
                                <input type="file" class="form-control" name="photo" id="photo">

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" maxlength="100" name="name" class="form-control validate[required]" id="name" placeholder="Name (Required)">
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="dpd">DPD</label>
                                <select class="form-control" name="dpd" id="dpd">
                                    <?php
                                    foreach ($dpd as $d){
                                        echo "<option value='".$d['id']."'>".$d['name']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bpr">BPR</label>
                                <select class="form-control" name="bpr" id="bpr">
                                    <option value='0'>--- Please Select ---</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="job_position">Job Position</label>
                                <select class="form-control" name="job_position" id="job_position">
                                    <?php
                                    foreach ($job_position as $jp){
                                        echo "<option value='".$jp['id']."'>".$jp['name']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" maxlength="150"  name="email" class="form-control validate[required]" id="email" placeholder="Email (Required)">
                            </div>
                            <div class="form-group">
                                <label for="no_hp">Mobile Phone</label>
                                <input type="text" maxlength="15" name="no_hp" class="form-control validate[required]" id="no_hp" placeholder="Mobile Phone (Required)">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

        </div>
        <div class="col-md-6">

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

        $("#photo").change(function(){
            readURL(this, "#photo");
        });

        $("#dpd").change(function(){
            getBpr($(this).val());
        });

        getBpr($("#dpd").val());

    });

    function saveFormData(){

        var target = base_url+"member/member/addnew";

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }

                resetForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Photo Preview</h1>");
        $(selector).val('');

    }

    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }

    function getBpr(dpd){

        var target = base_url+"member/member/getBpr";
        var data = "dpd="+dpd;
        $.post(target, data, function(res){

            $("#bpr").empty().append(res);

        },'json');
        

    }

</script>