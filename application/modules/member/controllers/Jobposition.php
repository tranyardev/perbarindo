<?php

class Jobposition extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MJobposition");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'job_position_view';
            $this->add_perm = 'job_position_add';
            $this->edit_perm = 'job_position_update';
            $this->delete_perm = 'job_position_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "job_positions";
        $this->dttModel = "MDJobposition";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-user-circle-o'></i> Job Position" => base_url()."member/jobposition"
        );

        $data['page'] = 'jobposition';
        $data['page_title'] = 'Member';
        $data['page_subtitle'] = 'Job Position';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'member_management';
        $data['data']['submenu'] = 'job_position';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "created_by" => $this->session->userdata("id"),
                "created_at" => date("Y-m-d h:i:s")
            );

            $insert = $this->db->insert($this->table, $data);

            if($insert){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-user-circle-o'></i> Job Position" => base_url()."member/jobposition",
                "Add New" => base_url()."member/jobposition/addnew",
            );

            $data['page'] = 'jobposition_add';
            $data['page_title'] = 'Job Position';
            $data['page_subtitle'] = 'Add New Job Position';
            /*$data['custom_css'] = array(
                "next_service_category" => base_url()."assets/modules/event/assets/css/event.css",
            );*/
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'member_management';
            $data['data']['submenu'] = 'job_position';
            $this->load->view('layout/body',$data);

        }

    }

    function edit($id){

        if(count($_POST)>0){

            $data = array(

                "name" => $this->input->post("name"),
                "description" => $this->input->post("description"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{


            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-user-circle-o'></i> Job Position" => base_url()."member/jobposition",
                "Edit" => base_url()."member/jobposition/edit/".$id,
            );

            $data['page'] = 'jobposition_edit';
            $data['page_title'] = 'Job Position';
            $data['page_subtitle'] = 'Edit Job Position';
            /*$data['custom_css'] = array(
                "next_service_category" => base_url()."assets/modules/event/assets/css/event.css",
            );*/
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MJobposition->getJobpositionById($id);
            $data['data']['parent_menu'] = 'member_management';
            $data['data']['submenu'] = 'job_position';
            $this->load->view('layout/body',$data);

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");            

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}