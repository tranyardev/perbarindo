<?php

class Paymentconfirmation extends MX_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mpaymentconfirmation");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'payment_confirmation_view';
            $this->edit_perm = 'payment_confirmation_update';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "payment_confirmations";
        $this->dttModel = "Mdpaymentconfirmation";
        $this->pk = "id";

    }


    function index(){

        $breadcrumb = array(
            "<i class='fa fa-university'></i> Payment Confirmation" => base_url()."paymentconfirmation"
        );

        $data['page'] = 'paymentconfirmation';
        $data['page_title'] = 'Payment Confirmation';
        $data['page_subtitle'] = 'Payment Confirmation';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['custom_js'] = array(
            "packages" => base_url()."assets/plugins/jquery-price-format/jquery.priceformat.min.js"
        );
        $data['data']['parent_menu'] = 'finance';
        $data['data']['submenu'] = 'payment_confirmation';
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $this->load->view('layout/body',$data);


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }   
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    function changeStatus(){

        if(count($_POST)>0){
            $id = $this->input->post("id");

            $data = array(
                "status" => $this->input->post("status"),
                "updated_by" => $this->session->userdata("id"),
                "updated_at" => date("Y-m-d h:i:s")
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){

                $pk_event_reg = "id";
                $table_event_reg = "event_registrations";
                $dataPaymentConf = $this->Mpaymentconfirmation->getPaymentConfirmationById($id);
                
                $st = "WAITING_TRANSACTION"; //sementara pake yang ini dulu
                // $st = "WAITING_CONFIRMATION"; //pake ini kalo alur di frontend udah berjalan ok

                if($this->input->post("status")=="1"){
                    $st = "TRANSACTION_COMPLETED";
                }

                $data = array(
                    "status" => $st
                );
                $this->db->where($pk_event_reg, $dataPaymentConf['event_registration_id']);
                $update_event_reg = $this->db->update($table_event_reg, $data);

                if($update_event_reg){
                    $res = array("status" => "1", "msg" => "Successfully update data!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }
                
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    function preview($id=null){
        if($id!=null){

            $data = $this->Mpaymentconfirmation->getPaymentConfirmationById($id);

            if(count($data)> 0){
                
                $dataPayment = $this->generate_detail_payment_preview($data['regcode'],$data);

                echo json_encode(array("status" => 1, "content" => $dataPayment));
            }else{
                echo json_encode(array("status" => 0,"content" => "No Data Found."));
            }
        }

    }

    function generate_detail_payment_preview($reg_code,$data_payment){

        $this->load->model("home/Mevent");


        $event_registration = $this->Mevent->getEventRegistration($reg_code);
        $event_member = $this->Mevent->getEventMember($event_registration['reg_id'],'0');
        $event_member_twin_sharing = $this->Mevent->getEventMember($event_registration['reg_id'],'1');

        $gender = ($event_registration['gender']=="L")? "Laki - laki":"Perempuan";

        $sdate=date_create($event_registration['start_date']);
        $start_date = date_format($sdate,"d/m/Y");

        $edate=date_create($event_registration['end_date']);
        $end_date = date_format($edate,"d/m/Y");        

        $total_cst = 'Rp '.number_format($event_registration['total_cost'],2,",",".");


        /*$real_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
        $full_path = $real_path.$event_registration['cover'];
        $path_cover_reg = base_url().'public/assets/img/default_event_cover.png';
        if(file_exists($full_path)){
            $path_cover_reg = base_url().'upload/event/'.$event_registration['cover'];
        }*/

        // $path_cover_reg = setEventImage($event_registration['cover']);

        $event_member_content = "";
        foreach($event_member as $em){
            $event_member_content .= '
            <tr>
              <td>'.$em['name'].'</td>
              <td>'.$em['no_hp'].'</td>
              <td>'.$em['package_name'].'</td>
              <td>Rp '.number_format($em['price'],2,",",".").'</td>
            </tr>
            ';
        }

        $twin_sharing_member_content = "";
        foreach($event_member_twin_sharing as $emt){ 
            $with_member = $this->Mevent->getMemberById($emt['twin_sharing_with']);
            $twin_sharing_member_content .= '
          <table class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>No HP</th>
                  <th>Paket</th>
                  <th style="width: 166px;">Harga</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>'.$emt['name'].'</td>
                  <td>'.$emt['no_hp'].'</td>
                  <td>'.$emt['package_name'].'</td>
                  <td>Rp '.number_format($emt['price'],2,",",".").'</td>
                </tr>
                <tr>
                  <td>'.@$with_member[0]['name'].'</td>
                  <td>'.@$with_member[0]['no_hp'].'</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>                                 
                </tr>
              </tbody>
          </table>
          <br><br>
          ';
        }

        $content = '
        <section>
          <div class="row">
            <div class="col-md-12">
                 <div class="card">
                        <div class="text-center padding-12" >
                            <h3 class="title margin-0"  >DETAIL TRANSAKSI</h3>
                            <h6 class="description margin-0">'.$event_registration['event_name'].' - #'.$event_registration['regcode'].'</h6>
                        </div>

                        <hr>

                          <div class="l-mar-top-3">
                              <div class="l-media clrfix listing-organizer-title">
                                  <div class="l-align-left">
                                      <span> <strong>Bukti Pembayaran</strong> <br> <img class="kc-summary-item-image" src="'.base_url().'upload/buktitransfer/'.$data_payment['doc_transfer'].'" role="presentation" style="max-width:100%;"></span>
                                  </div>
                              </div>
                          </div>

                          <div class="l-mar-top-3" style="padding-top: 10px;">
                              <table class="table">
                                  
                                  <tr>
                                      <td>
                                          <span ><b>Total</b></span>
                                      </td>                                  
                                      <td class="text-left">
                                        <span id="total"><strong>'.$total_cst.'</strong></span>
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>
                                          <span ><b>Kode Unik</b></span>
                                      </td>                                  
                                      <td class="text-left">
                                        <span id="total"><strong>'.$event_registration['uniq_number'].'</strong></span>
                                      </td>
                                  </tr>

                              </table> 
                          </div>

                          <hr>

                </div>
            </div>
          </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                      

                    <div class="kc-summary-details"> 
                      <div class="kc-summary-item-container">

                          <div class="kc-summary-item">
                              <div class="title-ivent">
                                  
                                  <div class="listing-hero-body ">
                                      <div class="l-mar-top-3">
                                          <div class="l-media clrfix listing-organizer-title">
                                              <div class="l-align-left">
                                                  <span><strong>Event</strong> <br>'.$event_registration['event_name'].'</span>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="l-mar-top-3" style="padding-top: 10px;">
                                          <div class="l-media clrfix listing-organizer-title">
                                              <div class="l-align-left">
                                                  <span><i class="fa fa-map-marker"></i> &nbsp;<strong>Lokasi</strong> <br>'.$event_registration['address'].'</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="l-mar-top-3" style="padding-top: 10px;">
                                          <div class="l-media clrfix listing-organizer-title">
                                              <div class="l-align-left">
                                                  <span><i class="fa fa-calendar"></i> <strong>Tanggal</strong> <br>'.$start_date.' - '.$end_date.'</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                 
                              </div>
                           
                          </div>
                      </div>
                      
                      <hr>

                    </div>

                    </div>
                </div>
            </div>

          <div class="row">
            <div class="col-md-12"> 
                <div class="card">        

                        <div class="panel panel-default">
                            
                            <div class="panel-body" style="padding: 15px;">

                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Perusahaan/BPR</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td>Nama</td>
                                    <td>'.$event_registration['bpr_name'].'</td>
                                  </tr>
                                  <tr>
                                    <td>DPD</td>
                                    <td>'.$event_registration['dpd_name'].'</td>
                                  </tr>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data PIC</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td>Nama</td>
                                    <td>'.$event_registration['member_name'].'</td>
                                  </tr>
                                  <tr>
                                    <td>Email</td>
                                    <td>'.$event_registration['member_email'].'</td>
                                  </tr>
                                  <tr>
                                    <td>No HP</td>
                                    <td>'.$event_registration['no_hp'].'</td>
                                  </tr>
                                  <tr>
                                    <td>Job Position</td>
                                    <td>'.$event_registration['member_job_position'].'</td>
                                  </tr>
                                  <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>'.$gender.'</td>
                                  </tr>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Peserta</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <thead>
                                    <tr>
                                      <th>Nama</th>
                                      <th>No HP</th>
                                      <th>Paket</th>
                                      <th style="width: 166px;">Harga</th>
                                    </tr>
                                  </thead>
                                  <tbody>'.
                                    $event_member_content
                                  .'</tbody>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Peserta Twin Sharing</strong></div>
                              </div>
                              <hr>'.
                                $twin_sharing_member_content
                               .'<table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td colspan="3">Total</td>
                                   
                                    <td style="width: 166px;"><strong>Rp '.number_format($event_registration['total_cost'],2,",",".").'</strong></td>
                                  </tr>
                              </table>
                            </div>
                      </div>
              </div>
            </div>
            
          </div>
        </section>
        ';
        return $content;
    }



}