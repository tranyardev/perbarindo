  <!-- Carosel end -->
 

<div class="wrapper" style="margin-top: -35px;">
          <div class="page-header page-header-small" filter-color="orange" > 
                <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url() ?>public/assets/img/faq.jpg');">
                </div> 
            </div>  
            <div class="section">
                <div class="container">
 
                  <section>
                    <div class="row" style="margin-top:-453px;"> 
                        <div class="col-md-12 padding-0">
                          <!-- Start Page -->


    <section> 

        <div class="container marketing">
            <div class="card card-x"> 
               <div class="search-travel">
                     <div class="seach-head text-center">
                          <div class="faq-header"><i class="fa fa-question-circle fa-lg"></i> Frequently Asked Questions</div>
                     </div>
                      
                    <div class="row">
                         
                      <div class="col-sm-12 col-lg-12" style="padding: 30px;">
                        

                      <?php foreach($content as $faq){ ?>
                          <div class="faq-c">
                            <div class="faq-q"> <span class="faq-t"><i class="fa fa-plus"></i></span>&nbsp; <?php echo $faq['title']; ?></div>
                            <div class="faq-a alert info" style="border-left: 4px solid #DDD;  margin-left: 8px;">
                              <p><?php echo $faq['content']; ?></p>
                            </div>
                          </div>
                      <?php } ?>    
                         

                      </div>
                        
                         
                    </div>            
               </div>
            </div>
        
        </div>

    </section>



         <!-- End Page -->
                 </div>
                </div>
             </section> 
        </div>
      </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();
            $(".faq-q").click( function () {
              var container = $(this).parents(".faq-c");
              var answer = container.find(".faq-a");
              var trigger = container.find(".faq-t");
              
              answer.slideToggle(200);
              
              if (trigger.hasClass("faq-o")) {
                trigger.removeClass("faq-o");
              }
              else {
                trigger.addClass("faq-o");
              }
            });
        });
    </script>