 
 <style type="text/css">
   .margin-100{
      margin-left: auto;
      margin-right: auto;
      margin-top: 100px;
      margin-bottom: 100px;
   }
 </style>
 <div class="wrapper" style="margin-top: 144px;">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image lazy" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>public/assets/img/post.png" >
        </div> 
    </div>  
    <div class="section">
        <div class="container">
          <section>  
            <div class="row marketing" style="margin-top: -477px;">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-left"> 
                  <div class="card"> 
                     <div class="search-travel">
                           <div class="seach-head text-center">
                               <h1 class="text-heading-epic homepage-header text-primary">
                                  Blog Artikel
                              </h1>
                           </div>
                          <form id="search_post">
                            <div class="row">
                             
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Search Blog" title="Search events or categories">
                                    </div>
                                </div> 
                                <div class="col-sm-6 col-lg-3" style="margin-top: -7px;">
                                    <div class="input-group"> 
                                        <button id="btn_search" class="btn btn-primary btn-block btn-xs">
                                            <i class="fa fa-seach"></i> SEARCH
                                        </button>
                                    </div>
                                </div>
                              
                            </div>  
                          </form>              
                     </div>
                  </div> 
                </div>
            </div> 
        </section>
        <section>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-left"> 
                <div class="card" style="padding:12px;">   
                      
                        <div class="panel panel-default">


                            <div class="padding-12">
                                <header class="entry-header-outer container-wrapper">
                                      <nav id="breadcrumb">
                                          <a href="<?php echo base_url(); ?>"> <span class="fa fa-home fa-lg" aria-hidden="true"></span> Home</a>
                                          <em class="delimiter">/</em>
                                          <span class="current">Blog</span>
                                      </nav>
                                      <!-- <h1 class="page-title-src">Search Results for: <span>BPR1</span></h1>  -->
                                  </header>
                              
                            </div>  
                            <div class="panel-body"> 
                              <div id="blogpost">

                              <section> 
                                
                                <div class="row">  

                                <?php

                                  $html = "";

                                  foreach($post as $p){ 

                                    if($p['cover'] != ""){

                                      $path = "upload/media";
                                      $upload_path = $_SERVER['DOCUMENT_ROOT'].'/'.$path;
                                      $file = $upload_path.'/'.$p['cover'];

                                      if(file_exists($file)){

                                          $cover = base_url().$path.'/'.$p['cover'];

                                      }else{

                                          $cover = base_url().'public/assets/img/default_event_cover.png';

                                      }

                                    }else{

                                      $cover = base_url().'public/assets/img/default_event_cover.png';

                                    }
                                    
                                    $content = strip_tags($p['content']);

                                    $content = truncate($content,200);


                                    $html.='<article class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                 <div class="card blefetHoverxs padding-0 updated">
                                                  <div class="row margin-0">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0" style="border-bottom: 1px solid #DDD;">
                                                      <div class="item__image center-block">
                                                        <div class="head-card framex">  
                                                            <img class="lazy img-responsive" src="'.base_url().'assets/dist/img/preloader.gif" data-src="'.$cover.'" alt="'.$p['title'].'" /> 
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
                                                      <div class="text-headbpr poster-card__title">
                                                         <h3><a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'">'.$p['title'].'</a> </h3> 
                                                      </div>
                                                    <time><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>'.$p['published_date'].'</span></span></time>
                                                    <div class="text-dexsbpr poster-card__dex">'.$content.'</div>
                                                   <div class="action clearfix pull-right">
                                                      <div class="text__content text--medium text-left">
                                                        <span class="green form-btn-read">
                                                        <a href="'.base_url().'bpr/post/'.$p['bpr'].'/'.$p['permalink'].'" class="btn btn-warning btn-round"  > Baca </a>
                                                        </span>
                                                      </div> 
                                                  </div> 
                                               </div>
                                             </div>
                                           </div>
                                         </article>'; 

                                                            

                                    } 

                                    echo $html;
                                ?>
                           
                              
                        
                         </div> 
                       </section> 


                              <div class="form-group text-center text-center center-block"><br/> 
                                  <?php echo $paging;?>
                              </div>
                                 </div> 
                        </div>   
                  </div>
              </div>
            </div> 
          </div>
        </section>



            </div> 
        </div>
    </div>
   
</div>
 <script type="text/javascript"> 

    $(document).ready(function() { 

        $(".lazy").lazy({
          effect: "fadeIn",
          effectTime: 300,
          threshold: 0
        });

        $("#btn_search").click(function(e) {
         
          var data = $("#search_post").serialize();
          var target = base_url + 'search/blog';

          $("#blogpost").html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

          $.post(target, data, function(res){

            $("#blogpost").html(res);
            $(".lazy").lazy({
              effect: "fadeIn",
              effectTime: 300,
              threshold: 0
            });

          });

          return false;

          
      });

      

    }); 
    function pagination(container,target){

      $("html, body").animate({ scrollTop: 0 }, "slow");

      $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");

      $("#"+container).load(target,function(){

        $(".lazy").lazy({
          effect: "fadeIn",
          effectTime: 300,
          threshold: 0
        });

      });


    }
</script>