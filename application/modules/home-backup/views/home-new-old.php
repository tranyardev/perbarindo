<section>
        
<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: -20px!important;">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">

    <?php
      $i = 0;
      foreach ($sliders as $sl) {
    ?>

      <div class="carousel-item <?php ($i==0)? $active = 'active': $active = '';echo $active; ?>">
      <div class="overlay-item-layer-orange"></div> 
      <div class="first-slide img-responsive lazy" style="height:600px;background: url('<?php echo setSliderImage($sl['picture']); ?>') no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"  alt="<?php echo $sl['name']; ?>" alt="<?php echo $sl['name']; ?>" width="100%" >   


          <!-- <img class="first-slide img-responsive" src="<?php //echo setEventImage($ue['cover']); ?>" alt="<?php //echo $ue['name']; ?>" alt="<?php //echo $ue['name']; ?>" width="100%"> -->
          <div class="container">
            <div class="carousel-caption d-nonex d-md-blockx text-left">
               <h1><?php echo $sl['name']; ?></h1>
                <span class="visible-lg visible-md visible-sm hidden-xs"><?php echo $sl['description']; ?></span>
                <?php if($sl['action_url']!=""){ ?>
                <p class="visible-lg visible-md visible-sm hidden-xs"><a class="btn btn-lg btn-primary" href="<?php echo $sl['action_url']; ?>" role="button">Detail</a></p>
                <?php } ?>
                <a class="btn btn-xs btn-primary hidden-lg hidden-md hidden-sm visible-xs" href="<?php echo base_url().'show/event/'.$sl['slider_id']; ?>" role="button">Detail</a>
            </div>
          </div> 
       </div> 
    </div>

    <?php
        $i++;    
      }
    ?>
    
    
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</section>


<section>
<style type="text/css">

      .tcarousel{height: 310px}
      /*.tlogin{border:solid 1px #ccc;height: 351px;padding: 0px;}*/
     /* @media only screen and (max-width: 768px) {
          .tcarousel{height: auto}
          .tlogin{border:solid 1px #ccc;height: 200px;padding: 10px;}
      }*/

 
</style>



        <!-- TYPE 2 -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center"> 
                   <div class="view-header">
                        <p></p>
                        <center>
                            <br>
                            <h3>PERBARINDO HOME</h3>
                        </center> 
                    </div> 
                </div>
            </div>  

            <!-- DISPLAY DEKSTOP - LG - MD -->

            <div class="rowx owl-carousel visible-lg visible-md hidden-sm hidden-xs">
 
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-university" style="float: left"></i> <h3>Balai Lelang</h3>
                                </div>
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <p>Daftar lelang yang diselegarakan oleh PERBARINDO.</p>
                                        <br>
                                        <h6>List Lelang</h6>
                                        <div id="auctionlist" class="tcarousel"></div>
                                         <div class="poster-card__datex text-center"> 
                                            
                                             <a href="<?php echo base_url(); ?>auction" class="btn btn-warning btn-block btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a>
                                        </div>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article> 
                  <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-calendar" style="float: left"></i><h3>Event</h3>
                                </div>
                                <div class="body-card">
                                    <div class="poster-card__body">        

                                        <p>List event terbaru yang dilaksanakan oleh PERBARINDO.</p> 
                                        <!-- <p>Event - event PERBARINDO terbaru.</p> -->
                                        <br>
                                        <br>
                                        <h6>List Event</h6>
                                        <div id="eventlist" class="tcarousel"></div>
                                         <div class="poster-card__datex text-center"> 
                                             <a href="<?php echo base_url(); ?>find/event" class="btn btn-warning btn-block btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a> 
                                        </div>
                                        
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article> 
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-user" style="float: left"></i> <h3>SIP</h3>
                                </div>
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <div class="poster-card__datex text-center"> 
                                             
                                        </div>
                                        
                                        

                                        <?php 

                                        if($this->aauth->is_loggedin()){

                                            $user_group = $this->Mcore->getUserGroup($this->session->userdata("id"));

                                        ?>
                                          <h6>Login SIP As</h6>
                                        <div class="tlogin tloginlg table-responsive" >
                                           <table class="table table-striped table-bordered" align="center">
                                                <tr>
                                                   <td><img style="width: 50px;height: auto;right-bottom: 20px;" <?php ($this->session->userdata('picture')!="")? $avatar=$this->session->userdata('picture'):$avatar=base_url().'public/assets/images/default-avatar.png';?> src="<?php echo $avatar; ?>" alt="Circle Image" class="rounded-circle"></td>
                                                   <td><span class="user_profile"><b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                </tr> 
                                                <tr>
                                                 <!--   <td><span class="user_profile"><b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td> -->
                                                </tr>
                                                <tr>
                                                   <td colspan="2"><span class="user_profile"> <?php echo $this->session->userdata('email');?></span></td>
                                                </tr>

                                                <tr>
                                                   <td colspan="2"><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                          <div>
                                          <a href="<?php echo base_url(); ?>admin" class="btn btn-warning btn-block btn-round"><i class="fa fa-cog"></i> Dashboard </a>

                                          <a href="<?php echo base_url(); ?>logout" class="btn btn-warning btn-block btn-round"><i class="fa fa-sign-out"></i> Logout </a>
                                          </div>
                                        
                                        <?php }else{ ?>


                                         <p>Login Sistem Informasi PERBARINDO (SIP) untuk BPR yang terdaftar di PERBARINDO.</p>
                                          <!-- <p>Login area untuk BPR yang terdaftar di PERBARINDO.</p> -->
                                          <br>
                                          <h6>Login SIP</h6>

                                          <form class="form" id="login">
                                            <div class="tcarousel">
                                                <div class="header header-primary text-center">
                                                  
                                                </div>
                                                <div class="content">
                                                  <div class="input-group form-group-no-border input-lg">

                                                     <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-map-marker"></i>
                                                        </span>
                                                       <select class="form-control select2" name="dpd" id="dpd" autocomplete="off" style="width: 100%">
                                                           <option value="">-- Pilih DPD --</option>

                                                            <?php 
                                                            foreach($dpd as $d){
                                                            
                                                             
                                                                  echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';
                                                              
                                                            } 
                                                            ?> 
                                                       </select>
                                                    </div>
                                                      
                                                    </div>
                                                    <div class="input-group form-group-no-border input-lg">

                                                       <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-university "></i>
                                                        </span> 

                                                           <select class="form-control select2" name="bpr" id="bpr" autocomplete="off" style="width: 100%;z-index: 9999;cursor: pointer!important;" disabled="disabled">
                                                              <option value="">-- Pilih BPR --</option>
                                                           </select>
                                                      </div>

                                                      
                                                     </div> 
                                                    <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon">
                                                            <i class="now-ui-icons users_circle-08"></i>
                                                        </span>
                                                        <input type="text" disabled data-prompt-position="bottomLeft"  name="username" id="username" class="form-control validate[required]" placeholder="Username">
                                                    </div>
                                                    <div class="input-group form-group-no-border input-lg">
                                                        <span class="input-group-addon">
                                                            <i class="now-ui-icons objects_key-25"></i>
                                                        </span>
                                                        <input type="password" disabled data-prompt-position="bottomLeft"  name="password" id="password" placeholder="Password" class="form-control validate[required]" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                 <button id="btn-submit" class="btn btn-warning btn-block btn-round"><i class="fa fa-sign-in"></i> Login SIP</button>
                                            </div>
                                          </form>

                                        <?php } ?>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article>



            </div>  



            <!-- DISPLAY MOBILE - SM - XS -->
            <div class="rowx  hidden-lg hidden-md visible-sm visible-xs">
 
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-university" style="float: left"></i> <h3>Balai Lelang</h3>
                                </div>
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                        <p>Daftar lelang yang diselegarakan oleh PERBARINDO.</p>
                                        <br>
                                        <h6>List Lelang</h6>
                                        <div id="auctionlist_m"></div>
                                         <div class="poster-card__datex text-center"> 
                                            
                                             <a href="<?php echo base_url(); ?>auction" class="btn btn-warning btn-blockx btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a>
                                        </div>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article> 
                  <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">
                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-calendar" style="float: left"></i> <h3>Event</h3>
                                </div>
                                <div class="body-card">
                                    <div class="poster-card__body">                             
                                       <!--  <p>Event - event PERBARINDO terbaru.</p> -->
                                        <p>List event terbaru yang dilaksanakan oleh PERBARINDO.</p>
                                        <br>
                                        <h6>List Event</h6>
                                        <div id="eventlist_m"></div>
                                         <div class="poster-card__datex text-center"> 
                                             <a href="<?php echo base_url(); ?>find/event" class="btn btn-warning btn-blockx btn-round"><i class="fa fa-eye"></i> Tampilkan Semua</a> 
                                        </div>
                                        
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article> 
                   <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="wrap-card poster-card card-lelang">

                                <div class="head-card label-lelang head-home">  
                                    <i class="fa fa-user" style="float: left"></i> <h3>SIP</h3>
                                </div>
                                <div class="body-card">
                                    
                                    <div class="poster-card__body">                             
                                        <div class="poster-card__datex text-center"> 
                                             
                                        </div>
                                        <?php 

                                        if($this->aauth->is_loggedin()){

                                            $user_group = $this->Mcore->getUserGroup($this->session->userdata("id"));

                                        ?>
                                          <h6>Login SIP As</h6>
                                          
                                          <div class="tlogin tloginsm hidden-lg hidden-md visible-sm hidden-xs table-responsive"> 
                                                <table class="table table-striped table-bordered" align="center" width="100%">
                                                    <tr>
                                                       <td align="center">
                                                          <center>
                                                          <img style="width: 90px;height: 90px;margin-bottom: 20px;" <?php ($this->session->userdata('picture')!="")? $avatar=$this->session->userdata('picture'):$avatar=base_url().'public/assets/images/default-avatar.png';?> src="<?php echo $avatar; ?>" alt="Circle Image" class="rounded-circle img-responsive img-fluid">
                                                          </center>
                                                        </td>
                                                        <td style="padding: 0px;">
                                                          <table width="100%">
                                                            <tr>
                                                               <td><span class="user_profile">  <b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                            </tr>

                                                            <tr>
                                                               <td><span class="user_profile">  <?php echo $this->session->userdata('email');?></span></td>
                                                            </tr>

                                                            <tr>
                                                               <td><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                    </tr>  

                                                    
                                                </table> 
                                          </div>
                                          <div class="tlogin tloginsm hidden-lg hidden-md hidden-sm visible-xs table-responsive"> 
                                                <table class="table table-striped table-bordered" align="center" width="100%">
                                                    <tr>
                                                       <td align="center">
                                                          <center>
                                                          <img style="width: 90px;height: 90px;margin-bottom: 20px;" src="<?php echo $this->session->userdata('picture'); ?>" alt="Circle Image" class="rounded-circle img-responsive img-fluid">
                                                          </center>
                                                        </td>
                                                    </tr>  

                                                    <tr>
                                                       <td><span class="user_profile">  <b> <?php echo $this->session->userdata('first_name') ?> <?php echo $this->session->userdata('last_name') ?></b></span></td>
                                                    </tr>

                                                    <tr>
                                                       <td><span class="user_profile">  <?php echo $this->session->userdata('email');?></span></td>
                                                    </tr>

                                                    <tr>
                                                       <td><span class="user_profile">IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></td>
                                                    </tr>
                                                </table> 
                                          </div>


                                          <div>
                                          <a href="<?php echo base_url(); ?>admin" class="btn btn-warning btn-block"><i class="fa fa-cog"></i> Dashboard </a>

                                          <a href="<?php echo base_url(); ?>logout" class="btn btn-warning btn-block"><i class="fa fa-sign-out"></i> Logout </a>
                                          </div>
                                        
                                        <?php }else{ ?>
                                         <p>Login Sistem Informasi PERBARINDO (SIP) untuk BPR yang terdaftar di PERBARINDO.</p>
                                         <!--  <p>Login area untuk BPR yang terdaftar di PERBARINDO.</p> -->
                                          <h6>Login SIP</h6>

                                          <form class="form" id="login_m">
                                            <div class="header header-primary text-center">
                                              
                                            </div>
                                            <div class="content">
                                              <div class="input-group form-group-no-border input-lg">

                                                 <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                   <select class="form-control select2" name="dpd" id="dpd_m" autocomplete="off" style="width: 100%">
                                                       <option value="">-- Pilih DPD --</option>

                                                        <?php 
                                                        foreach($dpd as $d){
                                                        
                                                         
                                                              echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';
                                                          
                                                        } 
                                                        ?> 
                                                   </select>
                                                </div>
                                                  
                                                </div>
                                                <div class="input-group form-group-no-border input-lg">

                                                   <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-university "></i>
                                                    </span> 

                                                       <select class="form-control select2" name="bpr" id="bpr_m" autocomplete="off" style="width: 100%;z-index: 9999;cursor: pointer!important;" disabled="disabled">
                                                          <option value="">-- Pilih BPR --</option>
                                                       </select>
                                                  </div>

                                                  
                                                 </div> 
                                                <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon">
                                                        <i class="now-ui-icons users_circle-08"></i>
                                                    </span>
                                                    <input type="text" disabled data-prompt-position="bottomLeft"  name="username" id="username_m" class="form-control validate[required]" placeholder="Username">
                                                </div>
                                                <div class="input-group form-group-no-border input-lg">
                                                    <span class="input-group-addon">
                                                        <i class="now-ui-icons objects_key-25"></i>
                                                    </span>
                                                    <input type="password" disabled data-prompt-position="bottomLeft"  name="password" id="password_m" placeholder="Password" class="form-control validate[required]" />
                                                </div>
                                            </div>
                                            <div class="footer text-center">
                                                 <button id="btn-submit" class="btn btn-warning btn-blockx btn-round"><i class="fa fa-sign-in"></i> Login SIP</button>
                                            </div>
                                            
                                          </form>

                                        <?php } ?>
                                    </div>   
                                </div>     
                          </div>
                      </div>
                  </article>



            </div>  
        </div> 
</section>

 <script type="text/javascript">
   $(document).ready(function(){

      $('#dpd').select2();
      $('#dpd').on('select2:selecting', function(e) {
         var id = e.params.args.data.id;
         var target = base_url + 'login_autocomplete/bpr/'+id;

         $.get(target, function(res){

              $('#bpr').html(res);
              $('#bpr').removeAttr("disabled");
              $('#bpr').select2();

              $('#bpr').on('select2:selecting', function(e) {

                $("#username").removeAttr("disabled");
                $("#password").removeAttr("disabled");
             
            });

         });


      });
      $('#bpr').select2();

        $("#login").validationEngine();

        $("#login").submit(function(){

          if($(this).validationEngine("validate")){

              var target = base_url + 'login_bpr';
              var data = $(this).serialize();

              $("body").waitMe({
                      effect: 'pulse',
                      text: 'Authenticating...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'
                  });
              
              $.post(target, data, function(res){

                  if(res.status == "1"){

                      $("body").waitMe({
                          effect: 'pulse',
                          text: 'Redirecting...',
                          bg: 'rgba(255,255,255,0.90)',
                          color: '#555'
                      });

                      toastr.success(res.msg, 'Response Server');
                     
                      setTimeout('window.location.href = base_url+"admin";',3000);
                      
                     
                  }else{
                      
                      $("body").waitMe("hide");
                      toastr.error(res.msg, 'Response Server');
                  
                  }

                 

              },'json');

              return false;

          }

      });

      $('#dpd_m').select2();
      $('#dpd_m').on('select2:selecting', function(e) {
         var id = e.params.args.data.id;
         var target = base_url + 'login_autocomplete/bpr/'+id;

         $.get(target, function(res){

              $('#bpr_m').html(res);
              $('#bpr_m').removeAttr("disabled");
              $('#bpr_m').select2();

              $('#bpr_m').on('select2:selecting', function(e) {

                $("#username_m").removeAttr("disabled");
                $("#password_m").removeAttr("disabled");
             
            });

         });


      });
      $('#bpr_m').select2();

        $("#login_m").validationEngine();

        $("#login_m").submit(function(){

          if($(this).validationEngine("validate")){

              var target = base_url + 'login_bpr';
              var data = $(this).serialize();

              $("body").waitMe({
                      effect: 'pulse',
                      text: 'Authenticating...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'
                  });
              
              $.post(target, data, function(res){

                  if(res.status == "1"){

                      $("body").waitMe({
                          effect: 'pulse',
                          text: 'Redirecting...',
                          bg: 'rgba(255,255,255,0.90)',
                          color: '#555'
                      });

                      toastr.success(res.msg, 'Response Server');
                     
                      setTimeout('window.location.href = base_url+"admin";',3000);
                      
                     
                  }else{
                      
                      $("body").waitMe("hide");
                      toastr.error(res.msg, 'Response Server');
                  
                  }

                 

              },'json');

              return false;

          }

      });

      $.get(base_url+"home/get_events/0/eventlist", function(res){

        $("#eventlist").html(res);

      });

      $.get(base_url+"home/get_events/0/eventlist_m", function(res){

        $("#eventlist_m").html(res);

      });

      $.get(base_url+"home/auction/get_auction/0/auctionlist", function(res){

        $("#auctionlist").html(res);

      });

      $.get(base_url+"home/auction/get_auction/0/auctionlist_m", function(res){

        $("#auctionlist_m").html(res);

      });


      $('.owl-carousel').owlCarousel({
            // loop:true,
            // margin:10,
            responsiveClass:true,
            // nav:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:false
                },
                600:{
                    items:2,
                    nav:true,
                    loop:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
      });

      $(".owl-prev").html("<i class='fa fa-angle-double-left fa-lg'></i>");
      $(".owl-next").html("<i class='fa fa-angle-double-right fa-lg'></i>");

    });
    function pagination(container,target){

      $("#"+container).html("<div class='preloader' style='padding: 100px;'><center><img src='"+base_url + "public/assets/img/loader.gif' /></center></div>");
      $("#"+container).load(target);

    }
</script>