<div class="wrapper" style="margin-top:-35px;">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>mocukup/bg-bulding.jpg');background-size: cover">
            </div>
          <!--   <div class="container">
                <div class="content-center">
                    <h2 class="title">Contact Us.</h2>
                    <div class="text-center">
                        <a href="<?php echo @$fb['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="<?php echo @$tw['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="<?php echo @$gg['value']; ?>" target="_blank" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
        
        <div class="section section-contact-us">
            <div class="container">
                <div class="row">
                    <div class="col col-md-6 pull-left">
                        <h3 class="titlex"><i class="fa fa-phone-square fa-lg"></i> Want to contact us?</h3>
                            <p class="descriptionx">Contact us if you have any question.</p>
                            <form id="contact_usx">
                            <div class="row">
                                <div class="col-md-12">
                                   <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user-circle"></i>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>

                                   <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>

                                   <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Judul">
                                    </div>
 
                                    <div class="textarea-container">
                                        <textarea class="form-control text-left validate[required]" name="message" rows="4" cols="80" placeholder="Type a message..."></textarea>
                                    </div>
                                    <div class="send-button text-left">
                                        <button type="submit" class="btn btn-primary">Send Message</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                    </div>
                    <div class="col col-md-6" style="border-left: 2px solid #ff7000;">
                        

                        <div class="label-contact">
                             <h4>Lokasi Kami</h4> 

                             <div class="wrap-footer">
                                <ul class="list-group list-group-footer list-group-info ">
                                    <li class="list-group-item">
                                        <a class="nav-link" href="#">
                                           <table class="nav-info">
                                               <tr>
                                                   <td>
                                                       <button class="btn btn-simple btn-round">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                                    </td>
                                                   <td class="cont">
                                                      Komplek Patra || No.46, Jl.Jendral Ahmad Yani <br> Bypass Cempaka Putih Jakarta Pusat
                                                   </td>
                                               </tr>
                                           </table>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="label-contact">
                            <h4>Hubungi Kami</h4> 

                             <div class="wrap-footer">
                                <ul class="list-group list-group-footer list-group-info">
                                   <li class="list-group-item">
                                        <a class="nav-link" href="#">
                                           <table class="nav-info">
                                               <tr>
                                                   <td>
                                                       <button class="btn btn-simple btn-round">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                                    </td>
                                                   <td class="cont">
                                                      Komplek Patra || No.46, Jl.Jendral Ahmad Yani <br> Bypass Cempaka Putih Jakarta Pusat
                                                   </td>
                                               </tr>
                                           </table>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>">
                                            <table class="nav-info">
                                               <tr>
                                                   <td>
                                                        <button class="btn btn-simple btn-round"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                                    </td>
                                                    <td class="cont">
                                                        Hubungi Kami Sekarang di <br> <h5><b>021-4261445</b></h5>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a class="nav-link" href="#">
                                            <table class="nav-info">
                                               <tr>
                                                   <td>
                                                        <button class="btn btn-simple btn-round"> <i class="fa fa-envelope fa-lg fa-1x"></i> </button> 
                                                    </td>
                                                    <td class="cont">
                                                         <h5><b>dpp_perbarindo@yahoo.com</b></h5>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <br/><br/> 
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.638093460429!2d106.87281831413719!3d-6.179173662266835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f4fc974af3c3%3A0xae439991ece8d270!2sPERBARINDO!5e0!3m2!1sen!2sid!4v1519383774835" width="600" height="100%" frameborder="0" style="border:0;width: 100%;height: 242%;border: 8px solid #fffbef;" allowfullscreen></iframe>
                    </div>
                </div>

                        <br/><br/> 

                        <br/><br/> 

                        <br/><br/>

                        <br/><br/> 

                        <br/><br/> 

                        <br/><br/>   
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".img-fill").imagefill();

            $("#contact_us").validationEngine();
            $("#contact_us").submit(function(){

              if($(this).validationEngine('validate')){

                var data = $(this).serialize();
                var target = base_url + 'submit/message';

                $("body").waitMe({

                      effect: 'pulse',
                      text: 'Sending Message...',
                      bg: 'rgba(255,255,255,0.90)',
                      color: '#555'

                });

                $.post(target, data, function(res){

                   $("body").waitMe("hide");

                    if(res.status=="1"){
                        toastr.success(res.msg, 'Response Server');
                    }else{
                        toastr.error(res.msg, 'Response Server');
                    }

                     $('#contact_us')[0].reset();

                },'json');

                return false;

              }
              

            });
        });
    </script>