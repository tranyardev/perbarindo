<style type="text/css">
    .tab-content .tab-pane{

        background-color: #eee;
        padding: 20px;
        border: solid 1px #ddd;
        margin-bottom: 50px;
        min-height: 500px;

    }
    
</style> 
<div class="wrapper">
          <div class="page-header page-header-small" filter-color="orange" > 
                <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo setMediaImage($auction['photo']) ?>');">
                </div> 
            </div>  
            <div class="section">
                <div class="container">
 
                <section>
                    <div class="row" style="margin-top:-453px;"> 
                        <div class="col-md-12">
                            <section>
                                <div class="card" >
                                    <div class="card-head row">
                                        <div class="card-img col-md-8">
                                            <img src="<?php echo setMediaImage($auction['photo']) ?>" class="img-responsive" width="100%">
                                        </div>
                                        <div class="card-heading col-md-4">  
                                             <div class="l-pad-top-6 l-pad-bot-2 l-lg-pad-bot-4 l-pad-hor-6">
                                                <div class="listing-hero-header hide-small ">
                                                    <time class="listing-hero-date" datetime="2017-10-21">
                                                        <p class="listing-hero-image--month">
                                                            <?php
                                                                $date=date_create($auction['auction_date']);
                                                                echo date_format($date,"M");
                                                             ?>
                                                        </p>
                                                        <p class="listing-hero-image--day">
                                                            <?php
                                                                $date=date_create($auction['auction_date']);
                                                                echo date_format($date,"d");
                                                             ?>
                                                        </p>
                                                    </time>
                                                </div>
                                                <div class="listing-hero-body ">
                                                    <h1 class="listing-hero-title" data-automation="listing-title"><?php echo $auction['auction_category_name']; ?> - <?php echo $auction['asset_code']; ?></h1>
                                                    <meta content="<?php echo $auction['auction_category_name']; ?>">
                                                    <div class="l-mar-top-3">
                                                        <div class="l-media clrfix listing-organizer-title">
                                                            <div class="l-align-left">
                                                                <a href="<?php echo base_url(); ?>profile/bpr/<?php echo $auction['bpr']; ?>" class="js-d-scroll-to listing-organizer-name text-default" data-d-duration="1500" data-d-offset="-70" data-d-destination="#listing-organizer" dorsal-guid="c4b13822-b124-5d86-db1b-ecdd65393a23" data-xd-wired="scroll-to">
                                                            by <?php echo $auction['bpr_name']; ?>
                                                        </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <br>

                                                  <div class="event-details l-lg-mar-left-6 l-lg-pad-left-6">
                                                <h3 class="label-primary l-mar-bot-2" data-automation="listing-info-language">Jadwal Lelang</h3>
                                                <div class="event-details-data">
                                                   
                                                        
                                                        <p>Tanggal : <?php echo $auction['auction_date']; ?></p>
                                                        <p>Waktu : <?php echo $auction['start_time'].' - '.$auction['end_time']; ?> </p>
                                                       
                                                    </time>
                                                </div>
                                                <h3 class="label-primary l-mar-bot-2">Lokasi</h3>
                                                <div class="event-details-data">
                                                    <p> <?php echo $auction['address'] ?></p>
                                                    <p> <?php echo $auction['city'] ?> </p>
                                                 
                                                    
                                                </div>
                                                <h3 class="label-primary l-mar-bot-2">Limit Lelang</h3>
                                                <div class="event-details-data">
                                                    
                                                    <ul>
                                                        

                                                            <li>Rp <?php echo number_format($auction['start_price'],2,",","."); ?></li>
                                                        
                                                       
                                                    </ul>
                                                    
                                                </div>
                                            </div> 
                                            </div> 
                                        </div>
                                    </div>
                                  
                                </div>
                            </section>
                        </div>

                         <div class="col-md-12">
                            <section>
                                <div class="card" >
                                   <div class="row">
                                        <div class="card-content col-md-12">
                                            <!-- ============= CONTENCT CENTER =====================--> 
                                         
                                    <ul id="myTab" class="nav nav-tabs" role="tablist" data-tabs="tabs">
                                        <li role="tabx" class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" role="tab" id="information" >
                                                Informasi
                                            </a>
                                        </li>
                                          <li role="tabx" class="nav-item">
                                            <a class="nav-link" data-toggle="tab" role="tab" id="term_of_condition" >
                                                Syarat & Ketentuan
                                            </a>
                                        </li>
                                        <li role="tabx" class="nav-item">
                                            <a class="nav-link" data-toggle="tab" role="tab" id="owner" >
                                                Pengelola
                                            </a>
                                        </li>
                                        
                                    </ul>
                               
                                    <!-- Tab panes -->
                                      <div class="tab-content">

                                        <div class="tab-pane active" id="information_content" >
                                            
                                            <?php echo $auction['description']; ?> 
                                            
                                        </div>
                                        
                                        <div class="tab-pane" id="term_of_condition_content" >
                                            <!--- BIGEN TAB HOME -->   
                                             <?php echo $auction['term_of_condition']; ?> 

                                        </div>

                                        <div class="tab-pane" id="owner_content" >
                                            
                                            <h5>Dikelola Oleh</h5>

                                            <p><?php echo $auction['bpr_name']; ?></p>

                                            <h5>Alamat</h5>

                                            <p><?php echo $auction['bpr_address']; ?></p>

                                            <h5>Kontak</h5>

                                            <p><?php echo $auction['telp']; ?></p>
                                            <p><?php echo $auction['bpr_email']; ?></p>
                                              
                                        </div>

                                     </div> <!-- / tab content --> 


                                            <div class="box-share">
                                                <div class="listing-info__footer clrfix">
                                                    <div class="share-wrapper l-sm-mar-bot-4">
                                                        <h3 class="label-primary l-sm-align-left">Share with friends</h3>
                                                        <!-- Listing Share Options -->
                                                        <div class="js-share-wrapper l-sm-align-left">
                                                            <section>
                                                                <section>
                                                                    <ul class="grouped-ico grouped-ico--share-links js-social-share-list fx--fade-in" data-automation="social-share-inline">
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round " href="https://www.facebook.com/sharer.php?u=<?php echo base_url().'home/bpr/asset_code/'.$auction['asset_code'];?>" target="_blank" title="Share this page on Facebook">
                                                                                <i class="ico-facebook-badge ico--color-understated-link ico--xlarge" data-automation="facebook-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-facebook-official"></i> Facebook</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://www.linkedin.com/shareArticle?url=<?php echo base_url().'home/bpr/asset_code/'.$auction['asset_code'];?>&title=lelang-<?php echo $auction['auction_category_name']; ?>-<?php echo $auction['asset_code']; ?>" target="_blank" title="Share this page on LinkedIn">
                                                                                <i class="ico-linkedin-badge ico--color-understated-link ico--xlarge" data-automation="linkedin-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-linkedin-square"></i> LinkedIn</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://twitter.com/intent/tweet?url=<?php echo base_url().'home/bpr/asset_code/'.$auction['asset_code'];?>&text=lelang-<?php echo $auction['auction_category_name']; ?>-<?php echo $auction['asset_code']; ?>&via=<?php echo base_url();?>&hashtags=<?php echo str_replace(" ","_","lelang-".$auction['auction_category_name']."-".$auction['asset_code']); ?>" target="_blank" title="Share this page on Twitter">
                                                                                <i class="ico-twitter-badge ico--color-understated-link ico--xlarge" data-automation="twitter-share-link">
                                                                                </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-twitter-square"></i> Twitter</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="js-social-share btn btn-primary btn-round" href="https://plus.google.com/share?url=<?php echo base_url().'home/bpr/asset_code/'.$auction['asset_code'];?>" target="_blank" title="Share this page on google">
                                                                                <i class="ico-google-badge ico--color-understated-link ico--xlarge" data-automation="google-share-link">
                                                                                 </i>
                                                                                <span class="is-hidden-accessible"><i class="fa fa-google-plus-official"></i> Google Plus</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </section>
                                                            </section>
                                                        </div>
                                                    </div>
                                                    <!-- Fallback anchor for the link to the ticket area -->
                                                    <span id="listing-ticket"></span>
                                                </div>
                                            </div>

                                            <!-- ============= END CONTENT CONTER ==================-->
                                        </div>
                                       
                                   </div>
                                   <div class="row">
                                       <div class="col-md-12">
                                       
                                        
                                            <section class="map-maker" id="map-target">
                                                <div class="map-body"> 
                                                    <article class="border-map">
                                                        <div class="text-center">
                                                            <button data-toggle="collapse" data-target="#mapmapker" class="btn btn-primary btn-simple btn-round fa-lg"><i class="fa fa-map-marker fa-lg"></i> Map Location</button> 
                                                        </div>
                                                        <div id="mapmapker" class="collapse show">
                                                            <div id="map" style="width: 100%;height: 400px;"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </section>
                                          

                                       </div>
                                   </div>
                                </div>
                            </section>
                            
                        </div>
                     
                    </div>
                </section>


           
                    </div> 
                </div>
            </div>
            
        </div>

<script type="text/javascript">
    
    $(document).ready(function(){
        initMap();

        $('#information').click(function (e) {

            e.preventDefault()
            $("#information_content").slideDown('slow');
            $("#term_of_condition_content").hide('slow');
            $("#owner_content").hide('slow');
           
        });

        $('#term_of_condition').click(function (e) {

            e.preventDefault()
            $("#information_content").hide('slow');
            $("#term_of_condition_content").slideDown('slow');
            $("#owner_content").hide('slow');
           
        });

        $('#owner').click(function (e) {

            e.preventDefault()
            $("#information_content").hide('slow');
            $("#term_of_condition_content").hide('slow');
            $("#owner_content").slideDown('slow');
           
        });

        $("#bookmarkme").click(function() {
            if (window.sidebar) { // Mozilla Firefox Bookmark
              window.sidebar.addPanel(location.href,document.title,"");
            } else if(window.external) { // IE Favorite
              window.external.AddFavorite(location.href,document.title); }
            else if(window.opera && window.print) { // Opera Hotlist
              this.title=document.title;
              return true;
            }
        });
    });

    function initMap() {

        var loc = {lat: parseFloat('<?php echo $auction['latitude']; ?>'), lng: parseFloat('<?php echo $auction['longitude']; ?>')}

        var map = new google.maps.Map(document.getElementById('map'), {
            center: loc,
            zoom: 13
        });
        

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: loc,
            map: map
        });

    }
    
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places"></script>