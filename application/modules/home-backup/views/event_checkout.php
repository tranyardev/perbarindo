 <div class="wrapper">
    <div class="page-header page-header-small" filter-color="orange"> 
        <div class="page-header-image" data-parallax="true" style="background-image: url('assets/img/bg5.jpg');">
        </div> 
    </div>  
    <div class="section">
        <div class="container">


        <section>
          <div class="row" style=" margin-top: -444px;" >
            <div class="col-md-8"> 
                <div class="card">        
                        <div class="text-center padding-12" >
                            <h3 class="title margin-0"  >RINGKASAN TRANSAKSI</h3>
                            <h6 class="description margin-0"><?php echo $event_registration['event_name']; ?> - #<?php echo $event_registration['regcode']; ?></h6>
                        </div>

                        <hr>

                        <div class="panel panel-default">
                            
                            <div class="panel-body" style="padding: 15px;">

                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Perusahaan/BPR</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td>Nama</td>
                                    <td><?php echo $event_registration['bpr_name']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>DPD</td>
                                    <td><?php echo $event_registration['dpd_name']; ?></td>
                                  </tr>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data PIC</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td>Nama</td>
                                    <td><?php echo $event_registration['member_name']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Email</td>
                                    <td><?php echo $event_registration['member_email']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>No HP</td>
                                    <td><?php echo $event_registration['no_hp']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Job Position</td>
                                    <td><?php echo $event_registration['member_job_position']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Jenis Kelamin</td>
                                    <td><?php ($event_registration['gender']=="L")? $gender="Laki - laki":$gender="Perempuan";echo $gender; ?></td>
                                  </tr>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Peserta</strong></div>
                              </div>
                              <hr>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <thead>
                                    <tr>
                                      <th>Nama</th>
                                      <th>No HP</th>
                                      <th>Paket</th>
                                      <th style="width: 166px;">Harga</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach($event_member as $em){ ?>
                                    <tr>
                                      <td><?php echo $em['name']; ?></td>
                                      <td><?php echo $em['no_hp']; ?></td>
                                      <td><?php echo $em['package_name']; ?></td>
                                      <td>Rp <?php echo number_format($em['price'],2,",","."); ?></td>
                                                                   
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                              </table>
                              <br><br>
                              <div class="text-left">
                                <div class="panel-heading"><strong>Data Peserta Twin Sharing</strong></div>
                              </div>
                              <hr>
                              <?php 
                              foreach($event_member_twin_sharing as $emt){ 
                                $with_member = $this->mevent->getMemberById($emt['twin_sharing_with']);
                              ?>
                              <table class="table table-bordered table-striped" style="width: 100%">
                                  <thead>
                                    <tr>
                                      <th>Nama</th>
                                      <th>No HP</th>
                                      <th>Paket</th>
                                      <th style="width: 166px;">Harga</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td><?php echo $emt['name']; ?></td>
                                      <td><?php echo $emt['no_hp']; ?></td>
                                      <td><?php echo $emt['package_name']; ?></td>
                                      <td>Rp <?php echo number_format($emt['price'],2,",","."); ?></td>                                 
                                    </tr>
                                    <tr>
                                      <td><?php echo @$with_member[0]['name']; ?></td>
                                      <td><?php echo @$with_member[0]['no_hp']; ?></td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>                                 
                                    </tr>
                                  </tbody>
                              </table>
                              <br><br>
                              <?php } ?>

                               <table class="table table-bordered table-striped" style="width: 100%">
                                  <tr>
                                    <td colspan="3">Total</td>
                                   
                                    <td style="width: 166px;"><strong>Rp <?php echo number_format($event_registration['total_cost'],2,",","."); ?></strong></td>
                                  </tr>
                              </table>
                            </div>
                      </div>
              </div>
            </div>
            <div class="col-md-4">
                 <div class="card">         
                        <div class="kc-summary-details"> 
                          <div class="kc-summary-item-container">
                          <img class="kc-summary-item-image" src="<?php echo setEventImage($event_registration['cover']) ?>" role="presentation">
                              <div class="kc-summary-item">   
                                  <div class="title-ivent">
                                      
                                      <div class="listing-hero-body ">
                                          <h5 class="listing-hero-title-x" data-automation="listing-title"><?php echo $event_registration['event_name']; ?></h5>
                                          <meta content="<?php echo $event_registration['event_name']; ?>">
                                          <div class="l-mar-top-3">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <span><i class="fa fa-map-marker"></i> &nbsp;<strong>Lokasi</strong> <br><?php echo $event_registration['address']; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="l-mar-top-3" style="padding-top: 10px;">
                                              <div class="l-media clrfix listing-organizer-title">
                                                  <div class="l-align-left">
                                                      <?php
                                                        $sdate=date_create($event_registration['start_date']);
                                                        $start_date = date_format($sdate,"d/m/Y");

                                                        $edate=date_create($event_registration['end_date']);
                                                        $end_date = date_format($edate,"d/m/Y");
                                                      ?>
                                                      <span><i class="fa fa-calendar"></i> <strong>Tanggal</strong> <br><?php echo $start_date; ?> - <?php echo $end_date; ?></span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                     
                                  </div>
                               
                              </div>
                          </div>
                          <div class="kc-summary-info-container"> 
                          <table class="table">
                              
                              <tr>
                                  <td>
                                      <span ><b>Total</b></span>
                                  </td>
                                  
                                  <td class="text-left">
                                    
                                    <span class="text-orange" id="total"></span>
                                  </td>
                              </tr>
                              
                              <tr>
                                  <td colspan="2" style="text-align: center;">
                                      <a href="javascript:cancel_reservation('<?php echo $event_registration['regcode']; ?>')" class="btn btn-danger">Cancel</a>&nbsp;
                                      <a href="<?php echo base_url(); ?>checkout/payment_info/<?php echo $event_registration['regcode']; ?>" class="btn btn-primary">Lanjut Pembayaran</a>
                                  </td>
                                 
                              </tr> 

                          </table> 
                          </div>
                      </div>

                </div>
            </div>
          </div>
        </section>


            </div> 
        </div>
    </div>
</div>
<section>
      <!-- Sart Modal -->
        <div class="modal fade" id="cancel_reservation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <h4 class="title title-up">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="kc-modal-body">
                            <p>Apakah anda yakin ingin membatalkan reservasi ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="do_cancel" class="btn btn-primary" data-regcode="" data-dismiss="modal">Ya</button> 
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button> 
                    </div>
                </div>
            </div>
        </div>
        <!--  End Modal -->
</section>
<script type="text/javascript">
  $(document).ready(function(){

    var total = '<?php echo $event_registration['total_cost']; ?>';

     // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');

    $('#total').html(numeral(total).format("$0,0"));

    $("#do_cancel").click(function(){

        var data = { regcode : $(this).data("regcode") }
        var target = base_url + 'cancel/transaction';
        $("body").waitMe({

                    effect: 'pulse',
                    text: 'Loading...',
                    bg: 'rgba(255,255,255,0.90)',
                    color: '#555'

                });

        $.post(target,data,function(res){

          if(res.status=="1"){
              toastr.success(res.msg, 'Response Server');
          }else{
              toastr.error(res.msg, 'Response Server');
          }

          $("body").waitMe('hide');

          setTimeout('window.location.href="'+base_url+'myprofile";',3000);

        },'json');

   });


  });

  function cancel_reservation(regcode){

    $("#do_cancel").attr("data-regcode", regcode);
    $("#cancel_reservation").modal("show");

  }
</script>