<?php

class Mbprfinanceassettype extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRFinanceAssetType(){

        $result = $this->db->query("SELECT * FROM bpr_finance_asset_types")->result_array();
        return $result;

    }
    function getBPRFinanceAssetTypeByIsProduct($is_product){

        $result = $this->db->query("SELECT * FROM bpr_finance_asset_types WHERE is_product='".$is_product."'")->result_array();
        return $result;

    }
    function getBPRFinanceAssetTypeById($id){

        $result = $this->db->query("SELECT * FROM bpr_finance_asset_types WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}