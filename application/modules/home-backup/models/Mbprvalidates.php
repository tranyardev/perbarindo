<?php

class Mbprvalidates extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getBPRValidates(){

        $result = $this->db->query("SELECT * FROM bpr_validates")->result_array();
        return $result;

    }
    function getBPRValidatesById($id){

        $result = $this->db->query("SELECT * FROM bpr_validates WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRValidatesByBPR($bpr){

        $result = $this->db->query("SELECT * FROM bpr_validates WHERE bpr='".$bpr."'")->result_array();
        return @$result[0];

    }
    function getBPRValidatesByToken($token, $bpr){

        $result = $this->db->query("SELECT * FROM bpr_validates WHERE token='".$token."' AND bpr='".$bpr."'")->result_array();
        return @$result[0];

    }



    /*function getBPRByDPDId($id){

        $result = $this->db->query("SELECT * FROM bpr WHERE dpd='".$id."'")->result_array();
        return $result;

    }

    function getBPRBranches($id){

        $result = $this->db->query("SELECT a.*, b.name as corp, c.name as dpd_name FROM bpr a 
                                    LEFT JOIN corporates b ON b.id = a.corporate
                                    LEFT JOIN dpd c ON c.id = a.dpd 
                                    WHERE a.central_branch='".$id."' AND is_branch='1'")->result_array();
        return $result;

    }

    function getBPRByName($bprName){

        $result = $this->db->query("SELECT * FROM bpr WHERE name='".$bprName."'")->result_array();
        return $result;

    }
    function getConfigurationByKey($key, $scope){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope='".$scope."' and key='".$key."'")->result_array();
        return @$result[0];

    }*/
 
}