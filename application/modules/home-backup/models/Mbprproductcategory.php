<?php

class Mbprproductcategory extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRProductCategory($bpr, $parent){

        if($parent){

            $where = " AND parent is null";

        }else{

            $where = "";

        }

        $result = $this->db->query("SELECT * FROM bpr_product_categories WHERE bpr='".$bpr."'".$where)->result_array();
        return $result;

    }
    
    function getBPRAllProductCategory($parent){

        if($parent){

            $where = " AND parent is null";

        }else{

            $where = "";

        }


        $result = $this->db->query("SELECT * FROM bpr_product_categories WHERE status='1'".$where)->result_array();
        return $result;

    }

    function getBPRProductCategoryByParent($bpr, $parent){

        $result = $this->db->query("SELECT * FROM bpr_product_categories WHERE bpr='".$bpr."' AND parent = '".$parent."'")->result_array();
        return $result;

    }
    function getBPRProductCategoryById($id){

        $result = $this->db->query("SELECT * FROM bpr_product_categories WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}