<?php

class Mbprfinanceasset extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
   
    function getBPRFinanceAsset(){

        $result = $this->db->query("SELECT * FROM bpr_finance_assets")->result_array();
        return $result;

    }
    function getBPRFinanceAssetById($id){

        $result = $this->db->query("SELECT * FROM bpr_finance_assets WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getBPRFinanceAssetByBPR($bpr){

        $result = $this->db->query("SELECT a.*, b.name FROM bpr_finance_assets a
                                    LEFT JOIN bpr_finance_asset_types b ON b.id = a.finance_asset_type 
                                    WHERE a.bpr='".$bpr."' AND year='".date("Y")."'")->result_array();
        return $result;


    }
    function getBPRFinanceAssetByAssetType($bpr,$type,$year){

        $result = $this->db->query("SELECT a.*, b.name FROM bpr_finance_assets a
                                    LEFT JOIN bpr_finance_asset_types b ON b.id = a.finance_asset_type 
                                    WHERE a.bpr='".$bpr."' AND finance_asset_type='".$type."' AND a.year='".$year."'")->result_array();
        return @$result[0];


    }
    
 
}