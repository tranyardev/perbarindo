<?php

class Mbprauction extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }

    function getAllAuctionList(){

        $result = $this->db->query("SELECT a.*,b.name as auction_category_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category")->result_array();
        return $result;

    }

    function getAuctionList($offset, $limit){

        $result = $this->db->query("SELECT a.*,b.name as auction_category_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    LIMIT ".$limit." OFFSET ".$offset)->result_array();
        return $result;

    }
   
    function getBPRAuction($bpr, $category, $offset, $limit, $asset_code){

        $where = "";

        if($category!==null && $category!="" && $category!="-"){

            $where .= " AND auction_category='".$category."'";

        }

        if($asset_code !== null){

            $where .= " AND asset_code ILIKE '%".$asset_code."%'";

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }



        $result = $this->db->query("SELECT a.*, b.name as auction_category_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    WHERE a.bpr='".$bpr."'".$where.$page)->result_array();
        return $result;

    }

    function getBPRAuctionList($category, $offset, $limit, $asset_code){

        $where = "";

        if($category!==null && $category!="" && $category!="-"){

            $where .= " AND auction_category='".$category."'";

        }

        if($asset_code !== null){

            $where .= " AND asset_code ILIKE '%".$asset_code."%'";

        }

        if($offset!==null && $limit!==null){

            $page = " LIMIT ".$limit." OFFSET ".$offset;

        }else{

            $page = "";

        }



        $result = $this->db->query("SELECT a.*, b.name as auction_category_name FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    WHERE a.id IS NOT NULL ".$where.$page)->result_array();
        return $result;

    }


    function getAuctionByAssetCode($asset_code){

        $result = $this->db->query("SELECT a.*, b.name as auction_category_name, concat(d.name,' ',c.name) as bpr_name, c.email as bpr_email, c.telp, c.address as bpr_address FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    LEFT JOIN bpr c ON c.id=a.bpr
                                    LEFT JOIN corporates d ON d.id=c.corporate
                                    WHERE a.asset_code='".$asset_code."'")->result_array();
        return @$result[0];

    }

    function getLatestAuction($limit){

        $result = $this->db->query("SELECT a.*, b.name as auction_category_name, concat(d.name,' ',c.name) as bpr_name, c.email as bpr_email, c.telp, c.address as bpr_address FROM bpr_auctions a 
                                    LEFT JOIN bpr_auction_categories b ON b.id=a.auction_category
                                    LEFT JOIN bpr c ON c.id=a.bpr
                                    LEFT JOIN corporates d ON d.id=c.corporate
                                    ORDER BY created_at DESC LIMIT ".$limit)->result_array();
        return $result;

    }

    function getCountBPRAuction($bpr){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE bpr='".$bpr."'")->num_rows();
        return $result;

    }
    function getBPRAuctionById($id){

        $result = $this->db->query("SELECT * FROM bpr_auctions WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    
 
}