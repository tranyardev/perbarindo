<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

class Home extends MX_Controller{

    function __construct(){

        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("ciqrcode");
        $this->load->model("mevent");
        $this->load->model("Mbprarticle");
        $this->load->model("Mbprproduct");
        $this->load->model("Mbprauction");
        $this->load->model("MSlider");
        $this->load->model("Mdpd");
        $this->load->model("MSubscribe");
        
    
    }

    function index(){

        $data['page'] = 'home-new';
        $data['og_title'] = 'PERBARINDO | EVENT';
        $data['og_site_name'] = 'PERBARINDO EVENT';
        $data['og_site_url'] = base_url();
        $data['og_site_description'] = 'Organisasi PERHIMPUNAN BANK PERKREDITAN RAKYAT INDONESIA disingkat PERBARINDO, dibentuk pada tanggal 24 Januari 1995. Organisasi PERBARINDO merupakan wadah bagi Bank Perkreditan Rakyat Indonesia yang bersifat independen berdasarkan Pancasila dan Undang-Undang Dasar 1945.';
        $data['og_site_image'] = 'http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg';

        $data['data']['upcoming_events'] = $this->mevent->getUpComingEvent();
        $data['data']['sliders'] = $this->MSlider->getSlider();
        $data['data']['dpd'] = $this->Mdpd->getDPD();
        // $data['data']['latest_events'] = $this->mevent->getLatestEvent(9);
        // $data['data']['latest_posts'] = $this->Mbprarticle->getLatestPost(8);
        // $data['data']['latest_products'] = $this->Mbprproduct->getLatestProduct(9);
        // $data['data']['latest_auctions'] = $this->Mbprauction->getLatestAuction(9);
        $data['header']['active'] = "home";
        $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
        $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
        $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
        $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
        $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");
       	$data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';

        $this->load->view('layout/main-new',$data);

    }

    public function doc_viewer($file){

        $array = explode('.', $file);
        $extension = end($array);

        if($extension=='pdf'){
            $html ='<script src="'.base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js"></script>';
            $html.='<script src="'.base_url().'assets/plugins/PDFObject/pdfobject.min.js"></script>';
            $html.='<div id="pdf_preview" style="height: 700px;"></div>';
            $html.= '<script type="text/javascript">';
            $html.= '$(document).ready(function(){';
            $html.= 'PDFObject.embed("'.base_url().'upload/media/'.$file.'", "#pdf_preview");';
            $html.= '});';
            $html.= '</script>';
            echo $html;
        }else{
            echo "<iframe style='width:100%;border:none;height:1000px;' src='https://view.officeapps.live.com/op/embed.aspx?src=".base_url()."upload/media/".$file."'></iframe>";
        }

       

    }
    

    function get_events($page=0,$selector = "eventlist"){

        $alldata = $this->mevent->getAllLatestEvent();
       
        $page_config = $this->config->item('pagination');
        $page_config['per_page'] = 4;
        $page_config['uri_segment'] = 3;
        $page_config['total_rows'] = count($alldata);
        $page_config['base_url'] = "javascript:pagination('".$selector."','".base_url() . "/home/get_events/";
        $page_config['first_url'] = "javascript:pagination('".$selector."','".base_url() . "/home/get_events/0/".$selector."')";
        $page_config['suffix'] = "/".$selector."')";

        $start=$page;

        if(empty($start)){

            $start=0;
        
        }

        $this->pagination->initialize($page_config);
        $events = $this->mevent->getListEvent($start,$page_config['per_page']);
        $paging = $this->pagination->create_links();

        $html = "<ul class='list-group ovhiden'>";

        foreach($events as $e){ 

           // $html.='<li class="list-group-item">'.$e['name'].' <label class="label-info pull-right"><a href="'.base_url().'show/event/'.$e['id'].'">(Read More)</a></label></li>';

            $html.= '
            <li class="list-group-item padding-0">
             <a href="'.base_url().'show/event/'.$e['id'].'" class="list-group-item list-group-item-action flex-column align-items-start">
              <div class="d-flex w-100 justify-content-between">
                <i class="fa fa-circle icon-orange" aria-hidden="true"></i>
                  <div class="marquee">
                  <h6 class="titlelistg padding-0"><span> '.$e['name'].' </span></h6>
                  </div>
                <small class="smallx">'.$e['created_at'].'</small>
              </div> 
              <small class="fsz12">'.$e['city'].'.</small>
            </a>

            </li>
          ';

        } 

        $html.= "</ul>";
        $html.= "<div class='text-center ml-auto pagning'>";
        $html.= $paging;
        $html.= "</div>";
               
        echo $html;

    }

    function show_event($id=null){

        if($id!=null){

            $data['page'] = 'event_detail';

            $data['data']['id'] = $id;
            $data['data']['event'] = $this->mevent->getEventById($id);
            $data['data']['other_events'] = $this->mevent->getEventByRandom($id, 3);
            $data['data']['event_packages'] = $this->mevent->getEventPackage($id);
            $data['data']['event_activity_dates'] = $this->mevent->getEventActivityDates($id);
            $data['body_class'] = 'profile-page';
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            $data['og_title'] = $data['data']['event']['name'];
            $data['og_site_name'] = $data['data']['event']['name'];
            $data['og_site_url'] = base_url().'show/event/'.$id;
            $data['og_site_description'] = strip_tags($data['data']['event']['description']);
            $data['og_site_image'] = $this->setEventImage($data['data']['event']['cover']);

            $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
            $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
            $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
            $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
            $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
            $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
            $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

            $this->load->view('layout/main-new',$data);

        }

    }

    function event_registration($id=null){

        if($id!=null){

            if($this->aauth->is_loggedin()){

                $data['page'] = 'event_registration';
                $data['body_class'] = 'profile-page';
                $data['data']['event'] = $this->mevent->getEventById($id);
                $data['data']['event_packages'] = $this->mevent->getEventPackage($id);
                $data['data']['dpd'] = $this->mevent->getDPD();
                $data['data']['job_positions'] = $this->mevent->getJobPosition();
                $data['data']['event_id'] = $id;
                $data['navbar']['navbar_class'] = 'navbar-transparent';

                $this->load->view('layout/main',$data);

            }else{

                $this->session->set_userdata('callback_url', base_url().'event/registration/'.$id);

                redirect('login');

            }

        }

    }

    function submit_registration(){

        if(count($_POST)>0){

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";exit;

            $event_id = $_POST['event_id'];
            $expired_trx = $this->mevent->getConfigurationItem('PAYMENT','EXPIRED_TRX');
            $expired_date = date("Y-m-d H:i:s", strtotime('+'.$expired_trx.' hours'));

           /*CHECK BPR*/

            if($_POST['new_bpr']!=""){

            /* INSERT NEW BPR*/

                $data_bpr = array(
                    "name" => $_POST['new_bpr'], 
                    "telp" => $_POST['bpr_telp'],
                    "fax" => $_POST['bpr_fax'],
                    "email" => $_POST['bpr_email'],
                    "address" => $_POST['bpr_address'],
                    "website" => $_POST['bpr_website'],
                    "dpd" => $_POST['dpd'],
                );

                $bpr = $this->db->insert("bpr",$data_bpr);

                $id_bpr = $this->db->insert_id();

            }else{

                $data_bpr = array(
                    
                    "telp" => $_POST['bpr_telp'],
                    "fax" => $_POST['bpr_fax'],
                    "email" => $_POST['bpr_email'],
                    "address" => $_POST['bpr_address'],
                    "website" => $_POST['bpr_website'],
                    "dpd" => $_POST['dpd'],

                );

                $this->db->where('id', $_POST['bpr']);
                $this->db->update('bpr', $data_bpr); 

                $id_bpr = $_POST['bpr'];

            }

            /* CHECK PIC */

            if($_POST['registrar_new_name']!="" && $_POST['registrar_new_name']!="0"){

                $data_pic = array(

                    "name" => $_POST['registrar_new_name'],
                    "job_position" => $_POST['registrar_job'],
                    "gender" => $_POST['registrar_gender'],
                    "no_hp" => $_POST['registrar_hp'],
                    "email" => $_POST['registrar_email']
                );

                $pic = $this->db->insert("members",$data_pic);

                $id_pic = $this->db->insert_id();

            }else{

                $data_pic = array(

                    "job_position" => $_POST['registrar_job'],
                    "gender" => $_POST['registrar_gender'],
                    "no_hp" => $_POST['registrar_hp'],
                    "email" => $_POST['registrar_email']
                );

                $this->db->where('id', $_POST['registrar_name']);
                $this->db->update('members', $data_pic); 

                $id_pic = $_POST['registrar_name'];

            }

            /* INSERT EVENT REGISTRATION */

            $regcode = "PRB-".date("Ymdhis");
            $uniq_number = rand ( 100 , 999 );

            $data_reg = array(

                "event_id" => $event_id,
                "date" => date("Y-m-d h:i:s"),
                "total_cost" => $_POST['total_cost'],
                "regcode" => $regcode,
                "bpr" => $id_bpr,
                "member_id" => $id_pic,
                "uniq_number" => $uniq_number,
                "expired_date" => $expired_date,
                "status" => 'WAITING_TRANSACTION',
                "aauth_user_id" => $this->session->userdata("id"),


            );

            $reg = $this->db->insert("event_registrations",$data_reg);
            $id_reg = $this->db->insert_id();

            /* INSERT DATA PARTICIPANT/EVENT MEMBER */

            foreach($_POST['member1_name'] as $index => $value) {

                $ex = explode("-",$_POST['member_package'][$index]);
                $member_gender = $_POST['member_gender'][$index];
                $member_no_hp = $_POST['member_no_hp'][$index];
                $member_position = $_POST['member_position'][$index];
                $member_package = $ex[0];
                $member_name = $_POST['member2_name'][$index];

                if($_POST['member1_name'][$index]!="" && $_POST['member1_name'][$index]!="0"){


                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,

                    );

                    $this->db->where('id', $_POST['member1_name'][$index]);
                    $this->db->update('members', $data_member); 

                    $id_member = $_POST['member1_name'][$index];

                }else{

                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,
                        "name" => $member_name,
                        "bpr" => $id_bpr

                    );

                    $member = $this->db->insert("members",$data_member);

                    $id_member = $this->db->insert_id();

                }

                /* INSERT EVENT MEMBER */

                $data_event_member = array(

                    "event_id" => $event_id,
                    "registration_id" => $id_reg,
                    "member_id" => $id_member,
                    "package_id" => $member_package,
                    "is_twin_sharing" => "0",

                );

                $this->db->insert("event_members",$data_event_member);

            }

            /* INSERT DATA PARTICIPANT/EVENT MEMBER(TWIN SHARING) */

            foreach($_POST['member1_name_twin'] as $index => $value) {

                /* First Member */
                $ex = $_POST['member_package_twin'][$index];
                $member_gender = $_POST['member_gender_twin'][$index];
                $member_no_hp = $_POST['member_no_hp_twin'][$index];
                $member_position = $_POST['member_job_twin'][$index];
                $member_package = $ex[0];
                $member_name = $_POST['member2_name_twin'][$index];

                if($_POST['member1_name_twin'][$index]!="" && $_POST['member1_name_twin'][$index]!="0"){


                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,

                    );

                    $this->db->where('id', $_POST['member1_name_twin'][$index]);
                    $this->db->update('members', $data_member); 

                    $id_member = $_POST['member1_name_twin'][$index];

                }else{

                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,
                        "name" => $member_name,
                        "bpr" => $id_bpr

                    );

                    $member = $this->db->insert("members",$data_member);

                    $id_member = $this->db->insert_id();

                }


                /* Second(with) Member */
                $member_gender = $_POST['member_gender_twin_with'][$index];
                $member_no_hp = $_POST['member_no_hp_twin_with'][$index];
                $member_position = $_POST['member_job_twin_with'][$index];
                $member_name = $_POST['member2_name_twin_with'][$index];

                if($_POST['member1_name_twin_with'][$index]!="" && $_POST['member1_name_twin_with'][$index]!="0"){


                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,

                    );

                    $this->db->where('id', $_POST['member1_name_twin_with'][$index]);
                    $this->db->update('members', $data_member); 

                    $id_with_member = $_POST['member1_name_twin_with'][$index];

                }else{

                    $data_member = array(

                        "gender" => $member_gender,
                        "no_hp" => $member_no_hp,
                        "job_position" => $member_position,
                        "name" => $member_name,
                        "bpr" => $id_bpr

                    );

                    $member = $this->db->insert("members",$data_member);

                    $id_with_member = $this->db->insert_id();

                }


                /* INSERT EVENT MEMBER */

                $data_event_member = array(

                    "event_id" => $event_id,
                    "registration_id" => $id_reg,
                    "member_id" => $id_member,
                    "package_id" => $member_package,
                    "is_twin_sharing" => "1",
                    "twin_sharing_with" => $id_with_member,

                );

                $this->db->insert("event_members",$data_event_member);

            }

            $res = array("status" => "1", "reg_code" => $regcode, "msg" => "success");

            echo json_encode($res);

        }

    }

    function checkout($regcode=null){

        if($regcode!=null){

           

            $data['page'] = 'event_checkout';
            $data['body_class'] = 'profile-page';
            $data['data']['event_registration'] = $this->mevent->getEventRegistration($regcode);
            $data['data']['event_member'] = $this->mevent->getEventMember($data['data']['event_registration']['reg_id'],'0');
            $data['data']['event_member_twin_sharing'] = $this->mevent->getEventMember($data['data']['event_registration']['reg_id'],'1');
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            // if($this->session->userdata('id') != $data['data']['event_registration']['aauth_user_id']){

            //     redirect('myprofile');

            // }

            $this->load->view('layout/main',$data);

        }

    }

    function payment_detail($regcode=null){

        if($regcode!=null){

           

            $data['page'] = 'event_payment_detail';
            $data['body_class'] = 'profile-page';
            $data['data']['event_registration'] = $this->mevent->getEventRegistration($regcode);
            $data['data']['event_member'] = $this->mevent->getEventMember($data['data']['event_registration']['reg_id'],'0');
            $data['data']['event_member_twin_sharing'] = $this->mevent->getEventMember($data['data']['event_registration']['reg_id'],'1');
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            // if($this->session->userdata('id') != $data['data']['event_registration']['aauth_user_id']){

            //     redirect('myprofile');

            // }

            $this->load->view('layout/main',$data);

        }

    }

    function checkout_payment_info($regcode=null){

        if($regcode!=null){

           
            $data['page'] = 'event_checkout_payment_info';
            $data['body_class'] = 'profile-page';
            $data['data']['event_registration'] = $this->mevent->getEventRegistration($regcode);
            $data['data']['bank_transfer_account'] = $this->mevent->getTransferBankAccount();
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            // if($this->session->userdata('id') != $data['data']['event_registration']['aauth_user_id']){

            //     redirect('myprofile');

            // }

            $this->load->view('layout/main',$data);

        }

    }

    function download_pdf($regcode)
    {
        $this->load->library("html2pdf");
         //Set folder to save PDF to
        $this->html2pdf->folder($_SERVER['DOCUMENT_ROOT'].'/upload/pdf/');
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'portrait');

        $event_registration = $this->mevent->getEventRegistration($regcode);
        $event_member = $this->mevent->getEventMember($event_registration['reg_id'],'0');
        $event_member_twin_sharing = $this->mevent->getEventMember($event_registration['reg_id'],'1');

        $html = '<!DOCTYPE html>
        <html>
            <head>  

            <style> 

              body {
                background: rgb(204,204,204); 
              }
              page {
                background: white;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
                box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
              }
              page[size="A4"] {  
                width: 21cm;
                min-height: 29.7cm; 
              }
              page[size="A4"][layout="portrait"] {
                width: 29.7cm;
                min-height: 21cm;  
              }
              page[size="A3"] {
                width: 29.7cm;
                height: 42cm;
              }
              page[size="A3"][layout="portrait"] {
                width: 42cm;
                height: 29.7cm;  
              }
              page[size="A5"] {
                width: 14.8cm;
                height: 21cm;
              }
              page[size="A5"][layout="portrait"] {
                width: 21cm;
                height: 14.8cm;  
              }
              @media print {
                body, page {
                  margin: 0;
                  box-shadow: 0;
                }
              }





              table {
                  font-size: 12pt;
                  font-family: "Times New Roman", Times, serif;
              }
              
              .table2 tr>td,
              table tr>td {
                  border: 1px solid #DDD; 
              }
              
              table tr>td>table {
                  border: 1px solid #9e9e9e;
                  padding: 0px;
              }
              
              table tr>td>table tr>th {  
                  border: 0px solid #fde0bc; 
              }
              
              table tr>td>table tr>td {
                  border: 0px solid #DDD;
                  background-color: #FFF; 
              }
              
              table tr>td>table tr>th>b {
                  font-weight: 600;
                  color: #757575; 
              }
              
              table tr>td>table tr>td>span {
                  color: #8a8585; 
              }
              
              .bordered-dashed {
                  border-style: dashed;
                  border-color: #c5c5c5;
                  border-width: 1.5px;
                  margin-top: 10px;
                  margin-bottom: 10px; 
              }
              
              th.rotate {
                  white-space: nowrap;
                  padding-top: 165px;
                  width: 12px; 
              }
              
              th.rotate>div {
                  transform: translate(1px, 5px) rotate(630deg);
                  width: 12px; 
              }
              
              th.rotate>div>span {
                  border-bottom: 1px solid #ccc;
                  width: 12px; 
              }

              @media print {
                  .page-break {page-break-after: always;}
              }

              /** End Print **/ 
            </style>
        </head>

        <body>

          <page size="A4" >
          <br>';

        $i=1;
        foreach($event_member as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $html.="<div class='page-break'></div>";

        $i=1;
        foreach($event_member_twin_sharing as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];
            $reservation_code2 = $event_registration['regcode'].'-'.$em['twin_sharing_with'];

            $sharing_with = $this->mevent->getEventMemberById($event_registration['reg_id'], $em['twin_sharing_with'], '2');


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);
            $qrcode2 = $this->generate_qr_code($reservation_code2, $reservation_code2);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$sharing_with['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$em['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$sharing_with['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code2.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode2.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $html.='  </page>
          <br>
        </body>
        </html>';
        
        //Set the filename to save/download as
        $this->html2pdf->filename($regcode.'.pdf');
        //Load html view
        $this->html2pdf->html($html);

        $this->html2pdf->create('download');

    }

    function generate_ticket($regcode){

        $event_registration = $this->mevent->getEventRegistration($regcode);
        $event_member = $this->mevent->getEventMember($event_registration['reg_id'],'0');
        $event_member_twin_sharing = $this->mevent->getEventMember($event_registration['reg_id'],'1');


        // if($this->session->userdata('id') != $event_registration['aauth_user_id']){

        //     redirect('myprofile');

        // }

        $html = "";

        $i=1;
        foreach($event_member as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $html.="<div class='page-break'></div>";

        $i=1;
        foreach($event_member_twin_sharing as $em){ 

            $sdate=date_create($event_registration['start_date']);
            $start_date = date_format($sdate,"d/m/Y");

            $edate=date_create($event_registration['end_date']);
            $end_date = date_format($edate,"d/m/Y");

            $reservation_code = $event_registration['regcode'].'-'.$em['member_id'];
            $reservation_code2 = $event_registration['regcode'].'-'.$em['twin_sharing_with'];

            $sharing_with = $this->mevent->getEventMemberById($event_registration['reg_id'], $em['twin_sharing_with'], '2');


            $qrcode = $this->generate_qr_code($reservation_code, $reservation_code);
            $qrcode2 = $this->generate_qr_code($reservation_code2, $reservation_code2);



        $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$sharing_with['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$em['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                $html.='<table class="bordered-dashed" border="1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <th valign="middle" class="rotate" width="1"><div><span>'.$regcode.'</span></div></th>
                        <td valign="top" bgcolor="#EEE">

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Even</b></th>   
                                                <td align="right"><h1>'.$event_registration['event_name'].'</h1></td>   
                                            </tr>
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Tanggal</b></th>   
                                                <td align="right"><span>'.$start_date.' - '.$end_date.'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Paket</b></th>   
                                                <td align="right"><span>'.$em['package_name'].' / '.$em['member_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>Lokasi</b></th>   
                                                <td align="right"><span>'.$event_registration['address'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="4" width="100%">
                                            <tr>
                                                <th align="left"><b>BPR</b></th>   
                                                <td align="right"><span>'.$event_registration['bpr_name'].'</span></td>   
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>

                        </td> 
                        <td>

                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td> 
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="center"><b><img src="http://bprmitracemawis.com/images/perbarindo.png" class="img-responsive" style="width: auto;height: 50px;"></b></th>    
                                            </tr> 
                                        </table> 
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Nama</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$sharing_with['member_name'].'</span></td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%">
                                            <tr>
                                                <th align="left"><b>Kode Reservasi</b></th>    
                                            </tr>
                                            <tr>
                                                <td align="center"><span>'.$reservation_code2.'</span></td>     
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                                 <tr>
                                    <td>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%"> 
                                            <tr>
                                                <td align="center">'.$qrcode2.'</td>    
                                            </tr>
                                        </table>
                                    </td>   
                                </tr>
                            </table>                

                        </td>
                    </tr>
                </table>
                <br/>';

                if($i%4==0){
                    $html.="<div class='page-break'></div>";
                }

                $i++;
        }

        $data['tickets'] = $html;
        $this->load->view('event_ticket',$data);

    }

    function setEventImage($file){
       
        $real_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
        $full_path = $real_path.$file;

        if($file != "" && file_exists($full_path)){
            return base_url().'upload/event/'.$file;
        }else{
            return base_url().'public/assets/img/default_event_cover.png';
        }

    }

    function get_event_registration_by_status($status=null){

        if($status!=null){

             $event_registration = $this->mevent->getAllEventRegistration($status);

             $html = "";

             foreach($event_registration as $er){

             $html.= '<article class="col-md-12 padding-0">
                <div class="card block__box padding-0 updated margin-b-0" data-expired-date="1507173746" data-expired-time="86400" data-transaction-status="12">
                    <div class="row margin-0">
                        <div class="col-md-6 padding-0">
                            <div class="item__content">
                                 <div class="row margin-0">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 padding-12">
                                         <div class="item__image">
                                            <figure> <img src="'.$this->setEventImage($er['cover']).'" alt="'.$er['name'].'"> </figure>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
                                       <div class="text__content text--medium"><span class="green">Transaksi <a href="'.base_url().'payment_detail/'.$er['regcode'].'" class="green">#'.$er['regcode'].'</a></span></div>
                                        <div class="text__content text--medium"><a href="'.base_url().'show/event/'.$er['event_id'].'" rel="noopener noreferrer">'.$er['event_name'].' - '.$er['city'].'</a></div>
                                        <div class="text__content text--medium orange mb10">Rp '.number_format($er['total_cost'],2,",",".").'</div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 bordered-left ">';

                            if($er['trx_status']=="WAITING_TRANSACTION"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                  <div class="clearfix padding-0">
                                      <div class="text--medium text-left">Menunggu Pembayaran</div>
                                      <div class="text--medium text-right"><a href="'.base_url().'payment_detail/'.$er['regcode'].'" class="text-right" style="float: right">Lihat Detail</a></div><hr class="hr-xs">
                                  </div> 
                                  <div class="clearfik item__content">  
                                      <div class="text--medium text__content mb10">Segera lakukan pembayaran dalam : <strong class="red" data-date="1507173746" data-expired="86489"><span id="days_'.$er['reg_id'].'">0 hari</span> <span id="hours_'.$er['reg_id'].'">0 jam</span> <span id="minutes_'.$er['reg_id'].'">0 menit</span> <span id="seconds_'.$er['reg_id'].'>">0 detik</span></strong></div>
                                      <div class="text--medium button__group"><a href="'.base_url().'payment_confirmation/'.$er['regcode'].'" class="btn btn-primary btn-simple btn-sm" target="_blank">Konfirmasi Pembayaran</a> <a href="'.base_url().'checkout/payment_info/'.$er['regcode'].'" class="btn btn-primary btn-simple  btn-sm" target="_blank">Info Bayar</a><a href="javascript:cancel_reservation(\''.$er['regcode'].'\');" class="btn btn-primary btn-simple  btn-sm" target="_blank">Cancel</a></div> 
                                  </div>
                                  <script type="text/javascript">
                                    countDownTime("#days_'.$er['reg_id'].'","#hours_'.$er['reg_id'].'","#minutes_'.$er['reg_id'].'","#seconds_'.$er['reg_id'].'","'.$er['expired_date'].'"");
                                  </script>
                            </div>';

                            }else if($er['trx_status']=="WAITING_CONFIRMATION"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Menunggu Konfirmasi</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Terimakasih telah melakukan konfirmasi. Kami akan segera memproses transaksi anda.</div>
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_COMPLETED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Selesai</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Terimakasih telah melakukan pembayaran. Ticket anda siap untuk dicetak.</div> 
                                    <div class="text--medium button__group"><a href="'.base_url().'home/generate_ticket/'.$er['regcode'].'" class="btn btn-primary btn-simple btn-sm" target="_blank">Cetak Ticket</a>
                                    </div>  
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_CANCELED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Batal</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Anda telah membatalkan reservasi ini.</div>
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_EXPIRED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Kadaluarsa</div> 
                                    <div class="text--medium text-right"><a href="#" class="text-right" style="float: right">Lihat Detail</a></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content">  
                                    <div class="text--medium text__content mb10">Maaf, transaksi dibatalkan karena Anda tidak melakukan pembayaran.</div>
                                    <div class="text--medium button__group"></div>  
                                </div>
                            </div>';

                            }

            $html.='    </div>
                    </div>
                </div>
            </article>';
            }

            if($html==""){

                $html = "<p style='text-align:center;width:100%'>Data tidak ditemukan</p>";

            }
            echo $html;

        }


    }

    function get_event_registration_by_regcode($regcode=null){

        if($regcode!=null){

             $event_registration = $this->mevent->getEventRegistrationByRegCode($regcode);

             $html = "";

             foreach($event_registration as $er){

             $html.= '<article class="col-md-12 padding-0">
                <div class="card block__box padding-0 updated margin-b-0" data-expired-date="1507173746" data-expired-time="86400" data-transaction-status="12">
                    <div class="row margin-0">
                        <div class="col-md-6 padding-0">
                            <div class="item__content">
                                 <div class="row margin-0">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 padding-12">
                                         <div class="item__image">
                                            <figure> <img src="'.$this->setEventImage($er['cover']).'" alt="'.$er['name'].'"> </figure>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-12 ccol-sm-12 col-xs-12 padding-12"> 
                                       <div class="text__content text--medium"><span class="green">Transaksi <a href="'.base_url().'payment_detail/'.$er['regcode'].'" class="green">#'.$er['regcode'].'</a></span></div>
                                        <div class="text__content text--medium"><a href="'.base_url().'show/event/'.$er['event_id'].'" rel="noopener noreferrer">'.$er['event_name'].' - '.$er['city'].'</a></div>
                                        <div class="text__content text--medium orange mb10">Rp '.number_format($er['total_cost'],2,",",".").'</div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 bordered-left ">';

                            if($er['trx_status']=="WAITING_TRANSACTION"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                  <div class="clearfix padding-0">
                                      <div class="text--medium text-left">Menunggu Pembayaran</div>
                                      <div class="text--medium text-right"><a href="'.base_url().'payment_detail/'.$er['regcode'].'" class="text-right" style="float: right">Lihat Detail</a></div><hr class="hr-xs">
                                  </div> 
                                  <div class="clearfik item__content">  
                                      <div class="text--medium text__content mb10">Segera lakukan pembayaran dalam : <strong class="red" data-date="1507173746" data-expired="86489"><span id="days_'.$er['reg_id'].'">0 hari</span> <span id="hours_'.$er['reg_id'].'">0 jam</span> <span id="minutes_'.$er['reg_id'].'">0 menit</span> <span id="seconds_'.$er['reg_id'].'>">0 detik</span></strong></div>
                                      <div class="text--medium button__group"><a href="'.base_url().'payment_confirmation/'.$er['regcode'].'" class="btn btn-primary btn-simple btn-sm" target="_blank">Konfirmasi Pembayaran</a> <a href="'.base_url().'checkout/payment_info/'.$er['regcode'].'" class="btn btn-primary btn-simple  btn-sm" target="_blank">Info Bayar</a><a href="javascript:cancel_reservation(\''.$er['regcode'].'\');" class="btn btn-primary btn-simple  btn-sm" target="_blank">Cancel</a></div> 
                                  </div>
                                  <script type="text/javascript">
                                    countDownTime("#days_'.$er['reg_id'].'","#hours_'.$er['reg_id'].'","#minutes_'.$er['reg_id'].'","#seconds_'.$er['reg_id'].'","'.$er['expired_date'].'"");
                                  </script>
                            </div>';

                            }else if($er['trx_status']=="WAITING_CONFIRMATION"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Menunggu Konfirmasi</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Terimakasih telah melakukan konfirmasi. Kami akan segera memproses transaksi anda.</div>
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_COMPLETED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Selesai</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Terimakasih telah melakukan pembayaran. Ticket anda siap untuk dicetak.</div> 
                                    <div class="text--medium button__group"><a href="'.base_url().'home/generate_ticket/'.$er['regcode'].'" class="btn btn-primary btn-simple btn-sm" target="_blank">Cetak Ticket</a>
                                    </div>  
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_CANCELED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Batal</div> 
                                    <div class="text--medium text-right"></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content"> 
                                    <div class="text--medium text__content mb10">Anda telah membatalkan reservasi ini.</div>
                                </div>
                            </div>';

                            }else if($er['trx_status']=="TRANSACTION_EXPIRED"){

                            $html.='<div class="status__content padding-12" user-type="buyer"> 
                                <div class="clearfix padding-0">
                                    <div class="text--medium text-left">Transaksi Kadaluarsa</div> 
                                    <div class="text--medium text-right"><a href="#" class="text-right" style="float: right">Lihat Detail</a></div> <hr class="hr-xs">
                                 </div> 
                                <div class="clearfix item__content">  
                                    <div class="text--medium text__content mb10">Maaf, transaksi dibatalkan karena Anda tidak melakukan pembayaran.</div>
                                    <div class="text--medium button__group"></div>  
                                </div>
                            </div>';

                            }

            $html.='    </div>
                    </div>
                </div>
            </article>';
            }

            if($html==""){

                $html = "<p style='text-align:center;width:100%'>Data tidak ditemukan. <a href='javascript:refresh_data();'>Refresh</a> </p>";

            }

            echo $html;

        }


    }

    function generate_qr_code($text,$filename){

        $params['data'] = $text;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $_SERVER['DOCUMENT_ROOT'].'/upload/qrcode/'.$filename;
        $this->ciqrcode->generate($params);

        $command = "chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/upload/qrcode';

        $this->ssh2($command);

        return '<img src="'.base_url().'upload/qrcode/'.$filename.'" class="img-responsive" style="width: auto;height: 70px;">';

    }

    function ssh2($command){

        $connection = ssh2_connect('10.50.11.50', 22);
        ssh2_auth_password($connection, 'root', '4k535TranyaR');

        $stream1= ssh2_exec($connection, $command);

        stream_set_blocking($stream1, true);

        $output = stream_get_contents($stream1);

    }

    function get_bpr_by_dpd($id=null){

        if($id!=""){

            $bpr = $this->mevent->getBPRbyDPD($id);

            $html = '<option value="">-- Pilih BPR --</option>';
            foreach ($bpr as $b) {
                
                $html.='<option value="'.$b['id'].'">'.$b['bpr_corporate'].' '.$b['name'].'</option>';

            }
            $html.='<option value="0">belum terdaftar</option>';
            echo $html;

        }


    }

    function get_bpr_by_dpd_lg($id=null){

        if($id!=""){

            $bpr = $this->mevent->getBPRbyDPD($id);

            $html = '<option value="">-- Pilih BPR --</option>';
            foreach ($bpr as $b) {
                
                $html.='<option value="'.$b['id'].'">'.$b['bpr_corporate'].' '.$b['name'].'</option>';

            }
            echo $html;

        }


    }

    function get_member_by_bpr($id=null){

        if($id!=""){

            $member = $this->mevent->getMemberByBPR($id);
            $bpr = $this->mevent->getBPRbyId($id);

            $html = '<option value="">-- Pilih Nama Pendaftar --</option>';
            foreach ($member as $m) {
                
                $html.='<option value="'.$m['id'].'">'.$m['name'].'</option>';

            }
            $html.='<option value="0">belum terdaftar</option>';
            $res = array("member" => $html, "bpr" => $bpr);
            echo json_encode($res);

        }


    }

    function get_member_by_id($id=null){

        if($id!=""){

            $member = $this->mevent->getMemberById($id);

            echo json_encode($member);

        }


    }


    function find_event(){

        $data['data']['latest_events'] = $this->mevent->getLatestEvent(9);

        if(count($_POST)>0){

            $street_number = $_POST['street_number'];
            $route = $_POST['route']; 
            $locality = $_POST['locality'];
            $administrative_area_level_1 = $_POST['administrative_area_level_1'];
            $postal_code = $_POST['postal_code']; 
            $country = $_POST['country'];
            $keyword = strip_tags($_POST['keyword']);
            $location = strip_tags($_POST['location']);
            $date = $_POST['date'];

            $ex = explode(",", $location);

            $f = array();

            for ($i=0; $i < count($ex) ; $i++) { 

                array_push($f,"'%".trim($ex[$i])."%'");
            
            }

            $filter = implode(",", $f);

            $where = "";

            if($keyword != ""){
                $where .= " AND name ILIKE '%$keyword%'";
            }
            if($location != ""){
                $where .= " AND (address ILIKE ANY(array[".$filter."]) OR city ILIKE ANY(array[".$filter."]))";
            }

            switch ($date) {
                case 'all':

                    $sql = "SELECT * FROM events WHERE id <> 0".$where;

                    break;
                case 'today':
                    $date = date("Y-m-d");

                    $sql = "SELECT * FROM events WHERE start_date = $date".$where;

                    break;
                case 'tomorrow':
                    $datetime = new DateTime('tomorrow');
                    $date = $datetime->format('Y-m-d');

                    $sql = "SELECT * FROM events WHERE start_date = $date".$where;

                    break;
                case 'this_week':
                    
                    $sql = "SELECT * FROM events WHERE start_date BETWEEN date_trunc('week',current_date)::date AND (date_trunc('week', current_date::timestamp)+ '6 days'::interval)::date".$where;

                    break;
                case 'next_week':
                    
                    $sql = "SELECT * FROM events WHERE start_date BETWEEN (date_trunc('week', current_date::timestamp)+ '7 days'::interval)::date AND (date_trunc('week', current_date::timestamp)+ '13 days'::interval)::date".$where;

                    break;
                case 'next_month':
                    
                    $sql = "SELECT * FROM events WHERE EXTRACT(month FROM start_date) >= EXTRACT(month FROM CURRENT_DATE + '1 month'::interval)".$where;

                    break;
                
            }

            $data['page'] = 'event_search';
            $data['body_class'] = 'profile-page';
            $data['data']['events'] = $this->db->query($sql)->result_array();
            $data['data']['post'] = true;
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            $data['header']['active'] = "event";
            $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
            $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
            $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
            $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
            $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
            $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
            $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

            $this->load->view('layout/main-new',$data);


        }else{

            $data['page'] = 'event_search';
            $data['body_class'] = 'profile-page';
            $data['data']['events'] = array();
            $data['data']['post'] = false;
            $data['navbar']['navbar_class'] = 'navbar-transparent';

            $data['header']['active'] = "event";
            $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
            $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
            $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
            $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
            $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
            $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
            $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

            $this->load->view('layout/main-new',$data);

        }

    }

    function about(){

        $data['page'] = 'page';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['content'] = $this->mevent->getCustomPage('about_us');
        $data['header']['active'] = "about";
        $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
        $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
        $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
        $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
        $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");
        
        $this->load->view('layout/main-new',$data);

    }

    function contact(){

        $data['page'] = 'contact';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['data']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['data']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");

        $data['header']['active'] = "contact";
        $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
        $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
        $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
        $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
        $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

        $this->load->view('layout/main-new',$data);

    }

    function faq(){

        $data['page'] = 'faq';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['content'] = $this->mevent->getFAQ();

        $data['header']['active'] = "faq";
        $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
        $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
        $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
        $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
        $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

        $this->load->view('layout/main-new',$data);

    }

    function term_of_condition(){

        $data['page'] = 'page';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['content'] = $this->mevent->getCustomPage('terms_and_conditions');

        $data['header']['active'] = "tos";
        $data['header']['fb'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "FACEBOOK");
        $data['header']['tw'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "TWITTER");
        $data['header']['gg'] =  $this->mevent->getConfigurationByKey("SOCIAL_MEDIA", "GOOGLE_PLUS");
        $data['header']['company_address'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "ADDRESS");
        $data['header']['company_phone'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "PHONE");
        $data['header']['company_latitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LATITUDE");
        $data['header']['company_longitude'] =  $this->mevent->getConfigurationByKey("COMPANY_INFO", "LONGITUDE");

        $this->load->view('layout/main-new',$data);

    }

    function myprofile(){

        if($this->aauth->is_loggedin()){

            $data['page'] = 'myprofile';
            $data['body_class'] = 'profile-page';
            $data['navbar']['navbar_class'] = 'navbar-transparent';
            $data['data']['dpd'] = $this->mevent->getDPD();
            $data['data']['job_positions'] = $this->mevent->getJobPosition();
            $data['data']['member'] = $this->mevent->getMemberByUserId($this->session->userdata('id'));
            $data['data']['bpr'] = $this->mevent->getBPRbyDPD(@$data['data']['member']['dpd']);
            $data['event_registration'] = $this->mevent->getAllEventRegistration("ALL");

            $this->load->view('layout/main',$data);

        }else{

            redirect('login');

        }

    }

    function myticket(){

        $data['page'] = 'profile';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';


        $this->load->view('layout/main',$data);


    }

    function payment_confirmation($regcode){
        
        $check = $this->mevent->checkPaymentConfirmation($regcode);
        if($check > 0){

            $data['page'] = 'payment_confirmation_completed';

        }else{

            $data['page'] = 'payment_confirmation';

        }

       
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';
        $data['data']['event_registration'] = $this->mevent->getEventRegistration($regcode);
        $data['data']['bank_transfer_account'] = $this->mevent->getTransferBankAccount();

        // if($this->session->userdata('id') != $data['data']['event_registration']['aauth_user_id']){

        //     redirect('myprofile');

        // }

        $this->load->view('layout/main',$data);

    }

    function confirm_transaction(){

        if(count($_FILES)>0){

            if($_FILES["file"]["name"]!=""){

                if($_FILES["file"]["type"]=="image/png" or
                    $_FILES["file"]["type"]=="image/jpg" or
                    $_FILES["file"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/buktitransfer/';
                    $array = explode('.', $_FILES['file']['name']);
                    $extension = end($array);
                    $buktitransfer = md5(uniqid(rand(), true)).".".$extension;

                    move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path."/".$buktitransfer);

                }else{


                    $res['msg'] = "Invalid file type! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }

        }

        if(count($_POST)>0){

            $date=date_create($_POST['transfer_date']);

            $data = array(

                "regcode" => $_POST['regcode'],
                "event_registration_id" => $_POST['event_registration_id'],
                "transfer_date" => date_format($date,"Y-m-d"),
                "bank" => $_POST['bank'],
                "to_account" => $_POST['to_account'],
                "from_account" => $_POST['from_account'],
                "rek_number" => $_POST['rek_number'],
                "status" => "0",
                "member_id" => $this->session->userdata('id'),
                "doc_transfer" => @$buktitransfer
            );

            $insert = $this->db->insert("payment_confirmations", $data);

            if($insert){

                $this->db->where("regcode", $_POST['regcode']);
                $this->db->update("event_registrations",array("status" => "WAITING_CONFIRMATION"));

                $res = array("status" => "1", "msg" => "Konfirmasi berhasil terkirim.");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Telah terjadi kesalahan. Silahkan coba lagi.");
            }

            echo json_encode($res);

        }

    }

    function payment_info($invoice_number){
        
        $data['page'] = 'payment_info';
        $data['body_class'] = 'profile-page';
        $data['navbar']['navbar_class'] = 'navbar-transparent';

        $this->load->view('layout/main',$data);

    }

    function login(){

        if(isset($_SESSION['id'])){

            redirect('/');

        }else{

            $data['page'] = 'login';
            $data['body_class'] = 'login-page';
            $data['navbar']['navbar_class'] = 'navbar-transparent';
            $data['fullscreen'] = true;
            $this->load->view('layout/main',$data);

        }    
    	

    }

    function signup(){

        if(isset($_SESSION['id'])){

            redirect('/');
        
        }else{

            $data['page'] = 'signup';
            $data['body_class'] = 'login-page';
            $data['navbar']['navbar_class'] = 'navbar-transparent';
            $data['fullscreen'] = true;
            $this->load->view('layout/main',$data);
        
        }
    	

    }
    function login_member(){
         if(count($_POST)>0){

             $username = strip_tags($_POST['username']);
             $pass = $_POST['password'];

             if($this->aauth->login($username, $pass)){

                 /* start session */
                $res = array("status" => "1", "msg" => "Login Success"); 

                if($this->session->userdata('picture') == ""){
                    $this->session->set_userdata('picture', base_url().'public/assets/img/default-avatar.png');
                }else{
                	$this->session->set_userdata('picture', base_url().'upload/photo/'.$this->session->userdata('picture'));
                }
               
                 
             }else{
                
                $res = array("status" => "0", "msg" => "Invalid username or password.");

             }

         }else{
             $res = array("status" => "0", "msg" => "Invalid username or password.");
         }

         echo json_encode($res);
    }

    function login_bpr(){
        
         if(count($_POST)>0){

             $username = strip_tags($_POST['username']);
             $pass = $_POST['password'];
             $dpd = $_POST['dpd'];
             $bpr = $_POST['bpr'];

             $user_exist = $this->Mdpd->checkDpdUser($dpd, $username);

             if($user_exist > 0){

                $this->session->unset_userdata('picture');

                if($this->aauth->login($username, $pass)){


                    $this->session->set_userdata('id_bpr', $bpr);

                     /* start session */
                    $res = array("status" => "1", "msg" => "Login Success"); 

                    if($this->session->userdata('picture') == ""){
                        $this->session->set_userdata('picture', base_url().'public/assets/img/default-avatar.png');
                    }else{

                        if($this->session->userdata('picture') != base_url().'public/assets/img/default-avatar.png'){

                            $this->session->set_userdata('picture', base_url().'upload/photo/'.$this->session->userdata('picture'));
                            
                        }
                        
                    }
                   
                     
                }else{
                    
                    $res = array("status" => "0", "msg" => "Invalid username or password.");

                }

             }else{

                $res = array("status" => "0", "msg" => "Invalid username");

             }

            

         }else{
             $res = array("status" => "0", "msg" => "Invalid username or password.");
         }

         echo json_encode($res);
    }

    function create_user(){

        $aauthTable = "aauth_users";
        $aauthPk = "id";

        $create_user = $this->aauth->create_user($this->input->post('email'),$this->input->post('password'),$this->input->post("username"));

        if($create_user){

            $last_id = $this->db->insert_id();
            $registration = $this->aauth->add_member($last_id, '10');

            if($registration){

                 $data = array(
                    "status" => '1',
                );
                $this->db->where($aauthPk, $last_id);
                $this->db->update($aauthTable, $data);

                /*$_SESSION['username'] = $this->input->post('username'); 
                $_SESSION['email'] = $this->input->post('email'); 
                $_SESSION['id'] = $last_id; 
                $_SESSION['avatar'] = base_url().'public/assets/img/default_avatar.png'; */

                $res = array("status" => "1", "msg" => "Registration Success");

            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }


        }else{
            $res = array("status" => "0", "msg" => "Oop! Something went wrong. Could not create user.");
           
        }

         echo json_encode($res);

    }

    function user_upload_photo(){

    	 /* upload cover */

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    $old_photo = $upload_path.'/'.@$_POST['old_photo'];

                    if(file_exists($old_photo)){

                        @unlink($old_photo);

                    }


                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            	$data = array(

	                "picture" => $photo,

	            );

	            $this->db->where("id", $this->session->userdata("id"));
	            $update = $this->db->update("aauth_users", $data);

	            if($update){

	            	$this->session->set_userdata('picture', base_url().'/upload/photo/'.$photo);

	                $res = array("status" => "1", "msg" => "Successfully update data!");

	            }else{

	                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

	            }

	            echo json_encode($res);

            }

           

    }

    function user_update_profile(){


            if(count($_POST)>0){

               
            	$data = array(

	                "first_name" => strip_tags($_POST['first_name']),
	                "last_name" => strip_tags($_POST['last_name']),
	                "email" => strip_tags($_POST['email']),

	            );

	            $this->db->where("id", $this->session->userdata("id"));
	            $update = $this->db->update("aauth_users", $data);

	            if($update){

	            	$data = array(
	                    
						"first_name" => strip_tags($_POST['first_name']),
	                	"last_name" => strip_tags($_POST['last_name']),
	                	"email" => strip_tags($_POST['email']),

	                );

                	$this->session->set_userdata($data);

	                $res = array("status" => "1", "msg" => "Successfully update data!");

	            }else{

	                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

	            }

	            echo json_encode($res);

            }

           

    }

     function member_update_profile(){


            if(count($_POST)>0){

               
                $data = array(

                    "bpr" => strip_tags($_POST['bpr']),
                    "name" => strip_tags($_POST['name']),
                    "job_position" => strip_tags($_POST['job_position']),
                    "no_hp" => strip_tags($_POST['no_hp']),
                    "email" => strip_tags($_POST['email']),
                    "gender" => strip_tags($_POST['gender']),
                    "aauth_user_id" => $this->session->userdata('id'),

                );

                if($_POST['member_id']!=""){

                    $this->db->where("id", $_POST['member_id']);
                    $update = $this->db->update("members", $data);

                }else{

                    $update = $this->db->insert("members", $data);

                }
                

                if($update){

                    $res = array("status" => "1", "msg" => "Successfully update data!");

                }else{

                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

                }

                echo json_encode($res);

            }

           

    }

    function change_pass(){

        if(count($_POST)>0){

            $user = $this->mevent->getCurrentUser($this->session->userdata('id'));

            $pass = $this->aauth->hash_password($this->input->post("old_password"),$user['id']);

            if($this->aauth->verify_password($pass,$user['pass'])){

                 $update = $this->aauth->update_user($this->session->userdata("id"), $this->session->userdata("email"), $this->input->post("new_password"), $this->session->userdata("username"));

                if($update){

                    $res = array("status" => "1", "msg" => "Successfully update password!");

                }else{

                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid old password!");

            }

            echo json_encode($res);

           

        }

    }

    function logout(){

        session_destroy();

        redirect('/');

    }
    function submit_message(){

        if(count($_POST)>0){

            $name = strip_tags($_POST['name']);
            $email = strip_tags($_POST['email']);
            $message = strip_tags($_POST['message']);
            $to_email = $this->mevent->getConfigurationByKey("COMPANY_INFO","EMAIL");
            $subject = "[PERBARINDO EVENT] VISITOR MESSAGE";
            $company = "PERBARINDO";

            $body = '
                <p>Hi, Admin PERBARINDO. Anda mendapatkan pesan dari visitor <a href="'.base_url().'">'.base_url().'</a>. Berikut data visitor beserta pesannya:</p>

                <br>
                <br>
                <br>
                <hr>
                <table>

                    <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>'.$name.'</td>
                    </tr>
                    <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>'.$email.'</td>
                    </tr>
                    <tr>
                    <td>Tanggal & Waktu</td>
                    <td>:</td>
                    <td>'.date("d-m-Y H:i:s").'</td>
                    </tr>
                </table>
                <hr>
                <p>Pesan :</p>

                <br>

                <p>'.$message.'</p>


            ';

            if($this->send_mail($to_email['value'], $subject, $body, $to_email['value'], $company )){

                $res = array("status"=>"1", "msg"=>"Pesan berhasil terkirim. Terimakasih!");

            }else{

                $res = array("status"=>"0", "msg"=>"Oop! Maaf, telah terjadi kesalahan. Silahkan coba beberapa saat lagi.");

            }

            echo json_encode($res);
        }

    }
    function send_mail($email, $subject, $message, $from, $company){

        $smtp_host = $this->mevent->getConfigurationByKey("EMAIL","SMTP_HOST");
        $smtp_port = $this->mevent->getConfigurationByKey("EMAIL","SMTP_PORT");
        $smtp_user = $this->mevent->getConfigurationByKey("EMAIL","SMTP_USER");
        $smtp_pass = $this->mevent->getConfigurationByKey("EMAIL","SMTP_PASSWORD");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => @$smtp_host['value'],
            'smtp_port' => @$smtp_port['value'],
            'smtp_user' => @$smtp_user['value'],
            'smtp_pass' => @$smtp_pass['value'],
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from($from, $company);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
          return true;
        }
         else
        {
          return false;
        }
    } 

    function cancel_transaction(){

        if(count($_POST)>0){

            $regcode = $_POST['regcode'];

            $this->db->where("regcode", $regcode);
            $update = $this->db->update("event_registrations", array("status" => "TRANSACTION_CANCELED"));

            if($update){
                $res = array("status" => "1", "msg" => "Reservasi berhasil dibatalkan!");
            }else{
                $res = array("status" => "0", "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba beberapa saat lagi.");
            }

            echo json_encode($res);

        }

    }

    function delete_transaction($regcode){

        $reg = $this->db->query("SELECT id FROM event_registrations WHERE regcode='$regcode'")->result_array();

        $this->db->query("DELETE FROM event_members WHERE registration_id = '".$reg[0]['id']."'");
        $this->db->query("DELETE FROM event_registrations WHERE regcode = '$regcode'");
        $this->db->query("DELETE FROM payment_confirmations WHERE regcode = '$regcode'");



    }  

    function subscribe(){

        if(count($_POST)>0){

            $email = strip_tags($_POST['email']);

            $row = $this->MSubscribe->getSubscriberByEmail($email);

            if($row > 0){

                $res = array("status" => "0", "msg" => "Email anda sudah pernah terdaftar. Terimakasih");

            }else{

                $insert = $this->db->insert("subscribers", array("email" => $email, "subscribe_date" => date("Y-m-d h:i:s") ));

                if($insert){

                    $res = array("status" => "1", "msg" => "Subscribe telah berhasil. Terimakasih");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba kembali");

                }

            }

           
        }else{

            $res = array("status" => "0", "msg" => "Oops! Telah terjadi kesalahan. Silahkan coba kembali");

        }

        echo json_encode($res);

    } 


}