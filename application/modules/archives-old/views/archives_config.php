<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>

    <?php if($receipt_config_perm=='1') { ?>
    <form role="form" id="form">
        <input type="hidden" name="old_logo" id="old_logo" value="<?php echo $dataedit['event_receipt_logo']; ?>">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Archive Receipt Config (Kwitansi)</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="receipt_format">Receipt Format</label>
                        <input type="text" maxlength="100" name="receipt_format" class="form-control validate[required]" id="receipt_format" placeholder="__/___________/__/____ (Required)" value="<?php echo $dataedit['event_receipt_format']; ?>">
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="logo">Receipt Logo</label>
                        <input type="file" class="form-control" name="logo" id="logo">
                        <div class="img-prev"  id="logo_preview">
                            <?php
                            if($dataedit['event_receipt_logo'] != ""){

                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/archives/';
                                $file = $upload_path.'/'.$dataedit['event_receipt_logo'];

                                if(file_exists($file)){

                                    $path = base_url().'upload/archives/'.$dataedit['event_receipt_logo'];

                                    echo "<img src='".$path."' class='img-responsive' />
                                        <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#logo_preview\");'><i class='fa fa-remove'></i>
                                        </a>";

                                }else{

                                    echo "<h1>Receipt Logo</h1>";

                                }
                            }else{ ?>

                                <h1>Receipt Logo</h1>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>
            </div>
        </div>        
    </form>
    <?php } ?>

    <?php if($certificate_config_perm=='1') { ?>
    <form role="form" id="form_certificate">
        <input type="hidden" name="old_background" id="old_background" value="<?php echo $dataedit['event_certificate_background']; ?>">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Archive Certificate Config (Sertifikat)</h3>
                </div>
                <!-- /.box-header -->
                
                <!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="background">Certificate Background</label>
                        <input type="file" class="form-control" name="background" id="background">
                        <div class="img-prev"  id="background_preview">
                            <?php
                            if($dataedit['event_certificate_background'] != ""){

                                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/archives/';
                                $file = $upload_path.'/'.$dataedit['event_certificate_background'];

                                if(file_exists($file)){

                                    $path = base_url().'upload/archives/'.$dataedit['event_certificate_background'];

                                    echo "<img src='".$path."' class='img-responsive' />
                                        <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#background_preview\");'><i class='fa fa-remove'></i>
                                        </a>";

                                }else{

                                    echo "<h1>Background Certificate</h1>";

                                }
                            }else{ ?>

                                <h1>Background Certificate</h1>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>
            </div>
        </div>        
    </form>
    <?php } ?>

</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#searchInput").trigger("click");
        $("#logo").change(function(){
            readURL(this, "#logo");
        });

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

        $("#background").change(function(){
            readURL(this, "#background");
        });

        $("#form_certificate").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormDataCertificate();',3000);
            }

            return false;

        });

        $("#form_certificate").validationEngine();

    });

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Preview</h1>");
        $(selector).val('');

    }

    function saveFormData(){

        var target = base_url+"archives/archives/config/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }

    function saveFormDataCertificate(){

        var target = base_url+"archives/archives/config/<?php echo $dataedit['id']; ?>";
        var data = $("#form_certificate").serialize();

        var formData = new FormData($("#form_certificate")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }
</script>