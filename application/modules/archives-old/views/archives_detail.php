<div class="box">
    <div class="box-header">
        <h3 class="box-title">Archives Detail</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Archives Detail" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Nama</th>
                <th>BPR</th>
                <th>DPP/DPD</th>
                <th>HP</th>
                <th>Kehadiran</th>
                <?php
                if($receipt_perm!='1' && $certificate_perm!='1'){

                }else{
                    echo '<th>Cetak</th>';
                }
                ?>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    var receipt_perm = '<?php echo $receipt_perm; ?>';
    var certificate_perm = '<?php echo $certificate_perm; ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Archives Detail';
            }
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Archives Detail';
            }
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Archives Detail';
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF',
            exportOptions: {
                    columns: ':visible'
            },
            title: function(){
                 return 'Archives Detail';
            }
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print',
            exportOptions: {
                    columns: ':visible'
            },
            
            title: function(){
                 return '<img src="http://purbarindo.dev/assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Archives Detail</span>';
            }
        }
    ];

    var columns =  [

        { "data" : "$.member_name"},
        { "data" : "$.bpr"},
        { "data" : "$.dpd"},
        { "data" : "b.no_hp"},
        { "data" : "$.kehadiran"},
        { "data" : "$.op", "orderable": false, "searchable": false},

    ];

    if(receipt_perm != '1' && certificate_perm != '1'){

        columns = [

            { "data" : "$.member_name"},
            { "data" : "$.bpr"},
            { "data" : "$.dpd"},
            { "data" : "b.no_hp"},
            { "data" : "$.kehadiran"},

        ];

    }

    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){


        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "archives/archives/dataTableDetail",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons

        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

</script>
