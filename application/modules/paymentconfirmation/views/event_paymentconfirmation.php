<div class="box">
    <div class="box-header">
        <h3 class="box-title">Event Payment Confirmation</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" id="dt_search" class="form-control" placeholder="Find Payment Confirmation" aria-describedby="sizing-addon1">
        </div>
        <br>
        <table id="dttable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>Paid By</th>
                <th>BPR</th>
                <th>Bank Sender</th>
                <th style="width: 60px;">Amount</th>
                <th>Bank Receiver</th>
                <th>Account Receiver</th>
                <?php
                if($edit_perm=='1'){
                    echo '<th>Status</th>';
                }
                ?>
                <th>Detail</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal" id="modal_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Data pada baris ini akan dihapus. Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="modal_change_confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure to change this row ?</p>
                <!-- 
                <p>You will APPROVE this payment. With this approve, you will : Send ticket to registrar.</p>                
                <p>You CANNOT UNDO this action. Continue ?</p> 
                -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_action_cs_no">No</button>
                <button type="button" class="btn btn-primary" id="btn_action_cs">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="article_preview_dialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment Detail</h4>
            </div>
            <div class="modal-body">
                <div id="article-preview-body-dialog">

                </div>
            </div>
            <div class="modal-footer" style="clear:both">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    var base_url = '<?php echo base_url(); ?>';
    var edit_perm = '<?php echo $edit_perm; ?>';

    var buttons = [
        {
            extend:    'copyHtml5',
            text:      '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy'
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel'
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV'
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-file-pdf-o"></i>',
            titleAttr: 'PDF'
        },
        {
            extend:    'print',
            text:      '<i class="fa fa-print"></i>',
            titleAttr: 'Print'
        }
    ];

    var columns =  [

            { "data" : "a.transfer_date", "searchable": false },
            { "data" : "a.from_account" },
            { "data" : "$.bpr" },
            { "data" : "$.banksender", "searchable": false },
            { "data" : "$.amount", "orderable": false, "searchable": false },
            { "data" : "$.bankreceiver" },
            { "data" : "c.no_rek" },
            { "data" : "$.status", "orderable": false, "searchable": false},
            { "data" : "$.op", "orderable": false, "searchable": false},
        ];

    if(edit_perm != '1'){

        columns = [
            { "data" : "a.transfer_date", "searchable": false },
            { "data" : "a.from_account" },
            { "data" : "$.bpr" },
            { "data" : "$.banksender", "searchable": false },
            { "data" : "$.amount", "searchable": false },
            { "data" : "$.bankreceiver" },
            { "data" : "c.no_rek" },
            { "data" : "$.op", "orderable": false, "searchable": false},
        ];

    }


    $(document).ready(function(){

        InitDatatable();

    });

    function InitDatatable(){
        
        var table = $('#dttable').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "paymentconfirmation/paymentconfirmation/dataTable",
                "type": "POST"
            },
            "columns": columns,
            "lengthChange":false,
            "dom": 'Bfrtip',
            "buttons": buttons,
            "drawCallback": function( settings ) {
                $("span.price").priceFormat({
                    prefix: 'Rp ',
                    thousandsSeparator: '.',
                    limit: 13,
                    centsLimit: 0
                });
            }
        });

        $('#dt_search').keyup(function(){
            table.search($(this).val()).draw() ;
        });

    }

    function edit(id){
        window.location.href="<?php echo base_url(); ?>paymentconfirmation/paymentconfirmation/edit/"+id;
    }
    function remove(id){

        $("#modal_confirm").modal('show');
        $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

    }

    function proceedRemove(id){

        var target = '<?php echo base_url(); ?>paymentconfirmation/paymentconfirmation/remove';
        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

    function changeStatus(oldStatus,newStatus,id){
        $("#modal_change_confirm").modal('show');
        $("#btn_action_cs_no").attr("onclick","undoChangeStatus('"+oldStatus+"','"+id+"')");
        $("#btn_action_cs").attr("onclick","proceedChangeStatus('"+newStatus.value+"','"+id+"')");
    }

    function undoChangeStatus(oldStatus,id){
        $("#status"+id).val(oldStatus);
    }

    function proceedChangeStatus(status,id){
        
        var target = '<?php echo base_url(); ?>paymentconfirmation/paymentconfirmation/changeStatus';
        var data = {status:status, id:id}

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_change_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                $('#dttable').dataTable().fnDestroy();

                InitDatatable();

            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

    function preview(id){
        var target = base_url + 'paymentconfirmation/paymentconfirmation/preview/'+id;

        $.get(target,function(res){

            $("#article-preview-body-dialog").html(res.content);

            $('#article_preview_dialog').modal({
                backdrop: false,
                show: true
            });
        },'json');
    }

</script>
