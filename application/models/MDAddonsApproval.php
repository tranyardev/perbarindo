<?php

class MDAddonsApproval extends MY_Model implements DatatableModel{

        function __construct(){

            parent::__construct();

        }
        public function appendToSelectStr() {
                return array(

                    "status" => "CASE WHEN a.status = '0' THEN concat('<div class=\"form-group\"><select onchange=\"updateStatus(',a.module_id,')\" id=\"module_id_',a.module_id,'\" class=\"form-control\"><option selected value=\"0\">Draft</option><option value=\"1\">Editor Review</option><option value=\"2\">Publish</option></select></div>') 
                                 WHEN a.status = '1' THEN concat('<div class=\"form-group\"><select id=\"module_id_',a.module_id,'\" onchange=\"updateStatus(',a.module_id,')\" class=\"form-control\"><option value=\"0\">Draft</option><option selected value=\"1\">Editor Review</option><option value=\"2\">Publish</option></select></div>')
                                 ELSE concat('<div  class=\"form-group\"><select id=\"module_id_',a.module_id,'\" onchange=\"updateStatus(',a.module_id,')\" class=\"form-control\"><option value=\"0\">Draft</option><option value=\"1\">Editor Review</option><option selected value=\"2\">Publish</option></select></div>') END",

                    "op" => "concat('<a class=\"btn btn-sm btn-primary\" href=\"javascript:view(',a.module_id,');\"><i class=\"fa fa-eye\"></i></a>&nbsp;<a class=\"btn btn-sm btn-danger\" href=\"javascript:remove(',a.module_id,');\"><i class=\"fa fa-remove\"></i></a>')"


                );
        }

        public function fromTableStr() {
            return 'addons_store a';
        }

        public function joinArray(){
            return null;
        }

        public function whereClauseArray(){
            return null;
        }


}