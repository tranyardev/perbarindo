<?php


class Cinta{
	
	var $params = array();
	var $ci;

	function __construct($params=null){

		$this->params = $params;
		$this->ci =& get_instance();

	}

	public function browse(){

		$data['html'] = $this->_get_datatable_template();
		$data['page_title'] = $this->params['page_title'];
		$data['page_subtitle'] = $this->params['page_subtitle'];
		$data['breadcrumb'] = $this->_get_breadcrumb('browse');
		$data['data']['parent_menu'] = $this->params['active_menu']['parent_menu'];
        $data['data']['submenu'] = $this->params['active_menu']['submenu'];

		$this->ci->load->view('layout/body',$data);

	}

	public function render(){

		$action = $this->params['params']['form']['action'];

		$data['page_title'] = $this->params['page_title'];
		$data['page_subtitle'] = $this->params['page_subtitle'];

		if($action=="add"){

			$data['html'] = $this->_get_form_add_template();
			$data['breadcrumb'] = $this->_get_breadcrumb('add');

		}elseif($action=="edit"){

			$data['html'] = $this->_get_form_edit_template();
			$data['breadcrumb'] = $this->_get_breadcrumb('edit');

		}

		$data['data']['parent_menu'] = $this->params['active_menu']['parent_menu'];
        $data['data']['submenu'] = $this->params['active_menu']['submenu'];

		$this->ci->load->view('layout/body',$data);

	}

	public function process(){

		$action = $this->params['params']['action'];

		switch ($action) {
			case 'save':
				$res = $this->_do_save();
				break;
			case 'update':
				$res = $this->_do_update();
				break;
			case 'delete':
				$res = $this->_do_delete();
				break;
		}

		if(isset($this->params['params']['data_return'])){

			if($this->params['params']['data_return']){

				return $res;
			
			}else{

				echo json_encode($res);
				
			}

		}else{

			echo json_encode($res);

		}

	}

	private function _do_save(){

		$table = $this->params['params']['table'];
		$data = $this->params['params']['post'];
		$insert = $this->ci->db->insert($table, $data);

		if($insert){

			$res = array("status" => "1", "msg" => "Data successfully inserted!");

		}else{

			$res = array("status" => "0", "msg" => "Oop! Something went wrong!");

		}

		return $res;

	}

	private function _do_update(){

		$table = $this->params['params']['table'];
		$pk = $this->params['params']['pk'];
		$id = $this->params['params']['id'];
		$data = $this->params['params']['post'];

		$this->ci->db->where($pk, $id);
		$update = $this->ci->db->update($table, $data);

		if($update){

			$res = array("status" => "1", "msg" => "Data successfully updated!");

		}else{

			$res = array("status" => "0", "msg" => "Oop! Something went wrong!");

		}

		return $res;

	}

	private function _do_delete(){

		$table = $this->params['params']['table'];
		$pk = $this->params['params']['pk'];
		$id = $this->params['params']['id'];

		if(isset($this->params['params']['file_field']) && isset($this->params['params']['file_path'])){

			$data_file = $this->_get_file_data();

            $file = $this->params['params']['file_path'].'/'.$data_file[$this->params['params']['file_field']];

            if(file_exists($file)){

                @unlink($file);

            }

		}

		$this->ci->db->where($pk, $id);
		$delete = $this->ci->db->delete($table);

		if($delete){

			$res = array("status" => "1", "msg" => "Data successfully deleted!");

		}else{

			$res = array("status" => "0", "msg" => "Oop! Something went wrong!");

		}

		return $res;

	}

	private function _get_file_data(){

		$table = $this->params['params']['table'];
		$pk = $this->params['params']['pk'];
		$id = $this->params['params']['id'];
		
		$result = $this->ci->db->query("SELECT * FROM ".$table." WHERE ".$pk."='".$id."'")->result_array();

		return @$result[0];


	}

	private function _get_form_add_template(){

		$action = $this->_get_action_add_url();
		$form_title = $this->params['page_title'];

		
		$html = '<div class="row" id="form_wrapper">';
    	$html.= '<form role="form" id="form">';

    	$col_class = "col-md-12";

    	if(isset($this->params['columns_size'])){

    		if($this->params['columns_size'] == "2"){

    			$col_class = "col-md-6";

    		}
    	}

		$html.= '<div class="'.$col_class.'">';
		$html.= '<div class="box box-primary box-solid">';
        $html.= '<div class="box-header with-border">';
        $html.= '<h3 class="box-title">Form '.$form_title.'</h3>';
        $html.= '</div>';
        $html.= '<div class="box-body">';

        if(isset($this->params['before_form'])){

        	$html.= $this->params['before_form'];
        	
        }

        $html.= $this->_generate_form_add();

        if(isset($this->params['after_form'])){

        	$html.= $this->params['after_form'];
        	
        }

        $html.='<div style="display:none;" id="upload-progress">
	        		<div class="progress progress-sm active">
		                <div id="progressbar" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">         
		                </div>
		             </div>
	        	</div>';

        $html.= '</div>';
        $html.=' <div class="box-footer">
                    &nbsp;<button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>';
        
        $html.= '</div>';
        $html.= '</div>';

        if(isset($this->params['columns_size'])){

    		if($this->params['columns_size'] == "2"){

    			
		        if(isset($this->params['second_column'])){

		        	$html.= $this->params['second_column'];

		        }
		        

    		}
    	}

 		$html.= '</form>';
		$html.= '</div>';

		$html.= '<script type="text/javascript">';

		$html.= 'var target = "'.$action.'";';

		$html.= '</script>';

		return $html;
		
	}

	private function _get_form_edit_template(){

		$action = $this->_get_action_edit_url();
		$delete = $this->_get_action_remove_url();
		$form_title = $this->params['page_title'];
		$id = $this->params['params']['id'];
		
		$html = '<div class="row" id="form_wrapper">';
    	$html.= '<form role="form" id="form">';

    	$col_class = "col-md-12";

    	if(isset($this->params['columns_size'])){

    		if($this->params['columns_size'] == "2"){

    			$col_class = "col-md-6";

    		}
    	}

		$html.= '<div class="'.$col_class.'">';
		$html.= '<div class="box box-primary box-solid">';
        $html.= '<div class="box-header with-border">';
        $html.= '<h3 class="box-title">Form '.$form_title.'</h3>';
        $html.= '</div>';
        $html.= '<div class="box-body">';


        if(isset($this->params['before_form'])){

        	$html.= $this->params['before_form'];
        	
        }

        $html.= $this->_generate_form_edit();

        if(isset($this->params['after_form'])){

        	$html.= $this->params['after_form'];
        	
       	}
      

      	$html.='<div style="display:none;" id="upload-progress">
	        		<div class="progress progress-sm active">
		                <div id="progressbar" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">         
		                </div>
		             </div>
	        	</div>';

        $html.= '</div>';
        $html.=' <div class="box-footer">
                    &nbsp;<button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="delete_confirm(\''.$id.'\');" class="btn btn-danger">Delete</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>';
     
       	$html.= '</div>';
        $html.= '</div>';

        if(isset($this->params['columns_size'])){

    		if($this->params['columns_size'] == "2"){

    			
		        if(isset($this->params['second_column'])){

		        	$html.= $this->params['second_column'];

		        }
		        

    		}
    	}
 		
 		$html.= '</form>';
 		$html.= '</div>';


		$html.= '<div class="modal" id="modal_confirm">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                    <span aria-hidden="true">&times;</span></button>
				                <h4 class="modal-title">Konfirmasi</h4>
				            </div>
				            <div class="modal-body">
				                <p>Data ini akan dihapus. Anda yakin?</p>
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				                <button type="button" class="btn btn-primary" id="btn_action">Yes</button>
				            </div>
				        </div>
				        <!-- /.modal-content -->
				    </div>
				    <!-- /.modal-dialog -->
				</div>';

		$html.= '<script type="text/javascript">';

		$html.= 'var target = "'.$action.'/'.$id.'";';
		$html.= 'function delete_confirm(id){

			        $("#modal_confirm").modal("show");
			        $("#btn_action").attr("onclick","proceedDelete(\'"+id+"\')");

			    };';
		$html.= 'function proceedDelete(id){

			        var url_delete = "'.$delete.'/'.$id.'";
			        var data = { id : id }

			        $("#btn_action").button("loading");

			        $.post(url_delete, data, function(res){

			            $("#btn_action").button("reset");
			            $("#modal_confirm").modal("hide");
			            if(res.status=="1"){
			                
			                window.history.back();

			            }else{
			                toastr.error(res.msg, "Response Server");
			            }

			        },"json");

			    };';

		$html.= '</script>';

		return $html;
		
	}

	private function _get_datatable_template(){

		$remove_action = $this->_get_action_remove_url();
		$edit_action = $this->_get_action_edit_url();

		/* markup */

		$html ='<div class="box box-primary box-solid">';
		$html.='<div class="box-header">';
		$html.='<h3 class="box-title">'.$this->params['page_title'].'</h3>';
		$html.='</div>';
		$html.='<form id="form">';
		$html.='<div class="box-body">';
		$html.='<input type="file" class="form-control" name="excel" id="excel" style="visibility: hidden;">';
		$html.='<div class="input-group input-group-lg">';
		$html.='<span class="input-group-addon"><i class="fa fa-search"></i></span>';
		$html.='<input type="text" id="dt_search" class="form-control" placeholder="'.$this->ci->lang->line("search_data").'" aria-describedby="sizing-addon1">';
		$html.='</div>';
		$html.='<br>';
		$html.='<table id="dttable" class="table table-bordered table-striped">';
		$html.='<thead>';
		$html.=$this->_get_datatable_thead();
		$html.='</thead>';
		$html.='<tbody></tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</form>';

		if(isset($this->params['lock_modul'])){

			if($this->params['lock_modul']){

				$html.='<div class="overlay lock-feature">';
		        $html.='<i class="fa fa-lock"></i>';
		        $html.='</div>';

			}

		}

		$html.='</div>';

		$html.='<div class="modal" id="modal_confirm">';
		$html.='<div class="modal-dialog">';
		$html.='<div class="modal-content">';
		$html.='<div class="modal-header">';
		$html.='<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
		$html.='<span aria-hidden="true">&times;</span></button>';
		$html.='<h4 class="modal-title">Konfirmasi</h4>';
		$html.='</div>';
		$html.='<div class="modal-body">';
		$html.='<p>Data pada baris ini akan dihapus. Anda yakin?</p>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">No</button>';
		$html.='<button type="button" class="btn btn-primary" id="btn_action">Yes</button>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';

		/* jquery */

		$html.='<script type="text/javascript">';
		$html.='var target="'.$remove_action.'";';
		$html.='var edit_url="'.$edit_action.'";';
		$html.='$(document).ready(function(){';
		$html.='InitDatatable();';
		$html.='});';

		$html.='function InitDatatable(){';

        $html.='var table = $("#dttable").DataTable('.$this->_json_encode_ex($this->_get_datatable_options()).');';
        $html.='$("#dt_search").keyup(function(){';
		$html.='table.search($(this).val()).draw();';
		$html.='});';

    	$html.='}';
    	$html.='</script>';

		return $html;

	}

	private function _get_datatable_thead(){

		$columns = array_keys($this->params['params']['datatable']['columns']);

		$thead = "<tr>";

		for($i=0;$i < count($columns);$i++){

			if($columns[$i] == "action"){

				if($this->params['permissions']['edit_perm']!="1" && $this->params['permissions']['delete_perm']!="1"){

				}else{

					$thead .= "<th>".$this->ci->lang->line($columns[$i])."</th>";
				
				}

			}else{

				$thead .= "<th>".$this->ci->lang->line($columns[$i])."</th>";

			}

		}

		$thead .= "</tr>";

		return $thead;

	}

	private function _get_datatable_columns(){

		$columns = $this->params['params']['datatable']['columns'];

		$keys = array_keys($this->params['params']['datatable']['columns']);

		$cols = array();

		for($i=0;$i < count($columns);$i++){

			if($keys[$i] == "action"){

				if($this->params['permissions']['edit_perm']!="1" && $this->params['permissions']['delete_perm']!="1"){

				}else{

					if(isset($columns[$keys[$i]]['render'])){

						array_push($cols, 
							array("data" => $columns[$keys[$i]]['data'], "searchable" => $columns[$keys[$i]]['searchable'], "orderable" => $columns[$keys[$i]]['orderable'], "render" => $columns[$keys[$i]]['render'])
						);

					}else{

						array_push($cols, 
							array("data" => $columns[$keys[$i]]['data'], "searchable" => $columns[$keys[$i]]['searchable'], "orderable" => $columns[$keys[$i]]['orderable'])
						);

					}
				
				}

			}else{

				if(isset($columns[$keys[$i]]['render'])){

					array_push($cols, 
						array("data" => $columns[$keys[$i]]['data'], "searchable" => $columns[$keys[$i]]['searchable'], "orderable" => $columns[$keys[$i]]['orderable'], "render" => $columns[$keys[$i]]['render'])
					);

				}else{

					array_push($cols, 
						array("data" => $columns[$keys[$i]]['data'], "searchable" => $columns[$keys[$i]]['searchable'], "orderable" => $columns[$keys[$i]]['orderable'])
					);

				}

			}

			

		}

		return $cols;

	}

	private function _get_datatable_options(){

		return array_merge(
			$this->params['params']['datatable']['options'], 
			array("columns" => $this->_get_datatable_columns()),
			array("buttons" => $this->params['params']['datatable']['buttons'])
		);

		// return array_merge(
		// 	$this->params['params']['datatable']['options'], 
		// 	array("columns" => $this->_get_datatable_columns()),
		// 	array("buttons" => $this->params['params']['datatable']['buttons']),
		// 	array("scrollY" => "300px"),
		// 	array("scrollX" => "true"),
		// 	array("scrollCollapse" => "true"),
		// 	array("fixedColumns" => array( "rightColumns" => 1))
		// );


	}

	private function _json_encode_ex($array) {

	    $var = json_encode($array);
	    preg_match_all('/\"function.*?\"/', $var, $matches);
	    foreach ($matches[0] as $key => $value) {
	        $newval = str_replace(array('\n', '\t','\/'), array(PHP_EOL,"\t",'/'), trim($value, '"'));
	        $var = str_replace($value, $newval, $var);
	    }
	    return $var;

	}

	private function _generate_form_add(){

		$params = $this->params['params']['form']['fields'];


		$keys = array_keys($params);

		$form_input = "";

		for($i=0;$i < count($keys);$i++){

			$type = @$params[$keys[$i]]['type'];

			$param['attribute'] = @$params[$keys[$i]];
			$param['field_name'] = @$keys[$i];
			$param['action'] = $this->params['params']['form']['action'];

			switch ($type) {

				case 'text':
					
					$form_input .= $this->_generate_input_text($param);

					break;

				case 'textarea':
					
					$form_input .= $this->_generate_input_textarea($param);

					break;

				case 'sourcequery':
					
					$form_input .= $this->_generate_input_source_query($param);

					break;

				case 'sourcearray':
					
					$form_input .= $this->_generate_input_source_array($param);

					break;

				case 'radio':
					
					$form_input .= $this->_generate_input_radio($param);

					break;	

				case 'checkbox':
					
					$form_input .= $this->_generate_input_checkbox($param);

					break;

				case 'upload_file':
					
					$form_input .= $this->_generate_input_upload_file($param);

					break;

				case 'geolocation':
					
					$form_input .= $this->_generate_input_geolocation($param);

					break;

				case 'editor':
					
					$form_input .= $this->_generate_input_editor($param);

					break;

				case 'dropzone':
					
					$form_input .= $this->_generate_input_dropzone($param);

					break;

				case 'datepicker':
					
					$form_input .= $this->_generate_input_datepicker($param);

					break;

				case 'daterangepicker':
					
					$form_input .= $this->_generate_input_daterangepicker($param);

					break;

				case 'timepicker':
					
					$form_input .= $this->_generate_input_timepicker($param);

					break;

				case 'duallistbox':
					
					$form_input .= $this->_generate_input_duallistbox($param);

					break;

				case 'tags':
					
					$form_input .= $this->_generate_input_tags($param);

					break;
				
			}

		}

		return $form_input;

	}

	private function _generate_form_edit(){

		$params = $this->params['params']['form']['fields'];

		$keys = array_keys($params);

		$form_input = "";

		for($i=0;$i < count($keys);$i++){

			$type = @$params[$keys[$i]]['type'];

			$param['attribute'] = @$params[$keys[$i]];
			$param['field_name'] = @$keys[$i];
			$param['action'] = $this->params['params']['form']['action'];
			$param['data_edit'] = $this->_get_data_edit();

			switch ($type) {

				case 'text':
					
					$form_input .= $this->_generate_input_text($param);

					break;

				case 'textarea':
					
					$form_input .= $this->_generate_input_textarea($param);

					break;

				case 'sourcequery':
					
					$form_input .= $this->_generate_input_source_query($param);

					break;

				case 'sourcearray':
					
					$form_input .= $this->_generate_input_source_array($param);

					break;

				case 'radio':
					
					$form_input .= $this->_generate_input_radio($param);

					break;	

				case 'checkbox':
					
					$form_input .= $this->_generate_input_checkbox($param);

					break;

				case 'upload_file':
					
					$form_input .= $this->_generate_input_upload_file($param);

					break;

				case 'geolocation':
					
					$form_input .= $this->_generate_input_geolocation($param);

					break;

				case 'editor':
					
					$form_input .= $this->_generate_input_editor($param);

					break;

				case 'dropzone':
					
					$form_input .= $this->_generate_input_dropzone($param);

					break;

				case 'datepicker':
					
					$form_input .= $this->_generate_input_datepicker($param);

					break;

				case 'daterangepicker':
					
					$form_input .= $this->_generate_input_daterangepicker($param);

					break;

				case 'timepicker':
					
					$form_input .= $this->_generate_input_timepicker($param);

					break;

				case 'duallistbox':
					
					$form_input .= $this->_generate_input_duallistbox($param);

					break;

				case 'tags':
					
					$form_input .= $this->_generate_input_tags($param);

					break;
				
			}

		}

		return $form_input;

	}

	private function _get_data_edit(){

		$table = $this->params['params']['table'];
		$pk = $this->params['params']['pk'];
		$id = $this->params['params']['id'];

		$query = $this->ci->db->query("SELECT * FROM ".$table." WHERE ".$pk."='".$id."'")->result_array();

		return @$query[0];

	}

	private function _generate_input_text($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$maxlength = $this->_set_max_length($params);
		$minlength = $this->_set_min_length($params);
		$placeholder = $this->_set_placeholder($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$type = $this->_set_input_type($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$style = $this->_set_input_style($params);

		if($type=="hidden"){
			$hidden = "style='display:none;'";
		}else{
			$hidden = "";
		}

		$html = '<div class="form-group" '.$hidden.'>';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<input type="'.$type.'" name="'.$field_name.'" id="'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" maxlength="'.$maxlength.'" minlength="'.$minlength.'" value="'.$value.'" style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';
        $html.= '</div>';

        return $html;

	}

	private function _generate_input_textarea($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$maxlength = $this->_set_max_length($params);
		$minlength = $this->_set_min_length($params);
		$placeholder = $this->_set_placeholder($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$rows = $this->_set_input_rows($params);
		$cols = $this->_set_input_cols($params);
		$style = $this->_set_input_style($params);

		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<textarea name="'.$field_name.'" rows="'.$rows.'" cols="'.$cols.'" id="'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" maxlength="'.$maxlength.'" minlength="'.$minlength.'" style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>'.$value.'</textarea>';
        $html.= '</div>';

        return $html;

	}

	private function _generate_input_source_query($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$type = $this->_set_input_type($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$style = $this->_set_input_style($params);
		$source = $this->_set_input_source($params);
		$keydt = $this->_set_input_keydt($params);
		$valuedt = $this->_set_input_valuedt($params);


		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<select name="'.$field_name.'" id="'.$id.'" class="'.$class.'"  style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';

        $html.= '<option value="">-- select '.strtolower($label).' --</option>';

        foreach ($source as $sc) {
        	
        	if($value!=""){

        		if($sc['id'] == $value){

        			$html.= '<option selected value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        		}else{

        			$html.= '<option value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        		}

        	}else{

        		$html.= '<option value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        	}

        }

        $html.= '</select>';
        $html.= '</div>';

        return $html;

	}

	private function _generate_input_source_array($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$type = $this->_set_input_type($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$style = $this->_set_input_style($params);
		$source = $this->_set_input_source($params);


		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<select name="'.$field_name.'" id="'.$id.'" class="'.$class.'"  style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';

        $html.= '<option value="">-- select '.strtolower($label).' --</option>';

        $keys = array_keys($source);

        for($i=0;$i < count($keys);$i++) {
        	
        	if($value!=""){

        		if($keys[$i] == $value){

        			$html.= '<option selected value="'.$keys[$i].'">'.$source[$keys[$i]].'</option>';

        		}else{

        			$html.= '<option value="'.$keys[$i].'">'.$source[$keys[$i]].'</option>';

        		}

        	}else{

        		$html.= '<option value="'.$keys[$i].'">'.$source[$keys[$i]].'</option>';

        	}

        }

        $html.= '</select>';
        $html.= '</div>';

        return $html;

	}

	private function _generate_input_radio($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$source = $this->_set_input_source($params);

		$html = '<div class="form-group">';
		$html.= '<label>'.$label.'</label>';
		
      	$keys = array_keys($source);

        for($i=0;$i < count($keys);$i++) {

        	$html.= '<div class="radio">';
        	$html.= '<label>';
        	
        	if($value!=""){

        		if($keys[$i] == $value){

        			$html.= '<input checked type="radio" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        		}else{

        			$html.= '<input type="radio" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        		}

        	}else{

        		if($i==0){
        			$checked = "checked";
        		}else{
        			$checked = "";
        		}

        		$html.= '<input '.$checked.' type="radio" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        	}

        	$html.= '</label>';
        	$html.= '</div>';

        }

        $html.= '</div>';

        return $html;

	}

	private function _generate_input_checkbox($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$source = $this->_set_input_source($params);

		$html = '<div class="form-group">';
		$html.= '<label>'.$label.'</label>';
		
      	$keys = array_keys($source);

        for($i=0;$i < count($keys);$i++) {

        	$html.= '<div class="checkbox">';
        	$html.= '<label>';
        	
        	if($value!=""){

        		if($keys[$i] == $value){

        			$html.= '<input checked type="checkbox" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        		}else{

        			$html.= '<input type="checkbox" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        		}

        	}else{

        		$html.= '<input type="checkbox" name="'.$field_name.'" id="'.$field_name.'_'.$i.'" value="'.$keys[$i].'"> '.$source[$keys[$i]];

        	}

        	$html.= '</label>';
        	$html.= '</div>';

        }

        $html.= '</div>';

        return $html;

	}
	
	private function _generate_input_upload_file($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$path = $params['attribute']['file_path'];

		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<input type="file" class="'.$class.'" name="'.$field_name.'" id="'.$id.'">';
        $html.= '<input type="hidden" class="'.$class.'" name="tmp_name" value="'.$field_name.'">';

        
        $upload_path = $_SERVER['DOCUMENT_ROOT'].$path;
	    $file = $upload_path.'/'.$value;

        if($value != ""){

        	$x = explode(".", $value);
        	$ext = end($x);

	        if($ext == "jpg" or $ext == "jpeg" or $ext == "png"){

	        	$html.= '<div class="img-prev"  id="'.$field_name.'_preview">';

	           
	            if(file_exists($file)){

	                $html.= "<img src='".$path.'/'.$value."' class='img-responsive' />
	                    <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#".$field_name."\");'><i class='fa fa-remove'></i>
	                    </a>";

	                $html.= "<input type='hidden' name='old_".$field_name."' value='".$value."'>";

	            }else{

	                $html.= "<h1>Preview ".$label."</h1>";

	            }

	            $html.= '</div>';

	        }else if($ext == "pdf"){

	        	$html.='<div id="'.$field_name.'_preview" style="height: 700px;margin-top:20px;"></div>';
	
	        }else if($ext == "csv" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx" || $ext == "doc" || $ext == "docx") {

	        	$html.= '<div  id="'.$field_name.'_preview">';

	        	$html.='<iframe style="height: 700px;margin-top:20px;border: none;" class="form-control" src="https://view.officeapps.live.com/op/embed.aspx?src='.base_url().'upload/media/'.$value.'"></iframe>';

	        	$html.= '</div>';

	       	}

	    }else{ 

	    	$html.= '<div class="img-prev"  id="'.$field_name.'_preview" style="display:none">';

            $html.= "<h1>Preview ".$label."</h1>";

            $html.= '</div>';

        }
	        

        
        $html.= '</div>';

        $html.= '<script type="text/javascript">';

		$html.= '$(document).ready(function(){';

		if($value != ""){

			$path = base_url().$path.$value;

        	$x = explode(".", $value);
        	$ext = end($x);

        	if($ext == "pdf"){

				$html.=	'PDFObject.embed("'.$path.'", "#'.$field_name.'_preview");';

			}else if ($ext == "csv" or $ext == "xls" or $ext == "xlsx") {
				
				$html.="$('#excelviewer').jexcel({
						    csv:'".$path."',
						    csvHeaders:true
						});

						$('#download').on('click', function () {
						    $('#excelviewer').jexcel('download');
						});";

			}

		}

		$html.=	'$("#'.$id.'").change(function(){
		            readURL(this, "#'.$id.'");
		        });

		    });
		';
		$html.= '</script>';

        return $html;

	}	

	private function _generate_input_geolocation($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$id = $this->_set_id($params);
		$libs_js = $params['attribute']['libs_js'];
		$libs_css = $params['attribute']['libs_css'];

		$keys = array_keys($libs_css);

		$html = "";

        for($i=0;$i < count($keys);$i++){

        	$html.='<link rel="stylesheet" href="'.$libs_css[$keys[$i]].'">';
        	
        }

        if(isset($params['attribute']['complete_address'])){

        	if($params['attribute']['complete_address']){

        		$extra_fields = '<div class="form-group">
                        <label for="address">Alamat</label>
                        <textarea name="address" class="form-control" id="location" placeholder="Alamat">'.@$params['data_edit']['address'].'</textarea
                    </div>
                    <div class="form-group" style="display:none">
                        <label for="country">Country</label>
                        <input type="text" maxlength="50" id="country" placeholder="Country">
                    </div>
                    <div class="form-group">
                        <label for="city">Kota</label>
                        <input type="text" maxlength="50" class="form-control" name="city" id="city" placeholder="Kota" value="'.@$params['data_edit']['city'].'">
                    </div>
                    <br>';

        	}else{

        		$extra_fields = '<div class="form-group" style="display:none">
                        <label for="address">Address</label>
                        <input type="text" id="location" placeholder="Address">
                    </div>
                    <div class="form-group" style="display:none">
                        <label for="country">Country</label>
                        <input type="text" maxlength="50" id="country" placeholder="Country">
                    </div>
                    <div class="form-group" style="display:none">
                        <label for="city">City</label>
                        <input type="text" maxlength="50" class="form-control" id="city" placeholder="City">
                    </div>
                    <br>';

        	}

        }else{

        	$extra_fields = '<div class="form-group" style="display:none">
                        <label for="address">Address</label>
                        <input type="text" id="location" placeholder="Address">
                    </div>
                    <div class="form-group" style="display:none">
                        <label for="country">Country</label>
                        <input type="text" maxlength="50" id="country" placeholder="Country">
                    </div>
                    <div class="form-group" style="display:none">
                        <label for="city">City</label>
                        <input type="text" maxlength="50" class="form-control" id="city" placeholder="City">
                    </div>
                    <br>';

        }

		$html.= '<label for="'.$id.'">'.$label.'</label>';
		$html.= '<input id="searchInput" class="controls" type="text" placeholder="Enter a location">
                <div id="map"  style="height:400px;"></div>
                '.$extra_fields.' 	
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="latitude">Latitude</label>
                            <input type="text" readonly name="latitude" class="form-control validate[required]" id="lat" placeholder="Latitude" value="'.@$params['data_edit']['latitude'].'">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="longitude">Longitude</label>
                            <input type="text" readonly name="longitude" class="form-control validate[required]" id="lon" placeholder="Longitude" value="'.@$params['data_edit']['longitude'].'">
                        </div>
                    </div>
                </div>';
        if(@$params['data_edit']['latitude']==""){
        	$params['data_edit']['latitude'] = 0;
        }

         if(@$params['data_edit']['longitude']==""){
        	$params['data_edit']['longitude'] = 0;
        }

        $html.='<script>
        			var latitude = '.@$params['data_edit']['latitude'].';
        			var longitude = '.@$params['data_edit']['longitude'].';
        			var zoom_point = 13;
        		</script>';

        $keys = array_keys($libs_js);

        for($i=0;$i < count($keys);$i++){

        	if($keys[$i]=="googlemaps"){
        		$html.='<script type="text/javascript" src="'.$libs_js[$keys[$i]].'" async defer></script>';
        	}else{
        		$html.='<script type="text/javascript" src="'.$libs_js[$keys[$i]].'"></script>';
        	}
        	

        }

        return $html;

	}

	private function _generate_input_editor($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$id = $this->_set_id($params);

		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<div id="validate_content" class="descriptionformError parentFormform formError" style="opacity: 0.87; position: absolute; top: 319px; left: 745px; right: initial; margin-top: -50px; display: none;"><div class="formErrorContent">* This field is required</div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>
                    <textarea class="form-control" id="'.$id.'">'.$value.'</textarea>
                    <input type="hidden" name="'.$field_name.'" id="post_'.$id.'" value="'.htmlentities($value).'">
                </div>';
        $html.= '<script type="text/javascript">';

		$html.= '
			$(document).ready(function(){

				CKEDITOR.replace( "'.$id.'",ckfinder_config);

		        for (var i in CKEDITOR.instances) {

		            CKEDITOR.instances[i].on("change", function() {

		                $("#post_'.$id.'").val(CKEDITOR.instances[i].getData());


		            });

		        }

		    });
		';
		$html.= '</script>';

        return $html;

	}

	private function _generate_input_dropzone($params){

	}

	private function _generate_input_datepicker($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$id = $this->_set_id($params);

		$html ='<div class="form-group">';
        $html.='<label>'.$label.'</label>';

        $html.='<div class="input-group">';
        $html.='<div class="input-group-addon">';
        $html.='<i class="fa fa-calendar"></i>';
        $html.='</div>';
        $html.='<input type="text" value="'.$value.'" name="'.$field_name.'" class="form-control pull-right" id="'.$id.'">';
        $html.='</div>';
        $html.='</div>';

        $html.= '<script type="text/javascript">';

		$html.= '
			$(document).ready(function(){

				$("#'.$id.'").datepicker();

		    });
		';
		$html.= '</script>';

		return $html;

	}

	private function _generate_input_daterangepicker($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$id = $this->_set_id($params);

		$html ='<div class="form-group">';
        $html.='<label>'.$label.'</label>';

        $html.='<div class="input-group">';
        $html.='<div class="input-group-addon">';
        $html.='<i class="fa fa-calendar"></i>';
        $html.='</div>';
        $html.='<input type="text" name="'.$field_name.'" class="form-control pull-right" id="'.$id.'">';
        $html.='</div>';
        $html.='</div>';

        $html.= '<script type="text/javascript">';

		$html.= '
			$(document).ready(function(){

				$("#'.$id.'").daterangepicker();

		    });
		';
		$html.= '</script>';

		return $html;
	}

	private function _generate_input_timepicker($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$value = $this->_set_input_value($params);
		$id = $this->_set_id($params);

		$html ='<div class="form-group">';
        $html.='<label>'.$label.'</label>';
		$html.='<div class="input-group  bootstrap-timepicker timepicker">';
        $html.='<input type="text" name="'.$field_name.'"  value="'.$value.'" class="form-control" id="'.$id.'">';
        $html.='<div class="input-group-addon">';
        $html.='<i class="fa fa-clock-o"></i>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';

        $html.= '<script type="text/javascript">';

		$html.= '
			$(document).ready(function(){

				$("#'.$id.'").timepicker({
                minuteStep: 1,
                showSeconds: true,
                showMeridian: false,
                defaultTime: false
            });

		    });
		';
		$html.= '</script>';

		return $html;

	}

	private function _generate_input_duallistbox($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$type = $this->_set_input_type($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$style = $this->_set_input_style($params);
		$source = $this->_set_input_source($params);
		$keydt = $this->_set_input_keydt($params);
		$valuedt = $this->_set_input_valuedt($params);


		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<select name="'.$field_name.'"  multiple="multiple" id="'.$id.'" class="'.$class.'"  style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';

        foreach ($source as $sc) {
        	
        	if($value!=""){

        		if($sc['id'] == $value){

        			$html.= '<option selected value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        		}else{

        			$html.= '<option value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        		}

        	}else{

        		$html.= '<option value="'.$sc[$keydt].'">'.$sc[$valuedt].'</option>';

        	}

        }

        $html.= '</select>';
        $html.= '</div>';

        $html.= '<script type="text/javascript">';

		$html.= '
			$(document).ready(function(){

				$("#'.$id.'").bootstrapDualListbox({
            
        		});

		    });
		';
		$html.= '</script>';

        return $html;

	}


	private function _generate_input_tags($params){

		$field_name = $params['field_name'];
		$label = $this->_set_label($params);
		$class = $this->_set_class($params);
		$id = $this->_set_id($params);
		$placeholder = $this->_set_placeholder($params);
		$event = $this->_set_dom_event($params);
		$data_attr = $this->_set_data_attr($params);
		$type = $this->_set_input_type($params);
		$value = $this->_set_input_value($params);
		$disabled = $this->_set_input_disabled($params);
		$readonly = $this->_set_input_readonly($params);
		$required = $this->_set_input_required($params);
		$style = $this->_set_input_style($params);

		$html = '<div class="form-group">';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<input type="'.$type.'" name="'.$field_name.'" id="'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" value="'.$value.'" style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.' data-role="tagsinput">';
        $html.= '</div>';

        return $html;

	}

	private function _set_label($params){

		if(isset($params['attribute']['label'])){

			if($this->ci->lang->line($params['attribute']['label']) != ""){

				return $this->ci->lang->line($params['attribute']['label']);

			}else{

				return $params['attribute']['label'];

			}

		}else{

			if($this->ci->lang->line($params['field_name']) != ""){

				return $this->ci->lang->line($params['field_name']);

			}else{

				return $params['field_name'];

			}

		}

	}

	private function _set_class($params){

		if(isset($params['attribute']['class'])){

			return $params['attribute']['class'];

		}else{

			return "form-control";

		}

	}

	private function _set_id($params){

		if(isset($params['attribute']['id'])){

			return $params['attribute']['id'];

		}else{

			return $params['field_name'];

		}

	}

	private function _set_max_length($params){

		if(isset($params['attribute']['maxlength'])){

			return $params['attribute']['maxlength'];

		}else{

			return 250;

		}

	}

	private function _set_min_length($params){

		if(isset($params['attribute']['minlength'])){

			return $params['attribute']['minlength'];

		}else{

			return 0;

		}

	}

	private function _set_placeholder($params){


		if(isset($params['attribute']['placeholder'])){

			return $params['attribute']['placeholder'];

		}else{

			return "";

		}


	}

	private function _set_dom_event($params){

		if(isset($params['attribute']['event'])){

			$keys = array_keys($params['attribute']['event']);

			$events = '';
			for($i=0; $i < count($keys); $i++){

				$event_name = $keys[$i];
				$function = $params['attribute']['event'][$keys[$i]];

				$events.=' '.$event_name.'="'.$function.'"';

			}

			return $events;

		}else{

			return "";

		}

	}

	private function _set_data_attr($params){

		if(isset($params['attribute']['data_attr'])){

			$keys = array_keys($params['attribute']['data_attr']);

			$data_attr = '';
			for($i=0; $i < count($keys); $i++){

				$attr_name = $keys[$i];
				$value = $params['attribute']['data_attr'][$keys[$i]];

				$data_attr.=' data-'.$attr_name.'="'.$value.'"';

			}

			return $data_attr;

		}else{

			return "";

		}

	}

	private function _set_input_type($params){

		if(isset($params['attribute']['input_type'])){

			return $params['attribute']['input_type'];

		}else{

			return "text";

		}


	}

	private function _set_input_required($params){

		if(isset($params['attribute']['required'])){

			if($params['attribute']['required']==true){

				return "required";

			}else{

				return "";

			}

		}else{

			return "";

		}


	}

	private function _set_input_readonly($params){

		if(isset($params['attribute']['readonly'])){

			if($params['attribute']['readonly']==true){

				return "readonly";

			}else{

				return "";

			}

		}else{

			return "";

		}

	}

	private function _set_input_disabled($params){

		if(isset($params['attribute']['disabled'])){

			if($params['attribute']['disabled']==true){

				return "disabled";

			}else{

				return "";

			}

		}else{

			return "";

		}

	}

	private function _set_input_value($params){

		if(isset($params['attribute']['value'])){

			return $params['attribute']['value'];
			
		}else{

			if($params['action']=="edit"){

				if(isset($params['data_edit'])){

					return @$params['data_edit'][$params['field_name']];

				}else{

					return "";

				}

			}else{

				return "";

			}

		}

	}

	private function _set_input_rows($params){

		if(isset($params['attribute']['rows'])){

			return $params['attribute']['rows'];

		}else{

			return 0;

		}

	}

	private function _set_input_cols($params){

		if(isset($params['attribute']['cols'])){

			return $params['attribute']['cols'];

		}else{

			return 0;

		}

	}

	private function _set_input_style($params){

		if(isset($params['attribute']['style'])){

			return $params['attribute']['style'];

		}else{

			return "";

		}

	}

	private function _set_input_source($params){

		if(isset($params['attribute']['source'])){

			return $params['attribute']['source'];

		}else{

			return array();

		}

	}

	private function _set_input_keydt($params){

		if(isset($params['attribute']['keydt'])){

			return $params['attribute']['keydt'];

		}else{

			return "id";

		}

	}

	private function _set_input_valuedt($params){

		if(isset($params['attribute']['valuedt'])){

			return $params['attribute']['valuedt'];

		}else{

			return "name";

		}

	}

	private function _get_action_add_url(){

		$dir = $this->params['current_class_dir'];
		$class = $this->params['current_class'];

		$x = explode("/", $dir);
		$module = $x[2];

		$action_url = base_url().$module.'/'.$class.'/add';

		return $action_url;

	}

	private function _get_action_edit_url(){

		$dir = $this->params['current_class_dir'];
		$class = $this->params['current_class'];

		$x = explode("/", $dir);
		$module = $x[2];

		$action_url = base_url().$module.'/'.$class.'/edit';

		return $action_url;

	}

	private function _get_action_remove_url(){

		$dir = $this->params['current_class_dir'];
		$class = $this->params['current_class'];

		$x = explode("/", $dir);
		$module = $x[2];

		$action_url = base_url().$module.'/'.$class.'/remove';

		return $action_url;

	}

	private function _get_breadcrumb($action){

		$dir = $this->params['current_class_dir'];
		$class = $this->params['current_class'];

		$x = explode("/", $dir);
		$module = $x[2];


		if($action=="browse"){

			$data = array(
	            "<i class='fa fa-info-circle'></i> ".$this->params['page_subtitle'] => base_url().$module.'/'.$class
	        );

		}else if($action=="add"){

			$data = array(
	            "<i class='fa fa-info-circle'></i> ".$this->params['page_subtitle'] => base_url().$module.'/'.$class,
	            $this->params['page_title'] => base_url().$module.'/'.$class.'/add',
	        );

		}else if($action=="edit"){

			$data = array(
	            "<i class='fa fa-info-circle'></i> ".$this->params['page_subtitle'] => base_url().$module.'/'.$class,
	            $this->params['page_title'] => base_url().$module.'/'.$class.'/edit/'.$this->params['params']['id'],
	        );

		}

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }

}