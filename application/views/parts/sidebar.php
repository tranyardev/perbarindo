<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image sidebar-profile">
                <?php if($this->session->userdata('picture')!=""){?>
                    <img src="<?php echo $this->session->userdata('picture'); ?>" class="scale" data-scale="best-fill" data-align="center">
                <?php }else{ ?>
                    <img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" class="scale" data-scale="best-fill" data-align="center">
                <?php } ?>
            </div>
            <div class="pull-left info">
                <p>  <?php echo truncate($this->session->userdata('first_name').' '.$this->session->userdata('last_name'),22); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <?php

                $user_group = getUserGroup($this->session->userdata('id'));

                $menus = getMenus();
                foreach($menus as $mn) {

                    if ($mn['parent'] == "") {
                        $childs = getMenuChilds($mn['menu_id']);

                        if(@$parent_menu == $mn['slug']){
                            $class = "active";
                        }else{
                            $class = "";
                        }

                        if (count($childs) > 0) {

                            $have_child_permission = 0;

                            foreach ($childs as $ch){

                                if($ch['permission']!=""){

                                    if($this->aauth->is_group_allowed($ch['perm'], $user_group['name'])){

                                        $have_child_permission++;

                                    }

                                }


                            }

                            if( $have_child_permission > 0){

                                $html = '<li class="'.$class.' treeview">
                                        <a href="#">
                                            <i class="fa '.$mn['fa_icon'].'"></i>
                                            <span>'.$mn['display_name'].'</span>
                                        </a>
                                        <ul class="treeview-menu">';

                                foreach ($childs as $ch){

                                    if($ch['permission']!=""){

                                        if($this->aauth->is_group_allowed($ch['perm'], $user_group['name'])){

                                            if(@$submenu == $ch['slug']){
                                                $chclass = "active";
                                            }else{
                                                $chclass = "";
                                            }

                                            $html.= '<li class="'.$chclass.'"><a href="'.base_url().$ch['path'].'"><i class="fa '.$ch['fa_icon'].'"></i> '.$ch['display_name'].'</a></li>';

                                        }

                                    }


                                }

                                $html.= '   </ul>
                                    </li>';

                                echo $html;

                            }



                        }else{

                            if($mn['permission']!=""){

                                if($this->aauth->is_group_allowed($mn['perm'], $user_group['name'])){

                                    echo '<li class="'.$class.'"><a href="'.base_url().$mn['path'].'"><i class="fa '.$mn['fa_icon'].'"></i> <span>'.$mn['display_name'].'</span></a></li>';

                                }

                            }

                        }
                    }
                }
            ?>

        </ul>
        
    </section>
    <!-- /.sidebar -->

</aside>