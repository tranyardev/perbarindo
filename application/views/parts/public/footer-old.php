 
 <footer class="footer footer-default">
    <div class="container visible-lg visible-md visible-sm hidden-xs">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <nav class="padding-footer">
                    <ul style="font-size: 14px;">
                        <li>
                            <a href="<?php echo base_url(); ?>">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>about">
                                ABOUT US
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>faq">
                                FAQ
                            </a>
                        </li>
                        <li><a  href="<?php echo base_url(); ?>term_of_condition">
                            TOS
                        </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>contact">
                                CONTACT
                            </a>
                        </li>
                      <!--   <li>
                            <a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                                TERM OF CONDITION
                            </a>
                        </li> -->
                    </ul>
                </nav> 
            </div> 
            <!--  <div class="col-lg-4 col-md-1 col-sm-1 col-xs-4 text-center" align="center">
                  <center> <nav class="padding-footer pull-center" style="float:none;">
                    <ul>
                        <li><a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                            TERM OF SERVICES
                        </a>
                        </li>
                    </ul>
                 </nav> </center>
             </div> -->
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="copyright padding-footer">
                        Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a>, 
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a>
                    </div>
            </div>
        </div>  
    </div>
    <div class="container hidden-lg hidden-md hidden-sm visible-xs">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" align="center">
                <navx class="padding-footer text-center pull-center" >
                    <ul style="font-size: 14px;">
                        <li>
                            <a href="<?php echo base_url(); ?>">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>about">
                                ABOUT US
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>faq">
                                FAQ
                            </a>
                        </li>
                        <li><a  href="<?php echo base_url(); ?>term_of_condition">
                            TOS
                        </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>contact">
                                CONTACT
                            </a>
                        </li>
                      <!--   <li>
                            <a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                                TERM OF CONDITION
                            </a>
                        </li> -->
                    </ul>
                </navx> 
            </div> 
            <!--  <div class="col-lg-4 col-md-1 col-sm-1 col-xs-4 text-center" align="center">
                  <center> <nav class="padding-footer pull-center" style="float:none;">
                    <ul>
                        <li><a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                            TERM OF SERVICES
                        </a>
                        </li>
                    </ul>
                 </nav> </center>
             </div> -->
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" align="center">
                <div class="copyright padding-footer text-center">
                        Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a>, 
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a>
                    </div>
            </div>
        </div> 
    </div>
</footer>

