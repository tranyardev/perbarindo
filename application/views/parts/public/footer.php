 
<footer class="footer footer-default footer-lg">
    <div class="footer-top">
        <div class="container-fluid container-fluid-md"> 
            <div class="row">
 
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-0">
                    <div class="wrap-footer">
                        <ul class="list-group list-group-footer list-group-info ">
                           <li class="list-group-item list-mb">
                                <a class="nav-link" href="https://www.google.com/maps/place/PERBARINDO/@-6.179179,106.875007,16z/data=!4m5!3m4!1s0x0:0xae439991ece8d270!8m2!3d-6.179179!4d106.875007?hl=en-US" target="_blank">
                                   <table class="nav-info">
                                       <tr>
                                           <td>
                                               <button class="btn btn-simple btn-round">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                            </td>
                                           <td class="cont">
                                               <?php if(isset($company_address['value'])){ echo @$company_address['value']; }else{ ?> 
                                               Komplek Patra II No. 46, Jl. Jendral Ahmad Yani <br/>
                                               Bypass, Cempaka Putih Jakarta Pusat 10510
                                                <?php } ?>
                                           </td>
                                       </tr>
                                   </table>
                                </a>
                            </li>
                            <li class="list-group-item list-mb">
                                <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>" target="_blank">
                                    <table class="nav-info">
                                       <tr>
                                           <td>
                                                <button class="btn btn-simple btn-round"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                            </td>
                                            <td class="cont">
                                                Hubungi Kami Sekarang di <br> <h5><b><?php echo @$company_phone['value']; ?></b></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </li>
                            <li class="list-group-item list-mb">
                                <a class="nav-link" href="mailto:dpp_perbarindo@yahoo.com" target="_blank">
                                    <table class="nav-info">
                                       <tr>
                                           <td>
                                                <button class="btn btn-simple btn-round"> <i class="fa fa-envelope fa-lg fa-1x"></i> </button> 
                                            </td>
                                            <td class="cont">
                                                 <h5><b>dpp_perbarindo@yahoo.com</b></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-0">

                     
                    <div class="wrap-footer">
                        <div class="row justify-content-md-center sharing-area text-center">
                            <div class="text-center col-md-12 col-lg-12 padding-0"> 
                                <div id="infosend" style="margin-top:4px;"></div>
                                <br>
                                <form>
                                  <div class="label-subscrive">
                                    <h3>Subscribe</h3>
                                    <p>Berlangganan Newsletter</p>
                                </div>
                                  <div class="form-group"> 
                                    <input type="text" class="form-control form-subscrive" id="email" aria-describedby="Subscribe" placeholder="Masukan E-mail anda"> 
                                  </div> 
                                  <button type="submit" class="btn btn-primary bg-greengr" onclick="contactsend();return false;"><b>Submit</b></button>
                                </form>
                                <br/> 
                            </div>
                            <div class="text-center">
                               
                               <div class="visible-lg visible-md hidden-sm hidden-xs">
                                    <div class="row">
                                        <div class="col col-lg-6 col-md-12 col-12 col-sm-12 col-xs-12">
                                            <div class="wrap-ojk">  
                                             <img src="http://perbarindo.tranyar.com/mocukup/Logo-OJK.png" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12" style="border-left: 2px solid #919395; padding-top: 1px;">
                                            <a target="_blank" href="<?php echo @$tw['value']; ?>" class="btn btn-foot btn-icon btn-twitter btn-md" rel="tooltip" title="" data-original-title="Follow us">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a target="_blank" href="<?php echo @$fb['value']; ?>" class="btn btn-foot btn-icon btn-facebook btn-md" rel="tooltip" title="" data-original-title="Like us">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a href="<?php echo @$gg['value']; ?>" target="_blank" class="btn btn-foot btn-icon btn-facebook btn-md">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                           <!--  <a target="_blank" href="https://www.linkedin.com/company-beta/9430489/" class="btn btn-foot btn-icon btn-linkedin btn-md" rel="tooltip" title="" data-original-title="Follow us">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <a target="_blank" href="https://github.com/creativetimofficial/now-ui-kit" class="btn btn-foot btn-icon btn-github btn-md" rel="tooltip" title="" data-original-title="Star on Github">
                                                <i class="fa fa-github"></i>
                                            </a> -->
                                        </div>
                                    </div>
                               </div>
                               <div class="hidden-lg hidden-md visible-sm visible-xs">
                                    <div class="row">
                                        <div class="col col-lg-6 col-md-12 col-12 col-sm-12 col-xs-12">
                                            <div class="wrap-ojk">  
                                             <img src="http://perbarindo.tranyar.com/mocukup/Logo-OJK.png" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col col-lg-6 col-md-12 col-sm-12 col-xs-12" style="padding-top: 1px;">
                                            <a target="_blank" href="<?php echo @$tw['value']; ?>" class="btn btn-foot btn-icon btn-twitter btn-md" rel="tooltip" title="" data-original-title="Follow us">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a target="_blank" href="<?php echo @$fb['value']; ?>" class="btn btn-foot btn-icon btn-facebook btn-md" rel="tooltip" title="" data-original-title="Like us">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a href="<?php echo @$gg['value']; ?>" target="_blank" class="btn btn-foot btn-icon btn-facebook btn-md">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                           <!--  <a target="_blank" href="https://www.linkedin.com/company-beta/9430489/" class="btn btn-foot btn-icon btn-linkedin btn-md" rel="tooltip" title="" data-original-title="Follow us">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <a target="_blank" href="https://github.com/creativetimofficial/now-ui-kit" class="btn btn-foot btn-icon btn-github btn-md" rel="tooltip" title="" data-original-title="Star on Github">
                                                <i class="fa fa-github"></i>
                                            </a> -->
                                        </div>
                                    </div>
                               </div>

 
                            </div>
                        </div>
                    </div>   
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-0">
                    <div class="wrap-footer text-right">
                        <ul class="list-group list-group-footer nav-footer visible-lg visible-md visible-sm hidden-xs">
                          <li class="list-group-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                          <li class="list-group-item"><a href="<?php echo base_url(); ?>about">About Us</a></li>
                          <li class="list-group-item"><a href="<?php echo base_url(); ?>faq">FAQ</a></li> 
                          <li class="list-group-item"><a  href="<?php echo base_url(); ?>term_of_condition">Term Of Condition</a></li>
                          <li class="list-group-item"><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                          <li class="list-group-item"><a href="http://perbarindo.tranyar.com/find/event"> Find Event </a></li> 
                        </ul>

                        <navx class="padding-footer text-center pull-center hidden-lg hidden-md hidden-sm visible-xs" >
                            <hr>
                            <ul style="font-size: 14px;">
                                <li>
                                    <a href="<?php echo base_url(); ?>">
                                        HOME
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>about">
                                        ABOUT US
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>faq">
                                        FAQ
                                    </a>
                                </li>
                                <li><a  href="<?php echo base_url(); ?>term_of_condition">
                                    TOS
                                </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>contact">
                                        CONTACT
                                    </a>
                                </li>
                              <!-- <li>
                                    <a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                                        TERM OF CONDITION
                                    </a>
                                </li> -->
                            </ul>
                        </navx> 

                    </div>
                </div>
            </div>
        </div>
   </div>
   <div class="footer-bottom text-center bg-greenkilot">
        <div class="copyrights">
           <!--  &copy; -->
            <!-- 2014-2019. --> Powered by <a style="color: #36b183;" href="https://www.tranyar.co.id/"><b>Tranyar</b></a> | Sahabat Bisnismu <br>
            Seluruh isi website dilindungi Undang-Undang
          <!--   <script>
                document.write(new Date().getFullYear())
            </script>, Designed by
            <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>. -->
        </div>
   </div>
</footer>


<?php /***

 <footer class="footer footer-default">
    <div class="container visible-lg visible-md visible-sm hidden-xs">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <nav class="padding-footer">
                    <ul style="font-size: 14px;">
                        <li>
                            <a href="<?php echo base_url(); ?>">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>about">
                                ABOUT US
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>faq">
                                FAQ
                            </a>
                        </li>
                        <li><a  href="<?php echo base_url(); ?>term_of_condition">
                            TOS
                        </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>contact">
                                CONTACT
                            </a>
                        </li>
                      <!--   <li>
                            <a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                                TERM OF CONDITION
                            </a>
                        </li> -->
                    </ul>
                </nav> 
            </div> 
            <!--  <div class="col-lg-4 col-md-1 col-sm-1 col-xs-4 text-center" align="center">
                  <center> <nav class="padding-footer pull-center" style="float:none;">
                    <ul>
                        <li><a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                            TERM OF SERVICES
                        </a>
                        </li>
                    </ul>
                 </nav> </center>
             </div> -->
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="copyright padding-footer">
                        Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a>, 
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a>
                    </div>
            </div>
        </div>  
    </div>
    <div class="container hidden-lg hidden-md hidden-sm visible-xs">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" align="center">
                <navx class="padding-footer text-center pull-center" >
                    <ul style="font-size: 14px;">
                        <li>
                            <a href="<?php echo base_url(); ?>">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>about">
                                ABOUT US
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>faq">
                                FAQ
                            </a>
                        </li>
                        <li><a  href="<?php echo base_url(); ?>term_of_condition">
                            TOS
                        </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>contact">
                                CONTACT
                            </a>
                        </li>
                      <!--   <li>
                            <a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                                TERM OF CONDITION
                            </a>
                        </li> -->
                    </ul>
                </navx> 
            </div> 
            <!--  <div class="col-lg-4 col-md-1 col-sm-1 col-xs-4 text-center" align="center">
                  <center> <nav class="padding-footer pull-center" style="float:none;">
                    <ul>
                        <li><a class="text-center" href="<?php //echo base_url(); ?>term_of_condition">
                            TERM OF SERVICES
                        </a>
                        </li>
                    </ul>
                 </nav> </center>
             </div> -->
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" align="center">
                <div class="copyright padding-footer text-center">
                        Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a>, 
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a>
                    </div>
            </div>
        </div> 
    </div>
</footer>



***/ ?>

