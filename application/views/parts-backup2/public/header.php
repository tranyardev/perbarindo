
<link href="https://fonts.googleapis.com/css?family=Carter+One" rel="stylesheet">
<style type="text/css">
.card-head{margin-top:-54px}.font-carter{font-family:'Carter One',cursive}.bg-greenmuda{background-color:#439D47!important}.bg-greenkilot{background-color:#216030!important}.footer-top{background-color:#fef6e1!important;padding:23px}.card-service{border-radius:23px!important}.title-rgs{margin-bottom:13px;font-size:24px;margin-top:27px;color:#555}.description-rgs{border-bottom-right-radius:21px;border-bottom-left-radius:21px;margin-bottom:-30px}.bg-orange-md{background-color:#fa9e25}.bg-yellow-md{background-color:#ffc107}.bg-purple-md{background-color:#ef00ff}.bg-ungu-md{background-color:#571d96}.title-md:before,.title-xs:before{content:'';position:absolute;left:52%;width:71px;height:2px;background-color:#555!important}.wrap-card{padding-top:14px}.pad-roro{padding:9px 9px 4px!important}.item__image{padding-bottom:7px}.text--medium{margin-right:-13px;margin-bottom:-6px;font-size:10px}.text-headbpr>h1{font-size:12px!important;padding:0!important;margin:0!important}.title-md:before{top:67px;-webkit-transform:translate(-50%,0);transform:translate(-50%,0)}.title-xs:before{top:66px;-webkit-transform:translate(-50%,0);transform:translate(-50%,0)}.p-lgt,.titlelistg{margin-bottom:1px!important}.smallx{font-size:70%}.fsz12{font-size:10px}.pagning{margin-top:9px}.ovhiden{height:261px;border-top:1px solid #DDD;border-bottom:1px solid #DDD;overflow-x:hidden}.list-group-itemx.active{z-index:2;color:#2b2f33!important;background-color:#fff!important;border-color:#fff!important}.title-md,.title-xs{margin-bottom:13px;font-size:29px;margin-top:27px;color:#555}.list-group-footer>.list-mb{margin-bottom:-14px!important}@media screen and (max-width:991px){.sidebar-collapse .navbar-collapse:before{background:-webkit-linear-gradient(#f96333 0,#64069c 95%);background:-o-linear-gradient(#f96333 0,#64069c 95%);background:-moz-linear-gradient(#f96333 0,#64069c 95%);background:linear-gradient(#215f31 0,#439d47 95%);opacity:23.76}}.nav-white button{width:16px;height:44px;color:#fff!important;border:1px solid #fff!important}.icon-purple{color:#A40DFF}.icon-orange{color:#fb9522}.icon-green{color:#A40DFF}.padding-xs{padding:4px}.img-bxs{border:1px solid #DDD}.page-header[filter-color=green]{background-color:#439d4733}.wrap-boxmetro{border-left:4px solid #215f31!important}.alert-red{background-color:#60913f!important;border-radius:4px}.alert-yellow{background-color:#578b3f!important;border-radius:4px}.body-boxmetro{background:#FFF;border:1px solid #DDD;padding:12px;margin-bottom:12px}.badge-warning{border-color:#60913f;border-radius:4px!important}.margin-topfxs{margin-top:-423px}.owl-next,.owl-prev{background:#439d47!important}.tab-content .tab-pane{background-color:#eee;padding:20px;border:1px solid #ddd;margin-bottom:50px;min-height:500px}.bg-white{background-color:#FFF}.section-header{font-size:1.714em;line-height:1.45em;margin-top:4px;margin-bottom:0}.dex-aboutus{padding:12px}.dex-aboutus h1{color:#a2a2a2;font-size:25px;margin-top:4px;margin-bottom:2px}.head-timeline h2{font-size:22px;font-weight:200;color:#439d47}.select2-container .select2-selection--single{overflow:hidden;width:99%}.btn-primary.btn-simple{color:#fff;border-color:#439d47}.dex-aboutus p{font-size:12px;color:#636262}.btn-iconhead{background-color:#fff!important;color:#33803c!important}.framex{position:relative!important}.img-aboutus-x img{width:37%;align-content:center}.text-purple{color:#8d3e93!important}.text-orange{color:#f7a328!important}.text-yellow{color:#ee5a28!important}.card-bx{margin-bottom:-5px!important}.body-card-px{padding:0}.poster-card__venue{padding:12px}.poster-card__body{padding-top:0!important}.text-heading-epic{font-weight:200;margin-bottom:3px;padding:14px;font-size:39px}.text-ellipsis{width:96%;display:inline-block;font-size:1em;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}.text-primary{color:#439d47!important}.btn-primary{background-color:#439d47;color:#FFF}.btn-warning{background-color:#3d8e41;color:#FFF}.blefetHover:hover,.blefetHoverxs:hover{background-color:#fdfcfc;transition:-9s}.bg-crimeimgx{background-size:cover;background-image:url(http://perbarindo.tranyar.com/mocukup/Bg-New.png)}.navbar-x{margin-bottom:0}@media screen and (min-width:200px){.margin-topfix{margin-top:22px}}@media screen and (min-width:300px){.margin-topfix{margin-top:22px}}@media screen and (min-width:320px){.margin-topfix{margin-top:22px}}@media screen and (min-width:375){.margin-topfix{margin-top:320px}}@media screen and (min-width:425px){.margin-topfix{margin-top:23px}}@media screen and (min-width:768px){.margin-topfix{margin-top:21px}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:5%;left:48%}}@media screen and (min-width:754px){.margin-topfix{margin-top:21px}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}@media screen and (min-width:800px){.margin-topfix{margin-top:23px}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}@media screen and (min-width:900px){.margin-topfix{margin-top:0}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}@media screen and (min-width:1024px){.margin-topfix{margin-top:0}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}@media screen and (min-width:1002px){.margin-topfix{margin-top:0}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}@media screen and (min-width:2560px){.margin-topfix{margin-top:0}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:7%;left:47%}}div.scrollmenu a{display:inline-block;color:#fff;text-align:center;padding:7px;text-decoration:none}div.scrollmenu{padding-left:13px!important}.fixed-bottom{margin-bottom:0!important;border-top:2px solid #60913f}.blefetHover:hover{border-left:3px solid #439d47}.blefetHoverxs:hover{box-shadow:2px 2px 8px 0 #555;border-bottom:3px solid #439d47}.paging-purple .pagination{margin-bottom:0}.paging-purple .pagination .page-item.active a:focus,.paging-purple .pagination .page-item.active>a,.paging-purple .pagination .page-item.active>a:hover{background-color:#832cd1;border-color:#888;color:#FFF}.paging-orange .pagination .page-item.active a:focus,.paging-orange .pagination .page-item.active>a,.paging-orange .pagination .page-item.active>a:hover{background-color:#fb9422;border-color:#888;color:#FFF}@media screen and (max-width:992px){.card-head>.icon-xs>img{width:29%;left:36%;top:-86px}.card-head>.spin>img{z-index:1;position:absolute;vertical-align:center;width:5%;left:48%}}.card-head>.icon-xs>img{width:29%;left:36%;top:-86px}.d-flex{margin-bottom:-5px}
 



</style>
<div class="visible-lg visible-md hidden-sm hidden-xs">
<nav class="navbar navbar-expand-lg bg-greenkilot fixed-topv" style="height: 2px;">
    <div class="container"> 
        <div class="navbar-translate">
            
            <a class="navbar-brand" href="<?php echo base_url(); ?>" rel="tooltip" title="Perhimpunan Bank Perkreditan Indonesia" data-placement="bottom" style="width: 100%">
              <center> <img src="http://perbarindo.tranyar.com/public/assets/img/logo.png" style="width: 164px;"> </center>
            </a>
         
        </div>
        <div class="collapse navbar-collapse justify-content-end hidden-sm hidden-xs" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://maps.google.com/maps?q=<?php echo @$company_latitude['value']; ?>,<?php echo @$company_longitude['value']; ?>&z=17" target="_blank">
                       <table class="nav-info nav-white nav-whead">
                           <tr>
                               <td>
                                   <button class="btn btn-simple btn-round btn-xs" style="width: 1px;">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                </td>
                               <td class="cont">
                                 <?php echo @$company_address['value']; ?>
                               </td>
                           </tr>
                       </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>">
                        <table class="nav-info nav-white nav-whead">
                           <tr>
                               <td>
                                    <button class="btn btn-simple btn-round btn-xs" style="width: 1px;"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                </td>
                                <td class="cont">
                                    Hubungi Kami Sekarang di <br> <h5><b><?php echo @$company_phone['value']; ?></b></h5>
                                </td>
                            </tr>
                        </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="<?php echo @$tw['value']; ?>" target="_blank">
                        <i class="fa fa-twitter fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="<?php echo @$fb['value']; ?>" target="_blank">
                        <i class="fa fa-facebook-square fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Google Plus" data-placement="bottom" href="<?php echo @$gg['value']; ?>" target="_blank">
                        <i class="fa fa-google-plus fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>