 <!-- Navbar -->
<nav class="navbar navbar-expand-lg bg-primary fixed-top <?php echo @$navbar_class; ?>" color-on-scroll="50">
    <div class="container">
        <div class="dropdown button-dropdown">
            <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                <span class="button-bar"></span>
                <span class="button-bar"></span>
                <span class="button-bar"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-header">PERBARINDO</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>">HOME</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>bpr">BPR</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>blog">BLOG</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>about">ABOUT</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>faq">FAQ</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>contact">CONTACT</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>term_of_condition">TERM OF CONDITION</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>find/event">FIND EVENT</a>
               
            </div>
        </div>
        <div class="navbar-translate">
            <a class="navbar-brand navbar-brand-img" href="<?php echo base_url(); ?>" rel="tooltip" title="Perhimpunan Bank Perkreditan Indonesia" data-placement="bottom">
                <img src="<?php echo base_url() ?>public/assets/img/logo.png" class="img-responsive" style="height: 50px !important;width: auto !important;">
               <!--  Now Ui Kit -->
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                
                <li class="nav-item dropdown hidden-lg hidden-md visible-sm visible-xs">
                    <a href="#" class="nav-link dropdown-toggle" id="accountLink" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-home" aria-hidden="true"></i> MAIN
                    </a>
                    <div class="dropdown-menu dropdown-menu-right showx" aria-labelledby="accountLink">
                       
                      <a class="dropdown-header">PERBARINDO</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>">HOME</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>bpr">BPR</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>blog">BLOG</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>about">ABOUT</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>faq">FAQ</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>contact">CONTACT</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>term_of_condition">TERM OF CONDITION</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>find/event">FIND EVENT</a>
                    </div>
                </li>

                <?php 

                if($this->aauth->is_loggedin()){

                    $user_group = $this->Mcore->getUserGroup($this->session->userdata("id"));

                ?>

                    <li class="nav-item dropdown showx">
                        <a href="#" class="nav-link dropdown-toggle" id="accountLink" data-toggle="dropdown" aria-expanded="true">
                           <img style="width: 32px;height: 32px;" src="<?php ($this->session->userdata('picture')!="")? $avatar=$this->session->userdata('picture'):$avatar=base_url().'public/assets/images/default-avatar.png';echo $avatar;?>" alt="Circle Image" class="rounded-circle"> <?php echo $this->session->userdata('username') ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right showx" aria-labelledby="accountLink">
                            <a class="dropdown-header">Account User</a>
                            <?php if($user_group['id'] == "10"){ ?>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>myprofile">My Dashboard</a>
                            <?php }else{ ?>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>admin">Admin Panel</a>
                            <?php } ?>
                            <div class="divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url() ?>logout"><i class="fa fa-sign-out fa-lg"></i> Sign Out</a>
                        </div>
                    </li>

                <?php
                }else{
                ?>
                    <li class="nav-item dropdown showx">
                        <a href="#" class="nav-link dropdown-toggle" id="accountLink" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right showx" aria-labelledby="accountLink">
                            <a class="dropdown-header">Account User</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>login"><i class="fa fa-sign-in fa-lg"></i> &nbsp;Masuk</a>
                            <div class="divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url() ?>signup"><i class="fa fa-user-plus fa-lg"></i> Daftar</a>
                        </div>
                    </li>
                <?php
                }
                ?>
               


            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->