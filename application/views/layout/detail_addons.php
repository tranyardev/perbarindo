 
<style>
     .screenshot-module{
            width: 100%;
            min-height: 200px;
        }
        .video-module{
            min-height: 200px;
            padding: 20px;
        }
        .screenshot-module > img{

            padding: 20px;
            width: 100%;

        }
        .rev-addon{
            min-height: 85px;
            margin-top: 10px;
            background-color: white;
        }
        .bs-component{
            background-color: white;
        }
        .bs-component > .tab-content{
            padding: 15px;
        }
        .bs-component > .nav-tabs{
            border-radius: 0px;
            padding: 0px;
        }
        /** ====================
         * Lista de Comentarios
         =======================*/
        .comments-container {
           
            width: 768px;
        }

        .comments-container h1 {
            font-size: 36px;
            color: #283035;
            font-weight: 400;
        }

        .comments-container h1 a {
            font-size: 18px;
            font-weight: 700;
        }

        .comments-list {
            margin-top: 30px;
            position: relative;
        }

        /**
         * Lineas / Detalles
         -----------------------*/
        .comments-list:before {
            content: '';
            width: 2px;
            height: 100%;
            background: #c7cacb;
            position: absolute;
            left: 32px;
            top: 0;
        }

        .comments-list:after {
            content: '';
            position: absolute;
            background: #c7cacb;
            bottom: 0;
            left: 27px;
            width: 7px;
            height: 7px;
            border: 3px solid #dee1e3;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }

        .reply-list:before, .reply-list:after {display: none;}
        .reply-list li:before {
            content: '';
            width: 60px;
            height: 2px;
            background: #c7cacb;
            position: absolute;
            top: 25px;
            left: -55px;
        }


        .comments-list li {
            margin-bottom: 15px;
            display: block;
            position: relative;
        }

        .comments-list li:after {
            content: '';
            display: block;
            clear: both;
            height: 0;
            width: 0;
        }

        .reply-list {
            padding-left: 88px;
            clear: both;
            margin-top: 15px;
        }
        /**
         * Avatar
         ---------------------------*/
        .comments-list .comment-avatar {
            width: 65px;
            height: 65px;
            position: relative;
            z-index: 99;
            float: left;
            border: 3px solid #FFF;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            overflow: hidden;
        }

        .comments-list .comment-avatar img {
            width: 100%;
            height: 100%;
        }

        .reply-list .comment-avatar {
            width: 50px;
            height: 50px;
        }

        .comment-main-level:after {
            content: '';
            width: 0;
            height: 0;
            display: block;
            clear: both;
        }
        /**
         * Caja del Comentario
         ---------------------------*/
        .comments-list .comment-box {
            width: 680px;
            float: right;
            position: relative;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
            -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
            box-shadow: 0 1px 1px rgba(0,0,0,0.15);
        }

        .comments-list .comment-box:before, .comments-list .comment-box:after {
            content: '';
            height: 0;
            width: 0;
            position: absolute;
            display: block;
            border-width: 10px 12px 10px 0;
            border-style: solid;
            border-color: transparent #FCFCFC;
            top: 8px;
            left: -11px;
        }

        .comments-list .comment-box:before {
            border-width: 11px 13px 11px 0;
            border-color: transparent rgba(0,0,0,0.05);
            left: -12px;
        }

        .reply-list .comment-box {
            width: 610px;
        }
        .comment-box .comment-head {
            background: #FCFCFC;
            padding: 10px 12px;
            border-bottom: 1px solid #E5E5E5;
            overflow: hidden;
            -webkit-border-radius: 4px 4px 0 0;
            -moz-border-radius: 4px 4px 0 0;
            border-radius: 4px 4px 0 0;
        }

        .comment-box .comment-head i {
            float: right;
            margin-left: 14px;
            position: relative;
            top: 2px;
            color: #A6A6A6;
            cursor: pointer;
            -webkit-transition: color 0.3s ease;
            -o-transition: color 0.3s ease;
            transition: color 0.3s ease;
        }

        .comment-box .comment-head i:hover {
            color: #03658c;
        }

        .comment-box .comment-name {
            color: #283035;
            font-size: 14px;
            font-weight: 700;
            float: left;
            margin-right: 10px;
        }

        .comment-box .comment-name a {
            color: #283035;
        }

        .comment-box .comment-head span {
            float: left;
            color: #999;
            font-size: 13px;
            position: relative;
            top: 1px;
        }

        .comment-box .comment-content {
            background: #FFF;
            padding: 12px;
            font-size: 15px;
            color: #595959;
            -webkit-border-radius: 0 0 4px 4px;
            -moz-border-radius: 0 0 4px 4px;
            border-radius: 0 0 4px 4px;
        }

        .comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {color: #03658c;}
        .comment-box .comment-name.by-author:after {
            content: 'autor';
            background: #03658c;
            color: #FFF;
            font-size: 12px;
            padding: 3px 5px;
            font-weight: 700;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        /** =====================
         * Responsive
         ========================*/
        @media only screen and (max-width: 766px) {
            .comments-container {
                width: 480px;
            }

            .comments-list .comment-box {
                width: 390px;
            }

            .reply-list .comment-box {
                width: 320px;
            }
        }

        .widget-area {
            background-color: #fff;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
            box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
            float: left;
            margin-top: 30px;
            padding: 25px 30px;
            position: relative;
            width: 100%;
            }
            .status-upload {
            background: none repeat scroll 0 0 #f5f5f5;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            float: left;
            width: 100%;
            }
            .status-upload form {
            float: left;
            width: 100%;
            }
            .status-upload form textarea {
            background: none repeat scroll 0 0 #fff;
            border: medium none;
            -webkit-border-radius: 4px 4px 0 0;
            -moz-border-radius: 4px 4px 0 0;
            -ms-border-radius: 4px 4px 0 0;
            -o-border-radius: 4px 4px 0 0;
            border-radius: 4px 4px 0 0;
            color: #777777;
            float: left;
            font-family: Lato;
            font-size: 14px;
            height: 142px;
            letter-spacing: 0.3px;
            padding: 20px;
            width: 100%;
            resize:vertical;
            outline:none;
            border: 1px solid #F2F2F2;
            }

            .status-upload ul {
            float: left;
            list-style: none outside none;
            margin: 0;
            padding: 0 0 0 15px;
            width: auto;
            }
            .status-upload ul > li {
            float: left;
            }
            .status-upload ul > li > a {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            color: #777777;
            float: left;
            font-size: 14px;
            height: 30px;
            line-height: 30px;
            margin: 10px 0 10px 10px;
            text-align: center;
            -webkit-transition: all 0.4s ease 0s;
            -moz-transition: all 0.4s ease 0s;
            -ms-transition: all 0.4s ease 0s;
            -o-transition: all 0.4s ease 0s;
            transition: all 0.4s ease 0s;
            width: 30px;
            cursor: pointer;
            }
            .status-upload ul > li > a:hover {
            background: none repeat scroll 0 0 #606060;
            color: #fff;
            }
            .status-upload form button {
            border: medium none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            color: #fff;
            float: right;
            font-family: Lato;
            font-size: 14px;
            letter-spacing: 0.3px;
            margin-right: 9px;
            margin-top: 9px;
            padding: 6px 15px;
            }
            .dropdown > a > span.green:before {
            border-left-color: #2dcb73;
            }
            .status-upload form button > i {
            margin-right: 7px;
            }

</style>
 <div class="row">
    <div class="col-md-6">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $addons_detail['name']; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
            <div class="box-body">
               
                 <img class="img-responsive" src="<?php ($addons_detail['cover']!="")?$cover=base_url().'repo/vendor/'.$addons_detail['slug'].'/'.$addons_detail['cover']:$cover="http://placehold.it/400x250/000/fff";echo $cover; ?>" alt="" style="width: 100%"/>
                 <div class="rev-addon well">
                    <div class="pull-left">
                        <div class="stars-default" data-rating="<?php echo $addons_detail['rate']; ?>">
                            <input type=hidden name="rating"/>
                       </div>
                       <span class="user-install">
                       <i class="fa fa-download"></i>&nbsp; 
                        <?php 
                            if($addons_detail['install_count']!=""){
                                echo $addons_detail['install_count'];
                            }else{
                                echo "0";
                            } 
                        ?>
                       </span>
                    </div>

                    <div class="form-group pull-right" style="margin-left:20px;">
                        <select onchange="updateStatus(<?php echo $addons_detail['module_id']; ?>)" id="module_status" class="form-control">
                            <option value="0" <?php ($addons_detail['status']=="0")?$attr="selected":$attr="";echo $attr; ?>>Draft</option>
                            <option value="1" <?php ($addons_detail['status']=="1")?$attr="selected":$attr="";echo $attr; ?>>Editor Review</option>
                            <option value="2" <?php ($addons_detail['status']=="2")?$attr="selected":$attr="";echo $attr; ?>>Publish</option>
                        </select>
                    </div>

                    <a  class="btn btn-default pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Downloading..." href="<?php echo base_url();?>repo/vendor/<?php echo $addons_detail['slug'];?>/<?php echo $addons_detail['file'];?>"><i class="fa fa-cloud-download"></i> Download</a>
                 </div>
                 <div class="bs-component">
                  <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                    <li class="active">
                        <a href="#description" data-toggle="tab" aria-expanded="true">
                            Description<div class="ripple-container"></div>
                        </a>
                         
                    </li>
                    <li>
                        <a href="#reviews" data-toggle="tab">
                            Reviews<div class="ripple-container"></div>
                        </a>
                    </li>
                   
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="description">
                        <p><?php echo $addons_detail['description']; ?></p>
                    </div>
                    <div class="tab-pane fade" id="reviews">
                      

                        <div class="row">
                        <!-- Contenedor Principal -->
                        <div class="comments-container">
                           

                            <ul id="comments-list" class="comments-list">
                                <li>
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" alt=""></div>
                                        <!-- Contenedor del Comentario -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">Agustin Ortiz</a></h6>
                                                <span>20 menit yang lalu</span>
                                                <i class="fa fa-reply"></i>
                                       
                                            </div>
                                            <div class="comment-content">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Respuestas de los comentarios -->
                                    <ul class="comments-list reply-list">
                                        <li>
                                            <!-- Avatar -->
                                            <div class="comment-avatar"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" alt=""></div>
                                            <!-- Contenedor del Comentario -->
                                            <div class="comment-box">
                                                <div class="comment-head">
                                                    <h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>
                                                    <span>10 menit yang lalu</span>
                                                   
                                                   
                                                </div>
                                                <div class="comment-content">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <!-- Avatar -->
                                            <div class="comment-avatar"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" alt=""></div>
                                            <!-- Contenedor del Comentario -->
                                            <div class="comment-box">
                                                <div class="comment-head">
                                                    <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">Agustin Ortiz</a></h6>
                                                    <span>10 menit yang lalu</span>
                                                   
                                                    
                                                </div>
                                                <div class="comment-content">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" alt=""></div>
                                        <!-- Contenedor del Comentario -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                <h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>
                                                <span>10 menit yang lalu</span>
                                                <i class="fa fa-reply"></i>
                                               
                                            </div>
                                            <div class="comment-content">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        </div>
                       
                
                        <div class="widget-area no-padding blank">
                            <div class="status-upload">
                                <form>
                                    <textarea placeholder="Tell us about this module?" ></textarea>
                                    
                                    <button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Submit</button>
                                </form>
                            </div><!-- Status Upload  -->
                        </div><!-- Widget Area -->
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Optional Info</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <div class="form-group">
                  <label for="video_url">Video URL</label>
                  
                  <div class="img-prev" id="youtube_preview" style="text-align: center;">
                    <?php 
                      if($addons_detail['video_url']!=""){
                        echo $addons_detail['video_url'];
                      }else{
                        echo '<h1>You Tube Video Preview</h1>';
                      }
                    ?>
                    
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="ss1">Screenshot 1</label>
                  
                  <div class="img-prev" id="ss1_preview">
                  
                    <?php 
                    if($addons_detail['ss1'] != ""){

                      $upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.$addons_detail['slug'];
                      $file = $upload_path.'/'.$addons_detail['ss1'];

                      if(file_exists($file)){

                        $path = base_url().'repo/vendor/'.$addons_detail['slug'].'/'.$addons_detail['ss1'];

                        echo "<img src='".$path."' class='img-responsive' />";

                      }else{

                        echo "<h1>Screenshot 1</h1>";

                      }

                    }else{ ?>
                    
                    <h1>Screenshot 1</h1>

                  <?php } ?>   

                  </div>
                </div>

                <div class="form-group">
                  <label for="ss1">Screenshot 2</label>
                  
                  <div class="img-prev" id="ss2_preview">
                    
                    <?php 
                    if($addons_detail['ss2'] != ""){

                      $upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.$addons_detail['slug'];
                      $file = $upload_path.'/'.$addons_detail['ss2'];

                      if(file_exists($file)){

                        $path = base_url().'repo/vendor/'.$addons_detail['slug'].'/'.$addons_detail['ss2'];

                        echo "<img src='".$path."' class='img-responsive' />";

                      }else{

                        echo "<h1>Screenshot 2</h1>";

                      }

                    }else{ ?>
                    
                    <h1>Screenshot 2</h1>

                  <?php } ?>   


                  </div>
                </div>

                <div class="form-group">
                  <label for="ss1">Screenshot 3</label>
                 
                  <div class="img-prev" id="ss3_preview">
                    
                    <?php 
                    if($addons_detail['ss3'] != ""){

                      $upload_path = $_SERVER['DOCUMENT_ROOT'].'/portal/repo/vendor/'.$addons_detail['slug'];
                      $file = $upload_path.'/'.$addons_detail['ss3'];

                      if(file_exists($file)){

                        $path = base_url().'repo/vendor/'.$addons_detail['slug'].'/'.$addons_detail['ss3'];

                        echo "<img src='".$path."' class='img-responsive' />";

                      }else{

                        echo "<h1>Screenshot 3</h1>";

                      }

                    }else{ ?>
                    
                    <h1>Screenshot 3</h1>

                  <?php } ?>   

                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            
          </div> 
    </div>
</div>

<script type="text/javascript">
    (function ( $ ) {
 
        $.fn.rating = function( method, options ) {
            method = method || 'create';
            // This is the easiest way to have default options.
            var settings = $.extend({
                // These are the defaults.
                limit: 5,
                value: 0,
                glyph: "glyphicon-star",
                coloroff: "gray",
                coloron: "gold",
                size: "2.0em",
                cursor: "default",
                onClick: function () {},
                endofarray: "idontmatter"
            }, options );
            var style = "";
            style = style + "font-size:" + settings.size + "; ";
            style = style + "color:" + settings.coloroff + "; ";
            style = style + "cursor:" + settings.cursor + "; ";
        

            
            if (method == 'create')
            {
                //this.html('');    //junk whatever was there
                
                //initialize the data-rating property
                this.each(function(){
                    attr = $(this).attr('data-rating');
                    if (attr === undefined || attr === false) { $(this).attr('data-rating',settings.value); }
                })
                
                //bolt in the glyphs
                for (var i = 0; i < settings.limit; i++)
                {
                    this.append('<span data-value="' + (i+1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
                }
                
                //paint
                this.each(function() { paint($(this)); });

            }
            if (method == 'set')
            {
                this.attr('data-rating',options);
                this.each(function() { paint($(this)); });
            }
            if (method == 'get')
            {
                return this.attr('data-rating');
            }
            //register the click events
            this.find("span.ratingicon").click(function() {
                rating = $(this).attr('data-value')
                $(this).parent().attr('data-rating',rating);
                paint($(this).parent());
                settings.onClick.call( $(this).parent() );
            })
            function paint(div)
            {
                rating = parseInt(div.attr('data-rating'));
                div.find("input").val(rating);  //if there is an input in the div lets set it's value
                div.find("span.ratingicon").each(function(){    //now paint the stars
                    
                    var rating = parseInt($(this).parent().attr('data-rating'));
                    var value = parseInt($(this).attr('data-value'));
                    if (value > rating) { $(this).css('color',settings.coloroff); }
                    else { $(this).css('color',settings.coloron); }
                })
            }

        };
     
    }( jQuery ));

    $(document).ready(function(){
         $(".stars-default").rating();
    });
    function updateStatus(id){

        var status = $("#module_status").val();
        var data = { status : status, id : id }
        var target = '<?php echo base_url(); ?>addons/update_addons_status';

        $.post(target, data, function(res){

            toastr.success(res.msg, 'Response Server');

        },'json');



    }
</script>