<?php

if($edit_perm == "1"){
    $disabled_edit = "";
}else{
    $disabled_edit = "disabled";
}


if($add_perm == "1"){
    $disabled_add = "";
}else{
    $disabled_add = "disabled";
}


if($edit_perm == "1"){
    $disabled_delete = "";
}else{
    $disabled_delete = "disabled";
}
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />

<style>
    .mjs-nestedSortable-error {
        background: #fbe3e4;
        border-color: transparent;
    }

    #tree {
        width: 550px;
        margin: 0;
    }

    ol {
        max-width: 450px;
        padding-left: 25px;
    }

    ol.sortable,ol.sortable ol {
        list-style-type: none;
    }

    .sortable li div {
        border: 1px solid #d4d4d4;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        cursor: move;
        border-color: #D4D4D4 #D4D4D4 #BCBCBC;
        margin: 0;
        padding: 3px;
    }

    li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
        border-color: #999;
    }

    .disclose, .expandEditor {
        cursor: pointer;
        width: 20px;
        display: none;
    }

    .sortable li.mjs-nestedSortable-collapsed > ol {
        display: none;
    }

    .sortable li.mjs-nestedSortable-branch > div > .disclose {
        display: inline-block;
    }

    .sortable span.ui-icon {
        display: inline-block;
        margin: 0;
        padding: 0;
    }

    .menuDiv {
        background: #EBEBEB;
    }

    .menuEdit {
        background: #FFF;
    }

    .itemTitle {
        vertical-align: middle;
        cursor: pointer;
    }

    .deleteMenu {
        float: right;
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Menus Position</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">

                <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
                    <?php foreach($menus as $mn){ ?>

                        <?php
                            if($mn['parent']==""){

                                $childs = getMenuChilds($mn['menu_id']);
                        ?>


                            <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $mn['menu_id']; ?>" data-foo="bar">
                                <div class="menuDiv">
                                    <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
                                        <span></span>
                                    </span>
                                        <span title="Click to show/hide item editor" data-id="<?php echo $mn['menu_id']; ?>" class="expandEditor ui-icon ui-icon-triangle-1-n">
                                        <span></span>
                                    </span>
                                    <span>
                                       <span data-id="<?php echo $mn['menu_id']; ?>" class="itemTitle"><?php echo $mn['name']; ?></span>
                                    </span>
                                    <div id="menuEdit<?php echo $mn['menu_id']; ?>" class="menuEdit">
                                        <p>
                                            <i class="fa <?php echo $mn['fa_icon']; ?>"></i>&nbsp;&nbsp;<span><?php echo $mn['display_name']; ?></span>
                                            <a <?php echo $disabled_edit; ?> href="javascript:editMenu('<?php echo $mn['menu_id']; ?>');" class="<?php echo $disabled_edit; ?> btn btn-sm btn-primary pull-right"><i class="fa fa-pencil"></i></a>
                                            <a <?php echo $disabled_delete; ?> href="javascript:deleteMenu('<?php echo $mn['menu_id']; ?>');" class="<?php echo $disabled_delete; ?> btn btn-sm btn-danger pull-right" style="margin-right: 5px;"><i class="fa fa-remove"></i></a>

                                            <form id="frm_menu_edit_<?php echo $mn['menu_id']; ?>" style="padding-top: 10px; display: none">
                                                <input type="hidden" name="menu_id" value="<?php echo $mn['menu_id']; ?>">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" reqired name="name" class="form-control" id="name" placeholder="Name" value="<?php echo $mn['name']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="slug">Slug</label>
                                                    <input type="text" readonly name="slug" class="form-control" id="slug" value="<?php echo $mn['slug']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Display Name</label>
                                                    <input type="text" reqired name="display_name" class="form-control" id="display_name" placeholder="Display Name" value="<?php echo $mn['display_name']; ?>">
                                                </div>

                                                <div class="form-group">
                                                    <label for="name">Font Awesome Icon</label>
                                                    <input type="text" reqired name="fa_icon" class="form-control" id="fa_icon" placeholder="fa-icon " value="<?php echo $mn['fa_icon']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Path/URL</label>
                                                    <input type="text" reqired name="path" class="form-control" id="path" placeholder="/url/to/module " value="<?php echo $mn['path']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="permission">Permission</label>
                                                    <select class="form-control" name="permission" id="permission">
                                                        <?php

                                                        foreach ($permission as $prm){
                                                            if($prm['id']==$mn['permission']){

                                                                echo " <option selected  value='".$prm['id']."'> ".$prm['name']." </option>";

                                                            }else{

                                                                echo " <option value='".$prm['id']."'> ".$prm['name']." </option>";

                                                            }

                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="group">Status</label>
                                                    <select class="form-control" name="status" id="status">
                                                        <option value="1"> Active </option>
                                                        <option value="0"> InActive </option>
                                                    </select>
                                                </div>
                                                <div class="box-footer">
                                                    <button type="button" <?php echo $disabled_edit; ?> onclick="updateMenu('<?php echo $mn['menu_id']; ?>');" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </p>
                                    </div>
                                </div>
                                <?php if(count($childs)>0){ ?>

                                    <ol>
                                        <?php foreach ($childs as $ch){ ?>
                                        <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $ch['menu_id']; ?>" data-foo="bar">

                                            <div class="menuDiv">
                                                <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
                                                    <span></span>
                                                </span>
                                                <span title="Click to show/hide item editor" data-id="<?php echo $ch['menu_id']; ?>" class="expandEditor ui-icon ui-icon-triangle-1-n">
                                                    <span></span>
                                                </span>
                                                <span>

                                                    <span data-id="<?php echo $ch['menu_id']; ?>" class="itemTitle"><?php echo $ch['name']; ?></span>

                                                </span>
                                                <div id="menuEdit<?php echo $ch['menu_id']; ?>" class="menuEdit">
                                                    <p>
                                                        <i class="fa <?php echo $ch['fa_icon']; ?>"></i>&nbsp;&nbsp;<span><?php echo $ch['display_name']; ?></span>
                                                        <a <?php echo $disabled_edit; ?> href="javascript:editMenu('<?php echo $ch['menu_id']; ?>');" class="<?php echo $disabled_edit; ?> btn btn-sm btn-primary pull-right"><i class="fa fa-pencil"></i></a>
                                                        <a <?php echo $disabled_delete; ?> href="javascript:deleteMenu('<?php echo $ch['menu_id']; ?>');" class="<?php echo $disabled_delete; ?> btn btn-sm btn-danger pull-right" style="margin-right: 5px;"><i class="fa fa-remove"></i></a>

                                                        <form id="frm_menu_edit_<?php echo $ch['menu_id']; ?>" style="padding-top: 10px; display: none">
                                                            <input type="hidden" name="menu_id" value="<?php echo $ch['menu_id']; ?>">
                                                            <div class="form-group">
                                                                <label for="name">Name</label>
                                                                <input type="text" reqired name="name" class="form-control" id="name" placeholder="Name" value="<?php echo $ch['name']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="slug">Slug</label>
                                                                <input type="text" readonly name="slug" class="form-control" id="slug" value="<?php echo $ch['slug']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name">Display Name</label>
                                                                <input type="text" reqired name="display_name" class="form-control" id="display_name" placeholder="Display Name" value="<?php echo $ch['display_name']; ?>">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="name">Font Awesome Icon</label>
                                                                <input type="text" reqired name="fa_icon" class="form-control" id="fa_icon" placeholder="fa-icon " value="<?php echo $ch['fa_icon']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name">Path/URL</label>
                                                                <input type="text" reqired name="path" class="form-control" id="path" placeholder="/url/to/module " value="<?php echo $ch['path']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="permission">Permission</label>
                                                                <select class="form-control" name="permission" id="permission">
                                                                    <?php

                                                                    foreach ($permission as $prm){
                                                                        if($prm['id']==$ch['permission']){

                                                                            echo " <option selected  value='".$prm['id']."'> ".$prm['name']." </option>";

                                                                        }else{

                                                                            echo " <option value='".$prm['id']."'> ".$prm['name']." </option>";

                                                                        }

                                                                    }

                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="group">Status</label>
                                                                <select class="form-control" name="status" id="status">
                                                                    <option value="1"> Active </option>
                                                                    <option value="0"> InActive </option>
                                                                </select>
                                                            </div>
                                                            <div class="box-footer">
                                                                <button type="button" <?php echo $disabled_edit; ?> onclick="updateMenu('<?php echo $ch['menu_id']; ?>');" class="btn btn-primary">Update</button>
                                                            </div>
                                                        </form>
                                                    </p>
                                                </div>
                                            </div>

                                        </li>
                                        <?php } ?>
                                    </ol>

                                <?php } ?>
                            </li>
                        <?php } ?>




                    <?php } ?>

                </ol>

                <div class="box-footer">
                    <button id="serialize" type="button" <?php echo $disabled_edit; ?> class="btn btn-primary">Save Config</button>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Create Menu</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frm-menu">
            <div class="box-body">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" reqired name="name" class="form-control" id="name" placeholder="Name" value="">
                </div>
                <div class="form-group">
                    <label for="name">Display Name</label>
                    <input type="text" reqired name="display_name" class="form-control" id="display_name" placeholder="Display Name" value="">
                </div>
                <div class="form-group">
                    <label for="group">Parent</label>
                    <select class="form-control" name="parent" id="parent">
                        <option value=""> - </option>
                        <?php foreach($menus as $m){ ?>
                            <option value="<?php echo $m['menu_id']; ?>"> <?php echo $m['name']; ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Font Awesome Icon</label>
                    <input type="text" reqired name="fa_icon" class="form-control" id="fa_icon" placeholder="fa-icon " value="">
                </div>
                <div class="form-group">
                    <label for="name">Path/URL</label>
                    <input type="text" reqired name="path" class="form-control" id="fa_icon" placeholder="/url/to/module " value="">
                </div>
                <div class="form-group">
                    <label for="name">Slug</label>
                    <input type="text" reqired name="slug" class="form-control" id="fa_icon" placeholder="Slug" value="">
                </div>
                <div class="form-group">
                    <label for="permission">Permission</label>
                    <select class="form-control" name="permission" id="permission">
                        <?php

                            foreach ($permission as $prm){

                                echo " <option value='".$prm['id']."'> ".$prm['name']." </option>";

                            }

                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="group">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="1"> Active </option>
                        <option value="0"> InActive </option>
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" <?php echo $disabled_add; ?> class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        var ns = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false,
            change: function(){
                console.log('Relocated item');
            }
        });

        $('.expandEditor').attr('title','Click to show/hide item editor');
        $('.disclose').attr('title','Click to show/hide children');
        $('.deleteMenu').attr('title', 'Click to delete item.');

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
        });

        $('.expandEditor, .itemTitle').click(function(){
            var id = $(this).attr('data-id');
            $('#menuEdit'+id).toggle();
            $(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
        });

        $('.deleteMenu').click(function(){
            var id = $(this).attr('data-id');
            $('#menuItem_'+id).remove();
        });

        $('#serialize').click(function(){
            var serialized = $('ol.sortable').nestedSortable('serialize');
            var target = '<?php echo base_url(); ?>dashboard/rearrange_menu_position';

            $.post(target, serialized, function(res){

                if(res.status=="1"){
                    toastr.success(res.msg, 'Response Server');
                }else{
                    toastr.error(res.msg, 'Response Server');
                }

            },'json');

        })

        $('#toHierarchy').click(function(e){
            hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
            hiered = dump(hiered);
            (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
                $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
        })

        $('#toArray').click(function(e){
            arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
            arraied = dump(arraied);
            (typeof($('#toArrayOutput')[0].textContent) != 'undefined') ?
                $('#toArrayOutput')[0].textContent = arraied : $('#toArrayOutput')[0].innerText = arraied;
        });

        $("#frm-menu").submit(function(){
            var data = $(this).serialize();
            var target = '<?php echo base_url(); ?>dashboard/insert_menu';

            $.post(target, data, function(res){

                if(res.status=="1"){

                    toastr.success(res.msg, 'Response Server');

                    setTimeout('window.location.reload();',3000);

                }else{

                    toastr.error(res.msg, 'Response Server');

                }

            },'json');

            return false;

        });
        $('.disabled').click(function(e){
            e.preventDefault();
        });
    });
    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function editMenu(id){

        $("#frm_menu_edit_"+id).slideToggle("slow",function(){

        });

    }

    function updateMenu(id){

        var data = $("#frm_menu_edit_"+id).serialize();
        var target = '<?php echo base_url(); ?>dashboard/update_menu';

        $.post(target, data, function(res){

            if(res.status=="1"){

                toastr.success(res.msg, 'Response Server');

                setTimeout('window.location.reload();',3000);

            }else{

                toastr.error(res.msg, 'Response Server');

            }

        },'json');

    }

    function deleteMenu(id){

        var data = { menu_id : id }
        var target = '<?php echo base_url(); ?>dashboard/delete_menu';

        $.post(target, data, function(res){

            if(res.status=="1"){

                toastr.success(res.msg, 'Response Server');

                setTimeout('window.location.reload();',3000);

            }else{

                toastr.error(res.msg, 'Response Server');

            }

        },'json');

    }

</script>