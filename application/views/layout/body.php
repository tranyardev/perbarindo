<?php
  $app['base_url'] = base_url();
  $app['assetdir'] = "assets";
  $app['skin'] = getActiveSkin();

  if($app['skin']==''){
      $app['skin'] = 'skin-purple';
  }

  /*
   * skin-black, skin-black-light, skin-blue,
   * skin-blue-light, skin-green, skin-green-light,
   * skin-purple, skin-purple-light, skin-red,
   * skin-red-light, skin-yellow, skin-yellow-light
  */
  $app['title'] = (isset($page_title)) ? "PERBARINDO | ".$page_title : "PERBARINDO | Admin";
  $app['meta'] = array(
      "keywords" => "PERBARINDO",
      "description" => "PERBARINDO" ,
      "author" => "PERBARINDO"
  );
  $app['css'] = array(
      "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
      "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
      "ionicons" => $app['base_url'].$app['assetdir'].'/bower_components/Ionicons/css/ionicons.min.css',
      "skin" => $app['base_url'].$app['assetdir'].'/dist/css/skins/'.$app['skin'].'.min.css',
      "adminlte" => $app['base_url'].$app['assetdir'].'/dist/css/AdminLTE.css',
      "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/flat/blue.css',
      "morris" => $app['base_url'].$app['assetdir'].'/plugins/morris/morris.css',
      "jvectormap" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
      "timepicker" => $app['base_url'].$app['assetdir'].'/plugins/timepicker/bootstrap-timepicker.css',
      "datepicker" => $app['base_url'].$app['assetdir'].'/plugins/datepicker/datepicker3.css',
      "daterangepicker" => $app['base_url'].$app['assetdir'].'/plugins/daterangepicker/daterangepicker.css',
      "tags-input" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-tags-input/bootstrap-tagsinput.css',
      "intro-js" => $app['base_url'].$app['assetdir'].'/plugins/intro-js/introjs.min.css',
      // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.css',
      "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.css',
      "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.css',
      "bootstrap-wysihtml5" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
      "select2" => $app['base_url'].$app['assetdir'].'/plugins/select2/select2.min.css',
      "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/css/jquery.jexcel.css',
      "datatable" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
      "datatable-button" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/css/buttons.dataTables.min.css',
      "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.css',
      "duallistbox" => $app['base_url'].$app['assetdir'].'/plugins/duallistbox/bootstrap-duallistbox.min.css',
      "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/css/validationEngine.jquery.css',
      "custom" => $app['base_url'].$app['assetdir'].'/dist/css/custom.css',

  );
  $app['header_js'] = array(
      "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
      "jquery-ui" => $app['base_url'].$app['assetdir'].'/dist/js/ui/1.11.4/jquery-ui.min.js',
  );
  $app['footer_js'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    "raphael" => $app['base_url'].$app['assetdir'].'/dist/js/raphael-min.js',
    "morris" => $app['base_url'].$app['assetdir'].'/plugins/morris/morris.min.js',
    "sparkline" => $app['base_url'].$app['assetdir'].'/plugins/sparkline/jquery.sparkline.min.js',
    "jvectormap" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
    "jvectormap_world" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
    "knob" => $app['base_url'].$app['assetdir'].'/plugins/knob/jquery.knob.js',
    "moment" => $app['base_url'].$app['assetdir'].'/dist/js/moment.min.js',
    "daterangepicker" => $app['base_url'].$app['assetdir'].'/plugins/daterangepicker/daterangepicker.js',
    "timepicker" => $app['base_url'].$app['assetdir'].'/plugins/timepicker/bootstrap-timepicker.js',
    "datepicker" => $app['base_url'].$app['assetdir'].'/plugins/datepicker/bootstrap-datepicker.js',
    "tags-input" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js',
    "intro-js" => $app['base_url'].$app['assetdir'].'/plugins/intro-js/intro.min.js',
    // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.min.js',
    "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.js',
    "bootstrap-wysihtml5" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
    "slimScroll" => $app['base_url'].$app['assetdir'].'/plugins/slimScroll/jquery.slimscroll.min.js',
    "fastclick" => $app['base_url'].$app['assetdir'].'/plugins/fastclick/fastclick.js',
    "select2" => $app['base_url'].$app['assetdir'].'/plugins/select2/select2.full.min.js',
    "app" => $app['base_url'].$app['assetdir'].'/dist/js/app.js',
    "datatable" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net/js/jquery.dataTables.min.js',
    "datatable-bs" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
    "datatable-button" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/dataTables.buttons.min.js',
    "datatable-button-print" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.print.min.js',
    "datatable-button-html5" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.html5.min.js',
    "datatable-button-flash" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.flash.min.js',
    "datatable-button-jszip" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/jszip.min.js',
    "datatable-button-pdfmake" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/pdfmake.min.js',
    "datatable-button-vfs-fonts" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/vfs_fonts.js',
    "datatable-button-col-visible" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.colVis.min.js',
    "datatable-fixedcolumn" => 'https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js',
    "numeral" => $app['base_url'].$app['assetdir'].'/plugins/numeral/numeral.min.js',
    "pdfobject" => $app['base_url'].$app['assetdir'].'/plugins/PDFObject/pdfobject.min.js',
    "jcsv" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.csv.min.js',
    "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.jexcel.js',
    "canvasjs" => $app['base_url'].$app['assetdir'].'/plugins/canvasjs/canvasjs.min.js',
    "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.js',
    "jqplot-pieRenderer" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/plugins/jqplot.pieRenderer.js',
    "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.js',
    "duallistbox" => $app['base_url'].$app['assetdir'].'/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js',
    "nestedsortable" => $app['base_url'].$app['assetdir'].'/plugins/nestedsortablejs/jquery.mjs.nestedSortable.js',
    "jquery_validation_engine_en" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/js/languages/jquery.validationEngine-en.js',
    "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/js/jquery.validationEngine.js',
    "ckeditor" => $app['base_url'].$app['assetdir'].'/plugins/ckeditor/ckeditor.js',
    "ckfinder" => $app['base_url'].$app['assetdir'].'/plugins/ckfinder/ckfinder.js',
    "images-loaded" => $app['base_url'].$app['assetdir'].'/plugins/image-fill/js/imagesloaded.pkgd.min.js',
    "image-fill" => $app['base_url'].$app['assetdir'].'/plugins/image-fill/js/jquery-imagefill.js',
    "image-scale" => $app['base_url'].$app['assetdir'].'/plugins/image-scale/image-scale.min.js',
    "cinta" => $app['base_url'].$app['assetdir'].'/dist/js/cinta.js',
    "layout" => $app['base_url'].$app['assetdir'].'/dist/js/layout.js',
    "dashboard-js" => $app['base_url'].$app['assetdir'].'/dist/js/pages/dashboard.js',
  );
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="http://perbarindo.tranyar.com/public/assets/img/logo.png" type="image/gif" sizes="16x16">
    <?php
        /* generate metatag */
        $meta_key = array_keys($app['meta']);
        for($i=0;$i < count($app['meta']);$i++){
            echo '<meta name="'.$meta_key[$i].'" content="'.$app['meta'][$meta_key[$i]].'">';
        }
        /* generate css */
        $css_key = array_keys($app['css']);
        for($i=0;$i < count($app['css']);$i++){
            echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
        }

        /* generate custom css */

        if(isset($custom_css)){
            if(count($custom_css)>0){
                $custom_css_key = array_keys($custom_css);
                for($i=0;$i < count($custom_css);$i++){
                    echo '<link rel="stylesheet" href="'.$custom_css[$custom_css_key[$i]].'">';
                }
            }
        }

        /* generate js */
        $header_js_key = array_keys($app['header_js']);
        for($i=0;$i < count($app['header_js']);$i++){
            echo '<script src="'.$app['header_js'][$header_js_key[$i]].'"></script>';
        }

    ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css">
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var ckfinder_config = {
            filebrowserBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        }
    </script>

</head>
<body class="hold-transition <?php echo $app['skin']; ?> sidebar-mini">
<div class="wrapper">

    <?php $this->load->view('parts/header'); ?>
    <?php $this->load->view('parts/sidebar',@$data); ?>

    <div class="content-wrapper" style="min-height: 900px;">
        <section class="content-header">
            <h1>
                <?php (isset($page_title)) ? $str = $page_title: $str = "Page Title Not Set"; echo $str; ?>
                <small><?php (isset($page_subtitle)) ? $str = $page_subtitle: $str = "Page Sub Title Not Set"; echo $str; ?></small>
            </h1>
            <ol class="breadcrumb">
                <?php (isset($breadcrumb)) ? $str = $breadcrumb: $str = ""; echo $str; ?>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php
                if(isset($page)) {
                    $this->load->view($page, @$data);
                }else if(isset($html)){
                    echo $html;
                }
             ?>

        </section>
    </div>

    <?php $this->load->view('parts/footer'); ?>
    <?php $this->load->view('parts/sidebar_control'); ?>

    <div class="control-sidebar-bg"></div>
</div>
<div class="modal" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-key"></i> Change Password</h4>
            </div>
            <div class="modal-body">
                <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Process</h4>
                    Authenticating... Please wait!
                </div>
                <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                    Invalid email or password. Please check your email or password!
                </div>
                <form id="frm_change_pass">
                    <div class="form-group has-feedback">
                        <input type="password" name="old_password" id="old_password" class="validate[required]  form-control" placeholder="Old Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="new_password" id="new_password" class="validate[required] form-control" placeholder="New Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="confirm_password" id="confirm_password" class="validate[required,equals[new_password]] form-control" placeholder="Confirm Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_update_pass" onclick="update_password()">Update</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal" id="edit_profile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-user"></i> Edit Profile</h4>
            </div>
            <div class="modal-body">
                <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Process</h4>
                    Authenticating... Please wait!
                </div>
                <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                    Invalid email or password. Please check your email or password!
                </div>
                <form id="frm_edit_profile">
                    <input type="hidden" name="old_pp" value="<?php echo $this->session->userdata('picture'); ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <div class="img-prev" id="pp_preview">
                                    <?php
                                    if($this->session->userdata('picture') != ""){

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                                        $file = $upload_path.'/'.$this->session->userdata('picture');

                                        if(file_exists($file)){

                                            $path = base_url().'upload/photo/'.$this->session->userdata('picture');

                                            echo "<img src='".$path."' class='img-responsive' />";

                                        }else{

                                            echo "<h1>Photo Preview</h1>";

                                        }
                                    }else{ ?>

                                        <h1>Photo Preview</h1>

                                    <?php } ?>
                                </div>
                                <br>
                                <input type="file" class="form-control" name="photo" id="pp">

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" required name="first_name" class="validate[required] form-control" id="first_name" value="<?php echo $this->session->userdata('first_name'); ?>" placeholder="First Name (Required)">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" required name="last_name" class="validate[required] form-control" id="last_name" value="<?php echo $this->session->userdata('last_name'); ?>" placeholder="Last Name (Required)">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_update_profile" onclick="update_profile()">Update</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
  <style type="text/css">
      /** COSTUME **/
      .textBody {

      }
     /* #leftBtn i {
        color: #ad9679; margin: 13px; font-size: 32px; float:left;
            position: absolute;
       
      }
      #rightBtn i {
        color: #ad9679; margin-left: 5px; font-size: 32px; float:right;
      } */
      #dttable {
        width: 100%!important;
      }


      #leftBtn  {
          left: 0px; 
          position: absolute; 
          bottom: 55px!important; 
          display: -ms-flexbox;
          display: flex;
          -ms-flex-align: center;
          align-items: center;
          -ms-flex-pack: center;
          justify-content: center;
          width: 15%;
          color: #555;
          text-align: center;
          opacity: .5;
          cursor: pointer;
      }


      #rightBtn  {
        right: 0px; 
          position: absolute; 
           bottom:  55px!important;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-align: center;
          align-items: center;
          -ms-flex-pack: center;
          justify-content: center;
          width: 15%;
          color: #555;
          text-align: center;
          opacity: .5;
          cursor: pointer;
      }

    @media only screen and (max-width:1337px) {
       .pagination {
          font-size: 12px;
        }
    }   

    @media only screen and (max-width:286px) {
       .pagination {
          font-size: 7px;
        }
    }   

    @media only screen and (max-width:338px) {
      .pagination {
            font-size: 7px;
          }
      }   

      .ip span {
          font-size: 14px!important;
          margin-bottom: 4px!important;
      }

      .ip {
          margin-top: 6px!important;
      }
      .main-header .logo .logo-lg {
          width: 100%!important;
          display: block;
      }
    </style>   
<script>
    var current_year = '<?php echo date("Y") ?>';
    $.widget.bridge('uibutton', $.ui.button);
    $(function() {
        $("img.scale").imageScale();

         $("#dttable").wrap("<div  id='textBody' style='overflow:auto;width:100%!important;'><div style='width:1302px;'></div></div>  "); 


         $("<a id='leftBtn' onclick='leftClick();' class='hidden-lg hidden-md visible-sm visible-xs'><i class='fa fa-chevron-left fa-lg' aria-hidden='true'></i> </a><a id='rightBtn' onclick='rightClick();' class='hidden-lg hidden-md visible-sm visible-xs'><i class='fa fa-chevron-right fa-lg' aria-hidden='true'></i> </a>").insertBefore("#textBody"); 

        // $("#dttable").attr("class","table table-bordered table-striped dataTable no-footer table-responsive");
        
        // $( "#leftBtn" ).click(function(){
        //       $('#textBody').scrollTop($('#textBody').scrollLeft()-20);
        // }); 

        //  $( "#rightBtn" ).click(function(){
        //      $('#textBody').scrollTop($('#textBody').scrollRight()+20);;
        //  });  

    });


    function leftClick(){
 

         // $("#textBody").animate({scrollLeft: -900}, 150);
          $('#textBody').scrollLeft($('#textBody').scrollLeft()-20);
        console.log("left");

    }

    function rightClick(){
        
          $('#textBody').scrollLeft($('#textBody').scrollLeft()+20);
        // $("#textBody").animate({scrollLeft: 900}, 150);
        console.log("right");

    }

    function change_password(){
        $('#change_password').modal({
            backdrop: false,
            show: true
        });
    }

    function edit_profile(){
        $('#edit_profile').modal({
            backdrop: false,
            show: true
        });
    }
    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Photo Preview</h1>");
        $(selector).val('');

    }

    function update_password(){

        var target = base_url + 'dashboard/dashboard/update_password';
        var data = $("#frm_change_pass").serialize();

        if($("#frm_change_pass").validationEngine('validate')) {

            showLoadingUpProf("btn_update_pass");

            $.post(target, data, function (res) {

                if (res.status == "1") {

                    toastr.success(res.msg, 'Response Server');

                    setTimeout('window.location.reload();', 3000);

                } else {
                    toastr.error(res.msg, 'Response Server');
                }

                hideLoadingUpProf("btn_update_pass");

            }, 'json');

        }

    }

    function update_profile(){

        var target = base_url + 'dashboard/dashboard/update_profile';
        var formData = new FormData($("#frm_edit_profile")[0]);

        if($("#frm_edit_profile").validationEngine('validate')) {

            showLoadingUpProf("btn_update_profile");

            $.ajax({
                url: target,
                type: 'POST',
                data: formData,
                dataType: "json",
                async: false,
                success: function (data) {

                    if (data.status == "1") {

                        toastr.success(data.msg, 'Response Server');

                        setTimeout('window.location.reload();', 3000);

                    } else {
                        toastr.error(data.msg, 'Response Server');
                    }

                    hideLoadingUpProf("btn_update_profile");

                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
    function showLoadingUpProf(btn_id){
        $("#"+btn_id).button("loading");
    }
    function hideLoadingUpProf(btn_id){
        $("#"+btn_id).button("reset");
    }
</script>
<?php
    /* generate js */
    $footer_js_key = array_keys($app['footer_js']);
    for($i=0;$i < count($app['footer_js']);$i++){
        echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
    }

    /* generate custom js */
    if(isset($custom_js)){
        if(count($custom_js)>0){
            $custom_js_key = array_keys($custom_js);
            for($i=0;$i < count($custom_js);$i++){
                if($custom_js_key[$i]=="googlemaps"){
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'" async defer></script>';
                }else{
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'"></script>';
                }

            }
        }
    }
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
</body>
</html>
