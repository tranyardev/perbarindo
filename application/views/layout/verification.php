<?php
$app['base_url'] = base_url();
$app['assetdir'] = "assets";
$app['title'] = "Tranyar Dev | Verification";
$app['meta'] = array(
    "keywords" => "Tranyar",
    "description" => "Tranyar CMS",
    "author" => "Tranyar Developer"
);
$app['css'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
    "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
    //"ionicons" => $app['base_url'].$app['assetdir'].'/bower_components/ionicons/css/ionicons.min.css',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/square/blue.css',
    "adminlte" => $app['base_url'].$app['assetdir'].'/dist/css/AdminLTE.css',
);
$app['footer_js'] = array(
    "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    //"icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/icheck.min.js',

);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
    /* generate metatag */
    $meta_key = array_keys($app['meta']);
    for($i=0;$i < count($app['meta']);$i++){
        echo '<meta name="'.$meta_key[$i].'" content="'.$app['meta'][$meta_key[$i]].'">';
    }
    /* generate css */
    $css_key = array_keys($app['css']);
    for($i=0;$i < count($app['css']);$i++){
        echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
    }
    ?>
    <style type="text/css">
       
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <!-- <a href="#"><b>Tanyar</b>Developer</a> -->
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Verification</p>
       
       
        <?php if($status==1){ ?>
        <div id="granted" class="alert alert-success alert-dismissible">
            
            <h4><i class="icon fa fa-check"></i> Verifikasi berhasil !</h4>
            <?php echo $response; ?>
        </div>
        <?php }else{ ?>

        <div id="denied" class="alert alert-danger alert-dismissible">
            
            <h4><i class="icon fa fa-ban"></i> Verifikasi gagal !</h4>
            <?php echo $response; ?>
        </div>

        <?php } ?>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php
/* generate js */
$footer_js_key = array_keys($app['footer_js']);
for($i=0;$i < count($app['footer_js']);$i++){
    echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
}
?>
<script>
    $(document).ready(function () {
       
        
    });
</script>
</body>
</html>

