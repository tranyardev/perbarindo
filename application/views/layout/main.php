<?php
  $app['base_url'] = base_url();
  $app['assetdir'] = "public/assets";

  $app['title'] = (isset($page_title)) ? "PERBARINDO | ".$page_title : "PERBARINDO | EVENT";
  $app['meta'] = array(
      "keywords" =>  (isset($page_keyword)) ? $page_keyword : "PERBARINDO",
      "description" => (isset($page_description)) ? $page_description : "PERBARINDO", 
      "author" => (isset($page_author)) ? $page_author : "PERBARINDO",
  );
  $app['css'] = array(

      "google-font" => 'https://fonts.googleapis.com/css?family=Montserrat:400,700,200',
      "font-awesome" => 'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
      "bootstrap" => $app['base_url'].$app['assetdir'].'/css/bootstrap.min.css',
      "ui-kit" => $app['base_url'].$app['assetdir'].'/css/now-ui-kit.css?v=1.1.0',
      "wait-me" => $app['base_url'].$app['assetdir'].'/css/waitMe.min.css',
      "select-2" => $app['base_url'].$app['assetdir'].'/plugins/select2/css/select2.min.css',
      "select-2-bootstrap" => $app['base_url'].$app['assetdir'].'/plugins/select2/css/select2-bootstrap.min.css',
      "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/js/plugins/jquery-validation-engine/css/validationEngine.jquery.css',
      "main" => $app['base_url'].$app['assetdir'].'/css/main.css',
      "custom" => $app['base_url'].$app['assetdir'].'/css/custom.css',
      "media" => $app['base_url'].$app['assetdir'].'/css/media-only.css',
      "media-print" => $app['base_url'].$app['assetdir'].'/css/media-print.css',
      "j-print" => $app['base_url'].$app['assetdir'].'/css/print-preview.css',
      "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.css',
      // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.css',
       "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.css',
      "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.css',
      "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/css/jquery.jexcel.css',
     

  );
  $app['header_js'] = array(
      // "gmaps" => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places',
      "jquery" => $app['base_url'].$app['assetdir'].'/js/core/jquery.3.2.1.min.js',
      // "jquery-ui" => $app['base_url'].$app['assetdir'].'/dist/js/ui/1.11.4/jquery-ui.min.js',
  );
  $app['footer_js'] = array(

    "jquery" => $app['base_url'].$app['assetdir'].'/js/core/jquery.3.2.1.min.js',
    "popper" => $app['base_url'].$app['assetdir'].'/js/core/popper.min.js',
    "bootstrap" => $app['base_url'].$app['assetdir'].'/js/core/bootstrap.min.js',
    "bootstrap-switch" => $app['base_url'].$app['assetdir'].'/js/plugins/bootstrap-switch.js',
    "nouislider" => $app['base_url'].$app['assetdir'].'/js/plugins/nouislider.min.js',
    "bootstrap-datepicker" => $app['base_url'].$app['assetdir'].'/js/plugins/bootstrap-datepicker.js',
    "wait-me" => $app['base_url'].$app['assetdir'].'/js/plugins/waitMe.min.js',
    "image-loaded" => $app['base_url'].$app['assetdir'].'/js/plugins/imagesloaded.pkgd.min.js',
    "image-fill" => $app['base_url'].$app['assetdir'].'/js/plugins/jquery-imagefill.js',
    "select-2" => $app['base_url'].$app['assetdir'].'/plugins/select2/js/select2.min.js',
    "lazy-load" => $app['base_url'].$app['assetdir'].'/plugins/lazy-load/jquery.lazy.min.js',
    "pdfobject" => $app['base_url'].$app['assetdir'].'/plugins/PDFObject/pdfobject.min.js',
    "jcsv" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.csv.min.js',
    "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.jexcel.js',
    "canvasjs" => $app['base_url'].$app['assetdir'].'/plugins/canvasjs/canvasjs.min.js',
    // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.min.js',
      "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.js',
    "canvasjs" => $app['base_url'].$app['assetdir'].'/plugins/canvasjs/canvasjs.min.js',
    "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.js',
    "jqplot-pieRenderer" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/plugins/jqplot.pieRenderer.js',
    "jprint" => $app['base_url'].$app['assetdir'].'/js/plugins/jquery.print-preview.js',
    "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.js',
     "jquery_validation_engine_en" => $app['base_url'].$app['assetdir'].'/js/plugins/jquery-validation-engine/js/languages/jquery.validationEngine-en.js',
    "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/js/plugins/jquery-validation-engine/js/jquery.validationEngine.js',
    "ui-kit" => $app['base_url'].$app['assetdir'].'/js/now-ui-kit.js?v=1.1.0',

  );
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title> 
    <link rel="icon" href="http://perbarindo.tranyar.com/public/assets/img/logo.png" type="image/gif" sizes="16x16">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta property="og:title" content="<?php (isset($og_title)) ? $value = $og_title : $value = "PERBARINDO | EVENT";echo $value;?>" />
    <meta property="og:site_name" content="<?php (isset($og_site_name)) ? $value = $og_site_name : $value = "PERBARINDO EVENT";echo $value;?>" />
    <meta property="og:url" content="<?php (isset($og_site_url)) ? $value = $og_site_url : $value = base_url(); echo $value;?>" />
    <meta property="og:description" content="<?php (isset($og_site_description)) ? $value = $og_site_description : $value = "Organisasi PERHIMPUNAN BANK PERKREDITAN RAKYAT INDONESIA disingkat PERBARINDO, dibentuk pada tanggal 24 Januari 1995. Organisasi PERBARINDO merupakan wadah bagi Bank Perkreditan Rakyat Indonesia yang bersifat independen berdasarkan Pancasila dan Undang-Undang Dasar 1945."; echo $value;?>" />
    <meta property="og:image" content="<?php (isset($og_site_image)) ? $value = $og_site_image : $value = "http://www.perbarindo.or.id/wp-content/uploads/2015/08/logo1.jpg"; echo $value;?>">
    <meta property="og:type" content="website" />

    <?php
        /* generate metatag */
        $meta_key = array_keys($app['meta']);
        for($i=0;$i < count($app['meta']);$i++){
            echo '<meta name="'.$meta_key[$i].'" content="'.$app['meta'][$meta_key[$i]].'">';
        }
        /* generate css */
        $css_key = array_keys($app['css']);
        for($i=0;$i < count($app['css']);$i++){
            echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
        }

        /* generate custom css */

        if(isset($custom_css)){
            if(count($custom_css)>0){
                $custom_css_key = array_keys($custom_css);
                for($i=0;$i < count($custom_css);$i++){
                    echo '<link rel="stylesheet" href="'.$custom_css[$custom_css_key[$i]].'">';
                }
            }
        }

        /* generate js */
        $header_js_key = array_keys($app['header_js']);
        for($i=0;$i < count($app['header_js']);$i++){
            echo '<script src="'.$app['header_js'][$header_js_key[$i]].'"></script>';
        }

    ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqMN4WoWiI_WZRaaGVoa2hABCpQhk_E1I&libraries=places"></script>
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var ckfinder_config = {
            filebrowserBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        }
    </script>
    <script type="text/javascript">

      function showMap(latitude, longitude, selector) {

          var loc = {lat: parseFloat(latitude), lng: parseFloat(longitude)}

          var map = new google.maps.Map(document.getElementById(selector), {
              center: loc,
              zoom: 10
          });
          

          var infowindow = new google.maps.InfoWindow();
          var marker = new google.maps.Marker({
              position: loc,
              map: map
          });

      }

      function countDownTime(container_days, container_hours, container_minutes, container_seconds, date){

            var countDownDate = new Date(date).getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

              // Get todays date and time
              var now = new Date().getTime();

              // Find the distance between now an the count down date
              var distance = countDownDate - now;

              // Time calculations for days, hours, minutes and seconds
              var days = Math.floor(distance / (1000 * 60 * 60 * 24));
              var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
              var seconds = Math.floor((distance % (1000 * 60)) / 1000);


              // Display the result in the element with id="demo"
              // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
              // + minutes + "m " + seconds + "s "; 

              $(container_days).html(" "+days+" hari");
              $(container_hours).html(" "+hours+" jam");
              $(container_minutes).html(" "+minutes+" menit");
              $(container_seconds).html(" "+seconds+" detik");


              // If the count down is finished, write some text 
              if (distance < 0) {
                clearInterval(x);
                // document.getElementById("demo").innerHTML = "EXPIRED";
              }
            }, 1000);

          
      }
     
  </script>
</head>
<body class="<?php echo $body_class; ?> sidebar-collapse">

    <?php 

        $this->load->view('parts/public/navbar',@$navbar);

        if(isset($page)) {

            $this->load->view($page, @$data);

        }
        
        if(!@$fullscreen){
          $this->load->view('parts/public/footer');
        }
        

        /* generate js */
        $footer_js_key = array_keys($app['footer_js']);
        for($i=0;$i < count($app['footer_js']);$i++){
            echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
        }

        /* generate custom js */
        if(isset($custom_js)){
            if(count($custom_js)>0){
                $custom_js_key = array_keys($custom_js);
                for($i=0;$i < count($custom_js);$i++){
                    if($custom_js_key[$i]=="googlemaps"){
                        echo '<script src="'.$custom_js[$custom_js_key[$i]].'" async defer></script>';
                    }else{
                        echo '<script src="'.$custom_js[$custom_js_key[$i]].'"></script>';
                    }

                }
            }
        }

    ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
</body>
</html>
