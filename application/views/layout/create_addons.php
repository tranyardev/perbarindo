<style type="text/css">
  .img-prev{
    min-height: 350px;
    padding: 10px;
    border: dashed 2px #ddd;
    margin-top: 10px;
    text-align: center;
    position: relative;
  }
  .img-prev h1{
    margin-top: 135px;
    color: #ddd;
  }
  .btn-reset-preview{
    position: absolute;
    top: 10px;
    right: 10px;
    border-radius: 0px;
  }
  #preloader h2{
    color: #665;
    text-align: center;
    margin-top: 427px;
  }
  .js{
    position: relative;
  }
  .js div#preloader { position: absolute; left: 0; top: 0; z-index: 999; width: 100%; height: 100%; overflow: visible; background:url('http://loading.io/assets/img/ajax.gif') no-repeat center 200px;background-color:rgba(255,255, 255, 0.8);}
</style>
<div class="row" id="form_wrapper">
  <div id="preloader" style="display: none;"><h2>Uploading ....</h2></div>
        <form role="form" id="form-upload-module">
        <input type="hidden" name="description" id="post_description">
       
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Module</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" reqired name="name" class="form-control" id="name" placeholder="Addon Name">
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                   <textarea class="form-control" id="description"></textarea>
                </div>
                <div class="form-group">
                  <label for="slug">Slug</label>
                  <input type="text" reqired name="slug" class="form-control" id="slug" placeholder="Addon Slug">
                </div>
                <div class="form-group">
                  <label for="version">Version</label>
                  <input type="text" reqired name="version" class="form-control" id="version" placeholder="Addon Version">
                </div>
                <div class="form-group">
                  <label for="group">Group</label>
                  <select class="form-control" name="group" id="group">
                    <option value="core">Core</option>
                    <option value="addons">Addons</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="status">Status</label>
                  <select class="form-control" name="status" id="status">
                    <option value="0">Draft</option>
                    <option value="1">Publish</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="cover">Cover</label>
                  <input type="file" class="form-control" name="cover" id="cover">
                  <div class="img-prev" reqired id="cover_preview"><h1>Cover Module</h1></div>
                </div>
                <div class="form-group">
                  <label for="file">Package Zip</label>
                  <input type="file" reqired class="form-control" name="file" id="file">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                &nbsp;
              </div>
            
          </div>
          <!-- /.box -->
         
          <!-- /.box -->
         
    </div>
     <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Optional Info</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <div class="form-group">
                  <label for="video_url">Video URL</label>
                  <input type="text" class="form-control" name="video_url" id="video_url" placeholder="Your video to explain about module (Youtube Embeded)">
                  <div class="img-prev" id="youtube_preview"><h1>You Tube Video Preview</h1></div>
                </div>
               
                <div class="form-group">
                  <label for="ss1">Screenshot 1</label>
                  <input type="file" class="form-control" name="ss1" id="ss1">
                  <div class="img-prev" id="ss1_preview"><h1>Screenshot 1</h1></div>
                </div>

                <div class="form-group">
                  <label for="ss1">Screenshot 2</label>
                  <input type="file" class="form-control" name="ss2" id="ss2">
                  <div class="img-prev" id="ss2_preview"><h1>Screenshot 2</h1></div>
                </div>

                <div class="form-group">
                  <label for="ss1">Screenshot 3</label>
                  <input type="file" class="form-control" name="ss3" id="ss3">
                  <div class="img-prev" id="ss3_preview"><h1>Screenshot 3</h1></div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            
          </div> 
      </div> 
   </form>  
</div>
<script type="text/javascript">
  $(document).ready(function(){
    CKEDITOR.replace('description');
    $("#ss1").change(function(){
        readURL(this, "#ss1");
    });
    $("#ss2").change(function(){
        readURL(this, "#ss2");
    });
    $("#ss3").change(function(){
        readURL(this, "#ss3");
    });
     $("#cover").change(function(){
        readURL(this, "#cover");
    });
    $("#video_url").on("change blur keyup", function(){
        $("#youtube_preview").html($(this).val()+'<a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetVideo();"><i class="fa fa-remove"></i></a>');
        if($(this).val()==""){
          $("#youtube_preview").html('<h1>You Tube Video Preview</h1>');
        }
    });

    for (var i in CKEDITOR.instances) {
                
                CKEDITOR.instances[i].on('change', function() { 
                
                  $("#post_description").val(CKEDITOR.instances[i].getData()); 

                });
                
        }

    $("#form-upload-module").submit(function(){
      

      $("#form_wrapper").addClass("js");
      $("#preloader").show();
      $("body,html").animate({ scrollTop: 0 }, 600);

      setTimeout('saveModule();',3000);
      
      return false;

    });
  });

  function saveModule(){
    var formData = new FormData($("#form-upload-module")[0]);
    $.ajax({
          url: '<?php echo base_url(); ?>addons/create',
          type: 'POST',
          data: formData,
          dataType: "json",
          async: false,
          success: function (data) {
              $("#form_wrapper").removeClass("js");
              $("#preloader").hide();
              if(data.status=="1"){
                toastr.success(data.msg, 'Response Server');
              }else{
                toastr.error(data.msg, 'Response Server');
              }
              
              resetForm();
          },
          cache: false,
          contentType: false,
          processData: false
    });

  }
  function resetForm(){


    $('#form-upload-module')[0].reset();
    $("#youtube_preview").html('<h1>You Tube Video Preview</h1>');
    $("#cover_preview").html('<h1>Cover Module</h1>');
    $("#ss1_preview").html('<h1>Screenshot 1</h1>');
    $("#ss2_preview").html('<h1>Screenshot 2</h1>');
    $("#ss3_preview").html('<h1>Screenshot 3</h1>');

     for (var i in CKEDITOR.instances) {
                
          CKEDITOR.instances[i].setData(''); 
     
        }

  }

  function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
          if (input.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
               var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
               img.attr('src', e.target.result);
               $(selector + '_preview').html(img);  
            }
            reader.readAsDataURL(input.files[i]);
           }
        }
  }
  function resetFileUpload(selector){
    var label = "";

    $(selector).val('');

    if(selector=="#ss1"){
      label = "<h1>Screenshot 1</h1>";
    }else if(selector=="#ss2"){
      label = "<h1>Screenshot 2</h1>";
    }else if(selector=="#ss3"){
      label = "<h1>Screenshot 3</h1>";
    }else{
      label = "<h1>Cover Module</h1>";
    }

    $(selector + '_preview').html(label); 

  }
  function resetVideo(){

    $("#video_url").val('');
    $("#youtube_preview").html('<h1>You Tube Video Preview</h1>');

  }
</script>