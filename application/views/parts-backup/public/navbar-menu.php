<nav class="navbar navbar-expand-lg bg-greenmuda navbar-down fixed-topx sticky">
    <div class="container">

         <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button> 
            <a class="navbar-brand hidden-lg hidden-md visible-sm visible-xs" href="<?php echo base_url(); ?>" rel="tooltip" title="Perhimpunan Bank Perkreditan Indonesia" data-placement="bottom" target="_blank" style="width: 100%">
              <center> <img src="http://perbarindo.tranyar.com/public/assets/img/logo.png" style="width: 164px;"> </center>
            </a>
         
        </div>


        <div class="collapse navbar-collapse" id="example-navbar" > 

            <ul class="navbar-nav navbar-right ml-auto" style="overflow-x: auto;">
                <li class="nav-item <?php (@$active=="home")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>">
                        HOME
                    </a>
                </li>

                <li class="nav-item <?php (@$active=="about")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>about">
                        ABOUT
                     </a>
                </li> 
                <li class="nav-item <?php (@$active=="bpr")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>bpr">
                        BPR
                     </a>
                </li> 
                 <li class="nav-item <?php (@$active=="event")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>find/event">
                        FIND EVENT
                     </a>
                </li> 
               
                <li class="nav-item <?php (@$active=="faq")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>faq">
                        FAQ
                     </a>
                </li> 
                <li class="nav-item <?php (@$active=="contact")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>contact">
                        CONTACT
                     </a>
                </li> 
                 <li class="nav-item <?php (@$active=="blog")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>blog">
                        BLOG
                     </a>
                </li> 
             <!--    <li class="nav-item <?php (@$active=="tos")? $class="active" : $class=""; echo $class;?>">
                    <a class="nav-link" href="<?php echo base_url(); ?>term_of_condition">
                        TERM OF CONDITION
                     </a>
                </li>  -->
                
               
                <li class="nav-item hidden-lg hidden-md visible-sm visible-xs" style="margin-top: 149px;">
                    <a class="nav-link" href="http://maps.google.com/maps?q=<?php echo @$company_latitude['value']; ?>,<?php echo @$company_longitude['value']; ?>&z=17" target="_blank">
                       <table class="nav-info nav-white">
                           <tr>
                               <td>
                                   <button class="btn btn-simple btn-round">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                </td>
                               <td class="cont">
                                 <?php echo @$company_address['value']; ?>
                               </td>
                           </tr>
                       </table>
                    </a>
                </li>
                <li class="nav-item hidden-lg hidden-md visible-sm visible-xs">
                    <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>">
                        <table class="nav-info nav-white">
                           <tr>
                               <td>
                                    <button class="btn btn-simple btn-round"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                </td>
                                <td class="cont">
                                    Hubungi Kami Sekarang di <br> <h5><b><?php echo @$company_phone['value']; ?></b></h5>
                                </td>
                            </tr>
                        </table>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>