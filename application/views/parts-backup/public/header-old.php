

<nav class="navbar navbar-expand-lg bg-primary fixed-topv">
    <div class="container"> 
        <div class="navbar-translate">
            
            <a class="navbar-brand" href="<?php echo base_url(); ?>" rel="tooltip" title="Perhimpunan Bank Perkreditan Indonesia" data-placement="bottom" target="_blank" style="width: 100%">
              <center> <img src="http://perbarindo.tranyar.com/public/assets/img/logo.png"> </center>
            </a>
         
        </div>
        <div class="collapse navbar-collapse justify-content-end hidden-sm hidden-xs" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://maps.google.com/maps?q=<?php echo @$company_latitude['value']; ?>,<?php echo @$company_longitude['value']; ?>&z=17" target="_blank">
                       <table class="nav-info">
                           <tr>
                               <td>
                                   <button class="btn btn-simple btn-round">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                </td>
                               <td class="cont">
                                 <?php echo @$company_address['value']; ?>
                               </td>
                           </tr>
                       </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>">
                        <table class="nav-info">
                           <tr>
                               <td>
                                    <button class="btn btn-simple btn-round"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                </td>
                                <td class="cont">
                                    Hubungi Kami Sekarang di <br> <h5><b><?php echo @$company_phone['value']; ?></b></h5>
                                </td>
                            </tr>
                        </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="<?php echo @$tw['value']; ?>" target="_blank">
                        <i class="fa fa-twitter fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="<?php echo @$fb['value']; ?>" target="_blank">
                        <i class="fa fa-facebook-square fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Google Plus" data-placement="bottom" href="<?php echo @$gg['value']; ?>" target="_blank">
                        <i class="fa fa-google-plus fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>