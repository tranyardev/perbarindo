
<link href="https://fonts.googleapis.com/css?family=Carter+One" rel="stylesheet">
<style type="text/css">
.card-head {
  margin-top: -54px;
}

.font-carter {
  font-family: 'Carter One', cursive;
}

.bg-greenmuda {
  background-color: #439D47!important;
}

.bg-greenkilot {
  background-color: #216030!important;
}

.footer-top {
    background-color: #fef6e1!important;
    padding: 23px;
}

.card-service {
  border-radius: 23px!important;
}

.title-rgs {
  margin-bottom: 13px;
  font-size: 24px;
  /*padding: 21px;*/
  margin-top: 27px;
  color: #555;
}

.description-rgs {
    border-bottom-right-radius: 21px;
    border-bottom-left-radius: 21px; 
    margin-bottom: -30px;
}

.bg-orange-md {
  background-color: #fa9e25;
}


.bg-yellow-md {
  background-color: #ffc107;
}

.bg-purple-md {
  background-color: #ef00ff;
}

.bg-ungu-md {
  background-color: #571d96;
}


.title-md:before {
    content: '';
    position: absolute;
    top: 67px;
    left: 52%;
    -webkit-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
    width: 71px;
    height: 2px;
    background-color: #555!important;
} 

 .title-xs:before {
    content: '';
    position: absolute;
    top: 66px;
    left: 52%;
    -webkit-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
    width: 71px;
    height: 2px;
    background-color: #555!important;
}


.titlelistg {
  margin-bottom: 1px!important;
}

.p-lgt  {
  margin-bottom: 1px!important;
}

.smallx {
    font-size: 70%;
}

.fsz12 {
  font-size: 10px;
}

.pagning {
  margin-top:9px;
}

.ovhiden {
      height: 261px;
      border-top: 1px solid #DDD;
      border-bottom: 1px solid #DDD;
    overflow-x: hidden;
}

.list-group-itemx.active {
    z-index: 2;
    color: #2b2f33!important;
    background-color: #ffffff!important;
    border-color: #ffffff!important;
}

.title-md {

    margin-bottom: 13px;
    font-size: 29px;
    /* padding: 21px; */
    margin-top: 27px;
    color: #555;
}


.title-xs {

    margin-bottom: 13px;
    font-size: 29px;
    /* padding: 21px; */
    margin-top: 27px;
    color: #555;
}

.list-group-footer > .list-mb { 
    margin-bottom: -14px!important; 
}

@media screen and (max-width: 991px){
  .sidebar-collapse .navbar-collapse:before {
      background: -webkit-linear-gradient(#f96333 0%, #64069c 95%);
      background: -o-linear-gradient(#f96333 0%, #64069c 95%);
      background: -moz-linear-gradient(#f96333 0%, #64069c 95%);
      background: linear-gradient(#215f31 0%, #439d47 95%);
      opacity: 23.76;
  }
}

.nav-white button {
    width: 16px;
    height: 44px;
    color: #ffffff!important;
    border: 1px solid #ffffff!important;
}

.icon-purple {
  color:#A40DFF;
}


.icon-orange {
  color:#fb9522;
}


.icon-green {
  color:#A40DFF;
}
</style>
<div class="visible-lg visible-md hidden-sm hidden-xs">
<nav class="navbar navbar-expand-lg bg-greenkilot fixed-topv" style="height: 2px;">
    <div class="container"> 
        <div class="navbar-translate">
            
            <a class="navbar-brand" href="<?php echo base_url(); ?>" rel="tooltip" title="Perhimpunan Bank Perkreditan Indonesia" data-placement="bottom" target="_blank" style="width: 100%">
              <center> <img src="http://perbarindo.tranyar.com/public/assets/img/logo.png" style="width: 164px;"> </center>
            </a>
         
        </div>
        <div class="collapse navbar-collapse justify-content-end hidden-sm hidden-xs" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://maps.google.com/maps?q=<?php echo @$company_latitude['value']; ?>,<?php echo @$company_longitude['value']; ?>&z=17" target="_blank">
                       <table class="nav-info nav-white nav-whead">
                           <tr>
                               <td>
                                   <button class="btn btn-simple btn-round btn-xs" style="width: 1px;">  <i class="fa fa-map-marker fa-lg fa-5x"></i></button>
                                </td>
                               <td class="cont">
                                 <?php echo @$company_address['value']; ?>
                               </td>
                           </tr>
                       </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="tel:<?php echo @$company_phone['value']; ?>">
                        <table class="nav-info nav-white nav-whead">
                           <tr>
                               <td>
                                    <button class="btn btn-simple btn-round btn-xs" style="width: 1px;"> <i class="fa fa-phone fa-lg fa-5x"></i> </button> 
                                </td>
                                <td class="cont">
                                    Hubungi Kami Sekarang di <br> <h5><b><?php echo @$company_phone['value']; ?></b></h5>
                                </td>
                            </tr>
                        </table>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="<?php echo @$tw['value']; ?>" target="_blank">
                        <i class="fa fa-twitter fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="<?php echo @$fb['value']; ?>" target="_blank">
                        <i class="fa fa-facebook-square fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link social-link" rel="tooltip" title="Follow us on Google Plus" data-placement="bottom" href="<?php echo @$gg['value']; ?>" target="_blank">
                        <i class="fa fa-google-plus fa-lg fa-5x"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>