
<header class="main-header">
    <!-- Logo -->
   
    <a href="<?php echo base_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <center> <img src="<?php echo base_url(); ?>public/assets/img/square-logo.png" class="img-responsive" style="height: 45px;width: 45px;"> </center> </span>
        <!-- logo for regular state and mobile devices -->
        <!-- <img src="<?php //echo base_url(); ?>assets/dist/img/app_head_logo.png" class="img-responsive" style="height: 45px;width: 45px;"> -->
       
        <span class="logo-lg"> <center> <img src="<?php echo base_url(); ?>public/assets/img/logo.png" class="img-responsive"> </center> </span>
    </a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu" data-step="2" data-intro="Selanjutnya, silahkan ganti password default anda disini dengan klik ikon <i class='fa fa-key'></i>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <?php if($this->session->userdata('picture')!=""){?>
                            <div class="user-profile-dropdown"><img src="<?php echo $this->session->userdata('picture'); ?>" class="scale" data-scale="best-fill" data-align="center"></div>
                        <?php }else{ ?>
                            <div class="user-profile-dropdown"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" class="scale" data-scale="best-fill" data-align="center"></div>
                        <?php } ?>

                        <span class="hidden-xs">
                        <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?>

                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if($this->session->userdata('picture')!=""){?>
                                <div class="user-profile-big"><img src="<?php echo $this->session->userdata('picture'); ?>" class="scale" data-scale="best-fill" data-align="center"></div>
                            <?php }else{ ?>
                                <div class="user-profile-big"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" class="scale" data-scale="best-fill" data-align="center"></div>
                            <?php } ?>

                            <p style="font-size: 13px;">
                                <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name')?> <br> <?php echo $this->session->userdata('email');?>
                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <div class="ip"><span class="label label-info"> IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></div>
                            </div>
                            <div class="pull-right">
                                <a href="javascript:edit_profile();" class="btn btn-success btn-flat"><i class="fa fa-user" aria-hidden="true"></i></a>
                                <a href="javascript:change_password();" class="btn btn-warning btn-flat"><i class="fa fa-key" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url('auth/session/logout'); ?>" alt="Logout" class="btn btn-danger btn-flat"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php 
                    $user_group = $this->mcore->getUserGroupName($this->session->userdata('id')) ;
                    if($user_group == "Admin"){
                ?>

                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>

                <?php
                    }
                ?>
            </ul>
        </div>
    </nav>
</header>
